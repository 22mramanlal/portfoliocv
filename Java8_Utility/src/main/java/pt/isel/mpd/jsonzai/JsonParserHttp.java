package pt.isel.mpd.jsonzai;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JsonParserHttp {

	public static String parseWeather(InputStream src){
        
		StringBuilder sb = new StringBuilder();
	
		String line;	
		
		try(BufferedReader br = new BufferedReader(new InputStreamReader(src))){
			
			while((line = br.readLine()) != null)
				sb.append(line);
			
		} catch (IOException e) {
			throw new RuntimeException(e);	
		}
		
		return sb.toString(); 
		
	}
}
