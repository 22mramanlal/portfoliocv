/*
 * Copyright (C) 2015 Miguel Gamboa at CCISEL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pt.isel.mpd.jsonzai;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;


public class JsonParser  {
	
	private static final Map<Class<?>, Function<String, Object>> FIELD_PARSER = new HashMap<Class<?>, Function<String, Object>>();

	private static final Map<Class<?>, Object> DEFAULT_PRIMITIVES = new HashMap<Class<?>, Object>();

	static {
		DEFAULT_PRIMITIVES.put(boolean.class, true);
		DEFAULT_PRIMITIVES.put(byte.class, 0);
		DEFAULT_PRIMITIVES.put(char.class, 'a');
		DEFAULT_PRIMITIVES.put(double.class, 0);
		DEFAULT_PRIMITIVES.put(float.class, 0);
		DEFAULT_PRIMITIVES.put(int.class, 0);
		DEFAULT_PRIMITIVES.put(long.class, 0);
		DEFAULT_PRIMITIVES.put(short.class, 0);

		FIELD_PARSER.put(String.class, String::toString);
		FIELD_PARSER.put(boolean.class, Boolean::parseBoolean);
		FIELD_PARSER.put(byte.class, Byte::parseByte);
		FIELD_PARSER.put(char.class, (s) -> s.charAt(0));
		FIELD_PARSER.put(double.class, Double::parseDouble);
		FIELD_PARSER.put(float.class, Float::parseFloat);
		FIELD_PARSER.put(int.class, Integer::parseInt);
		FIELD_PARSER.put(long.class, Long::parseLong);
		FIELD_PARSER.put(short.class, Short::parseShort);
		FIELD_PARSER.put(Date.class, JsonParser::dateParser);

	}
	
	public static Date dateParser(String s){
		DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD");
		Date date = null;
		try {
			date = dateFormat.parse(s);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		return date;
	}
	
	
	
    public <T> T toObject(String src, Class<T> dest){
    	T a = createObject(src, dest);
    
		return a;
    }

    public <T> List<T> toList(String src, Class<T> dest){
    	List<T> list = new ArrayList<T>();

		List<String> auxList = JsonParserUtils.divideListToObject(src);

		for (String str : auxList)
			list.add(createObject(str, dest));

		return list;
    }
    
    private <T> T createObject(String src, Class<T> dest) {
		Map<String, String> map = JsonParserUtils.parseJsonObject(src);
		Constructor<T> ctor = getConstructor(dest);

		T t = null;

		if (ctor.getGenericParameterTypes().length == 0)
			t = createInstanceWithOutParameters(ctor);
		else
			t = createInstaceWithParamenters(ctor);

		fillFields(t, map);

		return t;
	}

	private <T> Constructor<T> getConstructor(Class<T> dest) {
		Constructor<T> ctor = null;

		Constructor[] ctors = dest.getDeclaredConstructors();
		for (int i = 0; i < ctors.length; i++) {
			ctor = ctors[i];
			if (ctor.getGenericParameterTypes().length == 0)
				break;
		}
		ctor.setAccessible(true);
		return ctor;
	}

	private <T> T createInstanceWithOutParameters(Constructor<?> ctor) {
		T t;

		try {
			t = (T) ctor.newInstance();
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		return t;
	}

	private <T> T createInstaceWithParamenters(Constructor<?> ctor) {
		T t;
		Class[] c = ctor.getParameterTypes();
		ctor.setAccessible(true);

		Object objs[] = new Object[c.length];
		for (int i = 0; i < c.length; ++i) {
			if (c[i].isPrimitive())
				objs[i] = DEFAULT_PRIMITIVES.get(c[i]);
			else
				objs[i] = null;
		}

		try {
			t = (T) ctor.newInstance(objs);
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}

		return t;
	}

	private <T> void fillFields(T t, Map<String, String> map) {
									
		Field[] fields = t.getClass().getDeclaredFields();

		for (int i = 0; i < fields.length; ++i) {
			String name = fields[i].getName().toLowerCase();

			String value;
			if ((value = (String) map.get(name)) == null)
				continue;

			try {
				fields[i].setAccessible(true);
				
				if (!value.isEmpty() && value.charAt(0) == '[') {
					ParameterizedType listType = (ParameterizedType) fields[i].getGenericType();
					Class<?> listClass = (Class<?>) listType.getActualTypeArguments()[0];

					fields[i].set(t, toList(value, listClass));
					
				} else if (!value.isEmpty() && value.charAt(0) == '{') {
					Class<?> c = fields[i].getType();
					T tAux = (T) createObject(value, c);
					fields[i].set(t, tAux);
					
				} else
					fields[i].set(t, FIELD_PARSER.get(fields[i].getType()).apply(value));

			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
