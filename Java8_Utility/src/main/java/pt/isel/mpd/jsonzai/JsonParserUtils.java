package pt.isel.mpd.jsonzai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonParserUtils {

	public static Map<String, String> parseJsonObject(String src) {

		List<String> list = new ArrayList<String>();
		src = src.substring(src.indexOf("{") + 1, src.lastIndexOf("}"));

		char a;
		int start = 0;
		int countAspas = 0;
		int countChavetas = 0;
		int countParentises = 0;
		boolean doisPontos = false;

		for(int i = 0; i < src.length(); ++i){
			a = src.charAt(i);

			if(doisPontos == true){	

				if(a == '\\' && src.charAt(i+1) == '\"'){
					i += 1; continue;
				}
				if( a == '\"' && countParentises == 0 && countChavetas == 0)
					++countAspas;

				if(countAspas == 0){
					switch(a){
					case '{' : ++countChavetas; break;
					case '}' : --countChavetas; break;
					case '[' : ++countParentises; break;
					case ']' : --countParentises; break;
					}
				}
			}
			if( a == ':' )
				doisPontos = true;

			if( countAspas % 2 == 0 && countChavetas == 0 && countParentises == 0 && a == ',' && doisPontos == true){
				list.add(src.substring(start, i));
				start = i + 1;
				countAspas = 0;
				doisPontos = false;
			}			
		}

		list.add(src.substring(start, src.length()));

		return divideKeyAndValue(list);
	}


	private static Map<String, String> divideKeyAndValue(List<String> src) {

		Map<String, String> map = new HashMap<String, String>();

		for (String s : src) {

			String a = s.substring(0, s.indexOf(":")).trim().replaceAll("\"", "");

			String b = s.substring(s.indexOf(":") + 1).trim();
			if (b.charAt(0) != '{' && b.charAt(0) != '[')
				b = b.replaceAll("\"", "");

			map.put(a.toLowerCase(), b);
		}

		return map;
	}

	public static List<String> divideListToObject(String s) {
		List<String> list = new ArrayList<String>();

		if(s.contains("["))
			s = s.substring(s.indexOf("[")+1, s.lastIndexOf("]"));

		Integer start = null;
		int countChavetas = 0;
		char a;

		for (int i = 0; i < s.length(); ++i) {
			a = s.charAt(i);

			switch(a){
			case  '{' : ++countChavetas; break;
			case  '}' : --countChavetas; break;
			}

			if (a == '{' && start == null)
				start = i;

			if (a == '}' && start != null && countChavetas == 0) {
				list.add(s.substring(start, i + 1));
				start = null;
			}

		}

		return list;
	}

}
