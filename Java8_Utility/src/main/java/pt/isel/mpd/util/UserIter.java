package pt.isel.mpd.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import pt.isel.mpd.githubgw.model.IGhUser;
import pt.isel.mpd.githubgw.model.async.GhServiceAsync;
import pt.isel.mpd.githubgw.model.async.GhUser;
import pt.isel.mpd.githubgw.webapi.dto.GhUserDto;

public class UserIter<T> implements Spliterator<T> {

	private CompletableFuture<List<GhUserDto>> usersDto;
	private List<GhUserDto> dtos;
	private int dtosCount;
	private Function<Integer, List<GhUserDto>> request;
	private List<IGhUser> users;
	private int nrsGiven;
	private Supplier<GhServiceAsync> async;
	
	public UserIter(CompletableFuture<List<GhUserDto>> usersDto, Function<Integer, List<GhUserDto>> resquest, Supplier<GhServiceAsync> async){
		this.usersDto = usersDto;
		this.request = resquest;
		this.async = async;
		this.users = new ArrayList<IGhUser>();
		firstRequest();
	}
	
	private void firstRequest() {
		
		try {
			dtos = usersDto.get();
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException();
		}
		
		if(users.size() >= dtos.size())
			dtos = new ArrayList<>();
				
	}

	@Override
	public boolean tryAdvance(Consumer<? super T> consumer) {

		if(nrsGiven >= users.size()){
			
			if(dtosCount >= dtos.size()){
				dtos =  (List<GhUserDto>) request.apply(calculatePage(users.size()));
				dtosCount = 0;
			}
			
			if(dtos.size() == 0)
				return false;
			
			if(dtosCount < dtos.size())
				transform(dtos.get(dtosCount++));
				
		}

		if(nrsGiven < users.size()){

			IGhUser r = users.get(nrsGiven);
			consumer.accept((T) r);
			++nrsGiven;
			return true;
					
		}
		
		return false;
	}
	
	private void transform(GhUserDto ghUserDto) {
		  IGhUser user = null;							
		  user = GhUser.valueOf(ghUserDto, async);
		  users.add(user);
	}
	
	private int calculatePage(int listSize){
		int page = 1;
		while( listSize > 0){listSize -= 30; ++page;}
		return page;
	}
	
	@Override
	public int characteristics() {
		return NONNULL;
	}

	@Override
	public long estimateSize() {
		return Long.MAX_VALUE;
	}

	@Override
	public Spliterator<T> trySplit() {
		throw new UnsupportedOperationException();
	}

}
