package pt.isel.mpd.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.function.Supplier;

import pt.isel.mpd.githubgw.model.IGhOrg;
import pt.isel.mpd.githubgw.model.async.GhOrg;
import pt.isel.mpd.githubgw.model.async.GhServiceAsync;
import pt.isel.mpd.githubgw.webapi.dto.GhUserDto;

public class OrgIter<T> implements Spliterator<T> {
	
	private CompletableFuture<List<GhUserDto>> usersDto;
	private List<GhUserDto> dtos;
	private int dtosCount;
	private List<IGhOrg> orgs;
	private int nrsGiven;	
	private Supplier<GhServiceAsync> async;
	
	public OrgIter(CompletableFuture<List<GhUserDto>> usersDto, Supplier<GhServiceAsync> async){
		this.usersDto = usersDto;
		this.async = async;
		this.orgs = new ArrayList<IGhOrg>();
		firstRequest();
	}
	
	private void firstRequest() {
		
		try {
			dtos = usersDto.get();
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException();
		}
		
		if(orgs.size() >= dtos.size())
			dtos = new ArrayList<>();		
	}	
	
	@Override
	public boolean tryAdvance(Consumer<? super T> consumer) {

		if(nrsGiven >= orgs.size() && dtosCount < dtos.size() ){
				transform(dtos.get(dtosCount++));
		}

		if(nrsGiven < orgs.size()){

			IGhOrg r = orgs.get(nrsGiven);
			consumer.accept((T) r);
			++nrsGiven;
			return true;
					
		}
		return false;
	}

	private void transform(GhUserDto ghUserDto) {
		IGhOrg org = null;
		
		if(async.get().orgMap.containsKey(ghUserDto.id)){
			org = async.get().orgMap.get(ghUserDto.id);
		}
		else{
			org = GhOrg.valueOf(ghUserDto, async);
			async.get().orgMap.put(ghUserDto.id, org);
		}
		
	   orgs.add(org);
	}
	
	@Override
	public int characteristics() {
		return NONNULL;
	}

	@Override
	public long estimateSize() {
		return Long.MAX_VALUE;
	}

	@Override
	public Spliterator<T> trySplit() {
		throw new UnsupportedOperationException();
	}
	
}
