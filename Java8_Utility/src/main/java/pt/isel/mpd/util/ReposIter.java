package pt.isel.mpd.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import pt.isel.mpd.githubgw.model.IGhOrg;
import pt.isel.mpd.githubgw.model.IGhRepo;
import pt.isel.mpd.githubgw.model.async.GhRepo;
import pt.isel.mpd.githubgw.model.async.GhServiceAsync;
import pt.isel.mpd.githubgw.webapi.dto.GhRepoDto;

public class ReposIter<T> implements Spliterator<T> {
	
	private CompletableFuture<List<GhRepoDto>> usersDto;
	private List<GhRepoDto> dtos;
	private int dtosCount;
	private Function<Integer, List<GhRepoDto>> request;
	private List<IGhRepo> repos;
	private int nrsGiven;
	private Supplier<GhServiceAsync> async;
	
	public ReposIter(CompletableFuture<List<GhRepoDto>> usersDto, Function<Integer, List<GhRepoDto>> resquest, List<IGhRepo> repos, Supplier<GhServiceAsync> async){
		this.usersDto = usersDto;
		this.repos = repos;
		this.nrsGiven = 0;
		this.request = resquest;
		this.dtosCount = 0;
		this.dtos = new ArrayList<>();
		this.async = async;
		firstRequest();
	}
	
	private void firstRequest() {
		
		try {
			dtos = usersDto.get();
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException();
		}
		
		if(repos.size() >= dtos.size())
			dtos = new ArrayList<>();

	}


	@Override
	public boolean tryAdvance(Consumer<? super T> consumer) {

		if(nrsGiven >= repos.size()){
				
			if(dtosCount >= dtos.size()){ 
				dtos =  (List<GhRepoDto>) request.apply(calculatePage(repos.size()));
				dtosCount = 0;
			}
			
			if(dtos.size() == 0)
				return false;
			
			if(dtosCount < dtos.size())
				transform(dtos.get(dtosCount++));
						
		}
		
		if(nrsGiven < repos.size()){
			IGhRepo r = repos.get(nrsGiven);
			consumer.accept((T) r);
			++nrsGiven;
			return true;
					
		}
		
		return false;
	}
		
	private void transform(GhRepoDto r){
		
		String login = r.full_name.split("/")[0];
		
		IGhOrg org = null;
		if(async.get().orgMap.containsKey(login))
			org = async.get().orgMap.get(login);
		  
		IGhRepo rep = GhRepo.valueOf(r, org, login, async); 
		repos.add(rep);
	}
	
	private int calculatePage(int listSize){
		int page = 1;
		while( listSize > 0){listSize -= 30; ++page;}
		return page;
	}

	@Override
	public int characteristics() {
		return NONNULL;
	}

	@Override
	public long estimateSize() {
		return Long.MAX_VALUE;
	}

	@Override
	public Spliterator<T> trySplit() {
		throw new UnsupportedOperationException();
	}
}
