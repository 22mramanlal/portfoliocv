/*
 * Copyright (C) 2015 Miguel Gamboa at CCISEL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pt.isel.mpd.githubgw.model.async;

import java.util.List;
import java.util.Spliterator;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import pt.isel.mpd.githubgw.model.IGhOrg;
import pt.isel.mpd.githubgw.model.IGhUser;
import pt.isel.mpd.githubgw.webapi.dto.GhUserDto;
import pt.isel.mpd.util.OrgIter;

/**
 * Created by Miguel Gamboa on 08-06-2015.
 */
public class GhUser implements IGhUser{
	
	public final int id;
	public final String login;
	public final String name;
	public final String company;
		  
    public final Supplier<GhServiceAsync> async;
	private CompletableFuture<List<GhUserDto>> orgs;
	
	
	public GhUser(int id,
					String login,
					String name,
					String company,
					CompletableFuture<List<GhUserDto>> orgs,
					Supplier<GhServiceAsync> async){
		this.id = id;
		this.login = login;
		this.name = name;
		this.company = company;
		this.orgs = orgs;
		this.async = async;
	}
	
	public static IGhUser valueOf(GhUserDto dto, Supplier<GhServiceAsync> async){
		
		return new GhUser( dto.id, dto.login, dto.name, dto.company, async.get().getGhApi().getUserOrgs(dto.login), async);
	}
	
    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getCompany() {
        return company;
    }

    @Override
    public Stream<IGhOrg> getOrgs() {

    	Spliterator<IGhOrg> reposIter = new OrgIter<IGhOrg>(this.orgs, async);
    	
    	return StreamSupport.stream(reposIter, false);
    }
}
