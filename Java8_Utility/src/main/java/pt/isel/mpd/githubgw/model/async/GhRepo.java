/*
 * Copyright (C) 2015 Miguel Gamboa at CCISEL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pt.isel.mpd.githubgw.model.async;

import java.util.List;
import java.util.Spliterator;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import pt.isel.mpd.githubgw.model.IGhOrg;
import pt.isel.mpd.githubgw.model.IGhRepo;
import pt.isel.mpd.githubgw.model.IGhUser;
import pt.isel.mpd.githubgw.webapi.dto.GhRepoDto;
import pt.isel.mpd.githubgw.webapi.dto.GhUserDto;
import pt.isel.mpd.util.UserIter;

/**
 * Created by Miguel Gamboa on 08-06-2015.
 */
public class GhRepo implements IGhRepo {
    final int id;
    final String name;
    final String fullName;
    final String description;
    final int size;
    final int stargazersCount;
    final int watchersCount;
    final String language;
    final IGhOrg owner;
    
    final Supplier<GhServiceAsync> async;
    private CompletableFuture<List<GhUserDto>> contributors;


    public GhRepo(int id, String name, String fullName, String description, int size, int stargazersCount, int watchersCount, String language, IGhOrg owner, CompletableFuture<List<GhUserDto>> contributors, Supplier<GhServiceAsync> async) {
        this.id = id;
        this.name = name;
        this.fullName = fullName;
        this.description = description;
        this.size = size;
        this.stargazersCount = stargazersCount;
        this.watchersCount = watchersCount;
        this.language = language;
        this.owner = owner;
        this.contributors = contributors;
        this.async = async;
    }

    public GhRepo(GhRepoDto dto, IGhOrg owner, Supplier<GhServiceAsync> async, CompletableFuture<List<GhUserDto>> contributors) {
        this(
                dto.id,
                dto.name,
                dto.full_name,
                dto.description,
                dto.size,
                dto.stargazers_count,
                dto.watchers_count,
                dto.language,
                owner,
                contributors,
                async
                );
    }

    public static IGhRepo valueOf(GhRepoDto dto,  IGhOrg owner, String userName, Supplier<GhServiceAsync> async) {    	
    	return new GhRepo(
        			dto.id,
        			dto.name,
        			dto.full_name,
        			dto.description,
        			dto.size,
        			dto.stargazers_count,
        			dto.watchers_count,
        			dto.language,
        			owner,
        			async.get().getGhApi().getRepoContributors(userName, dto.full_name.split("/")[1]),
        			async	
        		);
    }
    
    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getFullName() {
        return fullName;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getStargazersCount() {
        return stargazersCount;
    }

    @Override
    public int getWatchersCount() {
        return watchersCount;
    }

    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public IGhOrg getOwner() {
        return owner;
    }

    @Override
    public Stream<IGhUser> getContributors() {
        String info[] = fullName.split("/");
    	String login = info[0];
        String repoName = info[1];
        
    	Function<Integer, List<GhUserDto>> request =  (page) -> { try {
																	     return async.get().getGhApi().getRepoContributors(login, repoName, page).get();
												 					  } catch (InterruptedException | ExecutionException e) {
												 						throw new RuntimeException(e);
												 					  }
    															};
    	
    	Spliterator<IGhUser> reposIter = new UserIter<IGhUser>(this.contributors, request, async);
    	
    	return StreamSupport.stream(reposIter, false);
        
    }
}
