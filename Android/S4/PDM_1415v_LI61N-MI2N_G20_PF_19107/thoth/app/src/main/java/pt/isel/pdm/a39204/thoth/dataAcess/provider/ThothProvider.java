package pt.isel.pdm.a39204.thoth.dataAcess.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;


public class ThothProvider extends ContentProvider {


    private static  final int TEACHERS_LST = 10;
    private static  final int TEACHERS_OBJ = 20;
    private static  final int LOCATIONS_LST = 30;
    private static  final int LOCATIONS_OBJ= 40;

    private static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

        URI_MATCHER.addURI(
                ThothContract.AUTHORITY,
                ThothContract.Teachers.RESOURCE,
                TEACHERS_LST);

        URI_MATCHER.addURI(
                ThothContract.AUTHORITY,
                ThothContract.Teachers.RESOURCE + "/#",
                TEACHERS_OBJ);

        URI_MATCHER.addURI(
                ThothContract.AUTHORITY,
                ThothContract.Locations.RESOURCE,
                LOCATIONS_LST);

        URI_MATCHER.addURI(
                ThothContract.AUTHORITY,
                ThothContract.Locations.RESOURCE + "/#",
                LOCATIONS_OBJ);

    }

    private ThothOpenHelper dbHelper = null;

    @Override
    public boolean onCreate() {
        dbHelper = new ThothOpenHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case TEACHERS_LST:
                return ThothContract.Teachers.CONTENT_TYPE;
            case TEACHERS_OBJ:
                return ThothContract.Teachers.CONTENT_ITEM_TYPE;
            case LOCATIONS_LST:
                return ThothContract.Locations.CONTENT_TYPE;
            case LOCATIONS_OBJ:
                return ThothContract.Locations.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder qbuilder = new SQLiteQueryBuilder();
        switch (URI_MATCHER.match(uri)) {
            case TEACHERS_LST:
                qbuilder.setTables(ThothSchema.Teachers.TBL_NAME);
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = ThothContract.Teachers.DEFAULT_SORT_ORDER;
                }
                break;
            case TEACHERS_OBJ:
                qbuilder.setTables(ThothSchema.Teachers.TBL_NAME);
                qbuilder.appendWhere(ThothSchema.Teachers.COL_ID + "=" + uri.getLastPathSegment());
                break;

            case LOCATIONS_LST:
                qbuilder.setTables(ThothSchema.Locations.TBL_NAME);
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = ThothContract.Locations.DEFAULT_SORT_ORDER;
                }
                break;
            case LOCATIONS_OBJ:
                qbuilder.setTables(ThothSchema.Locations.TBL_NAME);
                qbuilder.appendWhere(ThothSchema.Locations.COL_ID + "=" + uri.getLastPathSegment());
                break;
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = qbuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String table;
        switch (URI_MATCHER.match(uri)) {
            case TEACHERS_LST:
                table = ThothSchema.Teachers.TBL_NAME;
                break;
            case LOCATIONS_LST:
                table = ThothSchema.Locations.TBL_NAME;
                break;
            default:
                throw new IllegalArgumentException();
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long newId = db.insert(table, null, values);

        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, newId);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table;
        switch (URI_MATCHER.match(uri)) {
            case TEACHERS_LST:
                table = ThothSchema.Teachers.TBL_NAME;
                if (selection != null) {
                    throw new IllegalArgumentException("selection not supported");
                }
                break;
            case LOCATIONS_LST:
                table = ThothSchema.Locations.TBL_NAME;
                if (selection != null) {
                    throw new IllegalArgumentException("selection not supported");
                }
                break;
            default:
                throw new UnsupportedOperationException();
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int ndel = db.delete(table, selection, selectionArgs);

        getContext().getContentResolver().notifyChange(uri, null);
        return ndel;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }
}
