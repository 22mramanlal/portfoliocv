package pt.isel.pdm.a39204.thoth.userInterface.listAdapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothSchema;
import pt.isel.pdm.a39204.thoth.logic.PreferencesSchema;


public class LocationsCursorAdapter extends CursorAdapter {

    private LayoutInflater layoutInflater;
    private Context context;
    private String email;

    private int layout = R.layout.locations_detail_list;

    public LocationsCursorAdapter(Context context, Cursor c, int flags, String email) {
        super(context, c, flags);
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.email = email;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final String ssid = cursor.getString(cursor.getColumnIndex(ThothSchema.Locations.COL_LABEL));

        ViewModelLocations locationsView = new ViewModelLocations(view);

        locationsView.location.setChecked(false);
        locationsView.location.setText(ssid);

        final SharedPreferences preferences = context.getSharedPreferences(PreferencesSchema.FILENAME, context.MODE_PRIVATE);
        if (preferences.getString(email,"").contains(ssid))
            locationsView.location.setChecked(true);

        locationsView.location.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox c = (CheckBox) v;
                c.getText();

                if (c.isChecked()) {
                    String content = preferences.getString(email, "");
                    preferences.edit().putString(email, content+= ssid + ";" ).commit();
                }
                else {
                    String content = preferences.getString(email, "");
                    content = removeWordFormLine(content, ssid);
                    if(content.isEmpty())
                        preferences.edit().remove(email).commit();
                    else
                        preferences.edit().putString(email, content).commit();
                }
            }
        });


    }

    private String removeWordFormLine(String line, String word){
        String words[] = line.split(";");
        String ret = "";

        for(String w : words)
            if(!w.equals(word))
                ret += w + ";";

        return ret;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return layoutInflater.inflate(layout, parent, false);
    }

}

class ViewModelLocations{

    public final CheckBox location;

    public ViewModelLocations(View view) {
        location = (CheckBox) view.findViewById(R.id.location_locations_detail_checkBox);
    }


}
