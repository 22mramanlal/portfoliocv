package pt.isel.pdm.a39204.thoth.logic;


public interface PreferencesSchema {

    String FILENAME = "Thoth";
    String LOCATION_ACTIVE = "locationActive";
    String IS_TEACHER = "isTeacher";
    String FIRST_TIME = "firstTime";
    String LAST_LONG_UPDATE = "lastLongUpdate";
    String LAST_SHORT_UPDATE = "lastShortUpdate";

}
