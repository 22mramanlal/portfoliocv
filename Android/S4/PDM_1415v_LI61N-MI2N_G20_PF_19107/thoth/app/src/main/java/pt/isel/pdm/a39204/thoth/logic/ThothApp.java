package pt.isel.pdm.a39204.thoth.logic;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseUser;

import java.io.Console;

import pt.isel.pdm.a39204.thoth.dataAcess.handlers.DownloadImageHandler;
import pt.isel.pdm.a39204.thoth.dataAcess.handlers.SetImageHandler;
import pt.isel.pdm.a39204.thoth.dataAcess.handlers.threads.DownloadImageThread;
import pt.isel.pdm.a39204.thoth.userInterface.LoginActivity;


public class ThothApp extends Application {

    public static String API_URL_TEACHERS = "https://adeetc.thothapp.com/api/v1/teachers";

    public static DownloadImageThread dit = new DownloadImageThread();
    public static DownloadImageHandler dih;
    public static SetImageHandler sih;

    public static final String AUTHORITY = "pt.isel.pdm.a39204.thoth.dataAcess.provider";
    public static final String ACCOUNT_TYPE = "pt.isel.pdm.a39204.thothSync";
    public static final String ACCOUNT = "thothSync";
    private static Account mAccount;

    public static ParseUser user;

    public static final int NUMBER_IMAGES_CACHED = 5;
    public static final int LONGEST_ACTUALIZATION_INTERVAL_SEC = 6 * 60 * 60;
    public static final int SHORT_ACTUALIZATION_INTERVAL_SEC =  1 *  60/*15 * 60*/;

    @Override
    public void onCreate() {
        super.onCreate();

        dit.start();
        sih = new SetImageHandler(Looper.getMainLooper());
        dih = new DownloadImageHandler(sih, dit.getLooper());

        mAccount = createSyncAccount(this);

        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "9AP84uHSeqzOQ7G9JEUy3YZJvrDSheA5JrTdHaNC", "eVjpWHeIv2RLMIwI5AUwXhCrfoE6WKPhm5nrKT0O");

        SharedPreferences preferences = this.getSharedPreferences(PreferencesSchema.FILENAME, MODE_PRIVATE);
        if(preferences.getBoolean(PreferencesSchema.FIRST_TIME, true)){
            Intent i = new Intent(this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(i);
        }
    }

    private Account createSyncAccount(Context context){

        AccountManager accountManager = (AccountManager)context.getSystemService(ACCOUNT_SERVICE);

        Account[] accounts = accountManager.getAccounts();
        for(Account a : accounts)
           if(a.name.equals(ACCOUNT))
               return a;

        Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            ContentResolver.setSyncAutomatically(newAccount, AUTHORITY, true);
            ContentResolver.addPeriodicSync(newAccount, AUTHORITY, Bundle.EMPTY, SHORT_ACTUALIZATION_INTERVAL_SEC);
        }

        return newAccount;
    }

    public static Account getAccount() {
        return mAccount;
    }

}
