package pt.isel.pdm.a39204.thoth.dataAcess.syncAdapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class ThothSyncAdapterService extends Service {

    private ThothSyncAdapter mSyncAdapter = null;
    private static final Object syncAdapterLock = new Object();

    @Override
    public void onCreate() {
        super.onCreate();
        synchronized (syncAdapterLock) {
            if (mSyncAdapter == null) {
                if (mSyncAdapter == null) {
                    mSyncAdapter = new ThothSyncAdapter(getApplicationContext(), true);
                }
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mSyncAdapter.getSyncAdapterBinder();
    }

}
