package pt.isel.pdm.a39204.thoth.logic;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.BaseColumns;
import android.support.v4.app.NotificationCompat;

import com.parse.ParseGeoPoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.dataAcess.HttpRequest;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothContract;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothSchema;
import pt.isel.pdm.a39204.thoth.logic.descerializer.TeachersDescerializer;
import pt.isel.pdm.a39204.thoth.logic.descerializer.TeachersDetailDescerializer;
import pt.isel.pdm.a39204.thoth.logic.entities.Teachers;
import pt.isel.pdm.a39204.thoth.logic.entities.TeachersDetail;
import pt.isel.pdm.a39204.thoth.userInterface.DetailActivity;

public class ThothUtils {


    private final Context mContext;

    public ThothUtils(Context context){
        mContext = context;
    }

    //CODICO COMENTADO POR RAZOES DE TESTES!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public String getTeacherActualLocation() {
        WifiManager wifiman = (WifiManager) mContext.getSystemService(mContext.WIFI_SERVICE);
        WifiInfo info = wifiman.getConnectionInfo();
        //return info.getBSSID();

        return "24:01:c7:29:28:60";
    }


    public List<String> getTeachersEmailsSubscribed(){
        List<String> list = new ArrayList<>();

        Cursor cursor = mContext.getContentResolver().query(ThothContract.Teachers.CONTENT_URI,
                new String[]{ThothContract.Teachers.ACADEMICEMAIL}, null, null, null);

        SharedPreferences preferences = mContext.getSharedPreferences(PreferencesSchema.FILENAME, mContext.MODE_PRIVATE);
        String email = "";
        while (cursor.moveToNext()){

            email = cursor.getString(cursor.getColumnIndex(ThothContract.Teachers.ACADEMICEMAIL));
            if(preferences.getString(email, "").equals(""))
                continue;
            list.add(email);

        }

        return list;
    }

    public Teachers getTeacherByEmail(String email){

        Cursor cursor = mContext.getContentResolver().query(ThothContract.Teachers.CONTENT_URI, null,
                ThothSchema.Teachers.COL_ACADEMICEMAIL + "= ?" ,
                new String[]{email}, null );
        cursor.moveToNext();
        String name = cursor.getString(cursor.getColumnIndex(ThothSchema.Teachers.COL_SHORTNAME));
        int id = cursor.getInt(cursor.getColumnIndex(ThothSchema.Teachers.COL_ID));

        return new Teachers(id, 0, name, null, email, null, null, null);

    }

    public Teachers getTeacherById(int teacherId) {

        final ContentResolver resolver = mContext.getContentResolver();
        Cursor cursor = resolver.query(ThothContract.Teachers.CONTENT_URI, null, ThothSchema.Teachers.COL_ID + " = ?",
                new String[]{Integer.toString(teacherId)}, null);

        if(cursor.getCount() == 0)
            return null;

        cursor.moveToNext();

        int id = cursor.getInt( cursor.getColumnIndex(ThothSchema.Teachers.COL_ID));
        int number = cursor.getInt( cursor.getColumnIndex(ThothSchema.Teachers.COL_NUMBER));
        String shortName = cursor.getString(cursor.getColumnIndex(ThothSchema.Teachers.COL_SHORTNAME));
        String fullName = cursor.getString(cursor.getColumnIndex(ThothSchema.Teachers.COL_FULLNAME));
        String academicEmail = cursor.getString( cursor.getColumnIndex(ThothSchema.Teachers.COL_ACADEMICEMAIL));
        String avatarUrl128 = cursor.getString( cursor.getColumnIndex(ThothSchema.Teachers.COL_AVATARURL128));

        return new Teachers(id, number, shortName, fullName, academicEmail, null, avatarUrl128, null);
    }

    public String getLabelByCoordinates(String coordiantes){
        Cursor cursor = mContext.getContentResolver().query(ThothContract.Locations.CONTENT_URI, null,
                ThothSchema.Locations.COL_COORDINATES + "= ?" ,
                new String[]{coordiantes}, null );
        cursor.moveToNext();
        String label = cursor.getString(cursor.getColumnIndex(ThothSchema.Locations.COL_LABEL));

        return label;
    }

    private ContentValues createValuesTeachers(int id, int number, String shortName,
                                               String fullName, String academicEmail, String avatarUrl32,
                                               String avatarUrl128, String self) {

        ContentValues values = new ContentValues();
        values.put(ThothContract.Teachers._ID, id);
        values.put(ThothContract.Teachers.NUMBER, number);
        values.put(ThothContract.Teachers.SHORTNAME, shortName);
        values.put(ThothContract.Teachers.FULLNAME, fullName);
        values.put(ThothContract.Teachers.ACADEMICEMAIL, academicEmail);
        values.put(ThothContract.Teachers.AVATARURL32, avatarUrl32);
        values.put(ThothContract.Teachers.AVATARURL128, avatarUrl128);
        values.put(ThothContract.Teachers.SELF, self);

        return values;
    }

    public ContentValues createValuesLocations(ParseGeoPoint coordinates, String label, String ssid) {
        ContentValues values = new ContentValues();
        values.put(ThothContract.Locations.COORDINATES, coordinates.toString());
        values.put(ThothContract.Locations.LABEL, label);
        values.put(ThothContract.Locations.SSID, ssid);

        return values;
    }



    public void getAndInsertTeachers() {
        ContentResolver resolver = mContext.getContentResolver();
        String content = new HttpRequest().doHttpRequest(ThothApp.API_URL_TEACHERS);
        TeachersDescerializer des = new TeachersDescerializer();
        List<Teachers> list = des.deserializeElements(content);

        ContentValues values;
        Cursor cursor;
        for (Teachers t : list) {

            cursor = resolver.query(ThothContract.Teachers.CONTENT_URI, null, BaseColumns._ID + " = ?",
                    new String[]{Integer.toString(t.getId())}, null);

            if (cursor.getCount() == 0) {
                content = new HttpRequest().doHttpRequest(t.getSelf());
                TeachersDetailDescerializer d = new TeachersDetailDescerializer();
                TeachersDetail td = d.deserializeElements(content);

                values = createValuesTeachers(t.getId(), t.getNumber(), t.getShortName(),
                        td.getFullName(), t.getAcademicEmail(),
                        t.getAvatarUrl32(), td.getAvatarUrl128(), t.getSelf());

                resolver.insert(ThothContract.Teachers.CONTENT_URI, values);
            }
        }
    }



    public void notifyTecherLocation(Teachers teacher, String label){
        int id = teacher.getId();

        Intent notifIntent = new Intent(mContext, DetailActivity.class);
        notifIntent.putExtra("teacherID",teacher.getId() );
        notifIntent.putExtra("notification", true);

        PendingIntent pendNotifIntent =
                PendingIntent.getActivity(mContext, id, notifIntent, 0);

        Notification notif = new NotificationCompat.Builder(mContext)
                .setContentTitle("Teacher " + teacher.getShortName())
                .setContentText("Last Location At: " + label)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setAutoCancel(true)
                .setContentIntent(pendNotifIntent)
                .build();

        NotificationManager notifManager =
                (NotificationManager)mContext
                        .getSystemService(mContext.NOTIFICATION_SERVICE);

        notifManager.notify(id, notif);
    }

    public void cancelAllNotification(){
        NotificationManager notifManager =
                (NotificationManager)mContext
                        .getSystemService(mContext.NOTIFICATION_SERVICE);

        notifManager.cancelAll();
    }



    public boolean checkEmail(String email){

        if(!email.contains(".isel.ipl.pt"))
            return false;

        String [] emaiTermination =  {"pt", "ipl", "isel"};
        String [] splitEmail = email.split("\\.");
        for(int i = splitEmail.length - 1, v = 0 ; v < emaiTermination.length; --i, ++v){
            if(!splitEmail[i].equals(emaiTermination[v]))
                return false;
        }

        return true;
    }

    public boolean checkEmailVerified(){
        return ThothApp.user.getBoolean("emailVerified");
    }

    public boolean checkIsTeacherOrStrudent(String email){
        String []e = email.substring(email.indexOf("@") + 1 ).split("\\.");
        if(e[0].equals("alunos"))
            return false;
        return true;
    }

    public String transformDateToString(Date date){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        String year = c.get(Calendar.YEAR) +"";
        String month = c.get(Calendar.MONTH)+1+"";
        String day = c.get(Calendar.DAY_OF_MONTH)+"";

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minutos = c.get(Calendar.MINUTE);
        int seconds = c.get(Calendar.SECOND);

        if(month.length() == 1)
            month = "0" + month.charAt(0);
        if(day.length() == 1)
            day = "0" + day.charAt(0);

        String hr = Integer.toString(hour) + ":" + Integer.toString(minutos) + ":" + Integer.toString(seconds);

        return year + "-" + month + "-" + day + " " + hr;
    }


}
