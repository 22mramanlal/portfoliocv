package pt.isel.pdm.a39204.thoth.userInterface;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.logic.PreferencesSchema;

public class LocationSettingActivity extends ActionBarActivity {

    private Switch locationSwitch;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_setting);
        loadComponents();
    }

    private void loadComponents(){
       final SharedPreferences preferences = getSharedPreferences(PreferencesSchema.FILENAME, MODE_PRIVATE);

        locationSwitch = (Switch) findViewById(R.id.location_location_setting_switch);
        saveButton = (Button) findViewById(R.id.save_location_setting_button);

        locationSwitch.setChecked(preferences.getBoolean(PreferencesSchema.LOCATION_ACTIVE, false));

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferences.edit().putBoolean(PreferencesSchema.LOCATION_ACTIVE, locationSwitch.isChecked() ).commit();
                Intent i = new Intent();
                setResult(RESULT_OK,i);
                finish();
            }
        });

    }

}
