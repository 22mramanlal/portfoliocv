package pt.isel.pdm.a39204.thoth.userInterface;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.dataAcess.parse.Parse;
import pt.isel.pdm.a39204.thoth.logic.ThothUtils;

public class RegisterActivity extends ActionBarActivity {

    private EditText username;
    private EditText email;
    private EditText password;
    private Button register;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        loadComponents();
    }

    private void loadComponents(){
        username = (EditText) findViewById(R.id.username_register_editText);
        email = (EditText) findViewById(R.id.email_register_editText);
        password = (EditText) findViewById(R.id.password_register_editText);
        register = (Button) findViewById(R.id.register_register_button);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createParserUser();
            }
        });
    }

    private void createParserUser(){
        ParseUser user = new ParseUser();
        user.setUsername(username.getText().toString());
        user.setPassword(password.getText().toString());

        String e = email.getText().toString();
        if(!new ThothUtils(this).checkEmail(e)) {
            Toast.makeText(RegisterActivity.this, R.string.email_finish_phrase, Toast.LENGTH_LONG).show();
            return;
        }

        user.setEmail(e);

        user.signUpInBackground(new SignUpCallback() {

            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Toast.makeText(RegisterActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(RegisterActivity.this, R.string.user_created_phrase, Toast.LENGTH_LONG).show();
                    finish();
                }
            }});
    }

}
