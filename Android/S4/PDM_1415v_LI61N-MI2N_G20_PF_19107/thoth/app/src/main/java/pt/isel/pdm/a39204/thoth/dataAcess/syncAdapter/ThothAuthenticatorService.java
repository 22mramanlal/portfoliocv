package pt.isel.pdm.a39204.thoth.dataAcess.syncAdapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class ThothAuthenticatorService extends Service {

    private ThothAuthenticator mAuthenticator;

    @Override
    public void onCreate() {
        mAuthenticator = new ThothAuthenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
