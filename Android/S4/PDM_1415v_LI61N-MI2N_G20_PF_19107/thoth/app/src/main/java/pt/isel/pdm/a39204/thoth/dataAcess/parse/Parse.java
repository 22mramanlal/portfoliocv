package pt.isel.pdm.a39204.thoth.dataAcess.parse;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Date;
import java.util.List;

import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothContract;
import pt.isel.pdm.a39204.thoth.logic.PreferencesSchema;
import pt.isel.pdm.a39204.thoth.logic.ThothApp;
import pt.isel.pdm.a39204.thoth.logic.ThothUtils;
import pt.isel.pdm.a39204.thoth.logic.entities.Teachers;
import pt.isel.pdm.a39204.thoth.userInterface.MainActivity;

public class Parse {

    private final Context mContext;
    private final ThothUtils utils;

    public Parse (Context context){
        mContext = context;
        utils = new ThothUtils(context);
    }

    public ParseUser getCurrentUser(){
        return ParseUser.getCurrentUser();
    }

    //Codigo comentado por razões de testes!!!!
    public void logIn(String login, String password) {
        ThothApp.user = new ParseUser();
        ThothApp.user.logInInBackground(login, password, new LogInCallback() {

            @Override
            public void done(ParseUser user, ParseException e) {

                if (e != null) {
                    Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    ThothApp.user = user;
                    //COMENTADO POR RAZÔES DE TESTES!!!!
                    /*if(!utils.checkEmailVerified()){
                        Toast.makeText(mContext, R.string.email_not_verified_phrase, Toast.LENGTH_LONG).show();
                        return;
                    }*/

                    SharedPreferences preferences = mContext.getSharedPreferences(PreferencesSchema.FILENAME, mContext.MODE_PRIVATE);
                    preferences.edit()
                            .putBoolean(PreferencesSchema.FIRST_TIME, false)
                            .putBoolean(PreferencesSchema.IS_TEACHER,utils.checkIsTeacherOrStrudent(user.getEmail()))
                            .commit();
                    Intent i = new Intent(mContext, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(i);
                }
            }
        });

    }

    public void insertLocationsFromParseToDB() {
        final ContentResolver resolver = mContext.getContentResolver();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Locations");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> locations, ParseException e) {
                if (e == null) {
                    ContentValues values;
                    for (ParseObject l : locations) {
                        values = utils.createValuesLocations(l.getParseGeoPoint("Coordinates"),
                                l.getString("Label"),
                                l.getString("SSID"));

                        resolver.insert(ThothContract.Locations.CONTENT_URI, values);
                    }
                } else {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    public void actualizeTeacherLocationOnParse(ParseGeoPoint geo){
        ParseObject teacherLocation = new ParseObject("TeachersLocation");
        ParseUser user = ParseUser.getCurrentUser();
        teacherLocation.put("teacherEmail", user.getEmail());
        teacherLocation.put("Coordinates", geo);
        teacherLocation.saveInBackground();
    }

    public void getTeacherLastLocationFromParse(List<String> emails){
        SharedPreferences preferences = mContext.getSharedPreferences(PreferencesSchema.FILENAME, mContext.MODE_PRIVATE);
        for(String email : emails){

            ParseQuery<ParseObject> query = ParseQuery.getQuery("TeachersLocation");
            query.whereEqualTo("teacherEmail", email);
            query.orderByDescending("createdAt");

            ParseObject tLocation = null;

            try {
                tLocation = query.getFirst();
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

            ParseGeoPoint coor = tLocation.getParseGeoPoint("Coordinates");
            String label = utils.getLabelByCoordinates(coor.toString());

            if( preferences.getString(email, "").contains(label)){
                Teachers t = utils.getTeacherByEmail(email);
                utils.notifyTecherLocation(t, label);
            }

        }

    }

    public String getTeacherLastLocationDetailFromParse(String email){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("TeachersLocation");
        query.whereEqualTo("teacherEmail", email);
        query.orderByDescending("createdAt");

        ParseObject tLocation = null;

        try {
            tLocation = query.getFirst();
        } catch (ParseException e) {
            return "";
        }

        ParseGeoPoint coor = tLocation.getParseGeoPoint("Coordinates");
        String label = utils.getLabelByCoordinates(coor.toString());
        Date date = tLocation.getCreatedAt();

        return label + "  " + utils.transformDateToString(date);
    }

}
