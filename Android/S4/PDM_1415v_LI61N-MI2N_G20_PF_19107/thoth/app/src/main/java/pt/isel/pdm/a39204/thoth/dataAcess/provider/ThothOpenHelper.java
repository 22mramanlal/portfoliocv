package pt.isel.pdm.a39204.thoth.dataAcess.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ThothOpenHelper extends SQLiteOpenHelper{
    private static final String NAME = ThothSchema.DB_NAME;
    private static final int VERSION = ThothSchema.DB_VERSION;

    public ThothOpenHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        deleteDb(db);
        createDb(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        deleteDb(db);
        createDb(db);
    }


    private void createDb(SQLiteDatabase db) {
        db.execSQL(ThothSchema.Teachers.DDL_CREATE_TABLE);
        db.execSQL(ThothSchema.Locations.DDL_CREATE_TABLE);
    }

    private void deleteDb(SQLiteDatabase db) {
        db.execSQL(ThothSchema.Teachers.DDL_DROP_TABLE);
        db.execSQL(ThothSchema.Locations.DDL_DROP_TABLE);
    }

}
