package pt.isel.pdm.a39204.thoth.userInterface;

import android.app.Activity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;

import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothContract;
import pt.isel.pdm.a39204.thoth.userInterface.listAdapter.TeachersCursorAdapter;


public class TeachersList extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView teachersListView;
    private CursorAdapter adapter;

    private static final int TEACHERS_LOADER = 0;

    private MainActivity myActivity = null;
    int mCurCheckPosition = 1;


    @Override
    public void onAttach(Activity myActivity) {
        super.onAttach(myActivity);
        this.myActivity = (MainActivity)myActivity;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        if (icicle != null) {
            mCurCheckPosition = icicle.getInt("curChoice", 1);
        }

        getLoaderManager().initLoader(TEACHERS_LOADER, null, this);

    }

    @Override
    public void onActivityCreated(Bundle icicle) {
        super.onActivityCreated(icicle);

        loadComponents();

        myActivity.showDetails(mCurCheckPosition);
    }


    private void loadComponents(){
        teachersListView = getListView();
        teachersListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        teachersListView.setSelection(mCurCheckPosition);

        teachersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor cur = (Cursor) adapter.getItem(position);
                cur.moveToPosition(position);
                int teacherId = cur.getInt(cur.getColumnIndex(BaseColumns._ID));
                mCurCheckPosition = teacherId;

                if (getResources().getConfiguration().orientation
                        == Configuration.ORIENTATION_LANDSCAPE) {
                    myActivity.showDetails(teacherId);
                }else {
                    Intent i = new Intent(getActivity(), DetailActivity.class);
                    i.putExtra("teacherID", teacherId);
                    startActivity(i);
                }

            }
        });

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case TEACHERS_LOADER:
                return new CursorLoader(
                        getActivity(),
                        ThothContract.Teachers.CONTENT_URI,
                        ThothContract.Teachers.PROJECTION_ALL,
                        null,
                        null,
                        null
                );
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if(adapter == null) {
            adapter = new TeachersCursorAdapter(getActivity(), cursor, 0);
            setListAdapter(adapter);
        }
        else {
            adapter.swapCursor(cursor);
        }

       adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }


}
