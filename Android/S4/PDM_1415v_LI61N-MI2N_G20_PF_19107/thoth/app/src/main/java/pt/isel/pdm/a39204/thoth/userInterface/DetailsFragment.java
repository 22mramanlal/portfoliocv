package pt.isel.pdm.a39204.thoth.userInterface;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.concurrent.atomic.AtomicBoolean;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.dataAcess.parse.Parse;
import pt.isel.pdm.a39204.thoth.logic.ThothApp;
import pt.isel.pdm.a39204.thoth.dataAcess.cache.Cache;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothContract;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothSchema;
import pt.isel.pdm.a39204.thoth.logic.ThothUtils;
import pt.isel.pdm.a39204.thoth.logic.entities.Teachers;


public class DetailsFragment extends Fragment {

    private TextView number;
    private TextView name;
    private TextView email;
    private TextView lastLocation;
    private ImageView image;
    private Button locations;

    private Teachers teacher;
    private int teacherId = 0;


    public static DetailsFragment newInstance(int index) {

        DetailsFragment df = new DetailsFragment();

        Bundle args = new Bundle();
        args.putInt("teacherID", index);
        df.setArguments(args);
        return df;
    }

    public static DetailsFragment newInstance(Bundle bundle) {
        int index = bundle.getInt("teacherID", 0);
        return newInstance(index);
    }

    @Override
    public void onCreate(Bundle myBundle) {
        super.onCreate(myBundle);

        teacherId = getArguments().getInt("teacherID", 0);
    }

    public int getShownIndex() {
        return teacherId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.teachers_list, container, false);
        teacher = new ThothUtils(getActivity()).getTeacherById(teacherId);

        if(teacher != null) {
            loadComponents(v);
            setComponents();
        }

        return v;
    }

    private void loadComponents(View v){
        number = (TextView) v.findViewById(R.id.number_detail_textView);
        name = (TextView) v.findViewById(R.id.name_detail_textView);
        email = (TextView) v.findViewById(R.id.email_detail_textView);
        lastLocation = (TextView) v.findViewById(R.id.last_location_detail_textView);
        image = (ImageView) v.findViewById(R.id.image_detail_imageView);
        locations = (Button) v.findViewById(R.id.locations_detail_button);
      
    }


    private void setComponents(){
        number.setText(Integer.toString(teacher.getNumber()));
        name.setText(teacher.getFullName());
        email.setText(teacher.getAcademicEmail());
        String label = new Parse(getActivity()).getTeacherLastLocationDetailFromParse(teacher.getAcademicEmail());
        lastLocation.setText(label);
        image.setImageResource(R.drawable.default_teacher);

        locations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LocationsActivity.class);
                i.putExtra("teacherEmail", teacher.getAcademicEmail());
                startActivity(i);
            }
        });

        Bitmap map = Cache.getIfContains(teacher.getShortName() + "128", getActivity());
        if(map != null)
            image.setImageBitmap(map);

        else
            ThothApp.dih.fetchImage(image, teacher.getAvatarUrl128(),new AtomicBoolean(false),
                    teacher.getShortName() + "128",getActivity());
    }


}
