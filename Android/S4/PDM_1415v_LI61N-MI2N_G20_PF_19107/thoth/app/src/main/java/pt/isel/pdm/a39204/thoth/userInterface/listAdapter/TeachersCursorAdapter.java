package pt.isel.pdm.a39204.thoth.userInterface.listAdapter;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothSchema;


public class TeachersCursorAdapter extends CursorAdapter {

    private LayoutInflater layoutInflater;
    private Context context;

    private int layout = R.layout.teachers_detail_list;

    public TeachersCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        if(view == null){
            view = layoutInflater.inflate(layout, null);
            view.setTag( new ViewModelTeachers(view));
        }

        bindModel((ViewModelTeachers)view.getTag(), i);
        return view;
    }

    private void bindModel(ViewModelTeachers teachersView, int pos){
        Cursor cursor = this.getCursor();
        cursor.moveToPosition(pos);

        String shortName = cursor.getString(cursor.getColumnIndex(ThothSchema.Teachers.COL_SHORTNAME));
        teachersView.teacherName.setText(shortName);
        if(teachersView.loader != null){
            teachersView.loader.cancel();
            teachersView.teacherImage.setImageResource(R.drawable.default_teacher);
        }

        String imageUri = cursor.getString(cursor.getColumnIndex(ThothSchema.Teachers.COL_AVATARURL32));
        teachersView.loader = new ImageLoader(context, teachersView.teacherImage , imageUri, shortName + "32");
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {

    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return layoutInflater.inflate(layout, parent, false);
    }

}

class ViewModelTeachers{

    public final ImageView teacherImage;
    public final TextView teacherName;
    public  ImageLoader loader;

    public ViewModelTeachers(View view){
        teacherImage = (ImageView) view.findViewById(R.id.teachers_listView_imageView);
        teacherName = (TextView) view.findViewById(R.id.teachers_listView_textView);
    }

}
