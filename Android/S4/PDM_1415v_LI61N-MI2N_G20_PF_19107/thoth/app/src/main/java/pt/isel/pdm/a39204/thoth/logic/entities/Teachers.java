package pt.isel.pdm.a39204.thoth.logic.entities;


public class Teachers {

    int id;
    int number;
    String shortName;
    String fullName;
    String academicEmail;
    String avatarUrl32;
    String avatarUrl128;
    String self;


    public Teachers(int id, int number, String shortName, String fullName,
                    String academicEmail, String avatarUrl32, String avatarUrl128, String self) {
        this.id = id;
        this.number = number;
        this.shortName = shortName;
        this.fullName = fullName;
        this.academicEmail = academicEmail;
        this.avatarUrl32 = avatarUrl32;
        this.avatarUrl128 = avatarUrl128;
        this.self = self;
    }

    public int getId(){
       return id;
   }

   public int getNumber(){
        return number;
   }

   public String getShortName(){
       return shortName;
   }

   public String getFullName(){
        return fullName;
    }

   public String getAcademicEmail(){
       return academicEmail;
   }

   public String getAvatarUrl32(){
       return avatarUrl32;
   }

    public String getAvatarUrl128(){
        return avatarUrl128;
    }

    public String getSelf() {
        return self;
    }
}
