package pt.isel.pdm.a39204.thoth.dataAcess.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public interface ThothContract {

    String AUTHORITY = "pt.isel.pdm.a39204.thoth.dataAcess.provider";

    Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    String SELECTION_BY_ID = BaseColumns._ID + " = ? ";

    String MEDIA_BASE_SUBTYPE = "/vnd.thoth.";


    public interface Teachers extends BaseColumns {
        String RESOURCE = "teachers";

        Uri CONTENT_URI =
                Uri.withAppendedPath(
                        ThothContract.CONTENT_URI,
                        RESOURCE);

        String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE +
                        MEDIA_BASE_SUBTYPE + RESOURCE;

        String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE +
                        MEDIA_BASE_SUBTYPE + RESOURCE;

        String NUMBER = "number";
        String SHORTNAME = "shortName";
        String FULLNAME = "fullName";
        String ACADEMICEMAIL = "academicEmail";
        String AVATARURL32 = "avatarUrl32";
        String AVATARURL128 = "avatarUrl128";
        String SELF = "self";

        String[] PROJECTION_ALL = {_ID, NUMBER, SHORTNAME, FULLNAME, ACADEMICEMAIL, AVATARURL32, AVATARURL128, SELF};

        String DEFAULT_SORT_ORDER = _ID + " ASC";
    }

    public interface Locations extends BaseColumns {
        String RESOURCE = "locations";

        Uri CONTENT_URI =
                Uri.withAppendedPath(
                        ThothContract.CONTENT_URI,
                        RESOURCE);

        String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE +
                        MEDIA_BASE_SUBTYPE + RESOURCE;

        String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE +
                        MEDIA_BASE_SUBTYPE + RESOURCE;

        String COORDINATES = "coordinates";
        String LABEL = "label";
        String SSID = "ssid";

        String[] PROJECTION_ALL = {_ID, COORDINATES, LABEL, SSID};

        String DEFAULT_SORT_ORDER = _ID + " ASC";
    }

}
