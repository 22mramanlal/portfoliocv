package pt.isel.pdm.a39204.thoth.dataAcess.handlers;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.ImageView;

import java.util.concurrent.atomic.AtomicBoolean;


public class SetImageHandler extends Handler{

    public SetImageHandler(Looper l){
        super(l);
    }

    public void handleMessage (Message msg){
        Data data = (Data)msg.obj;

        if(data.cancelationToken.get())
            return;

        data.im.setImageBitmap(data.bm);
    }

    public void publishImage(ImageView im, Bitmap bm, AtomicBoolean cancelationToken){
        if(cancelationToken.get())
            return;

        Message m = obtainMessage();
        m.obj = new Data(im,bm, cancelationToken);

        sendMessage(m);
    }

    public final class Data {
        public final ImageView im;
        public final Bitmap bm;
        public final AtomicBoolean cancelationToken;

        public Data(ImageView im, Bitmap bm, AtomicBoolean cancelationToken) {
            this.im = im;
            this.bm = bm;
            this.cancelationToken = cancelationToken;
        }
    }

}
