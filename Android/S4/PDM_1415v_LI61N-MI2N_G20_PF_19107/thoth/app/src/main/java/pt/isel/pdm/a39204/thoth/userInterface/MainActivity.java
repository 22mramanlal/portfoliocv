package pt.isel.pdm.a39204.thoth.userInterface;

import android.accounts.Account;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.logic.PreferencesSchema;
import pt.isel.pdm.a39204.thoth.logic.ThothApp;
import pt.isel.pdm.a39204.thoth.dataAcess.cache.Cache;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothContract;
import pt.isel.pdm.a39204.thoth.logic.ThothUtils;

public class MainActivity extends ActionBarActivity{

    public static final String TAG = "Thoth";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager.enableDebugLogging(true);
        setContentView(R.layout.activity_main);
    }

    public boolean isMultiPane() {
        return getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE;
    }

    public void showDetails(int index) {

        if (isMultiPane()) {

            DetailsFragment details = (DetailsFragment)
                    getSupportFragmentManager().findFragmentById(R.id.fragmentLayout);
            if (details == null || details.getShownIndex() != index) {

                details = DetailsFragment.newInstance(index);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragmentLayout, details);
                ft.addToBackStack(TAG);
                ft.commit();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mif = getMenuInflater();
        mif.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_cleanData:
                cleanDataAction();
                break;
            case R.id.action_refresh:
                refreshAction();
                break;
            case R.id.action_logOut:
                logOutAction();
                break;
            case R.id.action_location:
                locationAction();
                break;
            default:break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void cleanDataAction(){
        this.getContentResolver().delete(ThothContract.Teachers.CONTENT_URI,null,null);
        Cache.deleteAllFiles(this);
    }

    private void refreshAction(){
        Account account = ThothApp.getAccount();
        String authority = ThothApp.AUTHORITY;

        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        ContentResolver.requestSync(account, authority, settingsBundle);
    }

    private void logOutAction(){
        SharedPreferences preferences = this.getSharedPreferences(PreferencesSchema.FILENAME, MODE_PRIVATE);
        preferences.edit()
                .clear()
                .commit();
        ThothApp.user.logOut();
        new ThothUtils(this).cancelAllNotification();
        Intent i = new Intent(this, LoginActivity.class);
        this.startActivity(i);
    }

    private void locationAction(){
        SharedPreferences preferences = getSharedPreferences(PreferencesSchema.FILENAME, MODE_PRIVATE);

        if(!preferences.getBoolean(PreferencesSchema.IS_TEACHER, false))
            Toast.makeText(MainActivity.this, R.string.location_settings_phrase, Toast.LENGTH_LONG).show();
        else {
            Intent i = new Intent(this, LocationSettingActivity.class);
            this.startActivityForResult(i,0);
        }


    }
}
