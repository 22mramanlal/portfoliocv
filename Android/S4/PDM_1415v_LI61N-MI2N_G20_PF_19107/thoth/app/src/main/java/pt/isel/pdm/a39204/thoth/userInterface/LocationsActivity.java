package pt.isel.pdm.a39204.thoth.userInterface;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothContract;
import pt.isel.pdm.a39204.thoth.userInterface.listAdapter.LocationsCursorAdapter;

public class LocationsActivity extends Activity  implements LoaderManager.LoaderCallbacks<Cursor> {


    private ListView locationsListView;
    private Button saveButton;
    private CursorAdapter adapter;
    private static final int LOCATIONS_LOADER = 0;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);

        loadComponents();

        email = getIntent().getStringExtra("teacherEmail");

        getLoaderManager().initLoader(LOCATIONS_LOADER, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOCATIONS_LOADER:
                return new CursorLoader(
                        this,
                        ThothContract.Locations.CONTENT_URI,
                        ThothContract.Locations.PROJECTION_ALL,
                        null,
                        null,
                        null
                );
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if(adapter == null) {
            adapter = new LocationsCursorAdapter(LocationsActivity.this, cursor, 0, email);
            locationsListView.setAdapter(adapter);
        }
        else {
            adapter.swapCursor(cursor);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    private void loadComponents(){
        locationsListView = (ListView) findViewById(R.id.locations_locations_listView);
        saveButton = (Button) findViewById(R.id.save_locations_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
