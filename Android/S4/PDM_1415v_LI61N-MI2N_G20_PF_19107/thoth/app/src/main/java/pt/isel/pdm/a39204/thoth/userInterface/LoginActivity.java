package pt.isel.pdm.a39204.thoth.userInterface;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.dataAcess.parse.Parse;


public class LoginActivity extends ActionBarActivity {

    private EditText loginUserName;
    private EditText loginPassword;
    private Button loginButton;
    private Button registarButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loadComponents();
    }

    public void loadComponents(){
        loginUserName = (EditText) findViewById(R.id.email_login_editText);
        loginPassword = (EditText) findViewById(R.id.password_login_editText);
        loginButton = (Button) findViewById(R.id.login_login_button);
        registarButton = (Button) findViewById(R.id.register_login_button);

        loginButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Parse p = new Parse(getBaseContext());
                p.logIn(loginUserName.getText().toString(), loginPassword.getText().toString());
            }
        });

        registarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });

    }




}
