package pt.isel.pdm.a39204.thoth.dataAcess.syncAdapter;


import android.accounts.Account;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.dataAcess.parse.Parse;
import pt.isel.pdm.a39204.thoth.logic.PreferencesSchema;
import pt.isel.pdm.a39204.thoth.logic.ThothApp;
import pt.isel.pdm.a39204.thoth.dataAcess.HttpRequest;
import pt.isel.pdm.a39204.thoth.dataAcess.cache.Cache;
import pt.isel.pdm.a39204.thoth.logic.ThothUtils;
import pt.isel.pdm.a39204.thoth.logic.descerializer.TeachersDescerializer;
import pt.isel.pdm.a39204.thoth.logic.descerializer.TeachersDetailDescerializer;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothContract;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothSchema;
import pt.isel.pdm.a39204.thoth.logic.entities.Teachers;
import pt.isel.pdm.a39204.thoth.logic.entities.TeachersDetail;
import pt.isel.pdm.a39204.thoth.userInterface.DetailActivity;

public class ThothSyncAdapter extends AbstractThreadedSyncAdapter {

    private Context mContext;
    private Parse parse;
    private ThothUtils utils;

    private final String TAG = "ThothSyncAdapter";

    public ThothSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContext = context;
        parse = new Parse(context);
        utils = new ThothUtils(context);
    }

    public ThothSyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContext = context;
        parse = new Parse(context);
        utils = new ThothUtils(context);
    }


    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        SharedPreferences prefs = mContext.getSharedPreferences(PreferencesSchema.FILENAME, Context.MODE_PRIVATE);

        long lastUpdate = prefs.getLong(PreferencesSchema.LAST_LONG_UPDATE, 0);

        boolean manual = extras.getBoolean(ContentResolver.SYNC_EXTRAS_MANUAL);
        if (manual || (System.currentTimeMillis() - lastUpdate) >= ThothApp.LONGEST_ACTUALIZATION_INTERVAL_SEC * 1000) {
            Log.i(TAG, "actualizeTeachers");
            actualizeTeachers();
            prefs.edit().putLong(PreferencesSchema.LAST_LONG_UPDATE, System.currentTimeMillis()).commit();
        }

        if ((System.currentTimeMillis() - lastUpdate) >= ThothApp.LONGEST_ACTUALIZATION_INTERVAL_SEC * 1000) {
            Log.i(TAG, "actualizeLocations");
            actualizeLocations();
            prefs.edit().putLong(PreferencesSchema.LAST_LONG_UPDATE, System.currentTimeMillis()).commit();
            return;
        }

        if( parse.getCurrentUser()  == null)
            return;

        lastUpdate = prefs.getLong(PreferencesSchema.LAST_SHORT_UPDATE, 0);

        if ((System.currentTimeMillis() - lastUpdate) >= ThothApp.SHORT_ACTUALIZATION_INTERVAL_SEC * 1000
                && prefs.getBoolean(PreferencesSchema.IS_TEACHER, false)
                && prefs.getBoolean(PreferencesSchema.LOCATION_ACTIVE, false)) {
            Log.i(TAG, "actualizeTeacherLocation");
            actualizeTeacherLocation();
            prefs.edit().putLong(PreferencesSchema.LAST_SHORT_UPDATE, System.currentTimeMillis()).commit();
        }

        if((System.currentTimeMillis() - lastUpdate) >= ThothApp.SHORT_ACTUALIZATION_INTERVAL_SEC * 1000){
            Log.i(TAG, "notifyTeachersLastLocation");
            notifyTeachersLastLocation();
            prefs.edit().putLong(PreferencesSchema.LAST_SHORT_UPDATE, System.currentTimeMillis()).commit();
        }

    }

    private void actualizeTeachers() {
        mContext.getContentResolver().delete(ThothContract.Teachers.CONTENT_URI, null, null);
        Cache.deleteAllFiles(mContext);
        utils.getAndInsertTeachers();
    }

    private void actualizeLocations() {
        mContext.getContentResolver().delete(ThothContract.Locations.CONTENT_URI, null, null);
        parse.insertLocationsFromParseToDB();
    }

    private void actualizeTeacherLocation() {
        ParseGeoPoint geo;

        Cursor cursor = mContext.getContentResolver().query(ThothContract.Locations.CONTENT_URI, null, ThothContract.Locations.SSID + "= ?",
                new String[]{utils.getTeacherActualLocation()}, null);

        if (cursor.getCount() < 1)
            return;
        else {
            cursor.moveToNext();
            String coordinates = cursor.getString(cursor.getColumnIndex(ThothSchema.Locations.COL_COORDINATES));
            String coord[] = coordinates.substring(coordinates.indexOf('[') + 1, coordinates.indexOf(']')).split(",");
            geo = new ParseGeoPoint(Double.parseDouble(coord[0]), Double.parseDouble(coord[1]));
        }

        parse.actualizeTeacherLocationOnParse(geo);
    }

    private void notifyTeachersLastLocation(){
        List<String> emails = utils.getTeachersEmailsSubscribed();
        parse.getTeacherLastLocationFromParse(emails);
    }


}