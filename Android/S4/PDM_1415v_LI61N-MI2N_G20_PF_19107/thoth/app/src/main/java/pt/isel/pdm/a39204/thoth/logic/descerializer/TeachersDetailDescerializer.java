package pt.isel.pdm.a39204.thoth.logic.descerializer;

import org.json.JSONException;
import org.json.JSONObject;

import pt.isel.pdm.a39204.thoth.logic.entities.TeachersDetail;


public class TeachersDetailDescerializer {

    public TeachersDetail deserializeElements (String str) {
        JSONObject getListInformation = null;

        try {
            getListInformation = new JSONObject(str);
        } catch (JSONException e){
            throw new RuntimeException(e);
        }

        TeachersDetail teacher = null;

        try {

             teacher = new TeachersDetail(
                                    getListInformation.getString("fullName"),
                                    getListInformation.getJSONObject("avatarUrl").getString("size128")
                                  );

        } catch (JSONException e) {
               e.printStackTrace();
        }

        return teacher;
    }


}
