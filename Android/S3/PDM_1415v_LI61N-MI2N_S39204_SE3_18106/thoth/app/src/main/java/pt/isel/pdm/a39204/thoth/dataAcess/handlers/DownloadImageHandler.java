package pt.isel.pdm.a39204.thoth.dataAcess.handlers;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

import pt.isel.pdm.a39204.thoth.dataAcess.cache.Cache;


public class DownloadImageHandler extends Handler {

    private SetImageHandler sih;

    public DownloadImageHandler(SetImageHandler sih, Looper l){
        super(l);
        this.sih = sih;
    }

    public void handleMessage (Message msg){
        Data data = (Data)msg.obj;
        URL url;
        try {
            if(data.cancelationToken.get())
                return;

            url = new URL(data.uri);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            InputStream s = c.getInputStream();
            Bitmap bm = BitmapFactory.decodeStream(s);
            sih.publishImage(data.im, bm, data.cancelationToken);
            Cache.add(data.fileName , bm, data.context);
        } catch (MalformedURLException e) {

        } catch (IOException e) {

        }
    }

    public void fetchImage(ImageView im, String uri, AtomicBoolean cancelationToken, String fileName, Context context){
        Message m = obtainMessage();
        m.obj = new Data(uri,im, cancelationToken, fileName, context);

        sendMessage(m);
    }

    static class Data{
        public final String uri;
        public final ImageView im;
        public final AtomicBoolean cancelationToken;
        public final String fileName;
        public final Context context;

        public Data(String uri, ImageView im, AtomicBoolean cancelationToken, String fileName, Context context){
            this.uri = uri;
            this.im = im;
            this.cancelationToken = cancelationToken;
            this.fileName = fileName;
            this.context = context;

        }
    }

}
