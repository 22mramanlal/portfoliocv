package pt.isel.pdm.a39204.thoth.logic;


public class TeachersDetail {

    private String fullName;
    private String avatarUrl128;

    public TeachersDetail(String fullName, String avatarUrl128) {
        this.fullName = fullName;
        this.avatarUrl128 = avatarUrl128;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAvatarUrl128() {
        return avatarUrl128;
    }
}
