package pt.isel.pdm.a39204.thoth.userInterface;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.dataAcess.cache.Cache;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothContract;
import pt.isel.pdm.a39204.thoth.dataAcess.ThothProviderService;

public class MainActivity extends ActionBarActivity{

    public static final String TAG = "Thoth";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager.enableDebugLogging(true);
        setContentView(R.layout.activity_main);
    }

    public boolean isMultiPane() {
        return getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE;
    }

    public void showDetails(int index) {

        if (isMultiPane()) {

            DetailsFragment details = (DetailsFragment)
                    getSupportFragmentManager().findFragmentById(R.id.fragmentLayout);
            if (details == null || details.getShownIndex() != index) {

                details = DetailsFragment.newInstance(index);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragmentLayout, details);
                ft.addToBackStack(TAG);
                ft.commit();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mif = getMenuInflater();
        mif.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_cleanData:
                this.getContentResolver().delete(ThothContract.Teachers.CONTENT_URI,null,null);
                Cache.deleteAllFiles(this);
                break;
            case R.id.action_refresh:
                ThothProviderService.startActionTeachers(this);
                break;
            default:break;
        }

        return super.onOptionsItemSelected(item);
    }

}
