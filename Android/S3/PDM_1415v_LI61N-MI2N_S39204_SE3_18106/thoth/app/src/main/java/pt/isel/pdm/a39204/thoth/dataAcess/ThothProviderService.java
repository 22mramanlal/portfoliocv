package pt.isel.pdm.a39204.thoth.dataAcess;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.BaseColumns;

import java.util.List;

import pt.isel.pdm.a39204.thoth.ThothApp;
import pt.isel.pdm.a39204.thoth.dataAcess.descerializer.TeachersDescerializer;
import pt.isel.pdm.a39204.thoth.dataAcess.descerializer.TeachersDetailDescerializer;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothContract;
import pt.isel.pdm.a39204.thoth.logic.Teachers;
import pt.isel.pdm.a39204.thoth.logic.TeachersDetail;

public class ThothProviderService extends IntentService {

    private static final String ACTION_GET_TEACHERS = "pt.isel.pdm.a39204.thoth.dataAcess.receiver.action.TEACHERS";

    public ThothProviderService() {
        super("HaGreveProviderService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_TEACHERS.equals(action))
                handleActionTeachers();
        }
    }

    private void handleActionTeachers(){

        final ContentResolver resolver = getContentResolver();
        String content = HttpRequest.doHttpRequest(ThothApp.API_URL_TEACHERS);
        TeachersDescerializer des = new TeachersDescerializer();
        List<Teachers> list = des.deserializeElements(content);

        ContentValues values;
        Cursor cursor;
        for(Teachers t : list) {

            cursor = resolver.query(ThothContract.Teachers.CONTENT_URI, null, BaseColumns._ID + " = ?",
                    new String [] { Integer.toString(t.getId())}, null);

            if(cursor.getCount() == 0) {


                content = HttpRequest.doHttpRequest(t.getSelf());
                TeachersDetailDescerializer d = new TeachersDetailDescerializer();
                TeachersDetail td =  d.deserializeElements(content);

                values = createValuesTeachers(t.getId(), t.getNumber(), t.getShortName(),
                                                 td.getFullName(), t.getAcademicEmail(),
                                               t.getAvatarUrl32(), td.getAvatarUrl128(),  t.getSelf());
                resolver.insert(ThothContract.Teachers.CONTENT_URI, values);
            }
        }

    }

    public static void startActionTeachers(Context context) {
        Intent intent = new Intent(context, ThothProviderService.class);
        intent.setAction(ACTION_GET_TEACHERS);
        context.startService(intent);
    }


    private ContentValues createValuesTeachers(int id, int number, String shortName,
                                               String fullName, String academicEmail, String avatarUrl32,
                                               String avatarUrl128, String self) {

        ContentValues values = new ContentValues();
        values.put(ThothContract.Teachers._ID, id);
        values.put(ThothContract.Teachers.NUMBER, number);
        values.put(ThothContract.Teachers.SHORTNAME, shortName);
        values.put(ThothContract.Teachers.FULLNAME, fullName);
        values.put(ThothContract.Teachers.ACADEMICEMAIL, academicEmail);
        values.put(ThothContract.Teachers.AVATARURL32, avatarUrl32);
        values.put(ThothContract.Teachers.AVATARURL128, avatarUrl128);
        values.put(ThothContract.Teachers.SELF, self);

        return values;
    }


}
