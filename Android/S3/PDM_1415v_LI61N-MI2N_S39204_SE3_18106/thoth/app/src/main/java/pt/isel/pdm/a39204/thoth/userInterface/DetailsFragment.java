package pt.isel.pdm.a39204.thoth.userInterface;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.concurrent.atomic.AtomicBoolean;

import pt.isel.pdm.a39204.thoth.R;
import pt.isel.pdm.a39204.thoth.ThothApp;
import pt.isel.pdm.a39204.thoth.dataAcess.cache.Cache;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothContract;
import pt.isel.pdm.a39204.thoth.dataAcess.provider.ThothSchema;
import pt.isel.pdm.a39204.thoth.logic.Teachers;


public class DetailsFragment extends Fragment {


    private TextView number;
    private TextView name;
    private TextView email;
    private ImageView image;

    private Teachers teacher;
    private int teacherId = 0;

    public static DetailsFragment newInstance(int index) {

        DetailsFragment df = new DetailsFragment();

        Bundle args = new Bundle();
        args.putInt("teacherID", index);
        df.setArguments(args);
        return df;
    }

    public static DetailsFragment newInstance(Bundle bundle) {
        int index = bundle.getInt("teacherID", 0);
        return newInstance(index);
    }

    @Override
    public void onCreate(Bundle myBundle) {
        super.onCreate(myBundle);

        teacherId = getArguments().getInt("teacherID", 0);
    }

    public int getShownIndex() {
        return teacherId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.teachers_list, container, false);
        teacher = cursorToTeacher(teacherId);

        if(teacher != null) {
            loadComponents(v);
            setComponents();
        }

        return v;
    }



    private Teachers cursorToTeacher(int teacherId) {

        final ContentResolver resolver = getActivity().getContentResolver();
        Cursor cursor = resolver.query(ThothContract.Teachers.CONTENT_URI, null, ThothSchema.Teachers.COL_ID + " = ?",
                new String[]{Integer.toString(teacherId)}, null);

        if(cursor.getCount() == 0)
            return null;

        cursor.moveToNext();

        int id = cursor.getInt( cursor.getColumnIndex(ThothSchema.Teachers.COL_ID));
        int number = cursor.getInt( cursor.getColumnIndex(ThothSchema.Teachers.COL_NUMBER));
        String shortName = cursor.getString(cursor.getColumnIndex(ThothSchema.Teachers.COL_SHORTNAME));
        String fullName = cursor.getString(cursor.getColumnIndex(ThothSchema.Teachers.COL_FULLNAME));
        String academicEmail = cursor.getString( cursor.getColumnIndex(ThothSchema.Teachers.COL_ACADEMICEMAIL));
        String avatarUrl128 = cursor.getString( cursor.getColumnIndex(ThothSchema.Teachers.COL_AVATARURL128));

        return new Teachers(id, number, shortName, fullName, academicEmail, null, avatarUrl128, null);
    }

    private void loadComponents(View v){
        number = (TextView) v.findViewById(R.id.number_detail_textView);
        name = (TextView) v.findViewById(R.id.name_detail_textView);
        email = (TextView) v.findViewById(R.id.email_detail_textView);
        image = (ImageView) v.findViewById(R.id.image_detail_imageView);
    }


    private void setComponents(){
        number.setText(Integer.toString(teacher.getNumber()));
        name.setText(teacher.getFullName());
        email.setText(teacher.getAcademicEmail());
        image.setImageResource(R.drawable.default_teacher);

        Bitmap map = Cache.getIfContains(teacher.getShortName() + "128", getActivity());
        if(map != null)
            image.setImageBitmap(map);

        else
            ThothApp.dih.fetchImage(image, teacher.getAvatarUrl128(),new AtomicBoolean(false),
                    teacher.getShortName() + "128",getActivity());
    }


}
