package pt.isel.pdm.a39204.thoth.dataAcess.provider;

import android.provider.BaseColumns;

public interface ThothSchema {

    String DB_NAME = "thoth.db";
    int DB_VERSION = 1;

    static interface Teachers{

        String TBL_NAME = "teachers";

        String COL_ID = BaseColumns._ID;
        String COL_NUMBER = "number";
        String COL_SHORTNAME = "shortName";
        String COL_FULLNAME = "fullName";
        String COL_ACADEMICEMAIL = "academicEmail";
        String COL_AVATARURL32 = "avatarUrl32";
        String COL_AVATARURL128 = "avatarUrl128";
        String COL_SELF = "self";

        String DDL_CREATE_TABLE =
                "CREATE TABLE " + TBL_NAME + "(" +
                        COL_ID    + " INTEGER PRIMARY KEY, " +
                        COL_NUMBER  + " INTEGER, " +
                        COL_SHORTNAME + " TEXT, " +
                        COL_FULLNAME + " TEXT, " +
                        COL_ACADEMICEMAIL + " TEXT, " +
                        COL_AVATARURL32 + " TEXT, " +
                        COL_AVATARURL128 + " TEXT, " +
                        COL_SELF + " TEXT " +
                        ")";

        String DDL_DROP_TABLE =
                "DROP TABLE IF EXISTS " + TBL_NAME;

    }

}
