package pt.isel.pdm.a39204.thoth.dataAcess.descerializer;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pt.isel.pdm.a39204.thoth.logic.Teachers;

public class TeachersDescerializer {


    public List<Teachers> deserializeElements (String str) {
        JSONObject getListInformation = null;
        JSONArray allClasses  = null;

        try {
            getListInformation = new JSONObject(str);
            allClasses = getListInformation.getJSONArray("teachers");
        } catch (JSONException e){
            throw new RuntimeException(e);
        }
        List <Teachers> teachers;
        teachers = new ArrayList<Teachers>();
        Teachers teacher = null;

        for (int i = 0 ; i < allClasses.length()  ; ++i){

            JSONObject info = null;
            try {
                info = allClasses.getJSONObject(i);
                teacher = new Teachers(info.getInt("id"),
                                        info.getInt("number"),
                                        info.getString("shortName"),
                                        null,
                                        info.getString("academicEmail"),
                                        info.getJSONObject("avatarUrl").getString("size32"),
                                        null,
                                       info.getJSONObject("_links").getString("self"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            teachers.add(teacher);
        }
        return teachers;
    }

}
