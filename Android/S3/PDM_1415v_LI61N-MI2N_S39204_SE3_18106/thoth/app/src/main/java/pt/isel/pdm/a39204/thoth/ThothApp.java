package pt.isel.pdm.a39204.thoth;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Looper;

import pt.isel.pdm.a39204.thoth.dataAcess.handlers.DownloadImageHandler;
import pt.isel.pdm.a39204.thoth.dataAcess.handlers.SetImageHandler;
import pt.isel.pdm.a39204.thoth.dataAcess.handlers.threads.DownloadImageThread;
import pt.isel.pdm.a39204.thoth.dataAcess.receiver.RegularlyAlarmReceiver;


public class ThothApp extends Application {

    public static String API_URL_TEACHERS = "https://adeetc.thothapp.com/api/v1/teachers";

    public static final int NumberImagesCached = 5;

    public static DownloadImageThread dit = new DownloadImageThread();

    public static DownloadImageHandler dih;

    public static SetImageHandler sih;


    @Override
    public void onCreate(){
        super.onCreate();

        dit.start();
        sih = new SetImageHandler(Looper.getMainLooper());
        dih = new DownloadImageHandler(sih, dit.getLooper());

        createAlarmRegularly();

    }

    private void createAlarmRegularly(){

        Intent alarmIntent = new Intent(this, RegularlyAlarmReceiver.class);

        if(PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_NO_CREATE) == null){

            PendingIntent pendingAlarmIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

            alarmManager.setRepeating(
                    AlarmManager.RTC_WAKEUP ,
                    System.currentTimeMillis() + 2 *1000,
                    AlarmManager.INTERVAL_HOUR,
                    pendingAlarmIntent
            );
        }
    }


}
