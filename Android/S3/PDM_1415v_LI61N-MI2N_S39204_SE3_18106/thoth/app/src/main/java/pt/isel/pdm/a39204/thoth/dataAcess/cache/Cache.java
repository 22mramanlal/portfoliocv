package pt.isel.pdm.a39204.thoth.dataAcess.cache;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import pt.isel.pdm.a39204.thoth.ThothApp;

public class Cache {

    private static boolean loaded = false;

    private static final String EXTENSION = ".png";

    private static int NumberImagesCurrentlyCached;
    private static final Map<String, Bitmap> MAP = new HashMap<String, Bitmap>();
    private static LinkedList<String> ORDER = new LinkedList<String>();


    public static Bitmap getIfContains(String imageName, Context context){
            loadCache(context);
            return MAP.containsKey(imageName)? MAP.get(imageName) : null;
    }

    public static synchronized void add(String imageName, Bitmap bitmap, Context context){

        loadCache(context);
        if(MAP.containsKey(imageName))
            return;

        MAP.put(imageName, bitmap);
        ORDER.addLast(imageName);
        saveFile(imageName, bitmap, context);

        NumberImagesCurrentlyCached++;

        cleanCache(context);

    }

    public static synchronized void deleteAllFiles(Context context){

        for(String name: ORDER) {
            deleteFile(name, context);
            MAP.remove(name);
        }
        NumberImagesCurrentlyCached = 0;
        ORDER = new LinkedList<>();
    }

    private static void loadCache(Context context){
        if(loaded)
            return;

        loaded = true;

        File FOLDER = context.getExternalCacheDir();

        String imageName;
        Bitmap bitmap;
        for(File file : FOLDER.listFiles()){
            imageName = file.getName().split(EXTENSION)[0];
            bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            add(imageName, bitmap, context);
        }
    }

    private static void cleanCache(Context context) {
        String imageName;

        while(NumberImagesCurrentlyCached > ThothApp.NumberImagesCached){
            imageName = ORDER.removeFirst();
            MAP.remove(imageName);
            deleteFile(imageName, context);
            NumberImagesCurrentlyCached--;
        }
    }


    private static void saveFile(String imageName, Bitmap bitmap, Context context){

        FileOutputStream fOut = null;
        try{
            File file = new File(context.getExternalCacheDir().getAbsolutePath(), imageName + EXTENSION);
            fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
        }catch(Exception e){
            throw new RuntimeException(e);
        }finally{
            if(fOut != null){
                try {
                    fOut.flush();
                    fOut.close();
                } catch (Exception e2) {
                    throw new RuntimeException(e2);
                }
            }
        }
    }

    private static void deleteFile(String imageName, Context context){
        File file = new File(context.getExternalCacheDir().getAbsolutePath() + "/" + imageName + EXTENSION);
        file.delete();
    }



}
