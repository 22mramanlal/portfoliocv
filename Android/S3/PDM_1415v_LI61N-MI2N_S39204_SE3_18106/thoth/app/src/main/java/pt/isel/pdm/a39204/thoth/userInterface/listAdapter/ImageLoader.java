package pt.isel.pdm.a39204.thoth.userInterface.listAdapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import java.util.concurrent.atomic.AtomicBoolean;

import pt.isel.pdm.a39204.thoth.ThothApp;
import pt.isel.pdm.a39204.thoth.dataAcess.cache.Cache;


public class ImageLoader {

    private final AtomicBoolean cancel;
    private final Context context;
    private final ImageView view;
    private final String uri;
    private final String fileName;


    public ImageLoader(Context context, ImageView view, String uri, String fileName){
        cancel = new AtomicBoolean(false);
        this.view = view;
        this.context = context;
        this.uri = uri;
        this.fileName = fileName;
        run();
    }

    public void cancel(){
        cancel.set(true);
    }

    public void run(){

        Bitmap map = Cache.getIfContains(fileName, context);
        if(map != null)
            view.setImageBitmap(map);

        else
            ThothApp.dih.fetchImage(view,uri,cancel,fileName,context);

    }

}
