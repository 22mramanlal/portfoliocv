package pt.isel.pdm.a39204.thoth.dataAcess.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.BatteryManager;

import pt.isel.pdm.a39204.thoth.dataAcess.ThothProviderService;

public class RegularlyAlarmReceiver extends BroadcastReceiver {

    @Override
    public  void onReceive(Context context, Intent intent){

        if(checkWifiAndBattery(context)) {
            ThothProviderService.startActionTeachers(context);
        }
    }

    public boolean checkWifiAndBattery(Context context){

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

        if( cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting() && level >= 20)
            return true;

        return false;
    }

}
