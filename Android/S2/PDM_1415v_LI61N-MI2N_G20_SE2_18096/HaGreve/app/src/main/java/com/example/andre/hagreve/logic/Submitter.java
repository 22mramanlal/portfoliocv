package com.example.andre.hagreve.logic;


public class Submitter {
    private String first_name;
    private String last_name;

    public Submitter(String first, String last){
        this.first_name = first;
        this.last_name = last;
    }

    public String getFirst_name(){
        return first_name;
    }

    public String getLast_name(){
        return last_name;
    }
}
