package com.example.andre.hagreve.userInterface.activity;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.dataAcess.HaGreveProviderService;
import com.example.andre.hagreve.dataAcess.provider.HaGreveContract;
import com.example.andre.hagreve.dataAcess.provider.HaGreveSchema;
import com.example.andre.hagreve.userInterface.HaGreveUtils;
import com.example.andre.hagreve.userInterface.listAdapter.StrikesCursorAdapter;


public class MainActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView strikesListView;
    private CursorAdapter adapter;
    private static final int STRIKES_LOADER = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadComponents();
        loadPreferences();
        copyCompaniesIdsNotRepresent();

        getLoaderManager().initLoader(STRIKES_LOADER, null, this);
    }

    public void loadComponents(){
        strikesListView = (ListView) findViewById(R.id.strikes_ListView);

        strikesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor cur = (Cursor) adapter.getItem(position);
                cur.moveToPosition(position);
                int strikeId = cur.getInt(cur.getColumnIndexOrThrow(BaseColumns._ID));

                Intent i = new Intent(MainActivity.this, StrikeDetailsActivity.class);
                i.putExtra("strikesID", strikeId);
                startActivity(i);
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case STRIKES_LOADER:
                return new CursorLoader(
                        this,
                        HaGreveContract.Strikes.CONTENT_URI,
                        HaGreveContract.Strikes.PROJECTION_ALL,
                        HaGreveUtils.queryNotRepresent,
                        HaGreveUtils.idsNotRepresent,
                        null
                );
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if(adapter == null) {
            adapter = new StrikesCursorAdapter(MainActivity.this, cursor, 0);
            strikesListView.setAdapter(adapter);
        }
        else {
            adapter.swapCursor(cursor);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    private void loadStrikes(){
        HaGreveProviderService.startActionStrikes(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch(id){
            case  R.id.action_settings:
                 startActivityForResult(new Intent(this, FilterActivity.class), 0); break;
            case R.id.action_definitions:
                startActivityForResult(new Intent(this, DefinitionsActivity.class),1);break;
            case R.id.action_refresh:
                loadStrikes(); break;
            case R.id.action_notification:
                startActivityForResult(new Intent(this, NotificationActivity.class),2);break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == 0) {
            copyCompaniesIdsNotRepresent();
            getLoaderManager().restartLoader(STRIKES_LOADER, null, this);
        }
        if(requestCode == 1 && resultCode == RESULT_OK)
            loadStrikes();
    }

    private void copyCompaniesIdsNotRepresent(){

        HaGreveUtils.idsNotRepresent = new String[HaGreveUtils.companyIdsFiltered.size() + 2];
        HaGreveUtils.queryNotRepresent = HaGreveSchema.Strikes.COL_STARTDATE + " BETWEEN ? AND ?";
        HaGreveUtils.idsNotRepresent[0] = HaGreveUtils.currentDateDB();
        HaGreveUtils.idsNotRepresent[1] = HaGreveUtils.afterYearsDateDB();

        for(int i = 0; i< HaGreveUtils.companyIdsFiltered.size() ; ++i){
            HaGreveUtils.queryNotRepresent += " AND " + HaGreveSchema.Strikes.COL_COMPANYID + " != ?";
            HaGreveUtils.idsNotRepresent[i+2] = Integer.toString(HaGreveUtils.companyIdsFiltered.get(i));
        }
    }

    @Override
    protected void onStop(){
        super.onStop();
        StringBuilder sb = new StringBuilder();

        for(int i : HaGreveUtils.companyIdsFiltered)
            sb.append(i + ",");

        SharedPreferences sp = getSharedPreferences(HaGreveUtils.SHAREDPREFERENCE_FILE,MODE_PRIVATE);
        sp.edit()
        .putString("companiesIDs",sb.toString())
        .putInt("numberOfDays", HaGreveUtils.numberOfDays)
        .remove("hostPortText").remove("isOnline")
        .commit();
    }

    private void loadPreferences(){
        SharedPreferences sp = getSharedPreferences(HaGreveUtils.SHAREDPREFERENCE_FILE,MODE_PRIVATE);
        HaGreveUtils.numberOfDays = sp.getInt("numberOfDays",0);
        String str = sp.getString("companiesIDs",",");
        if(str == "") return;
        String [] s = str.split(",");

        for(int i = 0; i< s.length ;++i)
                HaGreveUtils.companyIdsFiltered.add(Integer.parseInt(s[i]));
    }

}


