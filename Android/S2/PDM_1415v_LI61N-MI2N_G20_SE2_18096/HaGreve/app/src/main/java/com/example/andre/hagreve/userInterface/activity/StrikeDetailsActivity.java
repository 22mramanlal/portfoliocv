package com.example.andre.hagreve.userInterface.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.logic.Strike;
import com.example.andre.hagreve.userInterface.HaGreveApp;
import com.example.andre.hagreve.userInterface.HaGreveUtils;

public class StrikeDetailsActivity extends ActionBarActivity {

    private TextView company;
    private TextView description;
    private TextView startDate;
    private TextView endDate;
    private CheckBox allDay;
    private CheckBox canceled;
    private Button sourceLink;

    private Strike s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_strikes_details);

        Intent i = getIntent();
        int strikeId = i.getIntExtra("strikesID",0);

        s = HaGreveUtils.cursorToStrike(this, strikeId);

        loadComponents();
        setComponents();
    }

    public void loadComponents(){
        company = (TextView) findViewById(R.id.strikes_details_company_tv);
        description = (TextView) findViewById(R.id.strikes_details_description_tv);
        startDate = (TextView) findViewById(R.id.strikes_details_start_date_tv);
        endDate = (TextView) findViewById(R.id.strikes_details_end_date_tv_id);
        allDay = (CheckBox) findViewById(R.id.strikes_details_all_day_cb);
        canceled = (CheckBox) findViewById(R.id.strikes_details_canceled_cb_id);
        sourceLink = (Button) findViewById(R.id.strikes_details_go_to_browser_button_id);
    }

    public void setComponents(){
        company.setText(s.getCompany().getCompanyName());
        description.setText(s.getDescription());
        startDate.setText(HaGreveUtils.dateToString(s.getStart_date()));
        endDate.setText(HaGreveUtils.dateToString(s.getEnd_date()));
        allDay.setChecked(s.isAll_day());
        canceled.setChecked(s.isCanceled());
    }

    public void clickSourceLink(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri url = Uri.parse(s.getSource_link());
        intent.setData(url);
        startActivity(intent);
    }

    public void clickAddToCalendar(View view) {
        Intent intent = new Intent();
        intent.setType("vnd.android.cursor.item/event");

        long time_millis_beginTime = s.getStart_date().getTime();
        long time_millis_endTime = s.getEnd_date().getTime();

        intent.putExtra("beginTime", time_millis_beginTime);
        intent.putExtra("endTime", time_millis_endTime);
        intent.putExtra(CalendarContract.Events.TITLE, company.getText().toString());
        intent.putExtra(CalendarContract.Events.ALL_DAY,allDay.isChecked());
        intent.putExtra(CalendarContract.Events.DESCRIPTION, description.getText().toString());
        intent.setAction(Intent.ACTION_EDIT);
        startActivity(intent);
    }

}
