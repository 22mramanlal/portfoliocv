package com.example.andre.hagreve.dataAcess.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class HaGreveOpenHelper extends SQLiteOpenHelper  {

    private static final String NAME = HaGreveSchema.DB_NAME;
    private static final int VERSION = HaGreveSchema.DB_VERSION;

    public HaGreveOpenHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        deleteDb(db);
        createDb(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        deleteDb(db);
        createDb(db);
    }


    private void createDb(SQLiteDatabase db) {
        db.execSQL(HaGreveSchema.Companies.DDL_CREATE_TABLE);
        db.execSQL(HaGreveSchema.Strikes.DDL_CREATE_TABLE);
    }

    private void deleteDb(SQLiteDatabase db) {
        db.execSQL(HaGreveSchema.Strikes.DDL_DROP_TABLE);
        db.execSQL(HaGreveSchema.Companies.DDL_DROP_TABLE);
    }

}
