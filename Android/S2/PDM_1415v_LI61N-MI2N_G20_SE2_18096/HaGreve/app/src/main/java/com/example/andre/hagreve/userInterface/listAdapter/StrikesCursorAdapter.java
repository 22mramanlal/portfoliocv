package com.example.andre.hagreve.userInterface.listAdapter;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.dataAcess.provider.HaGreveContract;
import com.example.andre.hagreve.dataAcess.provider.HaGreveSchema;
import com.example.andre.hagreve.userInterface.HaGreveApp;
import com.example.andre.hagreve.userInterface.HaGreveUtils;

public class StrikesCursorAdapter extends CursorAdapter {


    private LayoutInflater layoutInflater;
    private Context context;

    private int layout = R.layout.strikes_list;

    public StrikesCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewModelStrikes strikesView = new ViewModelStrikes(view);

        int companyID = cursor.getInt(cursor.getColumnIndex(HaGreveSchema.Strikes.COL_COMPANYID));

        strikesView.description.setText(cursor.getString(cursor.getColumnIndex(HaGreveSchema.Strikes.COL_DESCRIPTION)));
        strikesView.startDate.setText(HaGreveUtils.stringToStringRepresentation(cursor.getString(cursor.getColumnIndex(HaGreveSchema.Strikes.COL_STARTDATE))));

        final ContentResolver resolver = context.getContentResolver();
        Cursor cCompany = resolver.query(HaGreveContract.Companies.CONTENT_URI, null, BaseColumns._ID + "= ?",
                new String[]{Integer.toString(companyID)}, null);
        cCompany.moveToNext();

        strikesView.company.setText(cCompany.getString(cCompany.getColumnIndex(HaGreveSchema.Companies.COL_NAME)));
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return layoutInflater.inflate(layout, parent, false);
    }

}

    class ViewModelStrikes{

        public final TextView company;
        public final TextView description;
        public final TextView startDate;

        public ViewModelStrikes(View view){
            company = (TextView) view.findViewById(R.id.strikes_list_company_tv_id);
            description = (TextView) view.findViewById(R.id.strikes_list_description_tv_id);
            startDate = (TextView) view.findViewById(R.id.strikes_details_start_date_list_tv);
        }

    }