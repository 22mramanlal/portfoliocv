package com.example.andre.hagreve.dataAcess.Deserializer;

import com.example.andre.hagreve.logic.Company;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CompaniesDeserializer {

    public List<Company> deserializeElements (String str) {
        JSONArray getListInformation = null;

        try {
            getListInformation = new JSONArray(str);
        } catch (JSONException e){
            throw new RuntimeException(e);
        }
        List <Company> companies;
        companies = new ArrayList<Company>();
        Company company = null;

        for (int i = 0 ; i < getListInformation.length()  ; ++i){
            try {
                JSONObject info = getListInformation.getJSONObject(i);
                company = new Company(info.getString("name"),Integer.parseInt(info.getString("id")));
                companies.add(company);
            } catch (JSONException e){
                throw new RuntimeException(e);
            }
        }
        return companies;
    }
}
