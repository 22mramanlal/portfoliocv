package com.example.andre.hagreve.userInterface.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.userInterface.HaGreveUtils;

public class DefinitionsActivity extends Activity {

    public String hostPortText;

    private CheckBox onlineCheckbox;
    private CheckBox offlineCheckbox;
    private EditText hostandPortEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_definitions);

        loadComponents();
        setComponents(savedInstanceState);
    }

    public void loadComponents() {
        onlineCheckbox = (CheckBox) findViewById(R.id.definitions_online_checkBox);
        offlineCheckbox = (CheckBox) findViewById(R.id.definitions_offline_checkBox);
        hostandPortEditText = (EditText) findViewById(R.id.definitions_hostandPort_EditText);
    }

    public void setComponents(Bundle savedInstanceState){
        SharedPreferences sp = getSharedPreferences(HaGreveUtils.SHAREDPREFERENCE_FILE, MODE_PRIVATE);
        boolean isOnline = sp.getBoolean("isOnline", true);

        if(isOnline)
            onlineCheckbox.setChecked(true);
        else
            offlineCheckbox.setChecked(true);

        if(savedInstanceState != null) {
            hostPortText = savedInstanceState.getString("hostPortText");
            hostandPortEditText.setText(hostPortText);
        }else {
            hostPortText = sp.getString("hostPortText", "");
            hostandPortEditText.setText(sp.getString("hostPortText", ""));
        }


    }

    public void checkOnline(View v) {
        CheckBox c = (CheckBox)v;
        if(c.isChecked())
            offlineCheckbox.setChecked(false);
    }

    public void checkOffline(View v) {
        CheckBox c = (CheckBox)v;
        if(c.isChecked())
            onlineCheckbox.setChecked(false);
    }


    public void clickSave(View v){
        SharedPreferences sp = getSharedPreferences(HaGreveUtils.SHAREDPREFERENCE_FILE, MODE_PRIVATE);

        if(onlineCheckbox.isChecked()){
            sp.edit().putBoolean("isOnline",true).commit();
            HaGreveUtils.API_URL_COMPANIES = HaGreveUtils.API_URL_COMPANIES_ONLINE;
            HaGreveUtils.API_URL_STRIKES = HaGreveUtils.API_URL_STRIKES_ONLINE;
        }
        else {
            sp.edit().putBoolean("isOnline",false).commit();
            hostPortText = hostandPortEditText.getText().toString();
            HaGreveUtils.API_URL_COMPANIES = HaGreveUtils.API_URL_COMPANIES_OFFLINE;
            HaGreveUtils.API_URL_COMPANIES = String.format(HaGreveUtils.API_URL_COMPANIES, hostPortText);
            HaGreveUtils.API_URL_STRIKES = HaGreveUtils.API_URL_STRIKES_OFFLINE;
            HaGreveUtils.API_URL_STRIKES = String.format(HaGreveUtils.API_URL_STRIKES, hostPortText);
        }
        this.setResult(RESULT_OK);

        sp.edit().putString("hostPortText",hostPortText).commit();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("hostPortText",hostPortText);
        super.onSaveInstanceState(savedInstanceState);
    }


}