package com.example.andre.hagreve.logic;

import java.util.Calendar;
import java.util.Date;


public class Strike {

    private int id;
    private String description;
    private Date start_date;
    private Date end_date;
    private String source_link;
    private boolean all_day;
    private boolean canceled;
    private Submitter submitter;
    private Company company;

    public Strike(int id, String desc, Date start_date, Date end_date, String link, boolean all_day, boolean canceled, Submitter sub, Company c){
        this.id = id;
        this.description = desc;
        this.start_date = start_date;
        this.end_date = end_date;
        this.source_link = link;
        this.all_day = all_day;
        this.canceled = canceled;
        this.submitter = sub;
        this.company = c;
    }

    public int getId() { return  id;}
    public String getDescription(){
        return description;
    }
    public Date getStart_date(){
        return start_date;
    }
    public Date getEnd_date(){
        return end_date;
    }
    public String getSource_link(){
        return source_link;
    }
    public boolean isAll_day(){
        return all_day;
    }
    public boolean isCanceled(){
        return canceled;
    }
    public Submitter getSubmitter(){
        return submitter;
    }
    public Company getCompany(){
        return company;
    }

    public String getStartDateDB(){
        Calendar c = Calendar.getInstance();
        c.setTime(start_date);

        String year =  c.get(Calendar.YEAR) +"";
        String month = c.get(Calendar.MONTH)+1 +"";
        String day = c.get(Calendar.DAY_OF_MONTH)+"";

        if(month.length() == 1)
            month = "0" + month.charAt(0);
        if(day.length() == 1)
            day = "0" + day.charAt(0);

        return year+month+day;
    }

    public String getEndDateDB(){
        Calendar c = Calendar.getInstance();
        c.setTime(end_date);

        String year =  c.get(Calendar.YEAR) +"";
        String month = c.get(Calendar.MONTH)+1 +"";
        String day = c.get(Calendar.DAY_OF_MONTH)+"";

        if(month.length() == 1)
            month = "0" + month.charAt(0);
        if(day.length() == 1)
            day = "0" + day.charAt(0);

        return year+month+day;
    }
}
