package com.example.andre.hagreve.dataAcess.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.BatteryManager;
import android.support.v4.app.NotificationCompat;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.dataAcess.provider.HaGreveContract;
import com.example.andre.hagreve.dataAcess.provider.HaGreveSchema;
import com.example.andre.hagreve.logic.Strike;
import com.example.andre.hagreve.userInterface.HaGreveUtils;
import com.example.andre.hagreve.userInterface.activity.StrikeDetailsActivity;


public class DailyAlarmReceiver extends BroadcastReceiver {

    @Override
    public  void onReceive(Context context, Intent intent){

        if( HaGreveUtils.checkWifiAndBattery(context)) {
            checkStrikeForToday(context);
            checkStrikeForWeek(context);
        }
    }

    private void checkStrikeForToday(Context context){
        final ContentResolver resolver = context.getContentResolver();
        Cursor c;

        String currentDay = HaGreveUtils.currentDate();

        c = resolver.query(HaGreveContract.Strikes.CONTENT_URI, null, HaGreveSchema.Strikes.COL_STARTDATE + "= ?",
                new String [] {currentDay}, null);

        if(c.getCount() != 0) {

            c.moveToNext();
            Strike s = HaGreveUtils.cursorToStrike(context, c.getInt(c.getColumnIndex(HaGreveSchema.Strikes.COL_ID)));
            notifyStrike(context, s, "Strike Today");

        }
    }

    private void checkStrikeForWeek(Context context){
        final ContentResolver resolver = context.getContentResolver();
        Cursor c;

        String [] allDays = calculateAllDays(HaGreveUtils.numberOfDays);
        if(allDays.length == 0)
            return;

        c = resolver.query(HaGreveContract.Strikes.CONTENT_URI, null, makeSelection(HaGreveSchema.Strikes.COL_STARTDATE + "= ?",allDays.length ),
                allDays, null);

        if(c.getCount() != 0) {
            while (c.moveToNext()) {
                Strike s = HaGreveUtils.cursorToStrike(context, c.getInt(c.getColumnIndex(HaGreveSchema.Strikes.COL_ID)));
                notifyStrike(context, s, "Strike under next " + HaGreveUtils.numberOfDays + " days");
            }
        }
    }

    private String makeSelection(String str, int size){
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i <size ; ++i){
            sb.append(str);
            if(i+1 < size)
                sb.append(" OR ");
        }
        return sb.toString();
    }

    private String[] calculateAllDays(int numberOfDays){
        String[] str = new String[numberOfDays];
        for(int i = 0; i<numberOfDays; ++i){
            str[i] = HaGreveUtils.getCurrentDatePlus(i + 1);
        }
        return str;
    }

    private void notifyStrike(Context context, Strike s, String txt){

        int id = HaGreveUtils.randomId();

        Intent notifIntent = new Intent(context, StrikeDetailsActivity.class);
        notifIntent.putExtra("strikesID", s.getId());

        PendingIntent pendNotifIntent =
                PendingIntent.getActivity(context, id, notifIntent, 0);

        Notification notif = new NotificationCompat.Builder(context)
                .setContentTitle(s.getCompany().getCompanyName())
                .setContentText(txt)
                .setSmallIcon(R.drawable.ic_ha_greve_1)
                .setAutoCancel(true)
                .setContentIntent(pendNotifIntent)
                .build();

        NotificationManager notifManager =
                (NotificationManager)context
                        .getSystemService(context.NOTIFICATION_SERVICE);

        notifManager.notify(id, notif);
    }


}
