package com.example.andre.hagreve.userInterface.activity;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.CursorAdapter;
import android.widget.ListView;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.dataAcess.HaGreveProviderService;
import com.example.andre.hagreve.dataAcess.provider.HaGreveContract;
import com.example.andre.hagreve.userInterface.listAdapter.CompaniesCursorAdapter;

public class FilterActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView filterListView;
    private CursorAdapter adapter;
    private static final int COMPANIES_LOADER = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        loadComponents();
        loadCompanies();

        getLoaderManager().initLoader(COMPANIES_LOADER, null, this);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case COMPANIES_LOADER:
                return new CursorLoader(
                        this,
                        HaGreveContract.Companies.CONTENT_URI,
                        HaGreveContract.Companies.PROJECTION_ALL,
                        null,
                        null,
                        null
                );
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if(adapter == null) {
            loadCompanies();
            adapter = new CompaniesCursorAdapter(FilterActivity.this, cursor, 0);
            filterListView.setAdapter(adapter);
        }
        else {
            adapter.swapCursor(cursor);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    private void loadComponents(){
        filterListView = (ListView) findViewById(R.id.filter_listView);
    }

    private void loadCompanies(){
        HaGreveProviderService.startActionCompanies(this);
    }

}
