package com.example.andre.hagreve.userInterface;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.BatteryManager;
import android.provider.BaseColumns;

import com.example.andre.hagreve.dataAcess.provider.HaGreveContract;
import com.example.andre.hagreve.dataAcess.provider.HaGreveSchema;
import com.example.andre.hagreve.logic.Company;
import com.example.andre.hagreve.logic.Strike;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class HaGreveUtils {

    public static List<Integer> companyIdsFiltered = new ArrayList<Integer>();

    public static  String API_URL_STRIKES = "http://hagreve.com/api/v2/allstrikes";;
    public static  String API_URL_COMPANIES = "http://hagreve.com/api/v2/companies";

    public static final String API_URL_STRIKES_ONLINE = "http://hagreve.com/api/v2/allstrikes";
    public static final String API_URL_COMPANIES_ONLINE = "http://hagreve.com/api/v2/companies";

    public static final String API_URL_STRIKES_OFFLINE = "http://%s/api/v2/allstrikes";
    public static final String API_URL_COMPANIES_OFFLINE = "http://%s/api/v2/companies";

    public static final String SHAREDPREFERENCE_FILE = "SharedPreference";

    public static int numberOfDays = 0;

    public static String [] idsNotRepresent = {};
    public static String queryNotRepresent = "";

    public static String currentDate(){
        Calendar c = Calendar.getInstance();

        String year =  c.get(Calendar.YEAR) +"";
        String month = c.get(Calendar.MONTH)+1 +"";
        String day = c.get(Calendar.DAY_OF_MONTH)+"";

        if(month.length() == 1)
            month = "0" + month.charAt(0);
        if(day.length() == 1)
            day = "0" + day.charAt(0);

        return year+"-"+month+"-"+day;
    }

    public static String currentDateDB(){
        Calendar c = Calendar.getInstance();

        String year =  c.get(Calendar.YEAR) +"";
        String month = c.get(Calendar.MONTH)+1 +"";
        String day = c.get(Calendar.DAY_OF_MONTH)+"";

        if(month.length() == 1)
            month = "0" + month.charAt(0);
        if(day.length() == 1)
            day = "0" + day.charAt(0);

        return year+month+day;
    }

    public static String afterYearsDateDB(){
        Calendar c = Calendar.getInstance();

        String year =  c.get(Calendar.YEAR) + 10 +"";
        String month = c.get(Calendar.MONTH)+1 +"";
        String day = c.get(Calendar.DAY_OF_MONTH)+"";

        if(month.length() == 1)
            month = "0" + month.charAt(0);
        if(day.length() == 1)
            day = "0" + day.charAt(0);

        return year+month+day;
    }


    public static String stringToStringRepresentation(String date){

        StringBuilder sb = new StringBuilder();

        sb.append(date.charAt(0)).append(date.charAt(1)).append(date.charAt(2)).append(date.charAt(3));
        sb.append("-").append(date.charAt(4)).append(date.charAt(5));
        sb.append("-").append(date.charAt(6)).append(date.charAt(7));

        return  sb.toString();
    }

    public static Strike cursorToStrike(Context context, int strikeId){

        final ContentResolver resolver = context.getContentResolver();
        Cursor cStrike = resolver.query(HaGreveContract.Strikes.CONTENT_URI, null, BaseColumns._ID + "= ?",
                new String[]{Integer.toString(strikeId)}, null);

        cStrike.moveToNext();
        int companyID = cStrike.getInt(cStrike.getColumnIndex(HaGreveSchema.Strikes.COL_COMPANYID));

        Cursor cCompany = resolver.query(HaGreveContract.Companies.CONTENT_URI, null,BaseColumns._ID + "= ?",
                new String[]{Integer.toString(companyID)}, null);
        cCompany.moveToNext();

        int id = cStrike.getInt( cStrike.getColumnIndex(HaGreveSchema.Strikes.COL_ID));
        String description =  cStrike.getString( cStrike.getColumnIndex(HaGreveSchema.Strikes.COL_DESCRIPTION));
        Date startDate = stringToDate(stringToStringRepresentation(cStrike.getString(cStrike.getColumnIndex(HaGreveSchema.Strikes.COL_STARTDATE))));
        Date endDate = stringToDate(stringToStringRepresentation(cStrike.getString(cStrike.getColumnIndex(HaGreveSchema.Strikes.COL_ENDDATE))));
        String sourceLink = cStrike.getString( cStrike.getColumnIndex(HaGreveSchema.Strikes.COL_SOURCELINK));
        String allDayStr = cStrike.getString(cStrike.getColumnIndex(HaGreveSchema.Strikes.COL_ALLDAY));
        Boolean allDay = allDayStr.equals("1") ? true : false;
        String canceledSTR = cStrike.getString(cStrike.getColumnIndex(HaGreveSchema.Strikes.COL_CANCELED));
        Boolean canceled = canceledSTR.equals("1") ? true : false;

        return  new Strike(id, description, startDate, endDate, sourceLink, allDay, canceled,
                null,
                new Company(cCompany.getString(cCompany.getColumnIndex(HaGreveSchema.Companies.COL_NAME)),companyID ));
    }

    private static Date stringToDate(String date){
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return  format.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String dateToString(Date date){

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        String year = c.get(Calendar.YEAR) +"";
        String month = c.get(Calendar.MONTH)+1+"";
        String day = c.get(Calendar.DAY_OF_MONTH)+"";

        if(month.length() == 1)
            month = "0" + month.charAt(0);
        if(day.length() == 1)
            day = "0" + day.charAt(0);

        return year + "-" + month + "-" + day;
    }

    public static String getCurrentDatePlus(int numberOfDays){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE,numberOfDays);

        String year =  c.get(Calendar.YEAR)+"";
        String month = c.get(Calendar.MONTH)+1 +"";
        String day = c.get(Calendar.DAY_OF_MONTH)+"";
        return year+"-"+month+"-"+day;
    }

    public static int randomId(){
        Random gerador = new Random();

        return gerador.nextInt(1000);
    }

    public static boolean checkWifiAndBattery(Context context){

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

        if( cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting() && level >= 20)
            return true;

        return false;
    }

}
