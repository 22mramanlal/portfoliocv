package com.example.andre.hagreve.userInterface.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.userInterface.HaGreveApp;
import com.example.andre.hagreve.userInterface.HaGreveUtils;

public class NotificationActivity extends Activity {

    private Button saveButton;
    private EditText numberOfDays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        loadComponents();
    }

    public void loadComponents(){
        saveButton = (Button) findViewById(R.id.notification_button);
        numberOfDays = (EditText) findViewById(R.id.notification_editText);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HaGreveUtils.numberOfDays = Integer.parseInt(numberOfDays.getText().toString());
            }
        });
        numberOfDays.setText(Integer.toString(HaGreveUtils.numberOfDays));
    }

}
