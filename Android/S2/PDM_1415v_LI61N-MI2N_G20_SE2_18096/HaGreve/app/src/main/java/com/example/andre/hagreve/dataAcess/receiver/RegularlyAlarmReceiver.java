package com.example.andre.hagreve.dataAcess.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.BatteryManager;
import android.util.Log;

import com.example.andre.hagreve.dataAcess.HaGreveProviderService;
import com.example.andre.hagreve.dataAcess.provider.HaGreveProvider;
import com.example.andre.hagreve.userInterface.HaGreveUtils;

public class RegularlyAlarmReceiver extends BroadcastReceiver {

    @Override
    public  void onReceive(Context context, Intent intent){

        if(HaGreveUtils.checkWifiAndBattery(context)) {
            HaGreveProviderService.startActionCompanies(context);
            HaGreveProviderService.startActionStrikes(context);
        }
    }

}
