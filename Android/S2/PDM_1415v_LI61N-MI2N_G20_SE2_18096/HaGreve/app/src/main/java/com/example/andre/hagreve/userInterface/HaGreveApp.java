package com.example.andre.hagreve.userInterface;


import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import com.example.andre.hagreve.dataAcess.receiver.DailyAlarmReceiver;
import com.example.andre.hagreve.dataAcess.receiver.RegularlyAlarmReceiver;

public class HaGreveApp extends Application {

    @Override
    public void onCreate(){
        super.onCreate();

        createAlarmRegularly();
        createAlarmDaily();
    }

    private void createAlarmRegularly(){
        Intent alarmIntent = new Intent(this, RegularlyAlarmReceiver.class);

        if(PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_NO_CREATE) == null){

            PendingIntent pendingAlarmIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

            alarmManager.setRepeating(
                    AlarmManager.RTC_WAKEUP ,
                    System.currentTimeMillis() + 2 *1000,
                    AlarmManager.INTERVAL_HOUR,
                    pendingAlarmIntent
            );
        }
    }

    private void createAlarmDaily(){
        Intent alarmIntent = new Intent(this, DailyAlarmReceiver.class);

        if(PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_NO_CREATE) == null){

            PendingIntent pendingAlarmIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

            alarmManager.setRepeating(
                    AlarmManager.RTC_WAKEUP ,
                    System.currentTimeMillis() + 40 *1000,
                    AlarmManager.INTERVAL_DAY,
                    pendingAlarmIntent
            );
        }
    }

}
