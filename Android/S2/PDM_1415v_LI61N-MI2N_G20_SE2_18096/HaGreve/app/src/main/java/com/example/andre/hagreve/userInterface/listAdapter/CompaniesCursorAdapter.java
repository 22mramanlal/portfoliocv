package com.example.andre.hagreve.userInterface.listAdapter;


import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.dataAcess.provider.HaGreveSchema;
import com.example.andre.hagreve.userInterface.HaGreveUtils;

public class CompaniesCursorAdapter extends CursorAdapter {

    private LayoutInflater layoutInflater;
    private Context context;

    private int layout = R.layout.filter_list;

    public CompaniesCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewModelCompanies companiesView = new ViewModelCompanies(view);

        companiesView.company.setText(cursor.getString(cursor.getColumnIndex(HaGreveSchema.Companies.COL_NAME)));

        companiesView.company.setChecked(false);

        final int companyID = cursor.getInt(cursor.getColumnIndex(HaGreveSchema.Companies.COL_ID));

        companiesView.company.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox c = (CheckBox) v;
                c.getText();

                if (c.isChecked())
                    HaGreveUtils.companyIdsFiltered.add(companyID);
                else {
                    for (int i = 0; i < HaGreveUtils.companyIdsFiltered.size(); i++)
                        if (HaGreveUtils.companyIdsFiltered.get(i) == companyID) {
                            HaGreveUtils.companyIdsFiltered.remove(i);
                            break;
                        }
                }
            }
        });

        if (HaGreveUtils.companyIdsFiltered.contains(companyID))
            companiesView.company.setChecked(true);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return layoutInflater.inflate(layout, parent, false);
    }

}

class ViewModelCompanies {

    public final CheckBox company;

    public ViewModelCompanies(View view) {
        company = (CheckBox) view.findViewById(R.id.filter_company_checkBox);
    }
}

