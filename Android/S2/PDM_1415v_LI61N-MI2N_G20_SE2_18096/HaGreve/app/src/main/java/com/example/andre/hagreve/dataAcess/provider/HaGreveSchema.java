package com.example.andre.hagreve.dataAcess.provider;


import android.provider.BaseColumns;

public interface HaGreveSchema {

    String DB_NAME = "hagreve.db";
    int DB_VERSION = 1;

    static interface Companies{

        String TBL_NAME = "companies";

        String COL_ID  = BaseColumns._ID;
        String COL_NAME  = "name";

        String DDL_CREATE_TABLE =
                "CREATE TABLE " + TBL_NAME + "(" +
                        COL_ID    + " INTEGER PRIMARY KEY, " +
                        COL_NAME  + " TEXT )";

        String DDL_DROP_TABLE =
                "DROP TABLE IF EXISTS " + TBL_NAME;

    }

    static interface Strikes{

        String TBL_NAME = "strikes";

        String COL_ID  = BaseColumns._ID;
        String COL_DESCRIPTION  = "description";
        String COL_STARTDATE  = "startdate";
        String COL_ENDDATE  = "enddate";
        String COL_SOURCELINK  = "sourcelink";
        String COL_ALLDAY  = "allday";
        String COL_CANCELED = "canceled";
        String COL_COMPANYID = "companyid";

        String DDL_CREATE_TABLE =
                "CREATE TABLE " + TBL_NAME + "(" +
                        COL_ID    + " INTEGER PRIMARY KEY, " +
                        COL_DESCRIPTION  + " TEXT, " +
                        COL_STARTDATE + " INTEGER, " +
                        COL_ENDDATE + " INTEGER, " +
                        COL_SOURCELINK + " TEXT, " +
                        COL_ALLDAY + " BOOLEAN, " +
                        COL_CANCELED + " BOOLEAN, " +
                        COL_COMPANYID + " INTEGER " +
                         ")";

        String DDL_DROP_TABLE =
                "DROP TABLE IF EXISTS " + TBL_NAME;

    }
}
