package com.example.andre.hagreve.dataAcess.provider;


import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public interface HaGreveContract {

    String AUTHORITY = "com.example.andre.hagreve.dataAcess.provider";

    Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    String SELECTION_BY_ID = BaseColumns._ID + " = ? ";

    String MEDIA_BASE_SUBTYPE = "/vnd.hagreve.";

    public interface Companies extends BaseColumns {
        String RESOURCE = "companies";

        Uri CONTENT_URI =
                Uri.withAppendedPath(
                        HaGreveContract.CONTENT_URI,
                        RESOURCE);

        String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE +
                        MEDIA_BASE_SUBTYPE + RESOURCE;

        String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE +
                        MEDIA_BASE_SUBTYPE + RESOURCE;

        String NAME  = "name";

        String[] PROJECTION_ALL = { _ID, NAME, };

        String DEFAULT_SORT_ORDER =  _ID + " ASC";
    }

    public interface Strikes extends BaseColumns {
        String RESOURCE = "strikes";

        Uri CONTENT_URI =
                Uri.withAppendedPath(
                        HaGreveContract.CONTENT_URI,
                        RESOURCE);

        String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE +
                        MEDIA_BASE_SUBTYPE + RESOURCE;

        String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE +
                        MEDIA_BASE_SUBTYPE + RESOURCE;

        String STARTDATE  = "startdate";
        String ENDDATE  = "enddate";
        String DESCRIPTION = "description";
        String ALLDAY = "allday";
        String CANCELED = "canceled";
        String SOURCELINK = "sourcelink";
        String COMPANYID = "companyID";

        String[] PROJECTION_ALL = { _ID, DESCRIPTION, STARTDATE, ENDDATE, SOURCELINK, ALLDAY, CANCELED, COMPANYID };

        String DEFAULT_SORT_ORDER =  _ID + " ASC";
    }

}
