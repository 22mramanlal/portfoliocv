package com.example.andre.hagreve.dataAcess;


import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.support.v4.app.NotificationCompat;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.dataAcess.Deserializer.CompaniesDeserializer;
import com.example.andre.hagreve.dataAcess.Deserializer.StrikesDeserializer;
import com.example.andre.hagreve.dataAcess.provider.HaGreveContract;
import com.example.andre.hagreve.dataAcess.provider.HaGreveSchema;
import com.example.andre.hagreve.logic.Company;
import com.example.andre.hagreve.logic.Strike;
import com.example.andre.hagreve.userInterface.HaGreveUtils;
import com.example.andre.hagreve.userInterface.activity.StrikeDetailsActivity;

import java.util.List;

public class HaGreveProviderService extends IntentService {

    private static final String ACTION_GET_STRIKES = "com.example.andre.hagreve.dataAcess.action.STRIKES";
    private static final String ACTION_GET_COMPANIES = "com.example.andre.hagreve.dataAcess.action.COMPANIES";

    public HaGreveProviderService() {
        super("HaGreveProviderService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_STRIKES.equals(action)) {
                handleActionStrikes();
            } else if (ACTION_GET_COMPANIES.equals(action)) {
                handleActionCompanies();
            }
        }
    }

    private void handleActionStrikes(){
       final ContentResolver resolver = getContentResolver();
       String content = HttpRequest.doHttpRequest(HaGreveUtils.API_URL_STRIKES);
       StrikesDeserializer d = new StrikesDeserializer();
       List<Strike> list = d.deserializeElements(content);

        ContentValues values;
        Cursor c;
        for(Strike s : list) {

            c = resolver.query(HaGreveContract.Strikes.CONTENT_URI, null, BaseColumns._ID + "= ?",
                                new String [] { Integer.toString(s.getId())}, null);

            if(c.getCount() != 0){

                c.moveToNext();
                String strc = c.getString(c.getColumnIndex(HaGreveSchema.Strikes.COL_CANCELED));
                Boolean canceled = strc.equals("1") ? true : false;
                if (canceled != s.isCanceled() ) {
                    notifyIsCanceled(s);
                    resolver.delete(HaGreveContract.Strikes.CONTENT_URI, HaGreveContract.Strikes._ID + " = ?", new String[]{Integer.toString(s.getId())});

                    values = createValuesStrikes(s.getId(), s.getDescription(), Integer.parseInt(s.getStartDateDB()), Integer.parseInt(s.getEndDateDB()),
                            s.getSource_link(), s.isAll_day(), s.isCanceled(), s.getCompany().getId());
                    resolver.insert(HaGreveContract.Strikes.CONTENT_URI, values);
                }
            }

            if(c.getCount() == 0) {
                values = createValuesStrikes(s.getId(), s.getDescription(), Integer.parseInt(s.getStartDateDB()), Integer.parseInt(s.getEndDateDB()),
                        s.getSource_link(), s.isAll_day(), s.isCanceled(), s.getCompany().getId());
                resolver.insert(HaGreveContract.Strikes.CONTENT_URI, values);
            }
        }
    }

    private  void handleActionCompanies(){
        final ContentResolver resolver = getContentResolver();
        String content = HttpRequest.doHttpRequest(HaGreveUtils.API_URL_COMPANIES);
        CompaniesDeserializer des = new CompaniesDeserializer();
        List<Company> list = des.deserializeElements(content);

        ContentValues values;
        Cursor cursor;
        for(Company c : list) {

            cursor = resolver.query(HaGreveContract.Companies.CONTENT_URI, null, BaseColumns._ID + "= ?",
                    new String [] { Integer.toString(c.getId())}, null);

            if(cursor.getCount() == 0) {
                values = createValuesCompany(c.getId(), c.getCompanyName());
                resolver.insert(HaGreveContract.Companies.CONTENT_URI, values);
            }
        }
    }

    public static void startActionStrikes(Context context) {
        Intent intent = new Intent(context, HaGreveProviderService.class);
        intent.setAction(ACTION_GET_STRIKES);
        context.startService(intent);
    }

    public static void startActionCompanies(Context context) {
        Intent intent = new Intent(context, HaGreveProviderService.class);
        intent.setAction(ACTION_GET_COMPANIES);
        context.startService(intent);
    }

    private ContentValues createValuesStrikes(int code, String description, long startDate, long endDate,
                                              String sourceLink, boolean allDay, boolean canceled,
                                              int companyId) {

        ContentValues values = new ContentValues();
        values.put(HaGreveContract.Strikes._ID, code);
        values.put(HaGreveContract.Strikes.DESCRIPTION, description);
        values.put(HaGreveContract.Strikes.STARTDATE, startDate);
        values.put(HaGreveContract.Strikes.ENDDATE, endDate);
        values.put(HaGreveContract.Strikes.SOURCELINK, sourceLink);
        values.put(HaGreveContract.Strikes.ALLDAY, allDay);
        values.put(HaGreveContract.Strikes.CANCELED, canceled);
        values.put(HaGreveContract.Strikes.COMPANYID, companyId);

        return values;
    }

    private ContentValues createValuesCompany(int id, String companyId) {

        ContentValues values = new ContentValues();
        values.put(HaGreveSchema.Companies.COL_ID, id);
        values.put(HaGreveSchema.Companies.COL_NAME, companyId);

        return values;
    }

    private void notifyIsCanceled(Strike s){
        int id = HaGreveUtils.randomId();
        Intent notifIntent = new Intent(getBaseContext(), StrikeDetailsActivity.class);
        notifIntent.putExtra("strikesID", s.getId());

        PendingIntent pendNotifIntent =
                PendingIntent.getActivity(getBaseContext(), id, notifIntent, 0);

        Notification notif = new NotificationCompat.Builder(getBaseContext())
                .setContentTitle(s.getCompany().getCompanyName())
                .setContentText("Strike Canceled")
                .setSmallIcon(R.drawable.ic_ha_greve_1)
                .setAutoCancel(true)
                .setContentIntent(pendNotifIntent)
                .build();

        NotificationManager notifManager =
                (NotificationManager)getBaseContext()
                        .getSystemService(getBaseContext().NOTIFICATION_SERVICE);

        notifManager.notify(id, notif);
    }
}
