package com.example.andre.hagreve.dataAcess.provider;


import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class HaGreveProvider extends ContentProvider{


    private static  final int COMPANIES_LST = 10;
    private static  final int COMPANIES_OBJ = 20;
    private static  final int STRIKES_LST = 30;
    private static  final int STRIKES_OBJ = 40;

    private static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

        URI_MATCHER.addURI(
                HaGreveContract.AUTHORITY,
                HaGreveContract.Companies.RESOURCE,
                COMPANIES_LST);

        URI_MATCHER.addURI(
                HaGreveContract.AUTHORITY,
                HaGreveContract.Companies.RESOURCE + "/#",
                COMPANIES_OBJ);

        URI_MATCHER.addURI(
                HaGreveContract.AUTHORITY,
                HaGreveContract.Strikes.RESOURCE,
                STRIKES_LST);

        URI_MATCHER.addURI(
                HaGreveContract.AUTHORITY,
                HaGreveContract.Strikes.RESOURCE + "/#",
                STRIKES_OBJ);
    }

    private HaGreveOpenHelper dbHelper = null;

    @Override
    public boolean onCreate() {
        dbHelper = new HaGreveOpenHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case COMPANIES_LST:
                return HaGreveContract.Companies.CONTENT_TYPE;
            case COMPANIES_OBJ:
                return HaGreveContract.Companies.CONTENT_ITEM_TYPE;
            case STRIKES_LST:
                return HaGreveContract.Strikes.CONTENT_TYPE;
            case STRIKES_OBJ:
                return HaGreveContract.Strikes.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder qbuilder = new SQLiteQueryBuilder();
        switch (URI_MATCHER.match(uri)) {
            case COMPANIES_LST:
                qbuilder.setTables(HaGreveSchema.Companies.TBL_NAME);
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = HaGreveContract.Companies.DEFAULT_SORT_ORDER;
                }
                break;
            case COMPANIES_OBJ:
                qbuilder.setTables(HaGreveSchema.Companies.TBL_NAME);
                qbuilder.appendWhere(HaGreveSchema.Companies.COL_ID + "=" + uri.getLastPathSegment());
                break;
            case STRIKES_LST:
                qbuilder.setTables(HaGreveSchema.Strikes.TBL_NAME);
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = HaGreveContract.Strikes.DEFAULT_SORT_ORDER;
                }
                break;
            case STRIKES_OBJ:
                qbuilder.setTables(HaGreveSchema.Strikes.TBL_NAME);
                qbuilder.appendWhere(HaGreveSchema.Strikes.COL_ID + "=" + uri.getLastPathSegment());
                break;
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = qbuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String table;
        switch (URI_MATCHER.match(uri)) {
            case COMPANIES_LST:
                table = HaGreveSchema.Companies.TBL_NAME;
                break;
            case STRIKES_LST:
                table = HaGreveSchema.Strikes.TBL_NAME;
                break;
            default:
                throw new IllegalArgumentException();
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long newId = db.insert(table, null, values);

        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, newId);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table;
        switch (URI_MATCHER.match(uri)) {
            case COMPANIES_LST:
                table = HaGreveSchema.Companies.TBL_NAME;
                if (selection != null) {
                    throw new IllegalArgumentException("selection not supported");
                }
                break;
            case STRIKES_LST:
                table = HaGreveSchema.Strikes.TBL_NAME;
                break;
            default:
                throw new UnsupportedOperationException();
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int ndel = db.delete(table, selection, selectionArgs);

        getContext().getContentResolver().notifyChange(uri, null);
        return ndel;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }
}
