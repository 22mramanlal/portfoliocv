package grupo20.isel.pt.anniversary.dataAcess;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import grupo20.isel.pt.anniversary.logic.Contact;

public class ContactsRequest extends AsyncTask<Context, Void, List<Contact>> {

    public List<Contact> doInBackground(Context... params){

        List<Contact> list = new ArrayList<Contact>();

        String str [] =  new String[] {
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Event.CONTACT_ID,
                ContactsContract.CommonDataKinds.Event.START_DATE
        };


        Cursor cursor = params[0].getContentResolver().query(
                ContactsContract.Data.CONTENT_URI,
                str,
                ContactsContract.Data.MIMETYPE + "= ? AND (" +
                ContactsContract.CommonDataKinds.Event.TYPE + "=" +
                ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY + ")",
                new String[]{ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE},
                null
        );

        if(cursor != null && cursor.moveToFirst()) {
            int idIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Event.CONTACT_ID);
            int nameIdx = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            int startDateIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Event.START_DATE);
            int cursor_size = cursor.getCount();
            while (cursor != null && cursor_size-->0) {

                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                Date date;
                try {
                   date = s.parse(cursor.getString(startDateIdx));
                }catch(ParseException e){
                    throw  new RuntimeException(e);
                }

                Contact c = new Contact(cursor.getInt(idIdx), cursor.getString(nameIdx), date);
                list.add(c);
                cursor.moveToNext();
            }
        }

          return list;
    }

}
