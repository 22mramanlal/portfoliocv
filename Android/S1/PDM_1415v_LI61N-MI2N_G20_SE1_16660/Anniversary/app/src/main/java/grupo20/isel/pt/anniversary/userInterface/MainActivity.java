package grupo20.isel.pt.anniversary.userInterface;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import grupo20.isel.pt.anniversary.R;
import grupo20.isel.pt.anniversary.Utils.Utility;
import grupo20.isel.pt.anniversary.dataAcess.ContactsRequest;
import grupo20.isel.pt.anniversary.logic.Contact;
import grupo20.isel.pt.anniversary.userInterface.listAdapter.AnniversaryListViewAdapter;


public class MainActivity extends ActionBarActivity {

    private ListView contactsListView;
    private AnniversaryListViewAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadComponents();
        loadSettings();
        loadContacts();
    }


    public void loadComponents(){
        contactsListView = (ListView) findViewById(R.id.contacts_listView);
        customAdapter = new AnniversaryListViewAdapter(this, R.layout.anniversary_list, AnniversaryApp.contactsListPresent);
        contactsListView.setAdapter(customAdapter);
    }

    public void loadSettings(){
        SharedPreferences sp = getPreferences(MODE_PRIVATE);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String before = sp.getString("dateBefore",null);
        String after = sp.getString("dateAfter",null);

        if(before != null) {
            try {
                Utility.d_before = format.parse(before);
                Utility.d_after = format.parse(after);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }else {
            Utility.d_before = new Date();
            Utility.d_after = new Date();
        }
    }

    public void loadContacts(){
        AsyncTask<Context, Void, List<Contact>> task = new ContactsRequest(){

            @Override
            protected void onPostExecute(List<Contact> list){
                AnniversaryApp.contactsList.clear();
                for(Contact c : list)
                    AnniversaryApp.contactsList.add(c);
                copyContactsToRepresent();
            };
        };
        task.execute(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                Intent i = new Intent(Intent.ACTION_INSERT_OR_EDIT);
                i.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                startActivity(i);
                break;
            case R.id.action_change_time:
                Intent in = new Intent(this, FilterActivity.class);
                startActivityForResult(in, 0);
                break;
            case R.id.action_refresh:
                loadContacts();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 0 && resultCode == RESULT_OK)
            copyContactsToRepresent();
    }

    public void copyContactsToRepresent(){

        AnniversaryApp.contactsListPresent.clear();
        if(Utility.d_before != null) {
            for (int i = 0; i < AnniversaryApp.contactsList.size(); ++i) {

                Contact c = AnniversaryApp.contactsList.get(i);

                if (c.birthday.compareTo(Utility.d_before) >= 0
                        &&
                        c.birthday.compareTo(Utility.d_after) <= 0
                        )
                    AnniversaryApp.contactsListPresent.add(c);
            }
        }else{
            AnniversaryApp.contactsListPresent.addAll(AnniversaryApp.contactsList);
        }
        customAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStop(){
        Calendar c = Calendar.getInstance();
        c.setTime(Utility.d_before);
        String before = "" + c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH)+1 + "-" + c.get(Calendar.DAY_OF_MONTH);

        c.setTime(Utility.d_after);
        String after = "" + c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH)+1 + "-" + c.get(Calendar.DAY_OF_MONTH);

        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        sp.edit().putString("dateBefore",before)
                .putString("dateAfter", after)
                .commit();
        super.onStop();
    }

}
