package grupo20.isel.pt.anniversary.logic;

import java.util.Calendar;
import java.util.Date;

public class Contact {

    public final int id;
    public final String name;
    public final Date birthday;

    public Contact(int id, String name, Date birthday){
        this.id = id;
        this.name = name;
        this.birthday = birthday;
    }

    public String getSimpleDate(){
        Calendar c = Calendar.getInstance();
        c.setTime(birthday);
        String year = c.get(Calendar.YEAR) +"";
        String month = c.get(Calendar.MONTH)+1+"";
        String day = c.get(Calendar.DAY_OF_MONTH)+"";
        return year + "-" + month + "-" + day;
    }

}
