package grupo20.isel.pt.anniversary.userInterface;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import grupo20.isel.pt.anniversary.R;
import grupo20.isel.pt.anniversary.Utils.Utility;

public class FilterActivity extends Activity {

    private EditText before;
    private EditText after;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_date);

        before = (EditText)findViewById(R.id.dateAfter_filter_editText);
        after = (EditText)findViewById(R.id.dateBefore_filter_editText);
    }

    public void clickOk (View v){
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Utility.d_before = format.parse(before.getText().toString());
            Utility.d_after =  format.parse(after.getText().toString());
        }catch (ParseException e){
            throw new RuntimeException(e);
        }
        setResult(RESULT_OK);
        onBackPressed();
    }
}
