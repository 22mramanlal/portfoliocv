package grupo20.isel.pt.anniversary.userInterface.listAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import grupo20.isel.pt.anniversary.R;
import grupo20.isel.pt.anniversary.Utils.Utility;
import grupo20.isel.pt.anniversary.logic.Contact;


public class AnniversaryListViewAdapter extends ArrayAdapter<Contact> {

    private ArrayList<Contact> objects;

    public AnniversaryListViewAdapter(Context context, int resource, List<Contact> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.anniversary_list, null);
        }


        Contact c = getItem(position);

        if( c!= null) {


            TextView name = (TextView) v.findViewById(R.id.name_anniversary_list_textView);
            TextView bday = (TextView) v.findViewById(R.id.anniversary_date_list_textViewTextView);
            TextView time_left = (TextView) v.findViewById(R.id.time_left_anniversary_list_textView);


            if (name != null) {
                name.setText(c.name);
            }

            if (bday != null) {
                bday.setText(c.getSimpleDate());
            }


            if (time_left != null) {
                time_left.setText(Utility.getLeftDays(c.birthday)+"d");
            }
        }
        return v;

    }
}
