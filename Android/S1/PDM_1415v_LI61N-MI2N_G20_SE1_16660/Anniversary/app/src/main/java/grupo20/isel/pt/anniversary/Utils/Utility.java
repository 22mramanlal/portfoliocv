package grupo20.isel.pt.anniversary.Utils;

import java.util.Calendar;
import java.util.Date;

public class Utility {
    public static Date d_before;
    public static Date d_after;


    public static long getLeftDays(Date date){
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);
        long a = cal1.getTimeInMillis();

        Date d = new Date();
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d);
        long b = cal2.getTimeInMillis();

        long cc = a-b;
        long diffDays = cc / (1000 * 60 * 60 * 24);

        while(diffDays <0 )
            diffDays += 365;

        return --diffDays;
    }

}
