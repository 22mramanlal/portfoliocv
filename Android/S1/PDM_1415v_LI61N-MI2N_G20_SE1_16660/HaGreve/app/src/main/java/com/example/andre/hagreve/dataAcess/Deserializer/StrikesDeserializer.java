package com.example.andre.hagreve.dataAcess.Deserializer;

import com.example.andre.hagreve.logic.Company;
import com.example.andre.hagreve.logic.Strike;
import com.example.andre.hagreve.logic.Submitter;
import com.example.andre.hagreve.userInterface.HaGreveApp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Andre on 3/20/2015.
 */
public class StrikesDeserializer {

    public List<Strike> deserializeElements (String str) {
        JSONArray getListInformation = null;

        try {
            getListInformation = new JSONArray(str);
        } catch (JSONException e){
            throw new RuntimeException(e);
        }
        List <Strike> strikes;
        strikes = new ArrayList<Strike>();
        Strike strike = null;
        Date startDate;
        Date endDate;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        for (int i = 0 ; i < getListInformation.length()  ; ++i){
            try {
                JSONObject info = getListInformation.getJSONObject(i);
                Submitter s = new Submitter(info.getJSONObject("submitter").getString("first_name"),info.getJSONObject("submitter").getString("last_name"));
                Company c = new Company(info.getJSONObject("company").getString("name"),info.getJSONObject("company").getInt("id"));

                try {
                    startDate = format.parse(info.getString("start_date"));
                    endDate =  format.parse(info.getString("end_date"));
                }catch (ParseException e){
                    throw new RuntimeException(e);
                }

                strike = new Strike(info.getString("description"),startDate,endDate,info.getString("source_link"),info.getBoolean("all_day"), info.getBoolean("canceled"),s,c);
                strikes.add(strike);
            } catch (JSONException e){
                throw new RuntimeException(e);
            }
        }
        return strikes;
    }


}
