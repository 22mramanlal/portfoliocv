package com.example.andre.hagreve.userInterface.listAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.logic.Strike;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andre on 3/21/2015.
 */
public class StrikesListViewAdapter extends ArrayAdapter<Strike> {

    private ArrayList<Strike> objects;

    public StrikesListViewAdapter(Context context, int resource, List<Strike> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if( v == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.strikes_list, null);
        }

        Strike s = getItem(position);

        if(s != null){

            TextView company = (TextView)  v.findViewById(R.id.strikes_list_company_tv_id);
            TextView description = (TextView) v.findViewById(R.id.strikes_list_description_tv_id);
            TextView startDate = (TextView) v.findViewById(R.id.strikes_details_start_date_list_tv);

            if(company != null)
                   company.setText(s.getCompany().getCompanyName());

            if(description != null)
                description.setText(s.getDescription());

            if(startDate != null)
                startDate.setText(s.getDate());


        }
        return v;
    }

}
