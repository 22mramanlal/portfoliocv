package com.example.andre.hagreve.userInterface.listAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.logic.Company;
import com.example.andre.hagreve.userInterface.HaGreveApp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Romil on 22/03/2015.
 */
public class CompaniesListViewAdapter extends ArrayAdapter<Company> {
    private ArrayList<Company> CompanyList;

    public CompaniesListViewAdapter(Context context, int textViewResourceId, List<Company> objects) {
        super(context, textViewResourceId, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.filter_list, null);
        }

        final Company company = getItem(position);

        CheckBox cb = (CheckBox) v.findViewById(R.id.filter_company_checkBox);
        cb.setChecked(false);
        cb.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox c = (CheckBox)v;
                c.getText();

                if(c.isChecked())
                    HaGreveApp.companyIdsFiltered.add(company.getId());
                else{
                    int id = company.getId();
                    for(int i = 0; i<HaGreveApp.companyIdsFiltered.size() ;i++)
                        if(HaGreveApp.companyIdsFiltered.get(i) == id ) {
                            HaGreveApp.companyIdsFiltered.remove(i);
                            break;
                        }
                }

            }
        });

        if (cb != null) {
            cb.setText(company.getCompanyName());

            if(HaGreveApp.companyIdsFiltered.contains(company.getId()))
                cb.setChecked(true);
        }

        return v;

    }
}


