package com.example.andre.hagreve.userInterface;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.dataAcess.Deserializer.CompaniesDeserializer;
import com.example.andre.hagreve.dataAcess.HttpRequest;
import com.example.andre.hagreve.logic.Company;
import com.example.andre.hagreve.userInterface.listAdapter.CompaniesListViewAdapter;

import java.util.List;

public class FilterActivity extends Activity {

    private ListView filterListView;
    private CompaniesListViewAdapter customAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        loadComponents();
        loadCompanies();

    }

    private void loadComponents(){
        filterListView = (ListView) findViewById(R.id.filter_listView);
        customAdapter = new CompaniesListViewAdapter(this, R.layout.filter_list, HaGreveApp.companyNames);
        filterListView.setAdapter(customAdapter);

    }

    private void loadCompanies(){

        if(HaGreveApp.companyNames.size()==0) {
            AsyncTask<String, Void, String> task = new HttpRequest() {

                @Override
                protected void onPostExecute(String str) {
                    CompaniesDeserializer d = new CompaniesDeserializer();
                    List<Company> list = d.deserializeElements(str);
                    HaGreveApp.companyNames.clear();

                    for (Company c : list)
                        HaGreveApp.companyNames.add(c);

                    customAdapter.notifyDataSetChanged();

                };
            };
            task.execute(HaGreveApp.API_URL_COMPANIES);
        }
    }

}
