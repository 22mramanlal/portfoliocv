package com.example.andre.hagreve.userInterface;

import com.example.andre.hagreve.logic.Company;
import com.example.andre.hagreve.logic.Strike;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andre on 3/21/2015.
 */
public class HaGreveApp {

    public static List<Strike> allStrikes = new ArrayList<Strike>();
    public static List<Strike> presentedStrikes = new ArrayList<Strike>();

    public static List<Company> companyNames = new ArrayList<Company>();
    public static List<Integer> companyIdsFiltered = new ArrayList<Integer>();

    public static  String API_URL_STRIKES = "http://hagreve.com/api/v2/allstrikes";;
    public static  String API_URL_COMPANIES = "http://hagreve.com/api/v2/companies";

    public static final String API_URL_STRIKES_ONLINE = "http://hagreve.com/api/v2/allstrikes";
    public static final String API_URL_COMPANIES_ONLINE = "http://hagreve.com/api/v2/companies";

    public static final String API_URL_STRIKES_OFFLINE = "http://%s/api/v2/allstrikes";
    public static final String API_URL_COMPANIES_OFFLINE = "http://%s/api/v2/companies";

    public static final String SHAREDPREFERENCE_FILE = "SharedPreference";

}
