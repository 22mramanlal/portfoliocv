package com.example.andre.hagreve.logic;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Andre on 3/20/2015.
 */


public class Strike {

    private String description;
    private Date start_date;
    private Date end_date;
    private String source_link;
    private boolean all_day;
    private boolean canceled;
    private Submitter submitter;
    private Company company;

    public Strike(String desc, Date start_date, Date end_date, String link, boolean all_day, boolean canceled, Submitter sub, Company c){
        this.description = desc;
        this.start_date = start_date;
        this.end_date = end_date;
        this.source_link = link;
        this.all_day = all_day;
        this.canceled = canceled;
        this.submitter = sub;
        this.company = c;
    }

    public String getDescription(){
        return description;
    }
    public Date getStart_date(){
        return start_date;
    }
    public Date getEnd_date(){
        return end_date;
    }
    public String getSource_link(){
        return source_link;
    }
    public boolean isAll_day(){
        return all_day;
    }
    public boolean isCanceled(){
        return canceled;
    }
    public Submitter getSubmitter(){
        return submitter;
    }
    public Company getCompany(){
        return company;
    }

    public String getDate(){
        Calendar c = Calendar.getInstance();
        c.setTime(start_date);

        String year =  c.get(Calendar.YEAR) +"";
        String month = c.get(Calendar.MONTH)+1 +"";
        String day = c.get(Calendar.DAY_OF_MONTH)+"";

        return year+"-"+month+"-"+day;
    }
}
