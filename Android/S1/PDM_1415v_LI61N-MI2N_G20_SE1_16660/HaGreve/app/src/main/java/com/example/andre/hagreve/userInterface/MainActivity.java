package com.example.andre.hagreve.userInterface;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.andre.hagreve.R;
import com.example.andre.hagreve.dataAcess.Deserializer.StrikesDeserializer;
import com.example.andre.hagreve.dataAcess.HttpRequest;
import com.example.andre.hagreve.logic.Strike;
import com.example.andre.hagreve.userInterface.listAdapter.StrikesListViewAdapter;


import java.util.Date;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private ListView strikesListView;
    private StrikesListViewAdapter customAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadComponents();
        loadPreferences();
        loadStrikes();
    }

    public void loadComponents(){
        strikesListView = (ListView) findViewById(R.id.strikes_ListView);
        customAdapter = new StrikesListViewAdapter(this, R.layout.strikes_list, HaGreveApp.presentedStrikes);
        strikesListView.setAdapter(customAdapter);

        strikesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(MainActivity.this, StrikeDetailsActivity.class);
                i.putExtra("strikesListPosition", position);
                startActivity(i);
            }
        });


    }


    private void loadStrikes(){

            AsyncTask<String, Void, String> task = new HttpRequest(){

                @Override
                protected void onPostExecute(String str){

                    StrikesDeserializer d = new StrikesDeserializer();
                    List<Strike> list = d.deserializeElements(str);
                    HaGreveApp.allStrikes.clear();

                    for(Strike s : list)
                        HaGreveApp.allStrikes.add(s);

                    copyStrikesToRepresent();
                    customAdapter.notifyDataSetChanged();
                };
            };
            task.execute(HaGreveApp.API_URL_STRIKES);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch(id){
            case  R.id.action_settings:
                 startActivityForResult(new Intent(this, FilterActivity.class), 0); break;
            case R.id.action_definitions:
                startActivityForResult(new Intent(this, DefinitionsActivity.class),1);break;
            case R.id.action_refresh:
                loadStrikes(); break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == 0) //BUG não esta ser feita a verificação da alteração. Caso n altere volta a carregar dados.
             copyStrikesToRepresent();

        if(requestCode == 1 && resultCode == RESULT_OK)
            loadStrikes();

    }


    public void copyStrikesToRepresent(){

        HaGreveApp.presentedStrikes.clear();
        Date date;
        Date todayDate = new Date();
        for (int i = 0; i<HaGreveApp.allStrikes.size() ;++i){

            date = HaGreveApp.allStrikes.get(i).getStart_date();

            if( date.compareTo(todayDate) < 0)
                continue;

            if(HaGreveApp.companyIdsFiltered.contains(HaGreveApp.allStrikes.get(i).getCompany().getId()))
                continue;

             HaGreveApp.presentedStrikes.add(HaGreveApp.allStrikes.get(i));

        }
        customAdapter.notifyDataSetChanged();
    }


    @Override
    protected void onStop(){
        super.onStop();
        StringBuilder sb = new StringBuilder();

        for(int i : HaGreveApp.companyIdsFiltered)
            sb.append(i + ",");

        SharedPreferences sp = getSharedPreferences(HaGreveApp.SHAREDPREFERENCE_FILE,MODE_PRIVATE);
        sp.edit()
        .putString("companiesIDs",sb.toString())
        .remove("hostPortText").remove("isOnline")
        .commit();
    }

    private void loadPreferences(){
        SharedPreferences sp = getSharedPreferences(HaGreveApp.SHAREDPREFERENCE_FILE,MODE_PRIVATE);
        String str = sp.getString("companiesIDs",",");
        if(str == "") return;
        String [] s = str.split(",");

        for(int i = 0; i< s.length ;++i)
                HaGreveApp.companyIdsFiltered.add(Integer.parseInt(s[i]));
    }

}



