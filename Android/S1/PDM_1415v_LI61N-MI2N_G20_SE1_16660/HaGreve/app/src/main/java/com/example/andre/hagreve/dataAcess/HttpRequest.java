package com.example.andre.hagreve.dataAcess;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;


public class HttpRequest extends AsyncTask<String, Void, String> {

    public String doInBackground(String... str){

        HttpURLConnection huc = null;

        try{
            URL url = new URL(str[0]);
            huc = (HttpURLConnection) url.openConnection();
            InputStream input = huc.getInputStream();
            String content = readFromUrlContent(input);
            return content;
        }catch (IOException e){
            throw new RuntimeException(e);
        }finally {
            huc.disconnect();
        }
    }

    private static  String readFromUrlContent(InputStream is){
        Scanner s =  new Scanner(is, "UTF-8");
        StringBuilder aux = new StringBuilder();
        try{
            while( s.hasNext())
                aux.append(s.next()).append(" ");
            return aux.toString();
        }finally{
            s.close();
        }
    }

}
