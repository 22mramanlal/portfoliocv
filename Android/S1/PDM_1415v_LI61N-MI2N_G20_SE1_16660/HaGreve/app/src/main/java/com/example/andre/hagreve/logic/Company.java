package com.example.andre.hagreve.logic;

/**
 * Created by Romil on 20/03/2015.
 */
public class Company {
    private String name;
    private int id;

    public Company(String name, int id){
        this.name = name;
        this.id = id;
    }

    public String getCompanyName(){
        return name;
    }
    public int getId(){
        return id;
    }
}
