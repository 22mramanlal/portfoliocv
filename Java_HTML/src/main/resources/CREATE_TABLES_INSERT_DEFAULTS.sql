/*
*	ISEL-DEETC-LEIC
*	2013-2014
*	Semestre Verao
*	LS-LID41-G05
* 	Rental Management
*/
/*
*	ISEL-DEETC-LEIC
*	2013-2014
*	Semestre Verao
*	LS-LID41-G05
* 	Rental Management
*/

set nocount on;
set xact_abort on;

use PropertyApp;
begin transaction
raiserror('Starting...',0,1) --info

--create and insert defaults

/*
*	dbo.Client
*/
if object_id('dbo.Client') is null
begin
create table dbo.Client
(
	username varchar(30) CONSTRAINT pk_Client PRIMARY KEY,
	pass nvarchar(30) NOT NULL,
	email varchar(200) NOT NULL,
	fullname nvarchar(200) NOT NULL
);

insert into dbo.Client 
	values ('Bruno', 'pass', 'Bruno@mail.com', 'Bruno Dantas'),
				('Andre', 'pass', 'Andre@mail.com', 'Andre Ramanlal'),
				('Carmen', 'pass', 'Carmen@mail.com', 'Carmen Fleitas'),
				('Bill', 'pass', 'Bill@mail.com', 'Bill Gates'),
				('Steve', 'pass', 'Steve@mail.com', 'Steve Jobs'),
				('Cristiano', 'pass', 'cristiano@mail.com', 'Cristiano Ronaldo'),
				('Jose', 'pass', 'Jose@mail.com', 'Jose Mourinho'),
				('Messi', 'pass', 'messi@mail.com', 'Leonel Messi'),
				('Freddy', 'pass', 'freddy@mail.com', 'freddy Mercury'),
				('Kurt', 'pass', 'Kurt@mail.com', 'Kurt Cobain'),
				('Jenis', 'pass', 'Jenis@mail.com', 'Jenis Joplin');
end

/*
*	dbo.Local
*/
if object_id('dbo.Local') is null
begin
create table dbo.Local
(
	locale nvarchar(50) NOT NULL,
	region nvarchar(50) NOT NULL,
	CONSTRAINT pk_Local PRIMARY KEY (region, locale)
);

insert into dbo.Local 
	values('Albergaria-a-Velha', 'Aveiro'), ('Anadia', 'Aveiro'), ('Arouca', 'Aveiro'),
		  ('Aveiro', 'Aveiro'), ('Castelo de Paiva', 'Aveiro'), ('Espinho', 'Aveiro'), 
		  ('Estarreja', 'Aveiro'), ('ilhavo', 'Aveiro'), ('Mealhada', 'Aveiro'), ('Murtosa', 'Aveiro'), 
		  ('Oliveira de Azemeis', 'Aveiro'), ('Oliveira do Bairro', 'Aveiro'), ('Ovar', 'Aveiro'), 
		  ('Santa Maria da Feira', 'Aveiro'), ('Sao Joao da Madeira', 'Aveiro'), ('Sever do Vouga', 'Aveiro'), 
		  ('Vagos', 'Aveiro'), ('Vale de Cambra', 'Aveiro'), ('Aljustrel', 'Beja'), ('Almodovar', 'Beja'), 
		  ('Alvito', 'Beja'), ('Barrancos', 'Beja'), ('Beja', 'Beja'), ('Castro Verde', 'Beja'),
		  ('Cuba', 'Beja'), ('Ferreira do Alentejo', 'Beja'), ('Mertola', 'Beja'), ('Moura', 'Beja'), 
		  ('Odemira', 'Beja'), ('Ourique', 'Beja'), ('Serpa', 'Beja'), ('Vidigueira', 'Beja'), 
		  ('Barcelos', 'Braga'), ('Braga', 'Braga'), ('Cabeceiras de Basto', 'Braga'), 
		  ('Celorico de Basto', 'Braga'), ('Esposende', 'Braga'), ('Fafe', 'Braga'), 
		  ('Guimaraes', 'Braga'), ('P�voa de Lanhoso', 'Braga'), ('Terras de Bouro', 'Braga'), 
		  ('Vieira do Minho', 'Braga'), ('Vila Nova de Famalicao', 'Braga'), ('Vila Verde', 'Braga'), 
		  ('Vizela', 'Braga'), ('Braganza', 'Braganza'), ('Carrazeda de Ansiaes', 'Braganza'), 
		  ('Freixo de Espada a Cinta', 'Braganza'), ('Macedo de Cavaleiros', 'Braganza'), 
		  ('Miranda do Douro', 'Braganza'), ('Mirandela Mogadouro', 'Braganza'), 
		  ('Torre de Moncorvo', 'Braganza'), ('Vila Flor', 'Braganza'), ('Vimioso Vinhais', 'Braganza'), 
		  ('Castelo Branco', 'Castelo Branco'), ('Covilha', 'Castelo Branco'), 
		  ('Fundao', 'Castelo Branco'), ('Idanha-a-Nova', 'Castelo Branco'), 
		  ('Oleiros', 'Castelo Branco'), ('Penamacor', 'Castelo Branco'), 
		  ('Proenca-a-Nova', 'Castelo Branco'), ('Serta', 'Castelo Branco'), 
		  ('Vila de Rei', 'Castelo Branco'), ('Vila Velha de R�dao', 'Castelo Branco'), 
		  ('Cantanhede', 'Coimbra'), ('Coimbra', 'Coimbra'), ('Condeixa-a-Nova', 'Coimbra'), 
		  ('Figueira da Foz', 'Coimbra'), ('G�is', 'Coimbra'), ('Lousa', 'Coimbra'), 
		  ('Mira', 'Coimbra'), ('Miranda do Corvo', 'Coimbra'), ('Montemor-o-Velho', 'Coimbra'), 
		  ('Oliveira do Hospital', 'Coimbra'), ('Pampilhosa da Serra', 'Coimbra'), 
		  ('Penacova', 'Coimbra'), ('Penela', 'Coimbra'), ('Soure', 'Coimbra'), ('Tabua', 'Coimbra'), 
		  ('Vila Nova de Poiares', 'Coimbra'), ('Alandroal', 'Evora'), ('Arraiolos', 'Evora'), 
		  ('Borba', 'Evora'), ('Estremoz', 'Evora'), ('evora', 'Evora'), 
		  ('Montemor-o-Novo', 'Evora'), ('Mora', 'Evora'), ('Mourao', 'Evora'), ('Portel', 'Evora'), 
		  ('Redondo', 'Evora'), ('Reguengos de Monsaraz', 'Evora'), ('Vendas Novas', 'Evora'), 
		  ('Viana do Alentejo', 'Evora'), ('Vila Vicosa', 'Evora'), ('Albufeira', 'Faro'), 
		  ('Alcoutim', 'Faro'), ('Aljezur', 'Faro'), ('Castro Marim', 'Faro'), ('Faro', 'Faro'), 
		  ('Lagoa', 'Faro'), ('Lagos', 'Faro'), ('Loule', 'Faro'), ('Monchique', 'Faro'), 
		  ('Olhao', 'Faro'), ('Portimao', 'Faro'), ('Sao Bras de Alportel', 'Faro'), 
		  ('Silves', 'Faro'), ('Tavira', 'Faro'), ('Vila do Bispo', 'Faro'), 
		  ('Vila Real de Santo Ant�nio', 'Faro'), ('Aguiar da Beira', 'Guarda'), 
		  ('Almeida', 'Guarda'), ('Celorico da Beira', 'Guarda'), 
		  ('Figueira de Castelo Rodrigo', 'Guarda'), ('Fornos de Algodres', 'Guarda'), 
		  ('Gouveia', 'Guarda'), ('Guarda Manteigas', 'Guarda'), ('Meda', 'Guarda'), 
		  ('Pinhel', 'Guarda'), ('Sabugal', 'Guarda'), ('Seia', 'Guarda'), 
		  ('Trancoso', 'Guarda'), ('Vila Nova de Foz Coa', 'Guarda'), ('Alcobaca', 'Leiria'), 
		  ('Alvaiazere', 'Leiria'), ('Ansiao', 'Leiria'), ('Batalha', 'Leiria'), 
		  ('Bombarral', 'Leiria'), ('Caldas da Rainha', 'Leiria'), 
		  ('Castanheira de Pera', 'Leiria'), ('Figueir� dos Vinhos', 'Leiria'), 
		  ('Leiria', 'Leiria'), ('Marinha Grande', 'Leiria'), ('Nazare', 'Leiria'), 
		  ('�bidos', 'Leiria'), ('Pedr�gao Grande', 'Leiria'), ('Peniche', 'Leiria'), 
		  ('Pombal', 'Leiria'), ('Porto de M�s', 'Leiria'), ('Alenquer', 'Lisboa'), 
		  ('Amadora', 'Lisboa'), ('Arruda dos Vinhos', 'Lisboa'), ('Azambuja', 'Lisboa'), 
		  ('Cadaval', 'Lisboa'), ('Cascais', 'Lisboa'), ('Lisboa', 'Lisboa'), 
		  ('Loures', 'Lisboa'), ('Lourinha', 'Lisboa'), ('Mafra', 'Lisboa'), 
		  ('Odivelas', 'Lisboa'), ('Oeiras', 'Lisboa'), ('Sintra', 'Lisboa'), 
		  ('Sobral de Monte Agraco', 'Lisboa'), ('Torres Vedras', 'Lisboa'), 
		  ('Vila Franca de Xira', 'Lisboa'), ('Alter do Chao', 'Portalegre'), 
		  ('Arronches', 'Portalegre'), ('Avis', 'Portalegre'), 
		  ('Campo Maior', 'Portalegre'), ('Castelo de Vide', 'Portalegre'), 
		  ('Crato', 'Portalegre'), ('Elvas', 'Portalegre'), ('Fronteira', 'Portalegre'), 
		  ('Gaviao', 'Portalegre'), ('Marvao', 'Portalegre'), ('Monforte', 'Portalegre'), 
		  ('Nisa', 'Portalegre'), ('Ponte de Sor', 'Portalegre'), 
		  ('Portalegre', 'Portalegre'), ('Sousel', 'Portalegre'), ('Amarante', 'Porto'), 
		  ('Baiao', 'Porto'), ('Felgueiras', 'Porto'), ('Gondomar', 'Porto'), 
		  ('Lousada', 'Porto'), ('Maia', 'Porto'), ('Marco de Canaveses', 'Porto'), 
		  ('Matosinhos', 'Porto'), ('Porto', 'Porto'), 
		  ('Pacos de Ferreira', 'Porto'), ('Paredes', 'Porto'), ('Penafiel', 'Porto'), 
		  ('P�voa de Varzim', 'Porto'), ('Santo Tirso', 'Porto'), ('Trofa', 'Porto'), 
		  ('Valongo', 'Porto'), ('Vila do Conde', 'Porto'), ('Vila Nova de Gaia', 'Porto'),
		  ('Abrantes', 'Santarem'), ('Alcanena', 'Santarem'), ('Almeirim', 'Santarem'), 
		  ('Alpiarca', 'Santarem'), ('Benavente', 'Santarem'), ('Cartaxo', 'Santarem'), 
		  ('Chamusca', 'Santarem'), ('Constancia', 'Santarem'), ('Coruche', 'Santarem'), 
		  ('Entroncamento', 'Santarem'), ('Ferreira do Zezere', 'Santarem'), 
		  ('Golega', 'Santarem'), ('Macao', 'Santarem'), ('Ourem', 'Santarem'), 
		  ('Rio Maior', 'Santarem'), ('Salvaterra de Magos', 'Santarem'), 
		  ('Santarem', 'Santarem'), ('Sardoal', 'Santarem'), ('Tomar', 'Santarem'), 
		  ('Torres Novas', 'Santarem'), ('Vila Nova da Barquinha', 'Santarem'), 
		  ('Alcacer do Sal', 'Setubal'), ('Alcochete', 'Setubal'), ('Almada', 'Setubal'), 
		  ('Barreiro', 'Setubal'), ('Grandola', 'Setubal'), ('Moita', 'Setubal'), 
		  ('Montijo', 'Setubal'), ('Palmela', 'Setubal'), ('Santiago do Cacem', 'Setubal'), 
		  ('Seixal', 'Setubal'), ('Sesimbra', 'Setubal'), ('Setubal', 'Setubal'), 
		  ('Sines', 'Setubal'), ('Arcos de Valdevez', 'Viana do Castelo'), 
		  ('Caminha', 'Viana do Castelo'), ('Melgaco', 'Viana do Castelo'), 
		  ('Moncao', 'Viana do Castelo'), ('Paredes de Coura', 'Viana do Castelo'), 
		  ('Ponte da Barca', 'Viana do Castelo'), ('Ponte de Lima', 'Viana do Castelo'), 
		  ('Valenca', 'Viana do Castelo'), ('Viana do Castelo', 'Viana do Castelo'), 
		  ('Vila Nova de Cerveira', 'Viana do Castelo'), ('Alij�', 'Vila Real'), 
		  ('Boticas', 'Vila Real'), ('Chaves', 'Vila Real'), ('Mesao Frio', 'Vila Real'), 
		  ('Mondim de Basto', 'Vila Real'), ('Montalegre', 'Vila Real'), ('Murca', 'Vila Real'), 
		  ('Peso da Regua', 'Vila Real'), ('Ribeira de Pena', 'Vila Real'), ('Sabrosa', 'Vila Real'), 
		  ('Santa Marta de Penaguiao', 'Vila Real'), ('Valpacos', 'Vila Real'), 
		  ('Vila Pouca de Aguiar', 'Vila Real'), ('Vila Real', 'Vila Real'), ('Armamar', 'Viseu'), 
		  ('Carregal do Sal', 'Viseu'), ('Castro Daire', 'Viseu'), ('Cinfaes', 'Viseu'), 
		  ('Lamego', 'Viseu'), ('Mangualde', 'Viseu'), ('Moimenta da Beira', 'Viseu'), 
		  ('Mortagua', 'Viseu'), ('Nelas', 'Viseu'), ('Oliveira de Frades', 'Viseu'), 
		  ('Penalva do Castelo', 'Viseu'), ('Penedono', 'Viseu'), ('Resende', 'Viseu'), 
		  ('Santa Comba Dao', 'Viseu'), ('Sao Joao da Pesqueira', 'Viseu'), 
		  ('Sao Pedro do Sul', 'Viseu'), ('Satao', 'Viseu'), ('Sernancelhe', 'Viseu'), 
		  ('Tabuaco', 'Viseu'), ('Tarouca', 'Viseu'), ('Tondela', 'Viseu'), 
		  ('Vila Nova de Paiva', 'Viseu'), ('Viseu', 'Viseu'), ('Vouzela', 'Viseu'), 
		  ('Angra do Heroismo', 'Acores'), ('Calheta', 'Acores'), ('Corvo', 'Acores'), 
		  ('Horta', 'Acores'), ('Lagoa', 'Acores'), ('Lajes das Flores', 'Acores'), 
		  ('Lajes do Pico', 'Acores'), ('Madalena', 'Acores'), ('Nordeste', 'Acores'), 
		  ('Ponta Delgada', 'Acores'), ('Povoacao', 'Acores'), ('Ribeira Grande', 'Acores'), 
		  ('Santa Cruz da Graciosa', 'Acores'), ('Santa Cruz das Flores', 'Acores'), 
		  ('Sao Roque do Pico', 'Acores'), ('Velas', 'Acores'), 
		  ('Vila da Praia da Vit�ria', 'Acores'), ('Vila do Porto', 'Acores'), 
		  ('Vila Franca do Campo', 'Acores'), ('Calheta', 'Madeira'), 
		  ('Camara de Lobos', 'Madeira'), ('Funchal', 'Madeira'), ('Machico', 'Madeira'), 
		  ('Ponta do Sol', 'Madeira'), ('Porto Moniz', 'Madeira'), ('Porto Santo', 'Madeira'), 
		  ('Ribeira Brava', 'Madeira'), ('Santa Cruz', 'Madeira'), ('Santana', 'Madeira'), 
		  ('Sao Vicente', 'Madeira');
		  
end

/*
*	dbo.Property
*/
if object_id('dbo.Property') is null
begin
create table Property(
id int CONSTRAINT pk_Property PRIMARY KEY,
sort nvarchar(30) NOT NULL,
text varchar(50) NOT NULL,
price int NOT NULL,
region nvarchar(50) NOT NULL,
locale nvarchar(50) NOT NULL,
owner varchar(30) NOT NULL,
CONSTRAINT fk_Property FOREIGN KEY (owner) References Client(username),
CONSTRAINT fk1_Property FOREIGN KEY(region, locale) References Local(region, locale)
);

insert into dbo.Property 
values(1, 'Hotel', '5 stars', 500000, 'Lisboa', 'Oeiras', 'Bruno'),
	(2, 'Hotel', 'Luxurious', 600000, 'Lisboa', 'Cascais', 'Bruno'),
	(3, 'Hotel', '5 stars', 500000, 'Lisboa', 'Sintra', 'Bruno'),
	(4, 'Hotel', 'Luxurious', 500000, 'Faro', 'Albufeira', 'Bruno'),
	(5, 'Hotel', '4 stars', 400000, 'Lisboa', 'Cascais', 'Bruno'),
	(6, 'Hotel', '4 stars', 400000, 'Faro', 'Lagos', 'Bruno'),
	(7, 'Garage', 'Place for 2 cars', 500, 'Porto', 'Maia', 'Andre'),
	(8, 'Garage', 'Place for 2 cars', 500, 'Lisboa', 'Cascais', 'Andre'),
	(9, 'Garage', 'Place for 3 cars', 1000, 'Lisboa', 'Oeiras', 'Andre'),
	(10, 'BedRoom', 'Great view over the city', 300, 'Porto', 'Maia', 'Carmen'),
	(11, 'BedRoom', 'Equiped', 400, 'Porto', 'Maia', 'Carmen'),
	(12, 'BedRoom', 'Great view over the city', 400, 'Lisboa', 'Cascais', 'Carmen'),
	(13, 'BedRoom', 'Equiped', 300, 'Lisboa', 'Cascais', 'Carmen'),
	(14, 'BedRoom', 'Great view over the city', 200, 'Faro', 'Faro', 'Carmen'),
	(15, 'Shop', 'Well located', 20000, 'Lisboa', 'Oeiras', 'Jose'),
	(16, 'Shop', 'Well located', 10000, 'Lisboa', 'Cascais', 'Jose'),
	(17, 'Apartment', 'Well located', 800, 'Faro', 'Lagos', 'Cristiano'),
	(18, 'Apartment', 'Great view over the city', 700, 'Faro', 'Lagos', 'Cristiano'),
	(19, 'Apartment', 'Equiped', 500, 'Faro', 'Albufeira', 'Cristiano'),
	(20, 'Studio', 'Equiped', 50000, 'Lisboa', 'Cascais', 'Bill'),
	(21, 'Studio', 'Well located', 60000, 'Lisboa', 'Oeiras', 'Bill'),
	(22, 'Gym', 'Equiped', 30000, 'Lisboa', 'Cascais', 'Steve');
	end
 
 /*
*	dbo.Rental
*/

if object_id('dbo.Rental') is null
begin
create table dbo.Rental
(
	id int,
	renter nvarchar(30) NOT NULL,
	cy int NOT NULL,
	cw int NOT NULL,
	rentState bit NOT NULL,
	orderDate smalldatetime NOT NULL,
	acceptanceDate smalldatetime,
	CONSTRAINT pk_Rental PRIMARY KEY(id, cw, cy),
	CONSTRAINT fk_Rental FOREIGN KEY(id) REFERENCES Property(id)
);

insert into dbo.Rental
values(1, 'Andre', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Carmen', 2015, 4, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Bill', 2015, 6, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Bill', 2016, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Bill', 2017, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Andre', 2018, 3, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Carmen', 2018, 7, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Cristiano', 2020, 6, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Cristiano', 2020, 20, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(2, 'Jose', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(2, 'Jose', 2015, 20, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(2, 'Bruno', 2015, 21, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(2, 'freddy', 2016, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(2, 'Bruno', 2018, 10, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(3, 'Steve', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(4, 'Steve', 2016, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(5, 'Kurt', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(6, 'Jenis', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(7, 'Bruno', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(8, 'Jose', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(9, 'Andre', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(10, 'Steve', 2016, 13, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(11, 'Carmen', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(12, 'Jenis', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(13, 'Kurt', 2020, 2, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(14, 'Jenis', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(15, 'Bruno', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(16, 'Steve', 2016, 2, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(17, 'Kurt', 2016, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(18, 'freddy', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(19, 'freddy', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(20, 'freddy', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(21, 'freddy', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0');

end
	
raiserror('... OK',0,1) --info
commit
set nocount on;
set xact_abort on;

use PropertyApp;
begin transaction
raiserror('Starting...',0,1) --info

--create and insert defaults

/*
*	dbo.Client
*/
if object_id('dbo.Client') is null
begin
create table dbo.Client
(
	username varchar(30) CONSTRAINT pk_Client PRIMARY KEY,
	pass nvarchar(30) NOT NULL,
	email varchar(200) NOT NULL,
	fullname nvarchar(200) NOT NULL
);

insert into dbo.Client 
	values('BillGates', 'Microsoft', 'bilgates@email.com','Bill Gates'),
				('Bruno', 'pass', 'Bruno@mail.com', 'Bruno Dantas'),
				('Andre', 'pass', 'Andre@mail.com', 'Andre Ramanlal'),
				('Carmen', 'pass', 'Carmen@mail.com', 'Carmen Fleitas'),
				('Bill', 'pass', 'Bill@mail.com', 'Bill Gates'),
				('Steve', 'pass', 'Steve@mail.com', 'Steve Jobs'),
				('Cristiano', 'pass', 'cristiano@mail.com', 'Cristiano Ronaldo'),
				('Jose', 'pass', 'Jose@mail.com', 'Jose Mourinho'),
				('Messi', 'pass', 'messi@mail.com', 'Leonel Messi'),
				('Freddy', 'pass', 'freddy@mail.com', 'freddy Mercury'),
				('Kurt', 'pass', 'Kurt@mail.com', 'Kurt Cobain'),
				('Jenis', 'pass', 'Jenis@mail.com', 'Jenis Joplin');
end

/*
*	dbo.Local
*/
if object_id('dbo.Local') is null
begin
create table dbo.Local
(
	region nvarchar(50) NOT NULL,
	locale nvarchar(50) NOT NULL,
	CONSTRAINT pk_Local PRIMARY KEY (region, locale)
);

insert into dbo.Local 
	values('Albergaria-a-Velha', 'Aveiro'), ('Anadia', 'Aveiro'), ('Arouca', 'Aveiro'),
		  ('Aveiro', 'Aveiro'), ('Castelo de Paiva', 'Aveiro'), ('Espinho', 'Aveiro'), 
		  ('Estarreja', 'Aveiro'), ('ilhavo', 'Aveiro'), ('Mealhada', 'Aveiro'), ('Murtosa', 'Aveiro'), 
		  ('Oliveira de Azemeis', 'Aveiro'), ('Oliveira do Bairro', 'Aveiro'), ('Ovar', 'Aveiro'), 
		  ('Santa Maria da Feira', 'Aveiro'), ('Sao Joao da Madeira', 'Aveiro'), ('Sever do Vouga', 'Aveiro'), 
		  ('Vagos', 'Aveiro'), ('Vale de Cambra', 'Aveiro'), ('Aljustrel', 'Beja'), ('Almodovar', 'Beja'), 
		  ('Alvito', 'Beja'), ('Barrancos', 'Beja'), ('Beja', 'Beja'), ('Castro Verde', 'Beja'),
		  ('Cuba', 'Beja'), ('Ferreira do Alentejo', 'Beja'), ('Mertola', 'Beja'), ('Moura', 'Beja'), 
		  ('Odemira', 'Beja'), ('Ourique', 'Beja'), ('Serpa', 'Beja'), ('Vidigueira', 'Beja'), 
		  ('Barcelos', 'Braga'), ('Braga', 'Braga'), ('Cabeceiras de Basto', 'Braga'), 
		  ('Celorico de Basto', 'Braga'), ('Esposende', 'Braga'), ('Fafe', 'Braga'), 
		  ('Guimaraes', 'Braga'), ('P�voa de Lanhoso', 'Braga'), ('Terras de Bouro', 'Braga'), 
		  ('Vieira do Minho', 'Braga'), ('Vila Nova de Famalicao', 'Braga'), ('Vila Verde', 'Braga'), 
		  ('Vizela', 'Braga'), ('Braganza', 'Braganza'), ('Carrazeda de Ansiaes', 'Braganza'), 
		  ('Freixo de Espada a Cinta', 'Braganza'), ('Macedo de Cavaleiros', 'Braganza'), 
		  ('Miranda do Douro', 'Braganza'), ('Mirandela Mogadouro', 'Braganza'), 
		  ('Torre de Moncorvo', 'Braganza'), ('Vila Flor', 'Braganza'), ('Vimioso Vinhais', 'Braganza'), 
		  ('Castelo Branco', 'Castelo Branco'), ('Covilha', 'Castelo Branco'), 
		  ('Fundao', 'Castelo Branco'), ('Idanha-a-Nova', 'Castelo Branco'), 
		  ('Oleiros', 'Castelo Branco'), ('Penamacor', 'Castelo Branco'), 
		  ('Proenca-a-Nova', 'Castelo Branco'), ('Serta', 'Castelo Branco'), 
		  ('Vila de Rei', 'Castelo Branco'), ('Vila Velha de R�dao', 'Castelo Branco'), 
		  ('Cantanhede', 'Coimbra'), ('Coimbra', 'Coimbra'), ('Condeixa-a-Nova', 'Coimbra'), 
		  ('Figueira da Foz', 'Coimbra'), ('G�is', 'Coimbra'), ('Lousa', 'Coimbra'), 
		  ('Mira', 'Coimbra'), ('Miranda do Corvo', 'Coimbra'), ('Montemor-o-Velho', 'Coimbra'), 
		  ('Oliveira do Hospital', 'Coimbra'), ('Pampilhosa da Serra', 'Coimbra'), 
		  ('Penacova', 'Coimbra'), ('Penela', 'Coimbra'), ('Soure', 'Coimbra'), ('Tabua', 'Coimbra'), 
		  ('Vila Nova de Poiares', 'Coimbra'), ('Alandroal', 'Evora'), ('Arraiolos', 'Evora'), 
		  ('Borba', 'Evora'), ('Estremoz', 'Evora'), ('evora', 'Evora'), 
		  ('Montemor-o-Novo', 'Evora'), ('Mora', 'Evora'), ('Mourao', 'Evora'), ('Portel', 'Evora'), 
		  ('Redondo', 'Evora'), ('Reguengos de Monsaraz', 'Evora'), ('Vendas Novas', 'Evora'), 
		  ('Viana do Alentejo', 'Evora'), ('Vila Vicosa', 'Evora'), ('Albufeira', 'Faro'), 
		  ('Alcoutim', 'Faro'), ('Aljezur', 'Faro'), ('Castro Marim', 'Faro'), ('Faro', 'Faro'), 
		  ('Lagoa', 'Faro'), ('Lagos', 'Faro'), ('Loule', 'Faro'), ('Monchique', 'Faro'), 
		  ('Olhao', 'Faro'), ('Portimao', 'Faro'), ('Sao Bras de Alportel', 'Faro'), 
		  ('Silves', 'Faro'), ('Tavira', 'Faro'), ('Vila do Bispo', 'Faro'), 
		  ('Vila Real de Santo Ant�nio', 'Faro'), ('Aguiar da Beira', 'Guarda'), 
		  ('Almeida', 'Guarda'), ('Celorico da Beira', 'Guarda'), 
		  ('Figueira de Castelo Rodrigo', 'Guarda'), ('Fornos de Algodres', 'Guarda'), 
		  ('Gouveia', 'Guarda'), ('Guarda Manteigas', 'Guarda'), ('Meda', 'Guarda'), 
		  ('Pinhel', 'Guarda'), ('Sabugal', 'Guarda'), ('Seia', 'Guarda'), 
		  ('Trancoso', 'Guarda'), ('Vila Nova de Foz Coa', 'Guarda'), ('Alcobaca', 'Leiria'), 
		  ('Alvaiazere', 'Leiria'), ('Ansiao', 'Leiria'), ('Batalha', 'Leiria'), 
		  ('Bombarral', 'Leiria'), ('Caldas da Rainha', 'Leiria'), 
		  ('Castanheira de Pera', 'Leiria'), ('Figueir� dos Vinhos', 'Leiria'), 
		  ('Leiria', 'Leiria'), ('Marinha Grande', 'Leiria'), ('Nazare', 'Leiria'), 
		  ('�bidos', 'Leiria'), ('Pedr�gao Grande', 'Leiria'), ('Peniche', 'Leiria'), 
		  ('Pombal', 'Leiria'), ('Porto de M�s', 'Leiria'), ('Alenquer', 'Lisboa'), 
		  ('Amadora', 'Lisboa'), ('Arruda dos Vinhos', 'Lisboa'), ('Azambuja', 'Lisboa'), 
		  ('Cadaval', 'Lisboa'), ('Cascais', 'Lisboa'), ('Lisboa', 'Lisboa'), 
		  ('Loures', 'Lisboa'), ('Lourinha', 'Lisboa'), ('Mafra', 'Lisboa'), 
		  ('Odivelas', 'Lisboa'), ('Oeiras', 'Lisboa'), ('Sintra', 'Lisboa'), 
		  ('Sobral de Monte Agraco', 'Lisboa'), ('Torres Vedras', 'Lisboa'), 
		  ('Vila Franca de Xira', 'Lisboa'), ('Alter do Chao', 'Portalegre'), 
		  ('Arronches', 'Portalegre'), ('Avis', 'Portalegre'), 
		  ('Campo Maior', 'Portalegre'), ('Castelo de Vide', 'Portalegre'), 
		  ('Crato', 'Portalegre'), ('Elvas', 'Portalegre'), ('Fronteira', 'Portalegre'), 
		  ('Gaviao', 'Portalegre'), ('Marvao', 'Portalegre'), ('Monforte', 'Portalegre'), 
		  ('Nisa', 'Portalegre'), ('Ponte de Sor', 'Portalegre'), 
		  ('Portalegre', 'Portalegre'), ('Sousel', 'Portalegre'), ('Amarante', 'Porto'), 
		  ('Baiao', 'Porto'), ('Felgueiras', 'Porto'), ('Gondomar', 'Porto'), 
		  ('Lousada', 'Porto'), ('Maia', 'Porto'), ('Marco de Canaveses', 'Porto'), 
		  ('Matosinhos', 'Porto'), ('Porto', 'Porto'), 
		  ('Pacos de Ferreira', 'Porto'), ('Paredes', 'Porto'), ('Penafiel', 'Porto'), 
		  ('P�voa de Varzim', 'Porto'), ('Santo Tirso', 'Porto'), ('Trofa', 'Porto'), 
		  ('Valongo', 'Porto'), ('Vila do Conde', 'Porto'), ('Vila Nova de Gaia', 'Porto'),
		  ('Abrantes', 'Santarem'), ('Alcanena', 'Santarem'), ('Almeirim', 'Santarem'), 
		  ('Alpiarca', 'Santarem'), ('Benavente', 'Santarem'), ('Cartaxo', 'Santarem'), 
		  ('Chamusca', 'Santarem'), ('Constancia', 'Santarem'), ('Coruche', 'Santarem'), 
		  ('Entroncamento', 'Santarem'), ('Ferreira do Zezere', 'Santarem'), 
		  ('Golega', 'Santarem'), ('Macao', 'Santarem'), ('Ourem', 'Santarem'), 
		  ('Rio Maior', 'Santarem'), ('Salvaterra de Magos', 'Santarem'), 
		  ('Santarem', 'Santarem'), ('Sardoal', 'Santarem'), ('Tomar', 'Santarem'), 
		  ('Torres Novas', 'Santarem'), ('Vila Nova da Barquinha', 'Santarem'), 
		  ('Alcacer do Sal', 'Setubal'), ('Alcochete', 'Setubal'), ('Almada', 'Setubal'), 
		  ('Barreiro', 'Setubal'), ('Grandola', 'Setubal'), ('Moita', 'Setubal'), 
		  ('Montijo', 'Setubal'), ('Palmela', 'Setubal'), ('Santiago do Cacem', 'Setubal'), 
		  ('Seixal', 'Setubal'), ('Sesimbra', 'Setubal'), ('Setubal', 'Setubal'), 
		  ('Sines', 'Setubal'), ('Arcos de Valdevez', 'Viana do Castelo'), 
		  ('Caminha', 'Viana do Castelo'), ('Melgaco', 'Viana do Castelo'), 
		  ('Moncao', 'Viana do Castelo'), ('Paredes de Coura', 'Viana do Castelo'), 
		  ('Ponte da Barca', 'Viana do Castelo'), ('Ponte de Lima', 'Viana do Castelo'), 
		  ('Valenca', 'Viana do Castelo'), ('Viana do Castelo', 'Viana do Castelo'), 
		  ('Vila Nova de Cerveira', 'Viana do Castelo'), ('Alij�', 'Vila Real'), 
		  ('Boticas', 'Vila Real'), ('Chaves', 'Vila Real'), ('Mesao Frio', 'Vila Real'), 
		  ('Mondim de Basto', 'Vila Real'), ('Montalegre', 'Vila Real'), ('Murca', 'Vila Real'), 
		  ('Peso da Regua', 'Vila Real'), ('Ribeira de Pena', 'Vila Real'), ('Sabrosa', 'Vila Real'), 
		  ('Santa Marta de Penaguiao', 'Vila Real'), ('Valpacos', 'Vila Real'), 
		  ('Vila Pouca de Aguiar', 'Vila Real'), ('Vila Real', 'Vila Real'), ('Armamar', 'Viseu'), 
		  ('Carregal do Sal', 'Viseu'), ('Castro Daire', 'Viseu'), ('Cinfaes', 'Viseu'), 
		  ('Lamego', 'Viseu'), ('Mangualde', 'Viseu'), ('Moimenta da Beira', 'Viseu'), 
		  ('Mortagua', 'Viseu'), ('Nelas', 'Viseu'), ('Oliveira de Frades', 'Viseu'), 
		  ('Penalva do Castelo', 'Viseu'), ('Penedono', 'Viseu'), ('Resende', 'Viseu'), 
		  ('Santa Comba Dao', 'Viseu'), ('Sao Joao da Pesqueira', 'Viseu'), 
		  ('Sao Pedro do Sul', 'Viseu'), ('Satao', 'Viseu'), ('Sernancelhe', 'Viseu'), 
		  ('Tabuaco', 'Viseu'), ('Tarouca', 'Viseu'), ('Tondela', 'Viseu'), 
		  ('Vila Nova de Paiva', 'Viseu'), ('Viseu', 'Viseu'), ('Vouzela', 'Viseu'), 
		  ('Angra do Heroismo', 'Acores'), ('Calheta', 'Acores'), ('Corvo', 'Acores'), 
		  ('Horta', 'Acores'), ('Lagoa', 'Acores'), ('Lajes das Flores', 'Acores'), 
		  ('Lajes do Pico', 'Acores'), ('Madalena', 'Acores'), ('Nordeste', 'Acores'), 
		  ('Ponta Delgada', 'Acores'), ('Povoacao', 'Acores'), ('Ribeira Grande', 'Acores'), 
		  ('Santa Cruz da Graciosa', 'Acores'), ('Santa Cruz das Flores', 'Acores'), 
		  ('Sao Roque do Pico', 'Acores'), ('Velas', 'Acores'), 
		  ('Vila da Praia da Vit�ria', 'Acores'), ('Vila do Porto', 'Acores'), 
		  ('Vila Franca do Campo', 'Acores'), ('Calheta', 'Madeira'), 
		  ('Camara de Lobos', 'Madeira'), ('Funchal', 'Madeira'), ('Machico', 'Madeira'), 
		  ('Ponta do Sol', 'Madeira'), ('Porto Moniz', 'Madeira'), ('Porto Santo', 'Madeira'), 
		  ('Ribeira Brava', 'Madeira'), ('Santa Cruz', 'Madeira'), ('Santana', 'Madeira'), 
		  ('Sao Vicente', 'Madeira');
		  
end

/*
*	dbo.Property
*/
if object_id('dbo.Property') is null
begin
create table Property(
id int CONSTRAINT pk_Property PRIMARY KEY,
sort nvarchar(30) NOT NULL,
text varchar(50) NOT NULL,
price int NOT NULL,
region nvarchar(50) NOT NULL,
locale nvarchar(50) NOT NULL,
owner varchar(30) NOT NULL,
CONSTRAINT fk_Property FOREIGN KEY (owner) References Client(username),
CONSTRAINT fk1_Property FOREIGN KEY(region, locale) References Local(region, locale)
);

insert into dbo.Property 
values(1, 'Hotel', '5 stars', 500000, 'Oeiras', 'Lisboa', 'Bruno'),
	(2, 'Hotel', 'Luxurious', 600000, 'Cascais', 'Lisboa', 'Bruno'),
	(3, 'Hotel', '5 stars', 500000, 'Sintra', 'Lisboa', 'Bruno'),
	(4, 'Hotel', 'Luxurious', 500000, 'Albufeira', 'Faro', 'Bruno'),
	(5, 'Hotel', '4 stars', 400000, 'Cascais', 'Lisboa', 'Bruno'),
	(6, 'Hotel', '4 stars', 400000, 'Lagos', 'Faro', 'Bruno'),
	(7, 'Garage', 'Place for 2 cars', 500, 'Maia', 'Porto', 'Andre'),
	(8, 'Garage', 'Place for 2 cars', 500, 'Cascais', 'Lisboa', 'Andre'),
	(9, 'Garage', 'Place for 3 cars', 1000, 'Oeiras', 'Lisboa', 'Andre'),
	(10, 'BedRoom', 'Great view over the city', 300, 'Maia', 'Porto', 'Carmen'),
	(11, 'BedRoom', 'Equiped', 400, 'Maia', 'Porto', 'Carmen'),
	(12, 'BedRoom', 'Great view over the city', 400, 'Cascais', 'Lisboa', 'Carmen'),
	(13, 'BedRoom', 'Equiped', 300, 'Cascais', 'Lisboa', 'Carmen'),
	(14, 'BedRoom', 'Great view over the city', 200, 'Faro', 'Faro', 'Carmen'),
	(15, 'Shop', 'Well located', 20000, 'Oeiras', 'Lisboa', 'Jose'),
	(16, 'Shop', 'Well located', 10000, 'Cascais', 'Lisboa', 'Jose'),
	(17, 'Apartment', 'Well located', 800, 'Lagos', 'Faro', 'Cristiano'),
	(18, 'Apartment', 'Great view over the city', 700, 'Lagos', 'Faro', 'Cristiano'),
	(19, 'Apartment', 'Equiped', 500, 'Albufeira', 'Faro', 'Cristiano'),
	(20, 'Studio', 'Equiped', 50000, 'Cascais', 'Lisboa', 'Bill'),
	(21, 'Studio', 'Well located', 60000, 'Oeiras', 'Lisboa', 'Bill'),
	(22, 'Gym', 'Equiped', 30000, 'Cascais', 'Lisboa', 'Steve');
	end
 
 /*
*	dbo.Rental
*/

if object_id('dbo.Rental') is null
begin
create table dbo.Rental
(
	id int,
	renter nvarchar(30) NOT NULL,
	cy int NOT NULL,
	cw int NOT NULL,
	rentState bit NOT NULL,
	orderDate smalldatetime NOT NULL,
	acceptanceDate smalldatetime,
	CONSTRAINT pk_Rental PRIMARY KEY(id, cw, cy),
	CONSTRAINT fk_Rental FOREIGN KEY(id) REFERENCES Property(id)
);

insert into dbo.Rental
values(1, 'Andre', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Carmen', 2015, 4, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Bill', 2015, 6, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Bill', 2016, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Bill', 2017, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Andre', 2018, 3, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Carmen', 2018, 7, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Cristiano', 2020, 6, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(1, 'Cristiano', 2020, 20, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(2, 'Jose', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(2, 'Jose', 2015, 20, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(2, 'Bruno', 2015, 21, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(2, 'freddy', 2016, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(2, 'Bruno', 2018, 10, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(3, 'Steve', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(4, 'Steve', 2016, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(5, 'Kurt', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(6, 'Jenis', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(7, 'Bruno', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(8, 'Jose', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(9, 'Andre', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(10, 'Steve', 2016, 13, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(11, 'Carmen', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(12, 'Jenis', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(13, 'Kurt', 2020, 2, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(14, 'Jenis', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(15, 'Bruno', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(16, 'Steve', 2016, 2, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(17, 'Kurt', 2016, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(18, 'freddy', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(19, 'freddy', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(20, 'freddy', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0'),
			(21, 'freddy', 2015, 1, 0, '1970-01-01 01:00:00.0', '1970-01-01 01:00:00.0');

end
	
raiserror('... OK',0,1) --info
commit