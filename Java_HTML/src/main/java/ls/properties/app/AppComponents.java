package ls.properties.app;

import ls.properties.commands.structure.HomePage;
import ls.properties.commands.structure.delete.DProperty;
import ls.properties.commands.structure.delete.DRentals;
import ls.properties.commands.structure.get.properties.GPAllProperties;
import ls.properties.commands.structure.get.properties.GPDetails;
import ls.properties.commands.structure.get.properties.GPLocation;
import ls.properties.commands.structure.get.properties.GPOwner;
import ls.properties.commands.structure.get.properties.GPRAllRentals;
import ls.properties.commands.structure.get.properties.GPRYear;
import ls.properties.commands.structure.get.properties.GPRYearAndCw;
import ls.properties.commands.structure.get.properties.GPType;
import ls.properties.commands.structure.get.users.GAllUsers;
import ls.properties.commands.structure.get.users.GSingleUser;
import ls.properties.commands.structure.get.users.GUPOwned;
import ls.properties.commands.structure.get.users.GURentals;
import ls.properties.commands.structure.listen.Listen;
import ls.properties.commands.structure.option.Option;
import ls.properties.commands.structure.patch.PchRentals;
import ls.properties.commands.structure.post.PProperty;
import ls.properties.commands.structure.post.PRentals;
import ls.properties.commands.structure.post.PUser;
import ls.properties.commands.suppliers.commandSupplier.ICommandSupplier;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.suppliers.managerSupplier.map.MapManagerSupplier;
import ls.properties.domain.entitiesManagers.ClientManager;
import ls.properties.domain.entitiesManagers.LocalManager;
import ls.properties.domain.entitiesManagers.PropertyManager;
import ls.properties.domain.entitiesManagers.RentalManager;
import ls.properties.httpservice.HttpServer;
import ls.properties.repositories.IRepository;

public class AppComponents {

	public static void initializeComponents(ICommandSupplier commandSupplier, IRepository repo) {	
		loadCommandsOnTree(commandSupplier, getManagerSupplier(repo));
	}

	public static void closeComponents() {
		HttpServer.stopServer();
	}

	private static IManagerSupplier getManagerSupplier(IRepository repo){
		IManagerSupplier manager = new MapManagerSupplier();
		
		RentalManager rM = new RentalManager(repo);
		manager.addManager(rM, "Rental");
		
		PropertyManager pM = new PropertyManager(repo, manager);
		manager.addManager(pM, "Property");
		
		LocalManager lM= new LocalManager(repo, manager);
		manager.addManager(lM, "Local");
		
		ClientManager cM = new ClientManager(repo, manager);
		manager.addManager(cM, "Client");
		
		return manager;
	}
	
	private static void loadCommandsOnTree(ICommandSupplier commandSupplier, IManagerSupplier manager){
		addGet(commandSupplier, manager);
		addPost(commandSupplier, manager);
		addPatch(commandSupplier, manager);
		addDelete(commandSupplier, manager);
		addOption(commandSupplier);
		addListen(commandSupplier);		
	}

	private static void addGet(ICommandSupplier commandSupplier, IManagerSupplier manager){
		commandSupplier.addCommand("GET", new HomePage());
		commandSupplier.addCommand("GET/users", new GAllUsers(manager));
		commandSupplier.addCommand("GET/properties", new GPAllProperties(manager));


		commandSupplier.addCommand("GET/users/Parameter", new GSingleUser(manager));
		commandSupplier.addCommand("GET/users/Parameter/rentals", new GURentals(manager));
		commandSupplier.addCommand("GET/users/Parameter/properties/owned", new GUPOwned(manager));

		commandSupplier.addCommand("GET/properties/details/Parameter", new GPDetails(manager));
		commandSupplier.addCommand("GET/properties/type/Parameter", new GPType(manager));
		commandSupplier.addCommand("GET/properties/owner/Parameter", new GPOwner(manager));
		commandSupplier.addCommand("GET/properties/location/Parameter", new GPLocation(manager));

		commandSupplier.addCommand("GET/properties/Parameter/rentals", new GPRAllRentals(manager));
		commandSupplier.addCommand("GET/properties/Parameter/rentals/Parameter", new GPRYear(manager));
		commandSupplier.addCommand("GET/properties/Parameter/rentals/Parameter/Parameter", new GPRYearAndCw(manager));
	}

	private static void addPost(ICommandSupplier commandSupplier, IManagerSupplier manager){
		commandSupplier.addCommand("POST/users", new PUser(manager));
		commandSupplier.addCommand("POST/properties", new PProperty(manager));
		commandSupplier.addCommand("POST/properties/Parameter/rentals", new PRentals(manager));
	}

	private static void addPatch(ICommandSupplier commandSupplier, IManagerSupplier manager){
		commandSupplier.addCommand("PATCH/properties/Parameter/rentals/Parameter/Parameter", new PchRentals(manager));
	}

	private static void addDelete(ICommandSupplier commandSupplier, IManagerSupplier manager){
		commandSupplier.addCommand("DELETE/properties/Parameter", new DProperty(manager));
		commandSupplier.addCommand("DELETE/properties/Parameter/rentals/Parameter/Parameter", new DRentals(manager));
	}

	private static void addOption(ICommandSupplier commandSupplier){
		commandSupplier.addCommand("OPTION", new Option());
	}

	private static void addListen(ICommandSupplier commandSupplier) {
		commandSupplier.addCommand("LISTEN", new Listen(commandSupplier));
	}
}
