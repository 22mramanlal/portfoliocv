package ls.properties.app;

import ls.properties.commands.suppliers.commandSupplier.tree.CommandTreeSupplier;
import ls.properties.repositories.sqlRepository.SQLRepository;

public class App {
	
	public static void main(String [] args){
		AppManagement rm = new AppManagement(new CommandTreeSupplier());
		rm.start(args, new SQLRepository());
	}
}
