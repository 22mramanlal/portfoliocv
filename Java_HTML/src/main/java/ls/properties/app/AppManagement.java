package ls.properties.app;

import java.util.LinkedList;
import java.util.Scanner;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.structure.Command;
import ls.properties.commands.structure.CommandUtils;
import ls.properties.commands.suppliers.commandSupplier.ICommandSupplier;
import ls.properties.commands.suppliers.commandSupplier.tree.CommandTreeSupplier;
import ls.properties.commands.views.IView;
import ls.properties.exceptions.AppException;
import ls.properties.exceptions.RepositoryException;
import ls.properties.repositories.IRepository;
import ls.properties.utils.print.Printer;
import ls.properties.utils.print.PrinterUtils;

public class AppManagement {
	
	private ICommandSupplier commandSupplier;

	public AppManagement(CommandTreeSupplier commandSupplier) {
		this.commandSupplier = commandSupplier;
	}

	public void start(String [] args, IRepository repo){

		if(!checkParameterNumber(args.length))
			return;

		try{
			AppComponents.initializeComponents(commandSupplier, repo);	
		}catch(RepositoryException e){
			System.out.println(e.getMessage());
			return;
		}

		if(args.length==0)
			executeWithPrompt();
		else
			executeWithoutPrompt(args);		

		AppComponents.closeComponents();
	}

	private  void executeWithPrompt(){
		String[] args;
		
		while(true){
			System.out.print("Command>");
			args = getArguments(getCommandFromPromt());	
			
			if(toFinish(args))
				break;

			execute(args[0], args[1], args[2]);
		}
	}

	private void executeWithoutPrompt(String[] args){
		args = getArguments(args);
		
		if(toFinish(args))
			return;

		execute(args[0], args[1], args[2]);
	}	

	private static boolean toFinish(String[] args) {
		return args[0] != null  && args[0].equals("EXIT");
	}

	private static String[] getArguments(String [] args){
		String path = (args.length!=1)? args[1] : null;
		String arguments = (args.length==3)? args[2] : null;
		
		return new String[] {args[0],  path, arguments};
	}

	protected static String [] getCommandFromPromt(){
		@SuppressWarnings("resource")
		Scanner in = new Scanner (System.in);

		String line = in.nextLine();

		return line.split(" ");
	}

	private static boolean checkParameterNumber(int length) {
		if(length>3){ 
			System.out.println("Invalid Command\n\n");
			return false;
		}
		return true;
	}

	private void execute(String method, String path, String arguments) {
		LinkedList<String> pathArguments = new LinkedList<String>();
		try{
			Command cmd =  commandSupplier.getCommand((method + path), pathArguments);
			if(cmd == null){
				System.out.println("There is no such Command\n\n");
				return;
			}
			
			ICommandResponse resp = cmd.execute(pathArguments,arguments);
			
			IView v = cmd.getView(CommandUtils.getArgument(arguments, "accept"));
		
			String res = v.getRepresentation(resp);
			
			Printer p = PrinterUtils.getPrinter(CommandUtils.getArgument(arguments, "output-file"));
			p.print(res);
	
		} catch(AppException e){
			System.out.println("\n\n"+e.getMessage()+"\n\n");
		}
	}
}
