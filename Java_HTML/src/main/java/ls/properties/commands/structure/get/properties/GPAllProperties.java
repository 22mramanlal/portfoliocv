package ls.properties.commands.structure.get.properties;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseProperties;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.property.AllPropertiesHtmlView;
import ls.properties.commands.views.jsonView.entities.properties.PropertiesJsonView;
import ls.properties.commands.views.textView.entities.properties.PropertiesTextView;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;

public class GPAllProperties extends GProperties{
	
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new AllPropertiesHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new PropertiesTextView()));
		views.add(new Pair<String, IView>("application/json", new PropertiesJsonView()));
	}

	public GPAllProperties(IManagerSupplier manager) {
		super(manager, views);
	}

	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		Collection<Property> properties = propertyManager.getAllProperties();
		
		ICommandResponse resp = new ResponseProperties(properties);
		return resp;
	}

}
