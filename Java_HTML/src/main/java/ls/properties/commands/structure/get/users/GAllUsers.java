package ls.properties.commands.structure.get.users;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseClients;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.clients.ClientsHtmlView;
import ls.properties.commands.views.jsonView.entities.clients.ClientsJsonView;
import ls.properties.commands.views.textView.entities.clients.ClientsTextView;
import ls.properties.domain.entities.Client;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;


public class GAllUsers extends GetUsers{
	
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new ClientsHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new ClientsTextView()));
		views.add(new Pair<String, IView>("application/json", new ClientsJsonView()));
	}

	public GAllUsers(IManagerSupplier manager) {
		super(manager, views);
	}

	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		Collection<Client> clients = clientManager.getAllClients();
		
		ICommandResponse resp = new ResponseClients(clients);
		return resp;
	}

}
