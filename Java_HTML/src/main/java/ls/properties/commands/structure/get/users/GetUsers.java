package ls.properties.commands.structure.get.users;

import ls.properties.commands.structure.get.Get;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.domain.entitiesManagers.ClientManager;
import ls.properties.utils.Pair;

public abstract class GetUsers extends Get {
	
	protected ClientManager clientManager;

	public GetUsers(IManagerSupplier manager, Iterable<Pair<String, IView>> views) {
		super(views);
		clientManager = (ClientManager) manager.getManager("Client");
	}

}
