package ls.properties.commands.structure.listen;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseMessage.ListenResponse;
import ls.properties.commands.structure.Command;
import ls.properties.commands.structure.CommandUtils;
import ls.properties.commands.suppliers.commandSupplier.ICommandSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.message.MessageHtmlView;
import ls.properties.commands.views.jsonView.message.MessageJsonView;
import ls.properties.commands.views.textView.message.MessageTextView;
import ls.properties.domain.UserInputValidations;
import ls.properties.exceptions.AppException;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.httpservice.HttpServer;
import ls.properties.httpservice.Servlet;
import ls.properties.utils.Pair;

public class Listen extends Command {

	private static List<Pair<String, IView>> views;

	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new MessageHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new MessageTextView()));
		views.add(new Pair<String, IView>("application/json", new MessageJsonView()));
	}
	
	public Listen(ICommandSupplier commandSupplier) {
		super(views);
		Servlet.setCommandSupplier(commandSupplier);
	}
	

	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArgs(pathArguments, commandArguments); 
		int port = Integer.parseInt(CommandUtils.getArgument(commandArguments, "port"));
		HttpServer.start(port);
		
		ICommandResponse resp = new ListenResponse("Server started succefully");
		
		return resp;
	}
	
	private void validateArgs(List<String> pathArguments, String commandArguments){
		if(!commandArguments.contains("port="))
			throw new InvalidUserParametersException("This command need port argument");
	
		UserInputValidations.validatePort(CommandUtils.getArgument(commandArguments, "port"));
	}
}
