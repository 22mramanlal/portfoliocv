package ls.properties.commands.structure.option;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseMessage.OptionResponse;
import ls.properties.commands.structure.Command;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.message.MessageHtmlView;
import ls.properties.commands.views.jsonView.message.MessageJsonView;
import ls.properties.commands.views.textView.message.MessageTextView;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;

public class Option extends Command {

	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new MessageHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new MessageTextView()));
		views.add(new Pair<String, IView>("application/json", new MessageJsonView()));
	}
	
	public Option() {
		super(views);
	}

	@Override
	public ICommandResponse execute(List<String> pathArguments, String arguments) throws AppException {
		StringBuilder options = new StringBuilder("{method} {path} {parameter list}:\n\n");
		
		options.append(optionGet());
		options.append(optionRepresentation());
		options.append(optionPost());
		options.append(optionPatch());
		options.append(optionDelete());
		options.append(optionListen());
				
		ICommandResponse resp = new OptionResponse(options.toString());
		return resp;
	}
	
	


	private StringBuilder optionGet(){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\t\t\tGET\n\n\n");
		sb.append("GET	/users\n \texample: GET /users\n\n");
		sb.append("GET	/users/{username}\n \texample: GET /users/Abc\n\n");
		sb.append("GET	/users/{username}/rentals\n \texample: GET /users/Abc/rentals\n\n");
		sb.append("GET	/users/{username}/properties/owned\n \texample: GET /users/Abc/properties/owned\n\n");
		sb.append("GET	/properties\n \texmple: GET /properties\n\n");
		sb.append("GET	/properties/details/{pid}\n \texample: GET /properties/details/1\n\n");
		sb.append("GET	/properties/location/{location}\n \texample: GET /properties/location/Abc-Def\n\n");
		sb.append("GET	/properties/owner/{owner}\n \texample: GET /properties/owner/A\n\n");
		sb.append("GET	/properties/type/{type}\n \texample: GET /properties/type/B\n\n");
		sb.append("GET	/properties/{pid}/rentals\n \texample: GET /properties/3/rentals\n\n");
		sb.append("GET	/properties/{pid}/rentals/{year}/{cw}\n \texample: GET /properties/3/rentals/2012/22\n\n");
		sb.append("GET  /properties/{pid}/rentals/{year}\n \texample: GET /properties/3/rentals/2012\n\n\n");
		
		return sb;
	}
	
	private StringBuilder optionRepresentation(){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("The following commands are optional and valid on Get commands on parameter list\n\n");
		sb.append("{parameter list}\n\n");
		sb.append("accept=\n");
		sb.append("\t-text/html\n");
		sb.append("\t-text/plain\n");
		sb.append("\t-application/json\n\n");
		sb.append("output-file=file name with extension\n\n");
		sb.append("examples:\n");
		sb.append("\taccept=text/html&output-file=txt.html\n");
		sb.append("\taccept=application/json\n\n\n");
		return sb;
		
	}
	
	private StringBuilder optionPost(){
		StringBuilder sb = new StringBuilder();
		
		sb.append("\t\t\tPOST\n\n\n");
		sb.append("POST	/users\n \texample: POST /users username=B&password=2&email=a@abc.com&fullname=ABC&usernamelog=A&passwordlog=1\n\n");
		sb.append("POST	/properties\n \texample: POST /properties type=apartment&description=new&price=123&location=Lisbon-Lisbon&usernamelog=A&passwordlog=1\n\n");
		sb.append("POST	/properties/{pid}/rentals\n \texample: POST /properties/3/rentals year=2014&cw=22&usernamelog=A&passwordlog=1\n\n\n");
		
		return sb;
	}
	
	private StringBuilder optionPatch(){
		
		StringBuilder sb = new StringBuilder();
		sb.append("\t\t\tPATCH\n\n\n");
		sb.append("PATCH	/properties/{pid}/rentals/{year}/{cw}\n \texample: PATCH /properties/3/rentals/2012/22 usernamelog=A&passerdlog=1\n\n\n");
		return sb;
	}
	
	private StringBuilder optionDelete(){
		
		StringBuilder sb = new StringBuilder();
		sb.append("\t\t\tDELETE\n\n\n");
		sb.append("DELETE	/properties/{pid}/rentals/{year}/[cw}\n \texample: DELETE /properties/3/rentals/2012/22\n\n\n");
		sb.append("DELETE   /properties/{pid}\n \texample: DELETE /properties/1");
		return sb;
	}
	

	private Object optionListen() {
		StringBuilder sb = new StringBuilder();
		sb.append("\t\t\tLISTEN\n\n\n");
		sb.append("LISTEN	/ port=\n \texample: LISTEN / port=8000\n\n\n");
		return sb;
	}

}
