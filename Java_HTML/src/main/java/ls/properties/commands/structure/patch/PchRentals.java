package ls.properties.commands.structure.patch;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseMessage.PatchResponse;
import ls.properties.commands.structure.CommandUtils;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.message.MessageHtmlView;
import ls.properties.commands.views.jsonView.message.MessageJsonView;
import ls.properties.commands.views.textView.message.MessageTextView;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.AppException;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.utils.Pair;

public class PchRentals extends Patch{

	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new MessageHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new MessageTextView()));
		views.add(new Pair<String, IView>("application/json", new MessageJsonView()));
	}
	
	public PchRentals(IManagerSupplier manager) {
		super(manager, views);
	}
	
	@Override
	public ICommandResponse execute(List<String> pathArguments,String commandArguments) throws AppException {
		ValidadeArguments(pathArguments, commandArguments);
		
		String id = pathArguments.get(0), cy = pathArguments.get(1), cw = pathArguments.get(2);
		String username = CommandUtils.getArgument(commandArguments, "usernamelog");
		String pass = CommandUtils.getArgument(commandArguments, "passwordlog");
		
		Property property = propertyManager.getProperty(id);		
		Rental rental = rentalManager.getRental(property, cy, cw);
		
		checkPermition(username, pass, property, rental);
		rental.setRentState(true);
		rental.setAcceptanceDate(new Timestamp(System.currentTimeMillis()));
		
		rentalManager.updateRental(rental);
		
		ICommandResponse resp = new PatchResponse("Rental succefully patched");
		return resp;
	}
	
	private void checkPermition(String userNameLog, String passWordLog, Property property, Rental rental){
		clientManager.userAuthentication(userNameLog, passWordLog);
		propertyManager.isOwnerOfProperty(clientManager.getClient(userNameLog), property);
		if(rental.getRentState())
			throw new InvalidUserParametersException("Rental already patched"); 
	}

	private void ValidadeArguments(List<String> pathArguments, String commandArguments) {
		String[] mustContain = {"usernamelog", "passwordlog"};
		validateCommandArguments(commandArguments, mustContain);
		validatePathArguments(pathArguments, 3);
	}	
}

