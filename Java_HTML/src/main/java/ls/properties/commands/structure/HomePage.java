package ls.properties.commands.structure;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.homePage.HomePageHtmlView;
import ls.properties.commands.views.jsonView.homePage.HomePageJsonView;
import ls.properties.commands.views.textView.homePage.HomePageTextView;
import ls.properties.exceptions.RepositoryException;
import ls.properties.utils.Pair;

public class HomePage extends Command {
private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new HomePageHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new HomePageTextView()));
		views.add(new Pair<String, IView>("text/plain", new HomePageJsonView()));
	}
	
	public HomePage() {
		super(views);
	}


	@Override
	public ICommandResponse execute(List<String> pathArguments,String commandArguments) throws RepositoryException {
		return null;
	}

}
