package ls.properties.commands.structure.get.properties;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOfType;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.property.PropertiesTypeHtmlView;
import ls.properties.commands.views.jsonView.entities.properties.PropertiesTypeJsonView;
import ls.properties.commands.views.textView.entities.properties.PropertiesTypeTextView;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;

public class GPType extends GProperties{
	
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new PropertiesTypeHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new PropertiesTypeTextView()));
		views.add(new Pair<String, IView>("application/json", new PropertiesTypeJsonView()));
	}

	public GPType(IManagerSupplier manager) {
		super(manager, views);
	}

	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments);
		
		String type = pathArguments.get(0);
		Collection<Property> properties = propertyManager.getPropertiesOfType(type);
		
		ICommandResponse resp = new ResponsePropertiesOfType(type, properties);
		return resp;
	}

	private void validateArguments(List<String> pathArguments) {
		validatePathArguments(pathArguments, 1);
	}
	
	
}
