package ls.properties.commands.structure.patch;

import ls.properties.commands.structure.Command;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.domain.entitiesManagers.ClientManager;
import ls.properties.domain.entitiesManagers.PropertyManager;
import ls.properties.domain.entitiesManagers.RentalManager;
import ls.properties.utils.Pair;


public abstract class Patch extends Command {

	protected PropertyManager propertyManager;
	protected RentalManager rentalManager;
	protected ClientManager clientManager;
	
	public Patch(IManagerSupplier manager, Iterable<Pair<String, IView>> views) {
		super(views);
		propertyManager = (PropertyManager)manager.getManager("Property");
		rentalManager = (RentalManager)manager.getManager("Rental");
		clientManager = (ClientManager)manager.getManager("Client");
	}
}
