package ls.properties.commands.structure.post;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseClient;
import ls.properties.commands.structure.CommandUtils;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.clients.ClientHtmlView;
import ls.properties.commands.views.jsonView.entities.clients.ClientJsonView;
import ls.properties.commands.views.textView.entities.clients.ClientTextView;
import ls.properties.domain.entities.Client;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;


public class PUser extends Post{
	
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new ClientHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new ClientTextView()));
		views.add(new Pair<String, IView>("application/json", new ClientJsonView()));
	}
	
	public PUser(IManagerSupplier manager) {
		super(manager, views);
	}
	
	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments, commandArguments);
		
		String username = CommandUtils.getArgument(commandArguments, "username");
		String password = CommandUtils.getArgument(commandArguments, "password");
		String email = CommandUtils.getArgument(commandArguments, "email");
		String fullname = CommandUtils.getArgument(commandArguments, "fullname").replaceAll("\\+", " "); 
		 
		checkPermition(CommandUtils.getArgument(commandArguments, "usernamelog"),
												CommandUtils.getArgument(commandArguments, "passwordlog"));
		
		Client newClient = clientManager.createClient(username, password, email, fullname);
		
		ICommandResponse response = new ResponseClient(newClient);
		return response;
	}
	
	private void validateArguments(List<String> pathArguments, String commandArguments) {
		String[] mustContain = {"username", "password", "fullname", "email", "usernamelog", "passwordlog"};
		validateCommandArguments(commandArguments, mustContain);
	}
	
}
