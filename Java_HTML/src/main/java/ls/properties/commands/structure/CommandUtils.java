
package ls.properties.commands.structure;

public class CommandUtils {
	
	public static String getArgument(String arguments, String argumentWanted){
		if(arguments == null || !arguments.contains(argumentWanted))
			return null;
		
		String arg = arguments.split(argumentWanted+"=")[1];
		
		int idx = arg.indexOf("&");
		
		if(idx!=-1)
			return arg.substring(0, idx);
		else
			return arg;
	}

}
