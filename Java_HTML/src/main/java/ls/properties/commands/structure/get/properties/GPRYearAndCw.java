package ls.properties.commands.structure.get.properties;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseRental;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.rentals.RentalHtmlView;
import ls.properties.commands.views.jsonView.entities.rentals.RentalJsonView;
import ls.properties.commands.views.textView.entities.rentals.RentalTextView;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;

public class GPRYearAndCw extends GPRAllRentals{
	
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new RentalHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new RentalTextView()));
		views.add(new Pair<String, IView>("application/json", new RentalJsonView()));
	}
	

	public GPRYearAndCw(IManagerSupplier manager) {
		super(manager, views);
	}
	
	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments);
		
		Property property = propertyManager.getProperty(pathArguments.get(0));
		String year = pathArguments.get(1);
		String cw = pathArguments.get(2);
		
		Rental rental = rentalManager.getRental(property, year, cw);
		
		ICommandResponse resp = new ResponseRental(rental);
		return resp;
	}
	
	private void validateArguments(List<String> pathArguments) {
		validatePathArguments(pathArguments, 3);
	}
}
