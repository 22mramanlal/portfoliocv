package ls.properties.commands.structure.get.users;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseClient;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.clients.ClientHtmlView;
import ls.properties.commands.views.jsonView.entities.clients.ClientJsonView;
import ls.properties.commands.views.textView.entities.clients.ClientTextView;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entitiesManagers.PropertyManager;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;

public class GSingleUser extends GetUsers{
	
	private static List<Pair<String, IView>> views;

	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new ClientHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new ClientTextView()));
		views.add(new Pair<String, IView>("application/json", new ClientJsonView()));
	}
	
	private PropertyManager propertyManager;

	public GSingleUser(IManagerSupplier manager) {
		super(manager, views);
		propertyManager = (PropertyManager)manager.getManager("Property");
	}

	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments);
		
		Client client = clientManager.getClient(pathArguments.get(0));
		Collection<Property> properties = propertyManager.getPropertiesOf(client);
		
		ICommandResponse resp = new ResponseClient(client, properties);
		return resp;
	}

	private void validateArguments(List<String> pathArguments) {
		validatePathArguments(pathArguments, 1);
	}
	
}
