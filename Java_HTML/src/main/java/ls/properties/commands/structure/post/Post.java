package ls.properties.commands.structure.post;

import ls.properties.commands.structure.Command;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.domain.entitiesManagers.ClientManager;
import ls.properties.domain.entitiesManagers.LocalManager;
import ls.properties.domain.entitiesManagers.PropertyManager;
import ls.properties.domain.entitiesManagers.RentalManager;
import ls.properties.utils.Pair;

public abstract class Post extends Command{
	
	protected PropertyManager propertyManager;
	protected ClientManager clientManager;
	protected RentalManager rentalManager;
	protected LocalManager localManager;
	
	public Post(IManagerSupplier manager, Iterable<Pair<String, IView>> views) {
		super(views);
		propertyManager = (PropertyManager)manager.getManager("Property");
		clientManager = (ClientManager)manager.getManager("Client");
		rentalManager = (RentalManager)manager.getManager("Rental");
		localManager =  (LocalManager)manager.getManager("Local");
	}

	protected void checkPermition(String userNameLog, String passWordLog){
		clientManager.userAuthentication(userNameLog, passWordLog);
	}
		
}



