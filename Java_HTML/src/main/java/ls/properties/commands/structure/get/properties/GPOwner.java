package ls.properties.commands.structure.get.properties;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOfClient;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.property.PropertiesOfOwnerHtmlView;
import ls.properties.commands.views.jsonView.entities.properties.PropertiesOfOwnerJsonView;
import ls.properties.commands.views.textView.entities.properties.PropertiesOfOwnerTextView;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entitiesManagers.ClientManager;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;


public class GPOwner extends GProperties {
	
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new PropertiesOfOwnerHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new PropertiesOfOwnerTextView()));
		views.add(new Pair<String, IView>("application/json", new PropertiesOfOwnerJsonView()));
	}

	private ClientManager clientManager;
	
	public GPOwner(IManagerSupplier manager) {
		super(manager, views);
		clientManager = (ClientManager)manager.getManager("Client");
	}

	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments);
		
		Client client = clientManager.getClient(pathArguments.get(0));
		Collection<Property> properties = propertyManager.getPropertiesOf(client);
		
		ICommandResponse resp = new ResponsePropertiesOfClient(client, properties);
		return resp;
	}

	private void validateArguments(List<String> pathArguments) {
		validatePathArguments(pathArguments, 1);
	}
	
	
}
