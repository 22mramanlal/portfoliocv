package ls.properties.commands.structure.delete;

import ls.properties.commands.structure.Command;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.domain.entitiesManagers.ClientManager;
import ls.properties.domain.entitiesManagers.PropertyManager;
import ls.properties.utils.Pair;

public abstract class Delete extends Command {
	protected PropertyManager propertyManager;
	protected ClientManager clientManager;
	
	public Delete(IManagerSupplier manager, Iterable<Pair<String, IView>> views) {
		super(views);
		propertyManager = (PropertyManager)manager.getManager("Property");
		clientManager = (ClientManager)manager.getManager("Client");
	}

}
