package ls.properties.commands.structure.get.properties;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsForProperty;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.rentals.RentalsForPropertyHtmlView;
import ls.properties.commands.views.jsonView.entities.rentals.RentalsForPropertyJsonView;
import ls.properties.commands.views.textView.entities.rentals.RentalsForPropertyTextView;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.domain.entitiesManagers.RentalManager;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;

public class GPRAllRentals extends GProperties {
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/plain", new RentalsForPropertyTextView()));
		views.add(new Pair<String, IView>("text/html", new RentalsForPropertyHtmlView()));
		views.add(new Pair<String, IView>("application/json", new RentalsForPropertyJsonView()));
	}
	
	protected RentalManager rentalManager;
	
	public GPRAllRentals(IManagerSupplier manager) {
		this(manager, views);
	}
	
	public GPRAllRentals(IManagerSupplier manager, Iterable<Pair<String, IView>> views) {
		super(manager, views);
		rentalManager = (RentalManager)manager.getManager("Rental");
	}

	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments);
		
		Property property = propertyManager.getProperty(pathArguments.get(0));
		Collection<Rental> rentals = rentalManager.getRentalsFor(property);
		
		ICommandResponse resp = new ResponseRentalsForProperty(property, rentals);
		return resp;
	}

	private void validateArguments(List<String> pathArguments) {
		validatePathArguments(pathArguments, 1);
	}
}