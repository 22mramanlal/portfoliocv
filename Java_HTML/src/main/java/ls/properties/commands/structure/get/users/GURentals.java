package ls.properties.commands.structure.get.users;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsOfClient;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.rentals.RentalsOfOwnerHtmlView;
import ls.properties.commands.views.jsonView.entities.rentals.RentalsOfOwnerJsonView;
import ls.properties.commands.views.textView.entities.rentals.RentalsOfOwnerTextView;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Rental;
import ls.properties.domain.entitiesManagers.RentalManager;
import ls.properties.exceptions.RepositoryException;
import ls.properties.utils.Pair;

public class GURentals extends GetUsers{
	
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new RentalsOfOwnerHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new RentalsOfOwnerTextView()));
		views.add(new Pair<String, IView>("application/json", new RentalsOfOwnerJsonView()));
	}
	
	private RentalManager rentalManager;
	
	public GURentals(IManagerSupplier manager) {
		super(manager, views);
		rentalManager = (RentalManager)manager.getManager("Rental");
	}

	
	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws RepositoryException {
		validateArguments(pathArguments);
		
		Client client = clientManager.getClient(pathArguments.get(0));
		Collection<Rental> rentals = rentalManager.getRentalsOf(client);
	
		ICommandResponse resp = new ResponseRentalsOfClient(client, rentals);
		return resp;
	}

	private void validateArguments(List<String> pathArguments) {
		validatePathArguments(pathArguments, 1);	
	}

}
