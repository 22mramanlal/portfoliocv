package ls.properties.commands.structure.get.properties;


import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsForYear;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.rentals.RentalsToYearHtmlView;
import ls.properties.commands.views.jsonView.entities.rentals.RentalsToYearJsonView;
import ls.properties.commands.views.textView.entities.rentals.RentalsForYearTextView;
import ls.properties.domain.UserInputValidations;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;

public class GPRYear extends GPRAllRentals{
	
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new RentalsToYearHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new RentalsForYearTextView()));
		views.add(new Pair<String, IView>("application/json", new RentalsToYearJsonView()));
	}

	public GPRYear(IManagerSupplier manager) {
		super(manager, views);
	}
	
	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments);
		
		int year = Integer.parseInt(pathArguments.get(1));
		String id = pathArguments.get(0);
		Property property = propertyManager.getProperty(id);
		
		Collection<Rental> rentals = rentalManager.getRentalsForOnYear(property, year);
		
		ICommandResponse resp = new ResponseRentalsForYear(year, property, rentals);
		return resp;
	}

	private void validateArguments(List<String> pathArguments) {
		validatePathArguments(pathArguments, 2);
		UserInputValidations.validateYear(pathArguments.get(1));
	}
}
