package ls.properties.commands.structure.delete;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseMessage.DeleteResponse;
import ls.properties.commands.structure.CommandUtils;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.message.MessageHtmlView;
import ls.properties.commands.views.jsonView.message.MessageJsonView;
import ls.properties.commands.views.textView.message.MessageTextView;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.domain.entitiesManagers.RentalManager;
import ls.properties.exceptions.AppException;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.utils.Pair;

public class DRentals extends Delete {

	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new MessageHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new MessageTextView()));
		views.add(new Pair<String, IView>("application/json", new MessageJsonView()));
	}
	
	private RentalManager rentalManager;
	
	public DRentals(IManagerSupplier manager) {
		super(manager, views);
		rentalManager = (RentalManager)manager.getManager("Rental");
	}
	
	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments, commandArguments);
		
		String id = pathArguments.get(0), cy = pathArguments.get(1), cw = pathArguments.get(2);
		String usernamelog = CommandUtils.getArgument(commandArguments, "usernamelog");
		String passwordlog = CommandUtils.getArgument(commandArguments, "passwordlog");
		
		Property property = propertyManager.getProperty(id);
		Rental rental = rentalManager.getRental(property, cy, cw);
		
		checkPermition(usernamelog, passwordlog, rental);
		
		rentalManager.deleteRental(rental);
		
		ICommandResponse resp = new DeleteResponse("Rental deleted succefully");
		return resp;
	}
	
	private void checkPermition(String userNameLog, String passWordLog, Rental rental){
		clientManager.userAuthentication(userNameLog, passWordLog);
		rentalManager.isRenterOrOwnerOfRental(clientManager.getClient(userNameLog), rental);
		if(rental.getRentState())
			throw new InvalidUserParametersException("Can only delete rentls not accepted");
	}

	private void validateArguments(List<String> pathArguments, String commandArguments) {
		String[] mustContain = {"usernamelog", "passwordlog"};
		validateCommandArguments(commandArguments, mustContain);
		validatePathArguments(pathArguments, 3);
	}
}
