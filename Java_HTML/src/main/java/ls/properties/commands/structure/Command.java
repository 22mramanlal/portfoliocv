
package ls.properties.commands.structure;

import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.views.IView;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.exceptions.RepositoryException;
import ls.properties.utils.Pair;


public abstract class Command {
	private Iterable<Pair<String,IView>> views;
	
	public Command(Iterable<Pair<String, IView>> views) {
		this.views = views;
	}

	public abstract ICommandResponse execute(List<String> pathArguments, String commandArguments) throws RepositoryException;
	
	public IView getView(String accept){
		if(accept==null || accept.isEmpty())
			accept = "text/plain";
		for(Pair<String, IView> p : views)
			if(p.getElem1().equals(accept))
				return p.getElem2();
		throw new InvalidUserParametersException("Inexistent View");
	}
	
	protected void validateCommandArguments(String commandArguments, String[] mustContain){
		if(commandArguments == null || commandArguments.isEmpty())
			throw new InvalidUserParametersException("Ivalid commandArguments");
		
		for(String s : mustContain)
			if(!commandArguments.contains(s+"="))
				throw new InvalidUserParametersException("This command need "+ s +" argument");
	}
	
	protected void validatePathArguments(List<String> pathArguments, int rightSize){
		if(pathArguments == null)
			throw new IllegalArgumentException("Null pahtArguments");
	
		if(pathArguments.size() != rightSize)
			throw new InvalidUserParametersException("Invalid path Arguments");
	}
	
}
