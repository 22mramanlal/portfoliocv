package ls.properties.commands.structure.get;

import ls.properties.commands.structure.Command;
import ls.properties.commands.views.IView;
import ls.properties.utils.Pair;


public abstract class Get extends Command{

	public Get(Iterable<Pair<String, IView>> views) {
		super(views);
	}
}

