package ls.properties.commands.structure.post;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseRental;
import ls.properties.commands.structure.CommandUtils;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.rentals.RentalHtmlView;
import ls.properties.commands.views.jsonView.entities.rentals.RentalJsonView;
import ls.properties.commands.views.textView.entities.rentals.RentalTextView;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;

public class PRentals extends Post {

	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new RentalHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new RentalTextView()));
		views.add(new Pair<String, IView>("application/json", new RentalJsonView()));
	}
	
	public PRentals(IManagerSupplier manager) {
		super(manager, views);
	}
	
	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments, commandArguments);
		
		String id = pathArguments.get(0);
		String cw = CommandUtils.getArgument(commandArguments, "cw");
		String cy = CommandUtils.getArgument(commandArguments, "year");
		String username = CommandUtils.getArgument(commandArguments, "usernamelog");
		
		checkPermition(username, CommandUtils.getArgument(commandArguments, "passwordlog"));
		
		Client renter = clientManager.getClient(username);
		Property property = propertyManager.getProperty(id);
		Rental rental = rentalManager.createRental(property, renter, cy, cw);
		
		ICommandResponse resp = new ResponseRental(rental);
		return resp;
	}
	
	private void validateArguments(List<String> pathArguments, String commandArguments) {
		String[] mustContain = {"cw", "year", "usernamelog", "passwordlog"};
		validateCommandArguments(commandArguments, mustContain);
		validatePathArguments(pathArguments, 1);
	}
	
}
