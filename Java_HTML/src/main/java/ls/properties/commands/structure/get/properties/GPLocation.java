package ls.properties.commands.structure.get.properties;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOnLocation;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.property.PropertiesLocationHtmlView;
import ls.properties.commands.views.jsonView.entities.properties.PropertiesLocationJsonView;
import ls.properties.commands.views.textView.entities.properties.PropertiesLocationTextView;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entitiesManagers.LocalManager;
import ls.properties.exceptions.AppException;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.utils.Pair;

public class GPLocation extends GProperties {
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new PropertiesLocationHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new PropertiesLocationTextView()));
		views.add(new Pair<String, IView>("application/json", new PropertiesLocationJsonView()));
	}

	private LocalManager localManager;
	
	public GPLocation(IManagerSupplier manager) {
		super(manager, views);
		localManager = (LocalManager)manager.getManager("Local");
	}

	
	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments);
		
		String region = pathArguments.get(0).split("-")[0];
		String locale = pathArguments.get(0).split("-")[1];
		Local local = localManager.getLocal(region, locale);
		
		Collection<Property> properties = propertyManager.getPropertiesLocated(local);
		
		ICommandResponse resp = new ResponsePropertiesOnLocation(local, properties);
		return resp;
	}

	private void validateArguments(List<String> pathArguments) {
		validatePathArguments(pathArguments, 1);
		
		String location = pathArguments.get(0);
		if(!location.contains("-"))
			throw new InvalidUserParametersException("Invalid location format");
		
	}
	
	
	
}
