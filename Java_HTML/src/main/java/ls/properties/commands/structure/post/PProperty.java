package ls.properties.commands.structure.post;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseProperty;
import ls.properties.commands.structure.CommandUtils;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.property.PropertyHtmlView;
import ls.properties.commands.views.jsonView.entities.properties.PropertyJsonView;
import ls.properties.commands.views.textView.entities.properties.PropertyTextView;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.AppException;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.utils.Pair;

public class PProperty extends Post{

	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new PropertyHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new PropertyTextView()));
		views.add(new Pair<String, IView>("application/json", new PropertyJsonView()));
	}
	
	public PProperty(IManagerSupplier manager) {
		super(manager, views);
	}
	
	
	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments, commandArguments);
		
		String type = CommandUtils.getArgument(commandArguments, "type");
		String description = CommandUtils.getArgument(commandArguments, "description").replaceAll("\\+", " ");
		String price = CommandUtils.getArgument(commandArguments, "price");
		String location = CommandUtils.getArgument(commandArguments, "location");
		String username = CommandUtils.getArgument(commandArguments, "usernamelog");
		
		checkPermition(username, CommandUtils.getArgument(commandArguments, "passwordlog"));
		
		Local local = localManager.getLocal(location.split("-")[0], location.split("-")[1]);
		Client owner = clientManager.getClient(username);
		Property property = propertyManager.createProperty(type, description, price, local, owner);
		
		ICommandResponse resp = new ResponseProperty(property);
		return resp;
	}

	private void validateArguments(List<String> pathArguments,String commandArguments) {
		String[] mustContain = {"type", "description", "price", "location", "usernamelog", "passwordlog"};
		validateCommandArguments(commandArguments, mustContain);		
		if(!CommandUtils.getArgument(commandArguments, "location").contains("-"))
			throw new InvalidUserParametersException("Invalid location format");
	}

}
