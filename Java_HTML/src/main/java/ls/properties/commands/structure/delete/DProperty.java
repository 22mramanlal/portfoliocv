package ls.properties.commands.structure.delete;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseMessage.DeleteResponse;
import ls.properties.commands.structure.CommandUtils;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.message.MessageHtmlView;
import ls.properties.commands.views.jsonView.message.MessageJsonView;
import ls.properties.commands.views.textView.message.MessageTextView;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.RepositoryException;
import ls.properties.utils.Pair;

public class DProperty extends Delete {

	private static List<Pair<String, IView>> views;

	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new MessageHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new MessageTextView()));
		views.add(new Pair<String, IView>("application/json", new MessageJsonView()));
	}
	
	public DProperty(IManagerSupplier manager) {
		super(manager, views);
	}
	
	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws RepositoryException {
		validateArguments(pathArguments, commandArguments);
		
		String usernamelog = CommandUtils.getArgument(commandArguments, "usernamelog");
		String passwordlog = CommandUtils.getArgument(commandArguments, "passwordlog");
		String id = pathArguments.get(0);
		Property property = propertyManager.getProperty(id);
		
		checkPermition(usernamelog, passwordlog, property);
		
		propertyManager.deleteProperty(property);
		
		ICommandResponse resp = new DeleteResponse("Property deleted succefully");
		return resp;
	}

	private void checkPermition(String userNameLog, String passWordLog, Property property){
		clientManager.userAuthentication(userNameLog, passWordLog);
		propertyManager.isOwnerOfProperty(clientManager.getClient(userNameLog), property);
	}
	
	private void validateArguments(List<String> pathArguments, String commandArguments) {
		String[] mustContain = {"usernamelog", "passwordlog"};
		validateCommandArguments(commandArguments, mustContain);
		validatePathArguments(pathArguments, 1);
	}
}
