package ls.properties.commands.structure.get.properties;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseProperty;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.entities.property.PropertyHtmlView;
import ls.properties.commands.views.jsonView.entities.properties.PropertyJsonView;
import ls.properties.commands.views.textView.entities.properties.PropertyTextView;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.AppException;
import ls.properties.utils.Pair;

public  class GPDetails extends GProperties {
	
	private static List<Pair<String, IView>> views;
	
	static{
		views = new LinkedList<Pair<String,IView>>();
		views.add(new Pair<String, IView>("text/html", new PropertyHtmlView()));
		views.add(new Pair<String, IView>("text/plain", new PropertyTextView()));
		views.add(new Pair<String, IView>("application/json", new PropertyJsonView()));
	}
	
	public GPDetails(IManagerSupplier manager) {
		super(manager, views);
	}

	@Override
	public ICommandResponse execute(List<String> pathArguments, String commandArguments) throws AppException {
		validateArguments(pathArguments);
		
		Property property = propertyManager.getProperty(pathArguments.get(0));
		
		ICommandResponse resp = new ResponseProperty(property);
		return resp;
	}

	private void validateArguments(List<String> pathArguments) {
		validatePathArguments(pathArguments, 1);
	}

}
