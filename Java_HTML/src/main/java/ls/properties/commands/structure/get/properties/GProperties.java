package ls.properties.commands.structure.get.properties;

import ls.properties.commands.structure.get.Get;
import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.views.IView;
import ls.properties.domain.entitiesManagers.PropertyManager;
import ls.properties.utils.Pair;

public abstract class GProperties extends Get {

	protected PropertyManager propertyManager;
	
	public GProperties(IManagerSupplier manager, Iterable<Pair<String, IView>> views) {
		super(views);
		propertyManager = (PropertyManager)manager.getManager("Property");
	}
}
