package ls.properties.commands.utils;


public class CommandNode<T> {
	
	private CommandNode<T> next;
	private CommandNode<T> children;
	private T value;
	
	public CommandNode(T value) {
		this.value = value;
	}
	
	public CommandNode() {}
	

	public void addChild(T value) {
		CommandNode<T> curr = new CommandNode<>(value);
		
		curr.setNext(children);
		children = curr;	
	}
	
	public void addNext(T value) {
		CommandNode<T> curr = new CommandNode<>(value);
		
		curr.setNext(next);
		next = curr;
	}
	
	public void setNext(CommandNode<T> next){
		this.next = next;
	}
	
	public T getValue() {
		return this.value;
	}
	
	public CommandNode<T> getNext() {
		return next;
	}
	
	public CommandNode<T> getChildren() {
		return children;
	}

}
