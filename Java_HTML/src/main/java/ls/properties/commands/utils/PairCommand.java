package ls.properties.commands.utils;

import ls.properties.commands.structure.Command;
import ls.properties.utils.Pair;

public class PairCommand extends Pair<String, Command> {

	public PairCommand(String elem1, Command elem2) {
		super(elem1, elem2);
	}

}
