package ls.properties.commands.responses.responseEntities;

import java.util.Collection;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.domain.entities.Client;

public class ResponseClients implements ICommandResponse{

	Collection<Client> clients;

	public ResponseClients(Collection<Client> clients) {
		this.clients = clients;
	}

	public Collection<Client> getClients() {
		return clients;
	}
	
}
