package ls.properties.commands.responses.responseEntities;

import java.util.Collection;

import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;

public class ResponseRentalsForProperty extends ResponseRentals{

	private Property property;
	public Property getProperty() {
		return property;
	}

	public ResponseRentalsForProperty(Property property, Collection<Rental> rentals) {
		super(rentals);
		this.property = property;
	}
	

}
