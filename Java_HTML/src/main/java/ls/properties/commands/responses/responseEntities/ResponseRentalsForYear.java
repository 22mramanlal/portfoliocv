package ls.properties.commands.responses.responseEntities;

import java.util.Collection;

import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;

public class ResponseRentalsForYear extends ResponseRentalsForProperty {
	private int year;
	public int getYear(){
		return year;
	}
	
	public ResponseRentalsForYear(int year, Property property, Collection<Rental> rentals) {
		super(property, rentals);
		this.year = year;
	}
	
	

}
