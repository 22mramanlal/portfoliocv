package ls.properties.commands.responses.responseEntities;

import java.util.Collection;

import ls.properties.domain.entities.Property;

public class ResponsePropertiesOfType extends ResponseProperties {

	private String type;	
	public String getType() {
		return type;
	}

	public ResponsePropertiesOfType(String type, Collection<Property> properties) {
		super(properties);
		this.type = type;
	}

	
}
