package ls.properties.commands.responses.responseEntity;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.domain.entities.Rental;

public class ResponseRental implements ICommandResponse{

	Rental rental;

	public ResponseRental(Rental rental) {
		this.rental = rental;
	}
	
	public Rental getRental(){
		return rental;
	}
	
	
}
