package ls.properties.commands.responses.responseEntity;

import java.util.Collection;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Property;

public class ResponseClient implements ICommandResponse{

	Client client;
	Collection<Property> properties;

	public ResponseClient(Client client, Collection<Property> properties) {
		this.client = client;
		this.properties = properties;
	}

	public ResponseClient(Client client) {
		this(client, null);
	}
	
	public Client getClient(){
		return client;
	}
	
	public Collection<Property> getProperties() {
		return properties;
	}
	
}

