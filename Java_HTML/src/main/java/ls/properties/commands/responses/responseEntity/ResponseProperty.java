package ls.properties.commands.responses.responseEntity;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.domain.entities.Property;

public class ResponseProperty implements ICommandResponse{

	Property property;

	public ResponseProperty(Property property) {
		this.property = property;
	}
	
	public Property getProperty(){
		return property;
	}
	
	
}
