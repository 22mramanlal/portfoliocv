package ls.properties.commands.responses.responseMessage;

import ls.properties.commands.responses.ICommandResponse;

public abstract class ResponseMessage implements ICommandResponse{
	
	private String message;
	
		
	public ResponseMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
