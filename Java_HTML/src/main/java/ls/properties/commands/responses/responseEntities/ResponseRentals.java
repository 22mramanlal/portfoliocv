package ls.properties.commands.responses.responseEntities;

import java.util.Collection;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.domain.entities.Rental;

public class ResponseRentals implements ICommandResponse{

	Collection<Rental> rentals;

	public ResponseRentals(Collection<Rental> rentals) {
		this.rentals = rentals;
	}

	public Collection<Rental> getRentals() {
		return rentals;
	}	
	
}
