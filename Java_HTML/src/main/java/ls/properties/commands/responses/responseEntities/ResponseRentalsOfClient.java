package ls.properties.commands.responses.responseEntities;

import java.util.Collection;

import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Rental;

public class ResponseRentalsOfClient extends ResponseRentals {
	
	private Client client;
	public Client getClient() {
		return client;
	}

	public ResponseRentalsOfClient(Client client, Collection<Rental> rentals) {
		super(rentals);
		this.client = client;
	}

}
