package ls.properties.commands.responses.responseEntities;

import java.util.Collection;

import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Property;

public class ResponsePropertiesOfClient extends ResponseProperties {

	private Client client;	
	public Client getClient() {
		return client;
	}

	public ResponsePropertiesOfClient(Client client, Collection<Property> properties) {
		super(properties);
		this.client = client;
	}

}
