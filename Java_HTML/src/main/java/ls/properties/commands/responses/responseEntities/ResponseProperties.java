package ls.properties.commands.responses.responseEntities;

import java.util.Collection;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.domain.entities.Property;

public class ResponseProperties implements ICommandResponse{

	Collection<Property> properties;

	public ResponseProperties(Collection<Property> properties) {
		this.properties = properties;
	}
	
	public Collection<Property> getProperties(){
		return properties;
	}
	
}
