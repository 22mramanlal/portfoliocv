package ls.properties.commands.responses.responseEntities;

import java.util.Collection;

import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;

public class ResponsePropertiesOnLocation extends ResponseProperties {

	private Local local;
	public Local getLocal() {
		return local;
	}
	
	public ResponsePropertiesOnLocation(Local local, Collection<Property> properties) {
		super(properties);
		this.local = local;
	}

	
	

}
