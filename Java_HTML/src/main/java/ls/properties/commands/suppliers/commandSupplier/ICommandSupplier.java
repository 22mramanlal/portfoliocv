package ls.properties.commands.suppliers.commandSupplier;

import java.util.Collection;

import ls.properties.commands.structure.Command;

public interface ICommandSupplier {
	public Command getCommand(String path, Collection<String> contentor);
	public void addCommand(String path, Command cmd);
}
