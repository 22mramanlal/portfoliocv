package ls.properties.commands.suppliers.managerSupplier;

import ls.properties.domain.entitiesManagers.EntityManager;

public interface IManagerSupplier {
	
	public abstract void addManager(EntityManager manager, String name);
	public abstract EntityManager getManager(String name);

}
