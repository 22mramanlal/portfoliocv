package ls.properties.commands.suppliers.managerSupplier.map;

import java.util.HashMap;
import java.util.Map;

import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.domain.entitiesManagers.EntityManager;

public class MapManagerSupplier implements IManagerSupplier {

	private Map<String, EntityManager> map = new HashMap<String, EntityManager>();
	
	@Override
	public void addManager(EntityManager manager, String name) {
		map.put(name, manager);
	}

	@Override
	public EntityManager getManager(String name) {
		return map.get(name);
	}

}
