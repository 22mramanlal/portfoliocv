package ls.properties.commands.suppliers.commandSupplier.tree;

import java.util.Collection;

import ls.properties.commands.structure.Command;
import ls.properties.commands.utils.CommandNode;
import ls.properties.commands.utils.PairCommand;

class CommandTreeSupplierUtils {

	static boolean isCommand(String commandName, String commandWanted,
			Collection<String> contentor) {
		if (commandName.equals("Parameter")) {
			contentor.add(commandWanted);
			return true;
		}

		return commandName.equals(commandWanted);
	}

	static boolean isRightCommand(String commandFind, String commandWanted) {
		if (commandFind.equals("Paramater"))
			return true;

		return commandFind.equals(commandWanted);
	}

	static String getSubPath(String path) {
		if (path == null || path.isEmpty())
			throw new IllegalArgumentException("Null path parameter");

		int idx = path.indexOf('/');
		String subPath = (idx == -1) ? path : path.substring(0, idx);

		return subPath;
	}

	static String cutPath(String path) {

		if (path == null || path.isEmpty())
			throw new IllegalArgumentException("Invalid path parameter");

		String[] subPaths = path.split("/");
		StringBuilder newPath = new StringBuilder("");

		for (int i = 1; i < subPaths.length; ++i) {
			newPath.append(subPaths[i]);
			if (i != subPaths.length - 1)
				newPath.append("/");
		}

		return newPath.toString();
	}

	public static void getNext(CommandNode<PairCommand> children, String path,
			String subPath, Command cmd) {

		if (children.getNext() == null) {
			if (path.isEmpty())
				children.addNext(new PairCommand(subPath, cmd));
			else
				children.addNext(new PairCommand(subPath, null));
		}
	}

	public static void getChildren(CommandNode<PairCommand> root, String path, String subPath, Command cmd) {
		
		if (root.getChildren() == null) {
			if (path.equals("/")) {
				root.addChild(new PairCommand(subPath, cmd));
				return;
			} 
			else
				root.addChild(new PairCommand(subPath, null));
		}
	}
}
