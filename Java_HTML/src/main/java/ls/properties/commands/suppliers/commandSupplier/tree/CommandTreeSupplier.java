package ls.properties.commands.suppliers.commandSupplier.tree;

import java.util.Collection;

import ls.properties.commands.structure.Command;
import ls.properties.commands.suppliers.commandSupplier.ICommandSupplier;
import ls.properties.commands.utils.CommandNode;
import ls.properties.commands.utils.PairCommand;
import ls.properties.exceptions.InvalidFunctionalityException;

public class CommandTreeSupplier implements ICommandSupplier {
	private CommandNode<PairCommand> root = new CommandNode<PairCommand>();

	@Override
	public Command getCommand(String path, Collection<String> contentor) {
		return CommandTreeSupplier.getCommandRecursive(path, root, contentor);
	}

	@Override
	public void addCommand(String path, Command cmd) {
		CommandTreeSupplier.addCommandRecursive(root, path, cmd);
	}

	private static void addCommandRecursive(CommandNode<PairCommand> root,
			String path, Command cmd) {
		if (root == null || path == null)
			throw new IllegalArgumentException("Null Parameter");
		if (path.isEmpty())
			throw new IllegalArgumentException("Empty path");

		if (path.indexOf('/') == -1) {
			root.addChild(new PairCommand(path, cmd));
			return;
		}

		String subPath = CommandTreeSupplierUtils.getSubPath(path);
		path = CommandTreeSupplierUtils.cutPath(path);

		CommandTreeSupplierUtils.getChildren(root, path, subPath, cmd);
		CommandNode<PairCommand> children = root.getChildren();

		while (children != null) {
			String commandName = children.getValue().getElem1();
			if (CommandTreeSupplierUtils.isRightCommand(commandName, subPath)) {
				addCommandRecursive(children, path, cmd);
				return;
			}

			CommandTreeSupplierUtils.getNext(children, path, subPath, cmd);
			children = children.getNext();
		}

		throw new IllegalArgumentException("There is no such path on tree");
	}

	private static Command getCommandRecursive(String path,
			CommandNode<PairCommand> root, Collection<String> contentor) {

		if (path == null || root == null || contentor == null)
			throw new IllegalArgumentException("Null parameter");

		if (path.isEmpty())
			return root.getValue().getElem2();

		CommandNode<PairCommand> children = root.getChildren();

		while (children != null) {
			String commandName = children.getValue().getElem1();
			String commandWanted = CommandTreeSupplierUtils.getSubPath(path);
			if (CommandTreeSupplierUtils.isCommand(commandName, commandWanted,
					contentor))
				return getCommandRecursive(
						CommandTreeSupplierUtils.cutPath(path), children,
						contentor);

			children = children.getNext();
		}

		if (path.equals("/"))
			return root.getValue().getElem2();

		throw new InvalidFunctionalityException(
				"There is no such path on tree.");
	}

}
