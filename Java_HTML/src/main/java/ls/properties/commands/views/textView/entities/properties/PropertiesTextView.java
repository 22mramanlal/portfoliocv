package ls.properties.commands.views.textView.entities.properties;

import java.util.Iterator;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseProperties;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Property;

public class PropertiesTextView extends TextView {
	
	@Override
	public String getRepresentation(ICommandResponse cr) {
		List<Property> p = (List<Property>)((ResponseProperties) cr).getProperties();
		
		if(p == null || p.isEmpty() )
			return "There is no Property";
		
		StringBuilder sb = new StringBuilder();

		sb.append(prepareInfo(p));
		return sb.toString();
	}
	
	
	private String prepareInfo(List<Property> list){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\n\tProperties\n");
		
		Iterator<Property> iter = list.iterator();

		while(iter.hasNext()){
			sb.append(constructPropertyList(iter.next()));
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	private String constructPropertyList(Property p) {

		 StringBuilder sb = new StringBuilder();

		  sb.append("\t\t>");
		  sb.append(p.getId());

	   return sb.toString();
	}

}
