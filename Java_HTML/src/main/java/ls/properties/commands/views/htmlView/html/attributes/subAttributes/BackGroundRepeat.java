package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Repeat;

public class BackGroundRepeat extends SubAttribute {

	private static String backGroundRepeatSubAttributeName = "background-repeat";
	private Repeat repeat;
	
	public BackGroundRepeat(Repeat repeat) {
		this.repeat = repeat;
	}

	@Override
	protected String getAttributeName() {
		return BackGroundRepeat.backGroundRepeatSubAttributeName;
	}

	@Override
	protected String getValue() {
		return repeat.toString();
	}

}
