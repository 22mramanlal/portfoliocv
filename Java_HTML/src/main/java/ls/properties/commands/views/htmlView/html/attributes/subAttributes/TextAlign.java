package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Alignment;

public class TextAlign extends SubAttribute {
	
	private static String textAlignSubAttributeName = "text-align";
	private Alignment align;
	
	public TextAlign(Alignment align) {
		this.align = align;
	}

	@Override
	protected String getAttributeName() {
		return TextAlign.textAlignSubAttributeName;
	}

	@Override
	protected String getValue() {
		return align.toString();
	}

}
