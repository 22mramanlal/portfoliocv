package ls.properties.commands.views.htmlView.entities.property;

import java.util.Collection;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseProperties;
import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlUtils;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;
import ls.properties.commands.views.htmlView.templates.ViewAllTemplate;
import ls.properties.domain.entities.Property;

public abstract class PropertiesHtmlView extends HtmlView{

	@Override
	public String getRepresentation(ICommandResponse cr) {
		ViewAllTemplate temp = new ViewAllTemplate();
		
		Collection<Property> properties = ((ResponseProperties)cr).getProperties();
		
		setTitle(temp, cr);
		setProperties(temp, properties);
		
		HtmlUtils.identHTML(temp.getHtml());
		return temp.getHtml().toString();
	}
		
	protected abstract void setTitle(ViewAllTemplate temp, ICommandResponse cr);
	
	private static void setProperties(ViewAllTemplate temp, Collection<Property> properties){
		if(properties == null || properties.isEmpty()){
			temp.addElementList(new Tag(TagName.Font, "There are no properties"));
			return;
		}
			
		for(Property p : properties){
			String id = Integer.toString(p.getId());
			String link = "/properties/details/"+id;
			temp.addElementList(getEntityTag(link, id));
		}
			
	}

}
