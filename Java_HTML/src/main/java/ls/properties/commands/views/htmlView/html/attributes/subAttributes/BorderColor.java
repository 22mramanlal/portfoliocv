package ls.properties.commands.views.htmlView.html.attributes.subAttributes;


public class BorderColor extends SubAttribute{
	
	private static String borderColorSubAttributeName = "border-color";
	private Color color;
	
	
	public BorderColor(Color color) {
		this.color = color;
	}

	@Override
	protected String getAttributeName() {
		return BorderColor.borderColorSubAttributeName;
	}

	@Override
	protected String getValue() {
		return color.toString();
	}

}
