package ls.properties.commands.views.jsonView.entities.rentals;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseRental;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonSimpleObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;

public class RentalJsonView extends JsonView {

	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		
		ResponseRental rr = (ResponseRental)cr;
		JsonSimpleObject j = EntitiesUtilsJsonView.constructJsonRental(rr.getRental());
		JsonObject json = new JsonObject(1);
		json.addJson(j);
		json.setTitle(getRepName());
		
		return json.toString();
	}

	@Override
	protected String getRepName() {
		return "Rental";
	}

}
