package ls.properties.commands.views.textView.message;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseMessage.ResponseMessage;
import ls.properties.commands.views.textView.TextView;



public class MessageTextView  extends TextView{

	@Override
	public String getRepresentation(ICommandResponse cr) {
		
		if(((ResponseMessage) cr).getMessage() == null){
			System.out.println("The command was not sucessfuly.");
			return null;
		}
			
		return ((ResponseMessage) cr).getMessage();

	}

}
