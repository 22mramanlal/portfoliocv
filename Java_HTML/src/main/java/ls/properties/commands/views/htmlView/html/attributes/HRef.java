package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;

public class HRef extends Attribute{
	
	private static String hrefAttName = "href";
	private String link;
	
	public HRef(String link){
		this.link = link;
	}
	
	@Override
	protected String getAttributeName() {
		return HRef.hrefAttName;
	}

	@Override
	protected String getValue() {
		return link;
	}

}
