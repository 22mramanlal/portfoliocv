package ls.properties.commands.views.jsonView.entities.properties;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseProperties;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;

public class PropertiesJsonView extends JsonView {

	@Override
	protected String getRepName() {
		return "All properties: ";
	}

	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		ResponseProperties rp = (ResponseProperties)cr;

		JsonObject json = EntitiesUtilsJsonView.constructJsonListProp(rp.getProperties());
		json.setTitle(getRepName());
		return json.toString();
	}

}
