package ls.properties.commands.views.htmlView.entities.property;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOfClient;
import ls.properties.commands.views.htmlView.templates.ViewAllTemplate;
import ls.properties.commands.views.htmlView.templates.ViewTemplateUtils;
import ls.properties.domain.entities.Client;


public class PropertiesOfOwnerHtmlView extends PropertiesHtmlView {

	@Override
	protected void setTitle(ViewAllTemplate temp, ICommandResponse cr) {
		Client c = ((ResponsePropertiesOfClient)(cr)).getClient();
		
		String userName = c.getUsername();
		String link = "/users/"+userName;
		ViewTemplateUtils.setComposeTitle(temp, "Properties of ", link, userName);
	}



}
