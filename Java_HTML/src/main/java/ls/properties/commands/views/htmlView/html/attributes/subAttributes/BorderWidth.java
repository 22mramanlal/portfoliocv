package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Format;

public class BorderWidth extends SubAttribute{

	private static String borderWidthSubAttributeName = "border-width";
	private int value;
	private Format format;
	
	public BorderWidth(int value, Format format) {
		this.value = value;
		this.format = format;
	}

	@Override
	protected String getAttributeName() {
		return BorderWidth.borderWidthSubAttributeName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value) + format.toString();
	}
	
}
