package ls.properties.commands.views.htmlView.entities.property;

import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.ALL_PROPERTIES_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.DEFAULT_PROPERTY_IMAGE_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.PROPERTIES_IMAGES_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.RENTALS_FOR_PROPERTY_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.setSimpleTitle;
import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseProperty;
import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlUtils;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.templates.ViewOneTemplate;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;

public class PropertyHtmlView extends HtmlView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		ViewOneTemplate temp = new ViewOneTemplate();
				
		Property property = ((ResponseProperty)(cr)).getProperty();
		
		setTitle(temp, property);
		setPropertyInfo(temp, property);
		setPropertyImage(temp, property);
		setMenuButtons(temp, property);
		
		HtmlUtils.identHTML(temp.getHtml());
		return temp.getHtml().toString();
	}
	
	private static void setPropertyInfo(ViewOneTemplate temp, Property property){	
		temp.addInfo(getOwnerTag(property.getOwner()));
		temp.addInfo(getSortTag(property.getSort()));
		temp.addInfo(getPriceTag(property.getPrice()));
		temp.addInfo(getLocationTag(property.getLocation()));
		temp.addInfo(getTextTag(property.getText()));
	}

	private static Tag getOwnerTag(Client owner) {
		String userName = owner.getUsername();
		
		Tag ownerTag = getEntityInfoTag("Owner : ");
		Tag link = creatLink(userName, "/users/"+userName);
		ownerTag.addTag(link);
		
		return ownerTag;
	}
	
	private static Tag getSortTag(String sort){
		Tag sortTag = getEntityInfoTag("Sort : ");
		Tag link = creatLink(sort, "/properties/type/"+sort);
		sortTag.addTag(link);
		
		return sortTag;
	}

	private static Tag getPriceTag(int price){
		Tag priceTag = getEntityInfoTag("Price : "+price+" $");
		
		return priceTag;
	} 

	private static Tag getTextTag(String text){
		Tag textTag = getEntityInfoTag("Text : "+text);
		
		return textTag;
	}

	private static Tag getLocationTag(Local local){
		Tag locationTag = getEntityInfoTag("Location : ");
		
		String location = local.getRegion()+"-"+ local.getLocale();
		Tag link = creatLink(location, "/properties/location/"+location);
		
		locationTag.addTag(link);
		
		return locationTag;
	} 
	
	private static void setTitle(ViewOneTemplate temp, Property property) {
		String title = "Property : " + property.getId();
		setSimpleTitle(temp, title);
	}
	
	private static void setPropertyImage(ViewOneTemplate temp, Property property){
		String wantedImage = PROPERTIES_IMAGES_PATH + property.getId()+".jpg";
		
		temp.setImage(getImagePath(wantedImage, DEFAULT_PROPERTY_IMAGE_PATH));
	}

	private static void setMenuButtons(ViewOneTemplate temp, Property property){
		temp.addButtonToLeftMenu(ALL_PROPERTIES_BUTTON_PATH, "/properties");
		temp.addButtonToLeftMenu(RENTALS_FOR_PROPERTY_BUTTON_PATH, "/properties/"+property.getId()+"/rentals");
	}
	
}

