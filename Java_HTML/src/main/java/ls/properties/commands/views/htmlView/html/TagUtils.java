package ls.properties.commands.views.htmlView.html;

import java.util.Collection;
import java.util.Iterator;

abstract class TagUtils {
	
	static String getIdentatiton(int identationLevel){
		StringBuilder sb = new StringBuilder();
		int count = 0;
		
		while(count++ < identationLevel)
			sb.append("\t");
		
		return sb.toString();
	}
	
	static String getAttributes(Collection<Attribute> at){
		Iterator<Attribute> atts = at.iterator();
		StringBuilder sb = new StringBuilder();
		while(atts.hasNext())
			sb.append(atts.next().toString()).append(" ");
		
		return sb.toString();
	}
	
	static String getTags(Collection<Tag> t){
		StringBuilder sb = new StringBuilder();
		Iterator<Tag> tags = t.iterator();
		
		while(tags.hasNext()){
			sb.append(tags.next().toString());
			if(tags.hasNext())
				sb.append("\n");
		}
			
		return sb.toString();
	}

}
