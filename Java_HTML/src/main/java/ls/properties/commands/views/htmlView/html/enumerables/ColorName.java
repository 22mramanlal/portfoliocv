package ls.properties.commands.views.htmlView.html.enumerables;

public enum ColorName {
	
	Black("#000000"),
	Blue("#0000FF"),
	BlueDark("##00008B"),
	BlueLight("#ADD8E6"),
	BlueMarine("#084B8A"),
	BlueTurque("#2E9AFE"),
	Brown("#8B4513"),		
	Gray("#808080"),
	GrayDark("#2E2E2E"),
	GrayLight("#C1C1C1"),
	Green("#008000"),
	GreenDark("#006400"),
	GreenLight("#90EE90"),
	GreenWater("#90EE90"),
	OliveDrab("#6B8E23"),
	Orange("#FF8C00"),
	OrangeDark("#FF4500"),
	OrangeLight("#FF8330"),
	Pink("#FF1493"),
	Purple("#800080"),
	Red("#FF0000"),
	RedDark("#8B0000"),
	Yellow("#FFFF00"),
	White("#FFFFFF");


	private final String color;
	ColorName(String code){
		this.color = code;
	}
	@Override
	public String toString() {
		return this.color;
	}
	
}
