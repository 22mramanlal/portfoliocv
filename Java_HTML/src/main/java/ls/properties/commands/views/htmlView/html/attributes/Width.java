package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;

public class Width extends Attribute {
	
	private static String widthAttName = "width";
	private int value;
	
	
	public Width(int value) {
		this.value = value;
	}

	@Override
	protected String getAttributeName() {
		return Width.widthAttName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value);
	}
	


}
