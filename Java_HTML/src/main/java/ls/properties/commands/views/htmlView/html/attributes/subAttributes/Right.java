package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Format;

public class Right extends SubAttribute {

	private static String rightSubAttributeName = "right";
	private int value;
	private Format format;
	
	public Right(int value, Format format) {
		this.value = value;
		this.format = format;
	}

	@Override
	protected String getAttributeName() {
		return Right.rightSubAttributeName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value) + format.toString();
	}

}
