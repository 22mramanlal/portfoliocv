package ls.properties.commands.views.jsonView.homePage;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.views.jsonView.JsonView;

public class HomePageJsonView extends JsonView {

	@Override
	protected String getRepName() {
		return "Home Page";
	}

	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		return "{\n\t\"To homepage we just have representation in text/html\":\""
						+ "For another options see:\nPress OPTION / for help\"\n}";
	}

}
