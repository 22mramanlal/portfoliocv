package ls.properties.commands.views.jsonView.entities.rentals;

import ls.properties.commands.views.jsonView.JsonSimpleObject;
import ls.properties.domain.entities.Rental;

public interface IRentalConstructJson {

	public JsonSimpleObject constructJsonRental(Rental r);
}
