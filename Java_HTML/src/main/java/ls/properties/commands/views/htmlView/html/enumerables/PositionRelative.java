package ls.properties.commands.views.htmlView.html.enumerables;

public enum PositionRelative {

	Static("static"),
	Absolute("absolute"),
	Fixed("fixed"),
	Relative("relative"),
	Initial("initial"),
	Inherit("inherit");


	private final String position;
	
	PositionRelative(String position){
		this.position = position;
	}
	@Override
	public String toString() {
		return this.position;
	}


}
