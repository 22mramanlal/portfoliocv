package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.PositionAbsolute;

public class BackGroundPosition extends SubAttribute {
	
	private static String backGroundPositionSubAttributeName = "background-position";
	private PositionAbsolute position;
	
	public BackGroundPosition(PositionAbsolute position) {
		this.position = position;
	}

	@Override
	protected String getAttributeName() {
		return BackGroundPosition.backGroundPositionSubAttributeName;
	}

	@Override
	protected String getValue() {
		return position.toString();
	}

}
