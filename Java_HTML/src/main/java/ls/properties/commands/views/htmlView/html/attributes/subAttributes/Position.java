package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.PositionRelative;

public class Position extends SubAttribute {

	private static String positionSubAttributeName = "position";
	private PositionRelative position;
	
	public Position(PositionRelative position) {
		this.position = position;
	}

	@Override
	protected String getAttributeName() {
		return Position.positionSubAttributeName;
	}

	@Override
	protected String getValue() {
		return position.toString();
	}

}
