package ls.properties.commands.views.textView.entities.clients;

import java.util.Iterator;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseClients;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Client;

public class ClientsTextView extends TextView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		List<Client> list = (List<Client>)((ResponseClients) cr).getClients();
		if(	list == null || list.isEmpty())
			return "There is no Client";
		
		
		StringBuilder sb = new StringBuilder();
		sb.append(prepareInfo(list));
		
		return sb.toString();	
	}
	
	private String prepareInfo(List<Client> list){
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		sb.append("\tClient\n");
		
		Iterator<Client> iter = list.iterator();

		while(iter.hasNext()){
			sb.append(constructClientList(iter.next()));
			sb.append("\n");
		}

		return sb.toString();
	}
	
	private String constructClientList(Client c) {

		 StringBuilder sb = new StringBuilder();

		  sb.append("\t\t>");
		  sb.append(c.getUsername());

	   return sb.toString();
	}
	
}
