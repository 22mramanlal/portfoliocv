package ls.properties.commands.views.htmlView.html.enumerables;

public enum CharsetFormat {
	Utf8("UTF-8"),
	Utf16("UTF-16");

	private final String format;
	
	private CharsetFormat(String format){
		this.format = format;
	}
	@Override
	public String toString() {
		return this.format;
	}
}
