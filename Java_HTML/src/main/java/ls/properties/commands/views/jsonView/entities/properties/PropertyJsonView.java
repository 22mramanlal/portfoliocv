package ls.properties.commands.views.jsonView.entities.properties;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseProperty;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonSimpleObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;

public class PropertyJsonView extends JsonView {

	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		
		ResponseProperty rp = (ResponseProperty)cr;
		JsonSimpleObject j = EntitiesUtilsJsonView.constructJsonProperty(rp.getProperty());
		JsonObject json = new JsonObject(1);
		json.addJson(j);
		json.setTitle(getRepName());
		return json.toString();
	}

	@Override
	protected String getRepName() {
		return "Property";
	}

}
