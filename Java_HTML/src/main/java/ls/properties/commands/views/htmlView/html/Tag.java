package ls.properties.commands.views.htmlView.html;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import ls.properties.commands.views.htmlView.html.enumerables.TagName;

public class Tag {
	protected Collection<Attribute> atts = new LinkedList<Attribute>();
	protected Collection<Tag> tags = new LinkedList<Tag>();
	
	protected int identationLevel;
	public void setIdentatio(int identation){ this.identationLevel = identation;}
	public int getIdentation(){ return identationLevel;}
	
	private String value;
	public String getValue(){return value;}
	public void setValue(String value){this.value = value;}
	
	private TagName tagName;
	
	public Tag(TagName tagName){
		this(tagName, "");
	}
	
	public Tag(TagName tagName, String value){
		this.tagName = tagName;
		this.value = value;
	}
		
	public void addAttribute(Attribute a){
		atts.add(a);
	}
	public void removeAttribute(Attribute a){
		atts.remove(a);
	}
	
	public void addTag(Tag tag){
		tags.add(tag);
	}
	public void removeTag(Tag tag){
		tags.remove(tag);
	}
		
	protected void identing(int base){
		this.identationLevel = base;
		Iterator<Tag> it = tags.iterator();
		
		while(it.hasNext())
			it.next().identing(base+1);
		
	}
	
	@Override
	public String toString(){	
		
		StringBuilder sb = new StringBuilder(TagUtils.getIdentatiton(identationLevel)).append("<");
		sb.append(tagName.toString()).append(" ").append(TagUtils.getAttributes(atts)).append(">");
		
		sb.append(getValue());
		
		if(tags.size() != 0){
			sb.append("\n");
			sb.append(TagUtils.getTags(tags));
		}
		
		if(tags.size() != 0){
			sb.append("\n");
			sb.append(TagUtils.getIdentatiton(identationLevel));
		}
	
		sb.append("</").append(tagName.toString()).append(">");
		
		return sb.toString();
	}
}
