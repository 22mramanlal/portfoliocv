package ls.properties.commands.views.jsonView.entities.properties;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOfType;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;

public class PropertiesTypeJsonView extends JsonView {

	private String type;

	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		
		ResponsePropertiesOfType rp = (ResponsePropertiesOfType)cr;
		type = rp.getType();
		JsonObject json = EntitiesUtilsJsonView.constructJsonListProp(rp.getProperties());
		json.setTitle(getRepName());
		return json.toString();
	}

	@Override
	protected String getRepName() {
		return "All properties for type: " + type;
	}

}
