package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Format;


public class Left extends SubAttribute {

	private static String leftSubAttributeName = "left";
	private int value;
	private Format format;
	
	public Left(int value, Format format) {
		this.value = value;
		this.format = format;
	}

	@Override
	protected String getAttributeName() {
		return Left.leftSubAttributeName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value) + format.toString();
	}

}
