package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Style;

public class BorderStyle extends SubAttribute {
	
	private static String borderStyleSubAttributeName = "border-style";
	private Style style;
	
	
	
	public BorderStyle(Style style) {
		this.style = style;
	}

	@Override
	protected String getAttributeName() {
		return BorderStyle.borderStyleSubAttributeName;
	}

	@Override
	protected String getValue() {
		return style.toString();
	}

}
