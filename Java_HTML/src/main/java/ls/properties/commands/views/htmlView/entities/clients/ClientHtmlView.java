package ls.properties.commands.views.htmlView.entities.clients;

import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.ALL_OWNERS_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.DEFAULT_OWNER_IMAGE_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.RENTALS_OF_OWNER_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.USERS_IMAGES_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.setSimpleTitle;

import java.util.Collection;
import java.util.Iterator;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseClient;
import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlUtils;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.attributes.Size;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;
import ls.properties.commands.views.htmlView.templates.ViewOneTemplate;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Property;

public class ClientHtmlView extends HtmlView {
	
	@Override
	public String getRepresentation(ICommandResponse cr) {
		ViewOneTemplate temp = new ViewOneTemplate();
				
		Client c = ((ResponseClient)(cr)).getClient();
		Collection<Property> properties = ((ResponseClient)(cr)).getProperties();
		
		setTitle(temp, c);
		setClientInfo(temp, c, properties);
		setClientImage(temp, c);
		setMenuButtons(temp, c);
		
		HtmlUtils.identHTML(temp.getHtml());
		return temp.getHtml().toString();
	}
	
	
	private static void setClientInfo(ViewOneTemplate temp, Client c, Collection<Property> p){
		temp.addInfo(getFullNameTag(c.getFullname()));
		temp.addInfo(getEmailTag(c.getEmail()));
		temp.addInfo(getPropertiesTag(p));
	}
	
	private static Tag getFullNameTag(String fullName){
		Tag fullNameTag = getEntityInfoTag("Full Name : "+fullName);
		
		return fullNameTag;
	}
	
	private static Tag getEmailTag(String email){
		Tag emailTag = getEntityInfoTag("Email : "+email);
		
		return emailTag;
	}
	
	private static Tag getPropertiesTag(Collection<Property> properties) {
		Tag font = new Tag(TagName.Font, "Properties Owned : ");
		
		if(properties == null || properties.isEmpty())
			font.setValue("Properties Owned : This user doesn't have any property" );
			
		else{
			Iterator<Property> it = properties.iterator();

			for(int i=0; it.hasNext(); ++i){
				if(i%6==0 && i!=0)
					font.addTag(new Tag(TagName.Br));
				Property p = it.next();
				String id = Integer.toString(p.getId());
				String path = "/properties/details/"+p.getId();
				Tag a = creatLink(id,path);
				font.addTag(a);	
			}
		}
		
		font.addAttribute(new Size(5));
		return font;
	}
	
 	private static void setClientImage(ViewOneTemplate temp, Client c){
		String wantedImage = USERS_IMAGES_PATH+c.getUsername()+".jpg";
		
		temp.setImage(getImagePath(wantedImage, DEFAULT_OWNER_IMAGE_PATH));
	}
	
	private static void setTitle(ViewOneTemplate temp, Client c) {
		String title = "User : " + c.getUsername();
		setSimpleTitle(temp, title); 
	}
	
	private static void setMenuButtons(ViewOneTemplate temp, Client c){
		temp.addButtonToLeftMenu(ALL_OWNERS_BUTTON_PATH, "/users");
		temp.addButtonToLeftMenu(RENTALS_OF_OWNER_BUTTON_PATH, "/users/"+c.getUsername()+"/rentals");
	}
	


}
