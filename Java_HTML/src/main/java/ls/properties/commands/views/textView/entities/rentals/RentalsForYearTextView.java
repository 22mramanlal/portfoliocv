package ls.properties.commands.views.textView.entities.rentals;

import java.util.Iterator;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsForYear;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;


public class RentalsForYearTextView extends TextView {
	
	@Override
	public String getRepresentation(ICommandResponse cr) {
		List<Rental> r = (List<Rental>)((ResponseRentalsForYear) cr).getRentals();
		if(r == null || r.isEmpty())
			return "There is no Rentals.";
		
		
		StringBuilder sb = new StringBuilder();
		 
		sb.append(prepareInfo(r, ((ResponseRentalsForYear) cr).getProperty(), ((ResponseRentalsForYear) cr).getYear()));

		return sb.toString();
	}

	

	private String prepareInfo(List<Rental> r, Property p, int year){	
		Iterator<Rental> iter = r.iterator();
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nRentals for this property Id: ");
		sb.append(p.getId());
		sb.append(", in this year: ");
		sb.append(year);
		sb.append("\n\n");
		
		sb.append(getTableRentalPropertyHeader());
		while(iter.hasNext())
			sb.append(prepareTable(iter.next()));
		
		return sb.toString();
	}
	
	
	
	
	
	private String prepareTable(Rental r){	
		 StringBuilder sb = new StringBuilder();

		 sb.append(getTableRentalPropertyInfo(r));
		 sb.append("\n");
		 sb.append(putSeparatorLine(sb.length()));
		 sb.append("\n");

		return sb.toString();
	}
	
	private String getTableRentalPropertyHeader(){ 
		StringBuilder sb = new StringBuilder();
		
		appendToString(sb,"Id");
		appendToString(sb,"Renter");
		
		sb.append("\n");
		sb.append(putSeparatorLine(sb.length()));
		sb.append("\n");
		
		return sb.toString();

	}
	
	private String getTableRentalPropertyInfo(Rental r) {
		StringBuilder sb  = new StringBuilder();
		  
		  appendToString(sb, Integer.toString(r.getProperty().getId()));
		  appendToString(sb,r.getRenter().getUsername());
		  
	     return sb.toString();
		
	}
	
}
