package ls.properties.commands.views;

import ls.properties.commands.responses.ICommandResponse;

public interface IView {

	public String getRepresentation(ICommandResponse cr);
}
