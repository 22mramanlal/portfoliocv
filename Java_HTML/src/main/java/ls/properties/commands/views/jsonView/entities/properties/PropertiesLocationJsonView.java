package ls.properties.commands.views.jsonView.entities.properties;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOnLocation;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;

public class PropertiesLocationJsonView extends JsonView {

	StringBuilder location;
	
	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		
		ResponsePropertiesOnLocation rp = (ResponsePropertiesOnLocation)cr;
		
		location = new StringBuilder(rp.getLocal().getRegion());
		location.append(", ").append(rp.getLocal().getLocale());
		
		JsonObject json = EntitiesUtilsJsonView.constructJsonListProp(rp.getProperties());
		json.setTitle(getRepName());
		return json.toString();
	}

	@Override
	protected String getRepName() {
		return "All properties for location: " +  location;
	}

}
