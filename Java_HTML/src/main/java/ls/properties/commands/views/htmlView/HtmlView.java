package ls.properties.commands.views.htmlView;

import ls.properties.commands.views.IView;
import ls.properties.commands.views.htmlView.html.HtmlFile;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.attributes.Face;
import ls.properties.commands.views.htmlView.html.attributes.HRef;
import ls.properties.commands.views.htmlView.html.attributes.Size;
import ls.properties.commands.views.htmlView.html.attributes.Src;
import ls.properties.commands.views.htmlView.html.attributes.Style;
import ls.properties.commands.views.htmlView.html.attributes.subAttributes.Left;
import ls.properties.commands.views.htmlView.html.attributes.subAttributes.Position;
import ls.properties.commands.views.htmlView.html.attributes.subAttributes.Right;
import ls.properties.commands.views.htmlView.html.attributes.subAttributes.Top;
import ls.properties.commands.views.htmlView.html.enumerables.FontStyle;
import ls.properties.commands.views.htmlView.html.enumerables.Format;
import ls.properties.commands.views.htmlView.html.enumerables.PositionRelative;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;
import ls.properties.utils.IOFiles;

public abstract class HtmlView implements IView{	
	
	public static Tag getEntityTag(String link, String text){
		Tag li = new Tag(TagName.Li);
		
		Tag linkTag = new Tag(TagName.A);
		linkTag.addAttribute(new HRef(link));
		
		Tag font  = new Tag(TagName.Font, text);
		font.addAttribute(new Size(16));
		font.addAttribute(new Face(FontStyle.Arial));
		
		linkTag.addTag(font);
		
		li.addTag(linkTag);
		
		return li;
	}

	public static String getImagePath(String wantedFilePath, String defaultFilePath){
		if(IOFiles.existsFile(wantedFilePath))
			return wantedFilePath;
			
		return defaultFilePath;
	}
	
	protected static Tag getEntityInfoTag(String text){
		Tag font = new Tag(TagName.Font, text);
		font.addAttribute(new Size(5));
		
		return font;
	} 
	
	protected static Tag creatLink(String text, String link){
		Tag a = new Tag(TagName.A, text);
		a.addAttribute(new HRef(link));
		
		return a;
	} 
	
	public static Tag creatButton(String imagePath, String link) {
		Tag a = creatLink("", link);

		Tag image = new Tag(TagName.Img);
		image.addAttribute(new Src(imagePath));
		a.addTag(image);

		return a;
	}
	
	public static void setButton(HtmlFile htmlFile, String imagePath, String link, boolean fromLeft, int left, int top){
		Tag div = getDivTag(left, fromLeft, top);
		
		div.addTag(creatButton(imagePath, link));
		
		htmlFile.addBodyTag(div);
	} 
	
	public static Tag getDivTag(int leftRight, boolean fromLeft, int top){
		Tag div = new Tag(TagName.Div);
		Style style = new Style("");
		style.addSubAttribute(new Position(PositionRelative.Absolute));
		
		if(fromLeft)
			style.addSubAttribute(new Left(leftRight, Format.Percentage));
		else
			style.addSubAttribute(new Right(leftRight, Format.Percentage));
	
		style.addSubAttribute(new Top(top, Format.Percentage));
		div.addAttribute(style);
		
		return div;
	} 	
}
