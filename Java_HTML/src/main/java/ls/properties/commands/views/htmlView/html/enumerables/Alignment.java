package ls.properties.commands.views.htmlView.html.enumerables;

public enum Alignment {
	Center("center"),
	Right("right"),
	Left("left");
	
    private final String alignment;
    Alignment(String align){
        this.alignment = align;
    }
    @Override
    public String toString() {
        return this.alignment;
    }
}
