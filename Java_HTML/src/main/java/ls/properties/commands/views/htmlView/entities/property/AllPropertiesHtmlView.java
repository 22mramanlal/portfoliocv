package ls.properties.commands.views.htmlView.entities.property;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.views.htmlView.templates.ViewAllTemplate;
import ls.properties.commands.views.htmlView.templates.ViewTemplateUtils;


public class AllPropertiesHtmlView extends PropertiesHtmlView {
	
	@Override
	protected void setTitle(ViewAllTemplate temp, ICommandResponse cr){
		ViewTemplateUtils.setSimpleTitle(temp, "All Properties");
	}

}
