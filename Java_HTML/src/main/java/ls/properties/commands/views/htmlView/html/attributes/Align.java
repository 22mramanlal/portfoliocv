package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;
import ls.properties.commands.views.htmlView.html.enumerables.Alignment;

public class Align extends Attribute{
	
	private static String alignAttributeName = "align"; 
	private Alignment value;
	
	public Align(Alignment align) {
		this.value = align;
	}
	
	@Override
	protected String getAttributeName() {
		return Align.alignAttributeName;
	}

	@Override
	protected String getValue() {
		return value.toString();
	}

}
