package ls.properties.commands.views.htmlView.entities.rentals;

import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.MAGNIFYING_GLASS_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.setComposeTitle;

import java.util.Collection;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentals;
import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlUtils;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.attributes.Size;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;
import ls.properties.commands.views.htmlView.templates.ViewAllTemplate;
import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Rental;

public abstract class RentalsHtmlView<T extends IEntity> extends HtmlView {
	
	@Override
	public String getRepresentation(ICommandResponse cr) {
		ViewAllTemplate temp = new ViewAllTemplate();
		
		T e = getResponseEntity(cr);
		Collection<Rental> rentals = ((ResponseRentals)(cr)).getRentals();
		
		setTitle(temp, e);
		setRentals(temp, rentals);
				
		HtmlUtils.identHTML(temp.getHtml());
		return temp.getHtml().toString();
	}

	private void setTitle(ViewAllTemplate temp, T e){
		String title = getTitle();
		String id = getEntityId(e);
		String link = getTitleLink(e);
		
		setComposeTitle(temp, title, link, id);
	}
	
	private void setRentals(ViewAllTemplate temp, Collection<Rental> rentals){
		if(rentals == null || rentals.isEmpty()){
			temp.addElementList(new Tag(TagName.Font, getNoRentalsMessage()));
			return;
		}
			
		for(Rental r : rentals)
			temp.addElementList(getInfoTagForRental(r));
		
	}
	
	private static Tag getInfoTagForRental(Rental r){
		String id = Integer.toString(r.getProperty().getId());
		String year = Integer.toString(r.getCy());
		String cw = Integer.toString(r.getCw());;
		
		Tag font = new Tag(TagName.Font);
		font.addAttribute(new Size(6));
		StringBuilder rentalPath = new StringBuilder();
		rentalPath.append("/properties/").append(id).append("/rentals/").append(year).append("/").append(cw);
		
		Tag rentalButton = creatButton(MAGNIFYING_GLASS_BUTTON_PATH, rentalPath.toString());
		Tag yearTag = creatLink(year, "/properties/"+id+"/rentals/"+r.getCy());
		Tag cwTag = new Tag(TagName.Font, cw);
		font.addTag(yearTag);
		font.addTag(cwTag);
		font.addTag(rentalButton);
		
		return font;
	}

	
	
	
	
	
	protected abstract String getTitle();
	protected abstract String getEntityId(T e);
	protected abstract String getTitleLink(T e);
	protected abstract String getNoRentalsMessage();
	protected abstract T getResponseEntity(ICommandResponse cr);
}
