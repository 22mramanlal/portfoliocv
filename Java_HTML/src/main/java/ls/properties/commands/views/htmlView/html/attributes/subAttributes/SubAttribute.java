package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

public abstract class SubAttribute {

	protected abstract String getAttributeName();
	protected abstract String getValue();
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder(getAttributeName());
		sb.append(":");
		sb.append(getValue());
		
		return sb.toString();
	}

}
