package ls.properties.commands.views.textView.entities.properties;

import java.util.Iterator;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOnLocation;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;

public class PropertiesLocationTextView extends TextView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		List<Property> p = (List<Property>)((ResponsePropertiesOnLocation) cr).getProperties();
		
		if(p == null || p.isEmpty())
			return "There is no Property";
	
		StringBuilder sb = new StringBuilder();
		
		sb.append(prepareInfo(p, (Local)((ResponsePropertiesOnLocation) cr).getLocal()));
		
		return sb.toString();
	}
	
	
	private String prepareInfo(List<Property> p, Local l){
		
		StringBuilder sb = new StringBuilder();
		Iterator<Property> iter = p.iterator();
		
		sb.append("\nProperties for this Location: ");
		sb.append(l.getLocale());
		sb.append("\n\n");
		sb.append(getTablePropertyLocationHeader());
	
		while(iter.hasNext())
			sb.append(prepareTable(iter.next()));
		
		return sb.toString();
	}
	
	private String prepareTable(Property p){	
		 StringBuilder sb = new StringBuilder();

		 sb.append(getTablePropertyLocationInfo(p));
		 sb.append("\n");
		 sb.append(putSeparatorLine(sb.length()));
		 sb.append("\n");

		return sb.toString();
	}
	
	private String getTablePropertyLocationHeader(){ 
		StringBuilder sb = new StringBuilder();
		
		appendToString(sb,"Id");
		appendToString(sb,"Region");
		appendToString(sb,"Locale");
		

		sb.append("\n");
		sb.append(putSeparatorLine(sb.length()));
		sb.append("\n");
		
		return sb.toString();

	}
	
	private String getTablePropertyLocationInfo(Property p) {
		StringBuilder sb  = new StringBuilder();
		
		  appendToString(sb,Integer.toString(p.getId()));
		  appendToString(sb,p.getLocation().getRegion());
		  appendToString(sb,p.getLocation().getLocale());
		 
	
	     return sb.toString();
		
	}
	
}
	