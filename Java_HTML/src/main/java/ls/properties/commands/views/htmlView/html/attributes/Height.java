package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;

public class Height extends Attribute {
	
	private static String heightAttName = "height";
	private int value;
	
	
	public Height(int value) {
		this.value = value;
	}

	@Override
	protected String getAttributeName() {
		return Height.heightAttName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value);
	}
	

}
