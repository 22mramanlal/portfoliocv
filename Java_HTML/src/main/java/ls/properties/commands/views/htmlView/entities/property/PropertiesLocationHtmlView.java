package ls.properties.commands.views.htmlView.entities.property;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOnLocation;
import ls.properties.commands.views.htmlView.templates.ViewAllTemplate;
import ls.properties.commands.views.htmlView.templates.ViewTemplateUtils;
import ls.properties.domain.entities.Local;

public class PropertiesLocationHtmlView extends PropertiesHtmlView {

	@Override
	protected void setTitle(ViewAllTemplate temp, ICommandResponse cr) {
		Local l = ((ResponsePropertiesOnLocation)(cr)).getLocal();
		StringBuilder sb = new StringBuilder();
		sb.append("Properties on ").append(l.getLocale()).append("-").append(l.getRegion());
		ViewTemplateUtils.setSimpleTitle(temp, sb.toString());
	}
	
}
