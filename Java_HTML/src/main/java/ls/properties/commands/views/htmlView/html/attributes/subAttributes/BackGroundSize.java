package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Format;


public class BackGroundSize extends SubAttribute {

	

	private static String backGroundSizeSubAttributeName = "background-size";
	Format format;
	private int height;
	private int width;
	
	public BackGroundSize(int width, int height, Format format) {
		this.height = height;
		this.width = width;
		this.format = format;
	}

	@Override
	protected String getAttributeName() {
		return BackGroundSize.backGroundSizeSubAttributeName;
	}

	@Override
	protected String getValue() {
		return height+format.toString()+" "+width+format.toString();
	}

}
