package ls.properties.commands.views.htmlView.html;

import ls.properties.commands.views.htmlView.html.enumerables.TagName;


public class HtmlFile {

	private Tag html;
	public Tag getHtml(){return html;}
	public void setHmlt(Tag html){
		this.html = html;
	}
			
	private Tag body;
	public Tag getBody(){return body;}
	public void setBody(Tag body){
		this.body = body;
	}
	
	
	private Tag head;
	public Tag getHead(){return head;}
	public void setHead(Tag head){
		this.head = head;
	}
	
	public HtmlFile(){
		this(new Tag(TagName.Head), new Tag(TagName.Body));
	}
	
	public HtmlFile(Tag head, Tag body){
		this.body = body;
		this.head = head;
		this.html = new Tag(TagName.Html);
		 
		 html.addTag(head);
		 html.addTag(body);
	}
	
	public void addBodyTag(Tag tag){
		body.addTag(tag);
	}
	
	public void addHeadTag(Tag tag){
		head.addTag(tag);
	}
	
	public void removeBodyTag(Tag tag){
		body.removeTag(tag);
	}
	
	public void removeHeadTag(Tag tag){
		head.removeTag(tag);
	}

	public void addBodyAttribute(Attribute a){
		body.addAttribute(a);
	}
	
	public void addHeadAttribute(Attribute a){
		head.addAttribute(a);
	}
		
	public void removeBodyAttribute(Attribute a){
		body.removeAttribute(a);
	}
	
	public void removeHeadAttribute(Attribute a){
		head.removeAttribute(a);
	}

	@Override
	public String toString(){
		return html.toString();
	}

}
