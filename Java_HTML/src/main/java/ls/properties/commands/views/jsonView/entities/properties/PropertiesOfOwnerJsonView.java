package ls.properties.commands.views.jsonView.entities.properties;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOfClient;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;

public class PropertiesOfOwnerJsonView extends JsonView {

	String username;
	
	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		
		ResponsePropertiesOfClient rpc = (ResponsePropertiesOfClient)cr;
		username = rpc.getClient().getUsername();
		JsonObject json = EntitiesUtilsJsonView.constructJsonListProp(rpc.getProperties());
		json.setTitle(getRepName());
		return json.toString();
	}

	@Override
	protected String getRepName() {
		return "Properties for user: " + username;
	}

}
