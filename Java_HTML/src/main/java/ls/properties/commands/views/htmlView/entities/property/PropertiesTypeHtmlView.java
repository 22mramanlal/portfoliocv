package ls.properties.commands.views.htmlView.entities.property;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOfType;
import ls.properties.commands.views.htmlView.templates.ViewAllTemplate;
import ls.properties.commands.views.htmlView.templates.ViewTemplateUtils;

public class PropertiesTypeHtmlView extends AllPropertiesHtmlView {

	@Override
	protected void setTitle(ViewAllTemplate temp, ICommandResponse cr){
		String type = ((ResponsePropertiesOfType)(cr)).getType();
		ViewTemplateUtils.setSimpleTitle(temp, "Properties of type "+type);
	}

}
