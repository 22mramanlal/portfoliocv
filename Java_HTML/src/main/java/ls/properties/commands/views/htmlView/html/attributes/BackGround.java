package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;

public class BackGround extends Attribute{

	private static String backGroundAttributeName = "background";
	private String path;

	public BackGround(String path) {
		this.path = path;
	}
	
	@Override
	protected String getAttributeName() {
		return BackGround.backGroundAttributeName;
	}

	@Override
	protected String getValue() {
		return path;
	}

}
