package ls.properties.commands.views.htmlView.homePage;

import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.DEFAULT_OWNER_IMAGE_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.DEFAULT_PROPERTY_IMAGE_PATH;
import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlFile;
import ls.properties.commands.views.htmlView.html.HtmlUtils;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.attributes.Align;
import ls.properties.commands.views.htmlView.html.attributes.HRef;
import ls.properties.commands.views.htmlView.html.attributes.Src;
import ls.properties.commands.views.htmlView.html.enumerables.Alignment;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;
import ls.properties.commands.views.htmlView.templates.ViewTemplateUtils;



public class HomePageHtmlView extends HtmlView{

	@Override
	public String getRepresentation(ICommandResponse cr) {
		HtmlFile htmlFile = ViewTemplateUtils.getTemplate();
		
		setPropertyButton(htmlFile);
		setClientButton(htmlFile);		
		
		HtmlUtils.identHTML(htmlFile);
		return htmlFile.toString();
	}

	private static void setPropertyButton(HtmlFile htmlFile){
		Tag div = getDivTag(10, true, 30);
		
		Tag button = getButton(DEFAULT_PROPERTY_IMAGE_PATH, Alignment.Left,"/properties");
			
		div.addTag(button);
		
		htmlFile.addBodyTag(div);
	}
	
	private static void setClientButton(HtmlFile htmlFile){
		Tag div = getDivTag(10, false, 30);
		
		Tag button = getButton(DEFAULT_OWNER_IMAGE_PATH, Alignment.Right, "/users");
		
		div.addTag(button);
		
		htmlFile.addBodyTag(div);
	}
	
	private static Tag getButton(String imagePath, Alignment align, String link){
		Tag image = new Tag(TagName.Img);
		image.addAttribute(new Src(imagePath));
		image.addAttribute(new Align(align));
		
		Tag linkTag = creatLink("", link);
		linkTag.addAttribute(new HRef(link));
		linkTag.addTag(image);
		
		return linkTag;
	}
	
}
