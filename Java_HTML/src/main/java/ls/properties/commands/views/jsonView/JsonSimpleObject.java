package ls.properties.commands.views.jsonView;

import ls.properties.exceptions.InvalidUserParametersException;

public class JsonSimpleObject {

	private String[] dataNames;
	private Object[] valNames;
	private int idxData = 0;
	private int idxVals = 0;
	
	public JsonSimpleObject(int data) {
		dataNames = new String[data];
		valNames = new Object[data];
	}
	
	public JsonSimpleObject(String data, Object val) {
		dataNames = new String[]{data};
		valNames = new Object[]{val};
	}

	public void addDataName(String dataName) {
		if(dataNames.length > idxData) {
			this.dataNames[idxData++] = dataName;
			return;
		}
		throw new InvalidUserParametersException("Exceeds Json parameters.");
	}

	public void addValNames(Object valName) {
		if(valNames.length > idxVals) {
			this.valNames[idxVals++] = valName;
			return;
		}

		throw new InvalidUserParametersException("Exceeds Json parameters.");
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder("\t");
		sb.append(JsonObject.begin);
		sb.append("\n");
		
		for (int i = 0; i < dataNames.length; i++) {
			sb.append("\t   \"");
			sb.append(dataNames[i]);
			sb.append("\":\"");
			sb.append(valNames[i]);
			sb.append("\"");
			
			if(i < dataNames.length)
				sb.append(",");
			sb.append("\n");
		}
		
		sb.append("\t");
		sb.append(JsonObject.end);
		return sb.toString();
	}
}
