package ls.properties.commands.views.textView.entities.clients;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseClient;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Client;

public class ClientTextView extends TextView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		Client c = (Client) ((ResponseClient)cr).getClient();
		 StringBuilder sb = new StringBuilder();
		if(	c == null)
			return "There is no Client";
		
		sb.append(prepareInfo(c));
		
		return sb.toString();
	}

	private String prepareInfo(Client c){

		 StringBuilder sb = new StringBuilder();
		 sb.append("\n");
		 sb.append(getTableUserHeader());

		 sb.append(getTableUserInfo(c));

		 sb.append("\n\n");

		return sb.toString();
	}

	private String getTableUserHeader(){
		StringBuilder sb = new StringBuilder();
		
		appendToString(sb,"username");
		sb.append("\t");
		appendToString(sb,"email");
		sb.append("\t");
		appendToString(sb,"fullname");
		sb.append("\t\n");
		sb.append(putSeparatorLine(sb.length()));
		sb.append("\n");
		
		return sb.toString();
	}
	
	private String getTableUserInfo(Client c) {
		
		StringBuilder sb  = new StringBuilder();

		  appendToString(sb,c.getUsername());
		  sb.append("\t");
		  appendToString(sb,c.getEmail());
		  sb.append("\t");	
		  appendToString(sb,c.getFullname());
		  sb.append("\t\n");		
		  sb.append(putSeparatorLine(sb.length()));
	     return sb.toString();
		
	}
	
	

}
