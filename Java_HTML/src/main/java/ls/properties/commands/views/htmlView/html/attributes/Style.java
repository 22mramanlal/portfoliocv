package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;

public class Style extends Attribute {

	private static String styleAttributeName = "style";
	private String value;
	
	
	public Style(String value) {
		this.value = value;
	}
	
	@Override
	protected String getAttributeName() {
		return Style.styleAttributeName;
	}

	@Override
	protected String getValue() {
		return value;
	}

}
