package ls.properties.commands.views.jsonView;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.views.IView;

public abstract class JsonView implements IView {
	
	@Override
	public String getRepresentation(ICommandResponse cr) {
		return getObjectRepresentation(cr);
	}

	protected abstract String getRepName();
	protected abstract String getObjectRepresentation(ICommandResponse cr);

}
