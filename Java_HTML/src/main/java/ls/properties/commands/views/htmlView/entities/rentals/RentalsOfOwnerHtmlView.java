package ls.properties.commands.views.htmlView.entities.rentals;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsOfClient;
import ls.properties.domain.entities.Client;

public class RentalsOfOwnerHtmlView extends RentalsHtmlView<Client> {

	@Override
	protected String getTitle() {
		return "Rentals of owner";
	}

	@Override
	protected String getEntityId(Client e) {
		return e.getUsername();
	}

	@Override
	protected String getTitleLink(Client e) {
		return "/users/"+e.getUsername();
	}

	@Override
	protected String getNoRentalsMessage() {
		return "There are to rentals for this owner";
	}

	@Override
	protected Client getResponseEntity(ICommandResponse cr) {
		return ((ResponseRentalsOfClient)(cr)).getClient();
	}
}
