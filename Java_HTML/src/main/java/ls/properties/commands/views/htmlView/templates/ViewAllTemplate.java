package ls.properties.commands.views.htmlView.templates;

import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;

public class ViewAllTemplate extends ViewTemplate{
	
	private Tag listDiv = HtmlView.getDivTag(45, true, 35);
	
	public ViewAllTemplate() {
		this.htmlFile.addBodyTag(listDiv);
	}
	
	public void addElementList(Tag tag){
		listDiv.addTag(tag);
		listDiv.addTag(new Tag(TagName.Br));
	}

}
