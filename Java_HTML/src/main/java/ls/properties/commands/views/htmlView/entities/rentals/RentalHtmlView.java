package ls.properties.commands.views.htmlView.entities.rentals;

import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.DEFAULT_PROPERTY_IMAGE_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.PROPERTIES_IMAGES_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.RENTALS_FOR_PROPERTY_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.RENTALS_OF_OWNER_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.setSimpleTitle;
import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseRental;
import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlUtils;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.templates.ViewOneTemplate;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;

public class RentalHtmlView extends HtmlView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		ViewOneTemplate temp = new ViewOneTemplate();
				
		Rental rental = ((ResponseRental)(cr)).getRental();
		
		setTitle(temp, rental);
		setRentalInfo(temp, rental);
		setRentalImage(temp, rental);
		setMenuButtons(temp, rental);
		
		HtmlUtils.identHTML(temp.getHtml());
		return temp.getHtml().toString();
	}
	
	private static void setRentalInfo(ViewOneTemplate temp, Rental r){				
		temp.addInfo(getPropertyTag(r.getProperty()));
		temp.addInfo(getRenterTag(r.getRenter()));
		temp.addInfo(getWeekTag(r.getCw()));
		temp.addInfo(getRentStateTag(r.getRentState()));
		temp.addInfo(getYearTag(r));
	}
	
	private static Tag getPropertyTag(Property property){
		String id = Integer.toString(property.getId());
		
		Tag propertytag = getEntityInfoTag("Property : ");
		Tag link = creatLink(id, "/properties/details/"+id);
		
		propertytag.addTag(link);
		
		return propertytag;
	}
	
	private static Tag getRenterTag(Client renter){
		Tag renterTag = getEntityInfoTag("Renter : ");
		Tag link = creatLink(renter.getUsername(), "/users/"+renter.getUsername());
		
		renterTag.addTag(link);
		
		return renterTag;
	}
	
	private static Tag getWeekTag(int week){
		Tag weekTag = getEntityInfoTag("Week : "+ week);
		
		return weekTag;
	}

	private static Tag getYearTag(Rental rental){
		String id = Integer.toString(rental.getProperty().getId());
		String cy = Integer.toString(rental.getCy());
		
		Tag yearTag = getEntityInfoTag("Year : ");
		
		StringBuilder yearLink = new StringBuilder();
		yearLink.append("/properties/").append(id).append("/rentals/").append(cy);
		
		Tag link = creatLink(cy, yearLink.toString());
		
		yearTag.addTag(link);
		
		return yearTag;
	}

	private static Tag getRentStateTag(boolean rentState){
		String text = (rentState)? "Accepted" : "Not Accepted";
		Tag rentStateTag = getEntityInfoTag("Rent State : "+text);
		
		return rentStateTag;
	}
	
	private static void setTitle(ViewOneTemplate temp, Rental r) {
		StringBuilder title = new StringBuilder();
		title.append("Rental for property : ").append(r.getProperty().getId()).append(" on ");
		title.append(r.getCw()).append(" of ").append(r.getCy());
		
		setSimpleTitle(temp, title.toString());
	}
	
	private static void setRentalImage(ViewOneTemplate temp, Rental r){
		String wantedImage = PROPERTIES_IMAGES_PATH + r.getProperty().getId()+".jpg";
		
		temp.setImage(getImagePath(wantedImage, DEFAULT_PROPERTY_IMAGE_PATH));
	}

	private static void setMenuButtons(ViewOneTemplate temp, Rental rental){
		String userName = rental.getRenter().getUsername();
		String id = Integer.toString(rental.getProperty().getId());
		
		temp.addButtonToLeftMenu(RENTALS_OF_OWNER_BUTTON_PATH, "/users/"+userName+"/rentals");
		temp.addButtonToLeftMenu(RENTALS_FOR_PROPERTY_BUTTON_PATH, "/properties/"+id+"/rentals");
	}
}
