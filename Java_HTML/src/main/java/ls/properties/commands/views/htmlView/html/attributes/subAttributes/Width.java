package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Format;


public class Width extends SubAttribute {
	
	private static String widthSubAttributeName = "width";
	private int value;
	private Format format;

	public Width(int value, Format format) {
		this.value = value;
		this.format = format;
	}
	
	@Override
	protected String getAttributeName() {
		return Width.widthSubAttributeName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value) + format.toString();
	}

}
