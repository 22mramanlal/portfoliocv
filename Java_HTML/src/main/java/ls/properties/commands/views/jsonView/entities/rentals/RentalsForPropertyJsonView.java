package ls.properties.commands.views.jsonView.entities.rentals;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsForProperty;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonSimpleObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;
import ls.properties.domain.entities.Rental;

public class RentalsForPropertyJsonView extends JsonView implements IRentalConstructJson {
	
	int id;
	
	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		
		ResponseRentalsForProperty rrp = (ResponseRentalsForProperty)cr;
		id = rrp.getProperty().getId();
		JsonObject json = EntitiesUtilsJsonView.constructJsonListRentals(rrp.getRentals(), this);
		json.setTitle(getRepName());
		
		return json.toString();
	}

	@Override
	protected String getRepName() {
		return "List of rentals for property: " + id;
	}

	@Override	
	public JsonSimpleObject constructJsonRental(Rental r) {

		JsonSimpleObject j = new JsonSimpleObject(4);

		j.addDataName("Property");
		j.addDataName("Year");
		j.addDataName("Week");
		j.addDataName("Renter");
		j.addValNames(r.getProperty().getId());
		j.addValNames(r.getCy());
		j.addValNames(r.getCw());
		j.addValNames(r.getRenter().getUsername());

		return j;
	}

}
