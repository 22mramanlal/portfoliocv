package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Format;

public class FontSize extends SubAttribute {
	
	private static String fontSizeSubAttributeName = "font-size";
	private int value;
	private Format format;

	public FontSize(int value, Format format) {
		this.value = value;
		this.format = format;
	}
	
	@Override
	protected String getAttributeName() {
		return FontSize.fontSizeSubAttributeName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value) + format.toString();
	}

}
