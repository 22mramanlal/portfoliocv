package ls.properties.commands.views.textView.homePage;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.views.textView.TextView;

public class HomePageTextView extends TextView {
	
	@Override
	public String getRepresentation(ICommandResponse cr) {
		return "Press OPTION / for help";
	}

}
