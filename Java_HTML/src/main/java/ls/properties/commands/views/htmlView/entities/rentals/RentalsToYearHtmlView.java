package ls.properties.commands.views.htmlView.entities.rentals;

import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.MAGNIFYING_GLASS_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.NEXT_YEAR_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.PREVIOUS_YEAR_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.RENTALS_FOR_PROPERTY_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.RENTALS_OF_OWNER_BUTTON_PATH;
import static ls.properties.commands.views.htmlView.templates.ViewTemplateUtils.setComposeTitle;

import java.util.Collection;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsForYear;
import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlUtils;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.attributes.Size;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;
import ls.properties.commands.views.htmlView.templates.ViewAllTemplate;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;

public class RentalsToYearHtmlView extends HtmlView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		ViewAllTemplate temp = new ViewAllTemplate();
		
		Property p = ((ResponseRentalsForYear)(cr)).getProperty();
		int year = ((ResponseRentalsForYear)(cr)).getYear();
		Collection<Rental> rentals = ((ResponseRentalsForYear)(cr)).getRentals();
		
		setTitle(temp, p, year);
		setRentals(temp, rentals);
		setMenuButtons(temp, p , year);
				
		HtmlUtils.identHTML(temp.getHtml());
		return temp.getHtml().toString();
	}


	private void setTitle(ViewAllTemplate temp, Property p, int year){
		String title = "Rentals for year " + year + " for property ";
		String id = Integer.toString(p.getId());
		String link = "/properties/details/" + id;
		setComposeTitle(temp, title, link, id);
	}
	
	private void setRentals(ViewAllTemplate temp, Collection<Rental> rentals){
		if(rentals == null || rentals.isEmpty()){
			temp.addElementList(new Tag(TagName.Font, "There are no rentals for this year"));
			return;
		}
			
		for(Rental r : rentals)
			temp.addElementList(getInfoTagForRental(r));
		
	}
	
	private static Tag getInfoTagForRental(Rental r){
		String id = Integer.toString(r.getProperty().getId());
		String cw = Integer.toString(r.getCw());
		String year = Integer.toString(r.getCy());
		String renterName = r.getRenter().getUsername();
		
		Tag font = new Tag(TagName.Font);
		font.addAttribute(new Size(6));
		
		StringBuilder rentalPath = new StringBuilder();
		rentalPath.append("/properties/").append(id).append("/rentals/").append(year).append("/").append(cw);
		
		Tag rentaltButton = creatButton(MAGNIFYING_GLASS_BUTTON_PATH, rentalPath.toString());
		
		Tag cwTag = new Tag(TagName.Font, cw);
		Tag renterTag = new Tag(TagName.Font, " for ");
		Tag renterLink = creatLink(renterName, "/users/"+renterName);
		renterTag.addTag(renterLink);
		
		font.addTag(cwTag);
		font.addTag(rentaltButton);
		font.addTag(renterTag);
		return font;
	}

	private void setMenuButtons(ViewAllTemplate temp, Property p, int year) {
		StringBuilder nextLink = new StringBuilder();
		nextLink.append("/properties/").append(p.getId()).append("/rentals/").append(year+1);
		StringBuilder previousLink = new StringBuilder();
		previousLink.append("/properties/").append(p.getId()).append("/rentals/").append(year-1);
		StringBuilder rentalsForPropertyLink = new StringBuilder();
		rentalsForPropertyLink.append("/properties/").append(p.getId()).append("/rentals");
		StringBuilder rentalsOfClientLink = new StringBuilder();
		rentalsOfClientLink.append("/users/").append(p.getOwner().getUsername()).append("/rentals");
		
		
		temp.addButtonToLeftMenu(PREVIOUS_YEAR_BUTTON_PATH, previousLink.toString());
		temp.addButtonToRightMenu(NEXT_YEAR_BUTTON_PATH, nextLink.toString());
		temp.addButtonToLeftMenu(RENTALS_FOR_PROPERTY_BUTTON_PATH, rentalsForPropertyLink.toString());
		temp.addButtonToLeftMenu(RENTALS_OF_OWNER_BUTTON_PATH, rentalsOfClientLink.toString());
	}
	
}
