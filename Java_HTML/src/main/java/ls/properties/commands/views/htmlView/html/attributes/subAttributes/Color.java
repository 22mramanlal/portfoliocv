package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.ColorName;


public class Color extends SubAttribute {

	private static String colorSubAttributeName = "color";
	private ColorName color;
	
	
	public Color(ColorName color) {
		this.color = color;
	}

	@Override
	protected String getAttributeName() {
		return Color.colorSubAttributeName;
	}

	@Override
	protected String getValue() {
		return color.toString();
	}

}
