package ls.properties.commands.views.textView.entities.rentals;

import java.util.Iterator;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsOfClient;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Rental;

public class RentalsOfOwnerTextView extends TextView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		List<Rental> r = (List<Rental>)((ResponseRentalsOfClient) cr).getRentals();
		if(r == null || r.isEmpty())
			return "There is no Rentals.";
		
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(prepareInfo(r, (Client) ((ResponseRentalsOfClient) cr).getClient()));
			
		return sb.toString();
	}
	
	private String prepareInfo(List<Rental> r, Client c){
		StringBuilder sb = new StringBuilder();
		Iterator<Rental> iter = r.iterator();
		
		sb.append("\nRentals of this Client: ");
		sb.append(c.getUsername());
		sb.append("\n");
		sb.append(getTableRentalOwnerHeader());
		while(iter.hasNext())
			sb.append(prepareTable(iter.next()));
		
		return sb.toString();
		
	}
	
	private String prepareTable(Rental r){	
		 StringBuilder sb = new StringBuilder();

		 sb.append(getTableRentalOwnerInfo(r));
		 sb.append("\n");
		 sb.append(putSeparatorLine(sb.length()));
		 sb.append("\n");

		return sb.toString();
	}
	
	private String getTableRentalOwnerHeader(){ 
		StringBuilder sb = new StringBuilder();
		
		appendToString(sb,"Id");
		appendToString(sb,"OwnerProperty");
		
		sb.append("\n");
		sb.append(putSeparatorLine(sb.length()));
		sb.append("\n");
		
		return sb.toString();

	}
	
	private String getTableRentalOwnerInfo(Rental r) {
		StringBuilder sb  = new StringBuilder();
		  
		appendToString(sb, Integer.toString(r.getProperty().getId()));
		appendToString(sb, r.getProperty().getOwner().getUsername());

	    return sb.toString();
		
	}

}
