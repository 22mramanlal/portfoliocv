package ls.properties.commands.views.jsonView;

import ls.properties.exceptions.InvalidUserParametersException;

public class JsonObject {

	private JsonSimpleObject[] data;
	public static final String begin = "{";
	public static final String end = "}";
	public static final String beginList = "[";
	public static final String endList = "]";
	private String title;
	private int idx = 0;
	
	public JsonObject(int elems){
		data = new JsonSimpleObject[elems];
	}
		
	private String startJson() {
		StringBuilder sb = new StringBuilder(begin);
		sb.append("\n\t\"");
		sb.append(title);
		sb.append("\": ");
		sb.append(beginList);
		sb.append("\n");

		return sb.toString();
	}

	private String endJson() {
		
		StringBuilder sb = new StringBuilder("\n   ");
		sb.append(endList);
		sb.append("\n");
		sb.append(end);

		return sb.toString();
	}
	
	public void addJson(JsonSimpleObject json) {
		if (data.length > idx) {
			this.data[idx++] = json;
			return;
		}

		throw new InvalidUserParametersException("Exceeds Json parameters.");
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder(startJson());
		
		for (int i = 0; i < data.length; i++) {
			if(data[i] == null)
				break;
			sb.append(data[i].toString());
			
			if(i < data.length - 1 && data.length - 1 != 0) {
				sb.append(",");
				sb.append("\n");
			}
			
		}
		
		sb.append(endJson());
		return sb.toString();
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
