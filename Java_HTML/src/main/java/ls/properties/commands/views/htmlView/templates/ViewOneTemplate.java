package ls.properties.commands.views.htmlView.templates;

import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.attributes.Height;
import ls.properties.commands.views.htmlView.html.attributes.Src;
import ls.properties.commands.views.htmlView.html.attributes.Width;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;

public class ViewOneTemplate extends ViewTemplate{

	private Tag imageDiv = HtmlView.getDivTag(20, false, 42);
	private Tag image;
	public void setImage(String imagePath) {
		if(this.image != null)
			imageDiv.removeTag(image);
		
		image = creatImage(imagePath);
		imageDiv.addTag(image);
	}
		
	private Tag infoDiv = HtmlView.getDivTag(20, true, 50);
	public void addInfo(Tag tag){
		infoDiv.addTag(tag);
		infoDiv.addTag(new Tag(TagName.Br));
	}
		
	public ViewOneTemplate() {
		this.htmlFile.addBodyTag(imageDiv);
		this.htmlFile.addBodyTag(infoDiv);
	}
	
	public static Tag creatImage(String imagePath){
		Tag image = new Tag(TagName.Img);
		image.addAttribute(new Src(imagePath));
		image.addAttribute(new Height(300));
		image.addAttribute(new Width(300));
		
		return image;
	}
	
}

