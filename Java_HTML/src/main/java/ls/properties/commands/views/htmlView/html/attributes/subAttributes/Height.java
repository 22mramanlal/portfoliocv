package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Format;

public class Height extends SubAttribute {

	private static String heigthSubAttributeName = "height";
	private int value;
	private Format format;

	public Height(int value, Format format) {
		this.value = value;
		this.format = format;
	}
	
	@Override
	protected String getAttributeName() {
		return Height.heigthSubAttributeName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value) + format.toString();
	}

}
