package ls.properties.commands.views.jsonView.entities.clients;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseClient;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonSimpleObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;

public class ClientJsonView extends JsonView {

	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		
		ResponseClient rc = (ResponseClient)cr;
		JsonSimpleObject j = EntitiesUtilsJsonView.constructJsonClient(rc.getClient()); 
		JsonObject json = new JsonObject(1);
		json.addJson(j);
		json.setTitle(getRepName());
		return json.toString();
	}

	@Override
	protected String getRepName() {
		return "User";
	}

}
