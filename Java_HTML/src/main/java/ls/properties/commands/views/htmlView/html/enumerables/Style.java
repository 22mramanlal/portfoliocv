package ls.properties.commands.views.htmlView.html.enumerables;

public enum Style {
	None("none"),
	Hidden("hidden"),
	Dotted("dotted"),
	Dashed("dashed"),
	Solid("solid"),
	Double("double"),
	Groove("groove"),
	Ridge("ridge"),
	Inset("inset"),
	Outset("outset"),
	Initial("initial"),
	Inherit("inherit");
 
    private final String style;
	
	Style(String style){
        this.style = style;
    }
	
    @Override
    public String toString() {
        return this.style;
    }
}
