package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;

public class Border extends Attribute{

	private static String borderAttributeName = "border";
	private int value;
	
	public Border(int value) {
		this.value = value;
	}
	
	@Override
	protected String getAttributeName() {
		return Border.borderAttributeName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value);
	}

}
