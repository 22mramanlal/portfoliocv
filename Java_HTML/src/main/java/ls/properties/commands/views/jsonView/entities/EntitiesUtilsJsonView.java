package ls.properties.commands.views.jsonView.entities;

import java.util.Collection;
import java.util.Iterator;

import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonSimpleObject;
import ls.properties.commands.views.jsonView.entities.rentals.IRentalConstructJson;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;

public class EntitiesUtilsJsonView {
	
	public static JsonSimpleObject constructJsonRental(Rental r) {

		JsonSimpleObject j = new JsonSimpleObject(7);

		j.addDataName("Property");
		j.addDataName("Renter");
		j.addDataName("Year");
		j.addDataName("Week");
		j.addDataName("RentState");
		j.addDataName("OrderDate");
		j.addDataName("AcceptanceDate");
		j.addValNames(r.getProperty().getId());
		j.addValNames(r.getRenter().getUsername());
		j.addValNames(r.getCy());
		j.addValNames(r.getCw());
		j.addValNames(Boolean.toString(r.getRentState()));
		j.addValNames(r.getOrderDate());
		j.addValNames(r.getAcceptanceDate());

		return j;
	}
	
	public static JsonObject constructJsonListRentals(
			Collection<Rental> rentals, IRentalConstructJson ir) {

		JsonObject json = new JsonObject(rentals.size());

		if (rentals == null | rentals.isEmpty()) {
			json.addJson(new JsonSimpleObject("rentals", "There are no rentals to show"));
			return json;
		}


		Iterator<Rental> iter = rentals.iterator();

		while(iter.hasNext())
			json.addJson(ir.constructJsonRental(iter.next()));			

		return json;
	}

	public static JsonSimpleObject constructJsonProperty(Property p) {
		JsonSimpleObject j = new JsonSimpleObject(7);

		j.addDataName("Id");
		j.addDataName("Sort");
		j.addDataName("Text");
		j.addDataName("Price");
		j.addDataName("Region");
		j.addDataName("Locale");
		j.addDataName("Owner");
		j.addValNames(p.getId());
		j.addValNames(p.getSort());
		j.addValNames(p.getText());
		j.addValNames(p.getPrice());
		j.addValNames(p.getLocation().getRegion());
		j.addValNames(p.getLocation().getLocale());
		j.addValNames(p.getOwner().getUsername());

		return j;
	}
	
	public static JsonSimpleObject constructJsonProperties(Property p) {
		JsonSimpleObject j = new JsonSimpleObject(1);

		j.addDataName("Id");
		j.addValNames(p.getId());

		return j;
	}

	public static JsonSimpleObject constructJsonClient(Client c) {

		JsonSimpleObject j = new JsonSimpleObject(3);

		j.addDataName("Username");
		j.addDataName("Email");
		j.addDataName("Fullname");
		j.addValNames(c.getUsername());
		j.addValNames(c.getEmail());
		j.addValNames(c.getFullname());

		return j;
	}
	
	public static JsonSimpleObject constructJsonClients(Client c) {

		JsonSimpleObject j = new JsonSimpleObject(1);

		j.addDataName("Username");
		j.addValNames(c.getUsername());

		return j;
	}
	
	public static JsonObject constructJsonListClient(
			Collection<Client> clients) {

		JsonObject json = new JsonObject(clients.size());

		if (clients == null | clients.isEmpty()) {
			json.addJson(new JsonSimpleObject("clients", "There are no clients to show"));
			return json;
		}

		Iterator<Client> iter = clients.iterator();

		while(iter.hasNext())
			json.addJson(constructJsonClients(iter.next()));			

		return json;
	}

	public static JsonObject constructJsonListProp(
			Collection<Property> properties) {

		JsonObject json = new JsonObject(properties.size());

		if (properties == null | properties.isEmpty()) {
			json.addJson(new JsonSimpleObject("properties", "There are no properties to show"));
			return json;
		}


		Iterator<Property> iter = properties.iterator();

		while(iter.hasNext())
			json.addJson(constructJsonProperties(iter.next()));			

		return json;
	}
}
