package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;
import ls.properties.commands.views.htmlView.html.enumerables.FontStyle;

public class Face extends Attribute{	

	private static String faceAttributeName = "face";
	private FontStyle value;
	
	
	public Face(FontStyle value) {
		this.value = value;
	}
	
	@Override
	protected String getAttributeName() {
		return Face.faceAttributeName;
	}

	@Override
	protected String getValue() {
		return value.toString();
	}

}
