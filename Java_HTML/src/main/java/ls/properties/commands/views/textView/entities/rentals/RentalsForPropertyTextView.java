package ls.properties.commands.views.textView.entities.rentals;

import java.util.Collection;
import java.util.Iterator;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsForProperty;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;

public class RentalsForPropertyTextView extends TextView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		Collection<Rental> list = ((ResponseRentalsForProperty) cr).getRentals();
		if( list == null || list.isEmpty())
			return "There is no rentals for this property.";
		
		
		StringBuilder sb = new StringBuilder();

		 sb.append(prepareInfo((Property)((ResponseRentalsForProperty) cr).getProperty(),list));
	
		return sb.toString();
	}
	
	
	private String prepareInfo(Property p, Collection<Rental> r){	
		Iterator<Rental> iter = r.iterator();
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nRentals for this property Id: ");
		sb.append(p.getId());
		sb.append("\n\n");
		
		sb.append(getTableRentalPropertyHeader());
		while(iter.hasNext())
			sb.append(prepareTable(iter.next()));
	
		return sb.toString();
	
	}
	
	private String prepareTable(Rental r){	
		 StringBuilder sb = new StringBuilder();

		 sb.append(getTableRentalPropertyInfo(r));
		 sb.append("\n");
		 sb.append(putSeparatorLine(sb.length()));
		 sb.append("\n");

		return sb.toString();
	}
	
	private String getTableRentalPropertyHeader(){ 
		StringBuilder sb = new StringBuilder();
		
		appendToString(sb,"Id");
		appendToString(sb,"Renter");
		appendToString(sb,"Cy");
		appendToString(sb,"Cw");
		
		sb.append("\n");
		sb.append(putSeparatorLine(sb.length()));
		sb.append("\n");
		
		return sb.toString();

	}
	
	private String getTableRentalPropertyInfo(Rental r) {
		StringBuilder sb  = new StringBuilder();
		  
	    appendToString(sb, Integer.toString(r.getProperty().getId()));
		appendToString(sb,r.getRenter().getUsername());
		appendToString(sb,Integer.toString(r.getCy()));
		appendToString(sb,Integer.toString(r.getCw()));
		  
	    return sb.toString();
		
	}


	
}
