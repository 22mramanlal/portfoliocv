package ls.properties.commands.views.htmlView.html.attributes.subAttributes;

import ls.properties.commands.views.htmlView.html.enumerables.Format;


public class Top extends SubAttribute {

	private static String topSubAttributeName = "top";
	private int value;
	private Format format;
	
	public Top(int value, Format format) {
		this.value = value;
		this.format = format;
	}
	
	@Override
	protected String getAttributeName() {
		return Top.topSubAttributeName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value) + format.toString();
	}

}
