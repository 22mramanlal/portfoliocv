package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;
import ls.properties.commands.views.htmlView.html.enumerables.ColorName;

public class BgColor extends Attribute{
	
	private static String bgColorAttributeName = "bgcolor";
	private ColorName colorName;
	
	public BgColor(ColorName colorName){
		this.colorName = colorName;
	} 
	
	@Override
	protected String getAttributeName() {
		return BgColor.bgColorAttributeName;
	}

	@Override
	protected String getValue() {
		return colorName.toString();
	}

}
