package ls.properties.commands.views.textView;

import ls.properties.commands.views.IView;

public abstract class TextView implements IView {

	private final static String SPACES = "                                        ";
	
	protected static StringBuilder appendToString(StringBuilder dest, String value) {
		if (value.length() >= 40) {
			dest.append(value.substring(0, 35));
			dest.append(".....");
		} else {
			dest.append(value);
			dest.append(SPACES.substring(0, (40 - value.length())));
		}
		dest.append(" | ");
		return dest;
	}

	protected static StringBuilder putSeparatorLine(int length) {
		String line = "----------------------------------------- -";
		StringBuilder separator = new StringBuilder();

		for(int i = 0; i < length/43; ++i)
			separator.append(line);		
		return separator;
	}

}
