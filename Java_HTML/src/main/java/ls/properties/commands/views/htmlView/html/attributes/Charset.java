package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;
import ls.properties.commands.views.htmlView.html.enumerables.CharsetFormat;

public class Charset extends Attribute {

	private static String charSetAttName = "charset";
	private CharsetFormat format;
	
	public Charset(CharsetFormat format){
		this.format = format;
	}
	
	@Override
	protected String getAttributeName() {
		return Charset.charSetAttName;
	}

	@Override
	protected String getValue() {
		return format.toString();
	}

}
