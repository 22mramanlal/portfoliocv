package ls.properties.commands.views.textView.entities.properties;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseProperty;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Property;

public class PropertyTextView extends TextView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		Property p = (Property) ((ResponseProperty)cr).getProperty();
		if(p == null)
			return "There is no Property";

		 StringBuilder sb = new StringBuilder();
		 
		 sb.append(prepareInfo(p));
		 
		 return sb.toString();
		
	}

	private String prepareInfo(Property p){

		 StringBuilder sb = new StringBuilder();
		 sb.append("\n");
		 sb.append(getTablePropertyHeader());

		 sb.append(getTablePropertyInfo(p));
		 
		 sb.append("\n");

		return sb.toString();
	}

	private String getTablePropertyHeader(){ 
		StringBuilder sb = new StringBuilder();
		
		appendToString(sb,"Id");
		appendToString(sb,"Sort");
		appendToString(sb,"Text");
		appendToString(sb,"Price");
		appendToString(sb,"Region");
		appendToString(sb,"Locale");
		appendToString(sb,"Owner");
		
		sb.append("\n");
		sb.append(putSeparatorLine(sb.length()));
		sb.append("\n");
		
		return sb.toString();

	}
	
	private String getTablePropertyInfo(Property p) {
		StringBuilder sb  = new StringBuilder();
		  
		 appendToString(sb,Integer.toString(p.getId()));
		 appendToString(sb,p.getSort());
		 appendToString(sb,p.getText());
		 appendToString(sb,Integer.toString(p.getPrice()));
		 appendToString(sb,p.getLocation().getRegion());
		 appendToString(sb,p.getLocation().getLocale());
		 appendToString(sb,p.getOwner().getUsername());
		
		 sb.append("\n");
		 sb.append(putSeparatorLine(sb.length())); 
		 
	     return sb.toString();
		
	}
	
	
}
