package ls.properties.commands.views.htmlView.entities.rentals;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsForProperty;
import ls.properties.domain.entities.Property;

public class RentalsForPropertyHtmlView extends RentalsHtmlView<Property> {

	@Override
	protected String getTitle() {
		return "Rentals for property";
	}

	@Override
	protected String getNoRentalsMessage() {
		return "There are no rentals for this property";
	}

	@Override
	protected Property getResponseEntity(ICommandResponse cr) {
		return ((ResponseRentalsForProperty)cr).getProperty();
	}

	@Override
	protected String getEntityId(Property e) {
		return Integer.toString(e.getId());
	}

	@Override
	protected String getTitleLink(Property e) {
		return "/properties/details/"+ e.getId();
	}
}
