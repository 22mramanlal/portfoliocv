package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;

public class Src extends Attribute {

	private static String srcAttName = "src";
	private String path;

	public Src(String path) {
		this.path = path;
	}
	
	@Override
	protected String getAttributeName() {
		return Src.srcAttName;
	}

	@Override
	protected String getValue() {
		return path;
	}

}
