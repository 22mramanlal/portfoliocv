package ls.properties.commands.views.htmlView.html.enumerables;

public enum Format {
	Percentage("%"),
	Pixel("px");
	
    private final String format;
    Format (String format){
        this.format = format;
    }
    @Override
    public String toString() {
        return this.format;
    }
}
