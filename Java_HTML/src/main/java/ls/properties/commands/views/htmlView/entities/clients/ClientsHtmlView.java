package ls.properties.commands.views.htmlView.entities.clients;

import java.util.Collection;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseClients;
import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlUtils;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;
import ls.properties.commands.views.htmlView.templates.ViewAllTemplate;
import ls.properties.commands.views.htmlView.templates.ViewTemplateUtils;
import ls.properties.domain.entities.Client;


public class ClientsHtmlView extends HtmlView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		ViewAllTemplate temp = new ViewAllTemplate();
		
		Collection<Client> clients = ((ResponseClients)cr).getClients();
		
		setTitle(temp);
		setClients(temp, clients);
		
		HtmlUtils.identHTML(temp.getHtml());
		return temp.getHtml().toString();
	}
		
	private static void setTitle(ViewAllTemplate temp){
		ViewTemplateUtils.setSimpleTitle(temp, "All Users");
	}
	
	private static void setClients(ViewAllTemplate temp, Collection<Client> clients){
		if(clients == null || clients.isEmpty()){
			temp.addElementList(new Tag(TagName.Font, "There are no clients"));
			return;
		}
			
		for(Client c : clients){
			String userName = c.getUsername();
			String link = "/users/"+userName;
			temp.addElementList(getEntityTag(link, userName));
		}
			
	}
	
	
}
