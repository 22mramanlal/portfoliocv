package ls.properties.commands.views.htmlView.html.attributes;

import ls.properties.commands.views.htmlView.html.Attribute;

public class Size extends Attribute{

	private static String sizeAttributeName = "size";
	private int value;
	
	
	public Size(int value) {
		this.value = value;
	}
	
	@Override
	protected String getAttributeName() {
		return Size.sizeAttributeName;
	}

	@Override
	protected String getValue() {
		return Integer.toString(value);
	}

}
