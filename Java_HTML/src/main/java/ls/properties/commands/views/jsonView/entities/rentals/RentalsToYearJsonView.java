package ls.properties.commands.views.jsonView.entities.rentals;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsForYear;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonSimpleObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;
import ls.properties.domain.entities.Rental;

public class RentalsToYearJsonView extends JsonView implements IRentalConstructJson {
	
	private int year;
	private int id;
	
	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		
		ResponseRentalsForYear rr = (ResponseRentalsForYear)cr;
		id = rr.getProperty().getId();
		year = rr.getYear();
		
		JsonObject json = EntitiesUtilsJsonView.constructJsonListRentals(rr.getRentals(), this);
		json.setTitle(getRepName());
		
		return json.toString();
	}

	@Override
	protected String getRepName() {
		return "All rentals for property " + id + " on year: " + year;
	}

	@Override
	public JsonSimpleObject constructJsonRental(Rental r) {

		JsonSimpleObject j = new JsonSimpleObject(2);

		j.addDataName("Property");
		j.addDataName("Renter");
		j.addValNames(r.getProperty().getId());
		j.addValNames(r.getRenter().getUsername());

		return j;
	}
}
