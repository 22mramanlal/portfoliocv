package ls.properties.commands.views.htmlView.templates;

import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlFile;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;

public class ViewTemplate {
	
	protected HtmlFile htmlFile = ViewTemplateUtils.getTemplate();
	public HtmlFile getHtml(){return htmlFile;}
	
	private Tag titleDiv;
	private Tag title;
	
	private Tag leftMenu;
	private Tag rightMenu;
	
	
	public void setTitle(Tag title){
		if(this.title == null){
			titleDiv = HtmlView.getDivTag(25, true, 24);
			htmlFile.addBodyTag(titleDiv);
		}
		else
			htmlFile.removeBodyTag(this.title);
			
		this.title = title;
		titleDiv.addTag(title);
	}
	
	public void addButtonToLeftMenu(String imagePath, String link){
		if(leftMenu == null){
			leftMenu = HtmlView.getDivTag(0, true, 35);
			htmlFile.addBodyTag(leftMenu);
		}
			
		leftMenu.addTag(HtmlView.creatButton(imagePath, link));
		leftMenu.addTag(new Tag(TagName.Br));
	}
	
	public void addButtonToRightMenu(String imagePath, String link){
		if(rightMenu == null){
			rightMenu = HtmlView.getDivTag(0, false, 35);
			htmlFile.addBodyTag(rightMenu);
		}
			
		rightMenu.addTag(HtmlView.creatButton(imagePath, link));
		rightMenu.addTag(new Tag(TagName.Br));	
	}

}
