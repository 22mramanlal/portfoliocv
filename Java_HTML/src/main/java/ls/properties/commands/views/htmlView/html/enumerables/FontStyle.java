package ls.properties.commands.views.htmlView.html.enumerables;

public enum FontStyle {
	Arial("arial"),
	ComicSansMS("Comic Sans MS"),
	Courier("courier"),
	Cursive("cursive"),
	Fantasy("fantasy"),
	Helvetica("helvetica"),
	Mistral("Mistral"),
	Monospace("monospace"),
	SansSerif("sans-serif"),
	Serif("serif"),
	Verdana("verdana");
		
	
    private final String fontStyle;
    FontStyle(String code){
        this.fontStyle = code;
    }
    @Override
    public String toString() {
        return this.fontStyle;
    }
}
