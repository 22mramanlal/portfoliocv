package ls.properties.commands.views.htmlView.html.enumerables;

public enum TagName {
	H1("h1"),H2("h2"),H3("h3"),H4("h4"),H5("h5"),H6("h6"),
	Ul("ul"),
	Tr("tr"),
	Title("title"),
	Th("th"),
	Td("td"),
	Table("table"),
	Meta("meta"),
	P("p"),
	Li("li"),
	Img("img"),
	Html("html"),
	Head("head"),
	Font("font"),
	Div("div"),
	Br("br"),
	Body("body"),
	A("a"),
	B("b");
	
    private final String name;
    
    TagName(String name){
        this.name = name;
    }
    
    @Override
    public String toString() {
        return this.name;
    }

}
