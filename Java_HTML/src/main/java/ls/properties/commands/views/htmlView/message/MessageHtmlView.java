package ls.properties.commands.views.htmlView.message;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseMessage.ResponseMessage;
import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlFile;
import ls.properties.commands.views.htmlView.html.HtmlUtils;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;

public class MessageHtmlView extends HtmlView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		HtmlFile page = new HtmlFile();
		
		page.addHeadTag(new Tag(TagName.Title, "Rental Properties"));
		page.addBodyTag(new Tag(TagName.P, ((ResponseMessage)cr).getMessage()));
		
		HtmlUtils.identHTML(page);
		
		return page.toString();
	}

}
