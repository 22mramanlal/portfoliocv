package ls.properties.commands.views.jsonView.entities.rentals;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseRentalsOfClient;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonSimpleObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;
import ls.properties.domain.entities.Rental;

public class RentalsOfOwnerJsonView extends JsonView implements IRentalConstructJson {

	String username;
	
	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		
		ResponseRentalsOfClient rrc = (ResponseRentalsOfClient)cr;
		username = rrc.getClient().getUsername();
		JsonObject json = EntitiesUtilsJsonView.constructJsonListRentals(rrc.getRentals(), this);
		json.setTitle(getRepName());
		return json.toString();
	}

	@Override
	protected String getRepName() {
		return "Rentals of user: " + username;
	}

	@Override
	public JsonSimpleObject constructJsonRental(Rental r) {

		JsonSimpleObject j = new JsonSimpleObject(6);

		j.addDataName("Property");
		j.addDataName("Year");
		j.addDataName("Week");
		j.addDataName("RentState");
		j.addDataName("OrderDate");
		j.addDataName("AcceptanceDate");
		j.addValNames(r.getProperty().getId());
		j.addValNames(r.getCy());
		j.addValNames(r.getCw());
		j.addValNames(Boolean.toString(r.getRentState()));
		j.addValNames(r.getOrderDate());
		j.addValNames(r.getAcceptanceDate());

		return j;
	}

}
