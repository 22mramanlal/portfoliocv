package ls.properties.commands.views.textView.entities.properties;

import java.util.Iterator;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOfClient;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Property;

public class PropertiesOfOwnerTextView extends TextView {
	
	@Override
	public String getRepresentation(ICommandResponse cr) {
		
		if(((ResponsePropertiesOfClient)cr).getProperties() == null || ((ResponsePropertiesOfClient)cr).getProperties().isEmpty() )
			return "There is no Property for this User";
		
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(prepareInfo((Client)((ResponsePropertiesOfClient) cr).getClient(), (List<Property>)((ResponsePropertiesOfClient) cr).getProperties() ));
	
		return sb.toString();
	}
	
	private String prepareInfo(Client c, List<Property> p){
		StringBuilder sb = new StringBuilder();
		
		sb.append("\n\tProperties for Owner: ");
		sb.append(c.getUsername());
		sb.append("\n\n");
	
		Iterator<Property> iter = p.iterator();

		while(iter.hasNext()){
			sb.append(constructPropertyList(iter.next()));
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	private String constructPropertyList(Property p) {

		 StringBuilder sb = new StringBuilder();

		  sb.append("\t\t>");
		  sb.append(p.getId());

	   return sb.toString();
	}
}
