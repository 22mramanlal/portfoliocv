package ls.properties.commands.views.jsonView.message;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseMessage.ResponseMessage;
import ls.properties.commands.views.jsonView.JsonView;

public class MessageJsonView extends JsonView {

	@Override
	protected String getRepName() {
		return "Message Result";
	}

	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {
		StringBuilder sb = new StringBuilder("{\n");
		sb.append("\t\"Message\":\"").append(((ResponseMessage) cr).getMessage())
				.append("\"\n}");
		return sb.toString();
	}
}