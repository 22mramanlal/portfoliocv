package ls.properties.commands.views.htmlView.templates;

import ls.properties.commands.views.htmlView.HtmlView;
import ls.properties.commands.views.htmlView.html.HtmlFile;
import ls.properties.commands.views.htmlView.html.Tag;
import ls.properties.commands.views.htmlView.html.attributes.BackGround;
import ls.properties.commands.views.htmlView.html.attributes.HRef;
import ls.properties.commands.views.htmlView.html.attributes.Size;
import ls.properties.commands.views.htmlView.html.attributes.Src;
import ls.properties.commands.views.htmlView.html.attributes.Style;
import ls.properties.commands.views.htmlView.html.attributes.subAttributes.BackGroundPosition;
import ls.properties.commands.views.htmlView.html.attributes.subAttributes.BackGroundRepeat;
import ls.properties.commands.views.htmlView.html.attributes.subAttributes.BackGroundSize;
import ls.properties.commands.views.htmlView.html.attributes.subAttributes.Left;
import ls.properties.commands.views.htmlView.html.attributes.subAttributes.Position;
import ls.properties.commands.views.htmlView.html.attributes.subAttributes.Top;
import ls.properties.commands.views.htmlView.html.enumerables.Format;
import ls.properties.commands.views.htmlView.html.enumerables.PositionAbsolute;
import ls.properties.commands.views.htmlView.html.enumerables.PositionRelative;
import ls.properties.commands.views.htmlView.html.enumerables.Repeat;
import ls.properties.commands.views.htmlView.html.enumerables.TagName;

public class ViewTemplateUtils {

	public static final String BACK_GROUND_PATH = "/resources/images/backGround.jpg";
	public static final String TOP_PATH = "/resources/images/top.jpg";

	// Buttons
	public static final String ALL_PROPERTIES_BUTTON_PATH = "/resources/images/buttons/allPropertiesButton.png";
	public static final String ALL_OWNERS_BUTTON_PATH = "/resources/images/buttons/allUsersButton.png";
	public static final String NEXT_YEAR_BUTTON_PATH = "/resources/images/buttons/nextYearButton.png";
	public static final String PREVIOUS_YEAR_BUTTON_PATH = "/resources/images/buttons/previousYearButton.png";
	public static final String RENTALS_OF_OWNER_BUTTON_PATH = "/resources/images/buttons/rentalsOfOwnerButton.png";
	public static final String RENTALS_FOR_PROPERTY_BUTTON_PATH = "/resources/images/buttons/rentalsForPropertyButton.png";
	public static final String MAGNIFYING_GLASS_BUTTON_PATH = "/resources/images/buttons/magnifyingGlassButton.png";
	public static final String HOME_PAGE_BUTTON_PATH = "/resources/images/buttons/homePageButton.jpg";

	public static final String USERS_IMAGES_PATH = "/resources/images/Owners/";
	public static final String DEFAULT_OWNER_IMAGE_PATH = "/resources/images/Owners/defaultOwner.jpg";
	public static final String PROPERTIES_IMAGES_PATH = "/resources/images/Properties/";
	public static final String DEFAULT_PROPERTY_IMAGE_PATH = "/resources/images/Properties/defaultProperty.jpg";

	
	public static void setSimpleTitle(ViewTemplate temp, String title) {
		Tag titleTag = new Tag(TagName.Font, title);
		titleTag.addAttribute(new Size(7));
		temp.setTitle(titleTag);
	}

	public static void setComposeTitle(ViewTemplate temp, String title, String link, String linkableTitlePart) {
		Tag titleTag = new Tag(TagName.Font, title+" ");
		
		titleTag.addAttribute(new Size(7));
		
		Tag a = new Tag(TagName.A, linkableTitlePart);
		a.addAttribute(new HRef(link));
		
		titleTag.addTag(a);

		temp.setTitle(titleTag);
	}

	public static HtmlFile getTemplate(){
		HtmlFile htmlFile = new HtmlFile();
		
		htmlFile.addHeadTag(new Tag(TagName.Title, "LS Rentals"));
		
		setBackGround(htmlFile);
		setTop(htmlFile);
		setHomePageButton(htmlFile);
		
		htmlFile.addBodyTag(new Tag(TagName.Br));htmlFile.addBodyTag(new Tag(TagName.Br));
		
		return htmlFile;
	}
	
	private static void setBackGround(HtmlFile htmlFile){
		htmlFile.addBodyAttribute(new BackGround(BACK_GROUND_PATH));
		Style style= new Style("");
		style.addSubAttribute(new BackGroundRepeat(Repeat.RepeatY));
		style.addSubAttribute(new BackGroundPosition(PositionAbsolute.CenterCenter));
		style.addSubAttribute(new BackGroundSize(890, 1700, Format.Pixel));
		htmlFile.addBodyAttribute(style);
	}
	
	private static void setTop(HtmlFile htmlFile){
		Tag div = new Tag(TagName.Div);
		Style style = new Style("");
		style.addSubAttribute(new Position(PositionRelative.Absolute));
		style.addSubAttribute(new Left(0, Format.Percentage));
		style.addSubAttribute(new Top(0, Format.Percentage));
		div.addAttribute(style);
		
		Tag image = new Tag(TagName.Img);
		image.addAttribute(new Src(TOP_PATH));
		
		div.addTag(image);
		htmlFile.addBodyTag(div);
	}
	
	private static void setHomePageButton(HtmlFile htmlFile){
		HtmlView.setButton(htmlFile, HOME_PAGE_BUTTON_PATH, "/", true, 1, 1);
	}
	
}
