package ls.properties.commands.views.textView.entities.rentals;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntity.ResponseRental;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Rental;

public class RentalTextView extends TextView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		return prepareInfo(cr);
	}

	private String prepareInfo(ICommandResponse cr){
		Rental r = (Rental) ((ResponseRental)cr).getRental();
		if(r == null )
			return "There is no Rental.";

		
		

		 StringBuilder sb = new StringBuilder();

		 sb.append(getTableRentalHeader());

		 sb.append(getTableRentalInfo(r));
		
		 sb.append("\n");


		return sb.toString();
	}

	protected String getTableRentalInfo(Rental r) {
		
		StringBuilder sb  = new StringBuilder();
		 sb.append("\n");
		 appendToString(sb,Integer.toString(r.getProperty().getId()));

		 appendToString(sb,r.getRenter().getUsername());

	     appendToString(sb,Integer.toString(r.getCy()));
		
	     appendToString(sb,Integer.toString(r.getCw()));

	     appendToString(sb, Boolean.toString(r.getRentState()));

		 appendToString(sb, r.getOrderDate().toString());

		 if(r.getAcceptanceDate() != null)
			 appendToString(sb, r.getAcceptanceDate().toString());
		 else
			 appendToString(sb, "Not Accepted");
		 
		 sb.append("\n");
		 sb.append(putSeparatorLine(sb.length()));
		  
	     return sb.toString();
		
	}
	
	protected String getTableRentalHeader(){
		StringBuilder sb = new StringBuilder();
		
		appendToString(sb,"Property");
		
		appendToString(sb,"Renter");
		
		appendToString(sb,"Cy");

		appendToString(sb,"Cw");

		appendToString(sb,"RentState");
	
		appendToString(sb,"OrderDate");

		appendToString(sb,"AcceptanceDate");

		
		sb.append("\n");
		sb.append(putSeparatorLine(sb.length()));
		sb.append("\n");
		
		return sb.toString();
	}
}
