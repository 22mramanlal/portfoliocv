package ls.properties.commands.views.htmlView.html.enumerables;

public enum Repeat {
	Repeat("repeat"),
	RepeatX("repeat-x"),
	RepeatY("repeat-y"),
	NoRepeat("no-repeat"),
	Initial("initial"),
	Inherit("inherit");
	
    private final String repeat;
    Repeat(String repeat){
        this.repeat = repeat;
    }
    @Override
    public String toString() {
        return this.repeat;
    }
}
