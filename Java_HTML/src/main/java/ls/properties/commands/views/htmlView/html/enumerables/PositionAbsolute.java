package ls.properties.commands.views.htmlView.html.enumerables;

public enum PositionAbsolute {
	LeftTop("left top"),
	LeftCenter("left center"),
	LeftBottom("left bottom"),
	RightTop("right top"),
	RightCenter("right center"),
	RightBottom("right bottom"),
	CenterTop("center top"),
	CenterCenter("center center"),
	CenterBottom("center bottom");
	
    private final String position;
    PositionAbsolute(String position){
        this.position = position;
    }
    @Override
    public String toString() {
        return this.position;
    }
}
