package ls.properties.commands.views.textView.entities.properties;

import java.util.Iterator;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOfType;
import ls.properties.commands.views.textView.TextView;
import ls.properties.domain.entities.Property;

public class PropertiesTypeTextView extends TextView {

	@Override
	public String getRepresentation(ICommandResponse cr) {
		List<Property> list = (List<Property>)((ResponsePropertiesOfType) cr).getProperties();
		if(list  == null || list.isEmpty() )
			return "There is no Property";
		
		
		StringBuilder sb = new StringBuilder();
		sb.append(prepareInfo(list,((ResponsePropertiesOfType) cr).getType()));

		return sb.toString();
	}
	
	private String prepareInfo(List<Property> list, String type){
		StringBuilder sb = new StringBuilder();
		Iterator<Property> iter = list.iterator();
		
		sb.append("\nProperties of Type: ");
		sb.append(type);
		sb.append("\n\n");
		sb.append(getTablePropertyTypeHeader());
		while(iter.hasNext())
			sb.append(prepareTable(iter.next()));
			
		return sb.toString();
	}
	
	private String prepareTable(Property p){	
		 StringBuilder sb = new StringBuilder();

		 sb.append(getTablePropertyTypeInfo(p));
		 sb.append("\n");
		 sb.append(putSeparatorLine(sb.length()));
		 sb.append("\n");

		return sb.toString();
	}
	
	private String getTablePropertyTypeHeader(){ 
		StringBuilder sb = new StringBuilder();
		
		appendToString(sb,"Id");
		appendToString(sb,"Type");

		sb.append("\n");
		sb.append(putSeparatorLine(sb.length()));
		sb.append("\n");
		
		return sb.toString();

	}
	
	private String getTablePropertyTypeInfo(Property p) {
		StringBuilder sb  = new StringBuilder();
		
		  appendToString(sb,Integer.toString(p.getId()));
		  appendToString(sb,p.getSort());
	
	     return sb.toString();
		
	}

}
