package ls.properties.commands.views.jsonView.entities.clients;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.responses.responseEntities.ResponseClients;
import ls.properties.commands.views.jsonView.JsonObject;
import ls.properties.commands.views.jsonView.JsonView;
import ls.properties.commands.views.jsonView.entities.EntitiesUtilsJsonView;

public class ClientsJsonView extends JsonView {

	@Override
	protected String getObjectRepresentation(ICommandResponse cr) {

		ResponseClients rc = (ResponseClients) cr;
		JsonObject json = EntitiesUtilsJsonView.constructJsonListClient(rc.getClients());
		json.setTitle(getRepName());
		return json.toString();
	}

	@Override
	protected String getRepName() {
		return "All users";
	}

}
