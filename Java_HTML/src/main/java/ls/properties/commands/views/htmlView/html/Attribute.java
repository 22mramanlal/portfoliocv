package ls.properties.commands.views.htmlView.html;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.views.htmlView.html.attributes.subAttributes.SubAttribute;

public abstract class Attribute {
	
	List<SubAttribute> subAttributes = new  LinkedList<SubAttribute>();
	
	protected abstract String getAttributeName();
	protected abstract String getValue();
	
	public void addSubAttribute(SubAttribute subAtt){
		subAttributes.add(subAtt);
	}
	
	public void removeSubAttribute(SubAttribute subAtt){
		subAttributes.remove(subAtt);
	}
	
	
	private String getSubAttributes(){
		StringBuilder sb = new StringBuilder();
		
		for(SubAttribute subAtt : subAttributes){
			sb.append(subAtt.toString());
			sb.append("; ");
		}
		
		return sb.toString();
	}
	
	@Override
	public String toString(){
		String value = getValue();
		value += getSubAttributes();
		
		StringBuilder sb = new StringBuilder(getAttributeName());
		sb.append("=");
		sb.append("\"");
		sb.append(value);
		sb.append("\"");
		
		return sb.toString();
	}
	
}
