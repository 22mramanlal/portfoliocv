package ls.properties.exceptions;

import java.util.HashMap;
import java.util.Map;

public enum EnumExceptions{
	Repository(0, RepositoryException.class.getName()),
	InvalidFunctionality(1, InvalidFunctionalityException.class.getName()),
	InvalidUserParameters(2, InvalidUserParametersException.class.getName());
	
	private final String name;
	private final int code;
	EnumExceptions(int code, String name){
		this.code = code;
		this.name = name;
	    
	}
	private static Map<Integer, EnumExceptions> codeToStatusMapping;

	public static EnumExceptions getEnum(String description) {
		for (int i = 0; i < codeToStatusMapping.size(); ++i) {
			if(codeToStatusMapping.get(i).getDescription().equals(description))
				return codeToStatusMapping.get(i);
		}
		
		return null;
	}

	private static void initMapping() {
		codeToStatusMapping = new HashMap<Integer, EnumExceptions>();
		for (EnumExceptions s : values()) {
			codeToStatusMapping.put(s.code, s);
		}
	}

	public static EnumExceptions value(String className) {
		if (codeToStatusMapping == null) {
			initMapping();
		}
		
		return getEnum(className);
	}

	private String getDescription() {
		return this.name;
	}

	public int getCode() {
		return this.code;
	}
}



