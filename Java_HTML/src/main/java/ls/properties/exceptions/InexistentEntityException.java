package ls.properties.exceptions;

public class InexistentEntityException extends AppException {

	private static final long serialVersionUID = 1L;

	public InexistentEntityException(){}
	
	public InexistentEntityException(String info){
		super(info);
	}
	
	public InexistentEntityException(String info, Throwable cause){
		super(info, cause);
	}

}
