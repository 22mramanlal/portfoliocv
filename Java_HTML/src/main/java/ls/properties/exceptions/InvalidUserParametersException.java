package ls.properties.exceptions;

public class InvalidUserParametersException extends AppException{


	private static final long serialVersionUID = 1L;

	public InvalidUserParametersException(){}
	
	public InvalidUserParametersException(String info){
		super(info);
	}
	
	public InvalidUserParametersException(String info, Throwable cause){
		super(info, cause);
	}
}
