package ls.properties.exceptions;

public class RepositoryException extends AppException{

	
	private static final long serialVersionUID = 1L;
	public RepositoryException(){}
	public RepositoryException(String info){
		super(info);
	}
	public RepositoryException(String info, Throwable cause){
		super(info, cause);
	}
}
