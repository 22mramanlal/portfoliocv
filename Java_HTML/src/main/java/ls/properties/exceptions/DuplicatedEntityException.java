package ls.properties.exceptions;

public class DuplicatedEntityException extends AppException{

	private static final long serialVersionUID = 1L;

	public DuplicatedEntityException(){}
	
	public DuplicatedEntityException(String info){
		super(info);
	}
	
	public DuplicatedEntityException(String info, Throwable cause){
		super(info, cause);
	}
	
}
