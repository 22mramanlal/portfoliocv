package ls.properties.exceptions;

public class InvalidFunctionalityException extends AppException{

	
	private static final long serialVersionUID = 1L;
	public InvalidFunctionalityException(){}
	
	public InvalidFunctionalityException(String info){
		super(info);
	}
	public InvalidFunctionalityException(String info, Throwable cause){
		super(info, cause);
	}
}
