package ls.properties.exceptions;

public class AppException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public AppException(){}
	
	public AppException(String info){
		super(info);
	}
	
	public AppException(String info, Throwable cause){
		super(info,  cause);
	}

}
