package ls.properties.httpservice;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import ls.properties.exceptions.AppException;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;

public abstract class HttpServer {

	private static Collection<Server>  servers = new LinkedList<Server>();
	
	public static void trace(String msg) {
		System.out.println(msg);
	}

	public static void start(int port){
		Server server = new Server(port);
		servers.add(server);
		ServletHandler handler = new ServletHandler();
		server.setHandler(handler);

		handler.addServletWithMapping(ImageServlet.class, "/favicon.ico");
		handler.addServletWithMapping(ImageServlet.class, "/resources/images/*");
		handler.addServletWithMapping(Servlet.class, "/*");

		System.out.println("Server is started.");
		   
        try{
        	server.start();	
        }catch(Exception e){
        	throw new AppException("Error trying to start Server", e);
        }
        
	}

	public static void stopServer(){
		if(servers.isEmpty())
			return;
		
		Iterator<Server> it = servers.iterator();
        try{
        	while(it.hasNext())
        		it.next().stop();	
        }catch(Exception e){
        	throw new AppException("Error trying to stop Server", e);
        }  
	}
	
}
