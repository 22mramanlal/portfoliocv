package ls.properties.httpservice;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filename = request.getPathInfo();
        
        if(filename ==null)
        	filename = "favicon.ico"; 
        else
        	filename = filename.substring(1);
    
        String extension = filename.substring(filename.indexOf(".")+1);
        
		response.setContentType("image/"+extension);
		
		File f = new File(ClassLoader.getSystemResource("").getPath().substring(1) + "resources/images/" + filename);
		byte[] fileContent = Files.readAllBytes(f.toPath());
		
		response.setStatus(HttpStatus.Ok.value());
		
		OutputStream out = response.getOutputStream();
		out.write(fileContent);

		out.close();
    }
	
}