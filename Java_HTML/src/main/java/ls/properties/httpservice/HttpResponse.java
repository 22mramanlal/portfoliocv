package ls.properties.httpservice;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import javax.servlet.http.HttpServletResponse;

import ls.properties.exceptions.AppException;
import ls.properties.exceptions.EnumExceptions;

public class HttpResponse {

	private final HttpStatus status;
	private final Charset charset = Charset.forName("UTF-8");
	private final String accept;
	private final String response;

	public HttpResponse(HttpStatus status) {
		this(status, null, null);
	}

	public HttpResponse(HttpStatus status, String page, String accept) {
		this.status = status;
		this.response = page;
		this.accept = accept;
	}

	public void send(HttpServletResponse resp) throws IOException {

		resp.setStatus(status.value());

		if (response != null) {

			byte[] bytes = response.getBytes();
			String contentType = String.format("%s;charset=%s", accept, charset);
			resp.setContentType(contentType);
			resp.setContentLength(bytes.length);
			try {
				OutputStream ros = resp.getOutputStream();
				ros.write(bytes);
				ros.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else
			resp.sendError(status.value());
	}

	public static HttpResponse forException(AppException e) {

		switch (EnumExceptions.value(e.getClass().getName())) {
		case Repository:
			return new HttpResponse(HttpStatus.GatewayTimeout);
		case InvalidFunctionality:
			return new HttpResponse(HttpStatus.BadRequest);
		case InvalidUserParameters:
			return new HttpResponse(HttpStatus.PreconditionFailed);
		default:
			return new HttpResponse(HttpStatus.NotFound);
		}
	}
}
