package ls.properties.httpservice;

public enum HttpStatus {

	//Good Requests 2xx
	Ok(200),
	Created(201),
	Accepted(202),
	NonAuthoritativeInformation(203),
	NoContent(204),
	ResetContent(205),
	PartialContent(206),
	
	// Redirections 3xx
	MultipleChoices(300),
	MovedPermanently(301),
	Found(302), 
	SeeOther(303),
	NotModified(304),
	TemporaryRedirect(307),
	
	// Client errors 4xx
	BadRequest(400),
	Unauthorized(401),
	Forbidden(403),
	NotFound(404),
	MethodNotAllowed(405),
	NotAcceptable(406),
	RequestTimeout(408),
	Conflict(409),
	Gone(410),
	LengthRequired(411),
	PreconditionFailed(412),
	UnsupportedMediaType(415),
	ExpectationFailed(417),
	UnprocessableEntity(422),
	Locked(423),
	FailedDependency(424),
	UnorderedCollection(425),
	RetryWith(449),
	
	// Server errors
	InternalServerError(500),
	NotImplemented(501),
	BadGateway(502),
	ServiceUnavailable(503),
	GatewayTimeout(504),
	HTTPVersionNotSupported(505),
	;
	 
    private final int _status;
    HttpStatus(int code){
        _status = code;
    }
    public int value() {
        return _status;
    }
}
