package ls.properties.httpservice;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.structure.Command;
import ls.properties.commands.suppliers.commandSupplier.ICommandSupplier;
import ls.properties.commands.views.IView;
import ls.properties.exceptions.AppException;
import ls.properties.exceptions.RepositoryException;

public class Servlet extends HttpServlet {

	private static final long serialVersionUID = 2L;

	private static ICommandSupplier commandSupplier;

	public static void setCommandSupplier(ICommandSupplier commandSupplier) {
		Servlet.commandSupplier = commandSupplier;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		printReqInfo(req);
		String method = req.getMethod();
		String path = req.getRequestURI();

		if (!method.equals("GET"))
			new HttpResponse(HttpStatus.MethodNotAllowed).send(resp);

		try {
			getResponse(req, path).send(resp);
		} catch (AppException e) {
			HttpResponse.forException(e).send(resp);
		}

	}

	private void printReqInfo(HttpServletRequest req) {
		System.out.println();
		System.out.println("HTTP Command");
		System.out.println("\t"
				+ String.format("Received %s request for %s", req.getMethod(),
						req.getRequestURI()));
		System.out.println();
	}

	private HttpResponse getResponse(HttpServletRequest req, String path) throws RepositoryException {

		String commandPath = getCommandPath(req, path);
		String accept = getFormat(req);
		LinkedList<String> pathArguments = new LinkedList<>();
 
		Command cmd = commandSupplier.getCommand(commandPath, pathArguments);
		ICommandResponse cmdResp = cmd.execute(pathArguments, "");
		IView v = cmd.getView(accept);
		String resp = v.getRepresentation(cmdResp);
		return new HttpResponse(HttpStatus.Ok, resp, accept);		
	}

	private String getFormat(HttpServletRequest req) {
		StringBuilder format = new StringBuilder(req.getHeader("accept"));
		int idx = format.indexOf(",");
		if (idx != -1)
			return format.substring(0, idx);
		return format.toString();
	}

	private String getCommandPath(HttpServletRequest req, String path) {
		StringBuilder res = new StringBuilder(req.getMethod());
		res.append(path);
		return res.toString();
	}

}
