package ls.properties.utils.print;

import ls.properties.utils.IOFiles;

public class FilePrinter implements Printer{

	private String fileName;
	
	public FilePrinter(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public void print(String toPrint) {
		IOFiles.fileWrite(toPrint, fileName);
	}
	
	
	
}

