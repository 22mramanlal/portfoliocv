package ls.properties.utils.print;

public class PrinterUtils {
	
	public static Printer getPrinter(String printerWanted){
		if(printerWanted == null  || !printerWanted.contains("."))
			return new ConsolePrinter();
		else
			return new FilePrinter(printerWanted);
	}

}
