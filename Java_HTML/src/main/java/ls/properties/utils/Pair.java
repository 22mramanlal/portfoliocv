package ls.properties.utils;

public class Pair<T, R> {

	
	private T elem1;
	private R elem2;
	
	public Pair(T elem1, R elem2){
		this.elem1 = elem1;
		this.elem2 = elem2;
	}
	
	public T getElem1(){
		return elem1;
	}
	
	public R getElem2(){
		return elem2;
	}
	
	public void setElem1(T elem1){
		this.elem1 = elem1;
	}
	
	public void setElem2(R elem2){
		this.elem2 = elem2;
	}
	
	@Override
	public boolean equals(Object other){
		if(other == this)
			return true;
		
		if(!(other instanceof Pair))
			return false;
		
		Pair<?,?> otherPair= (Pair<?,?>)other;

		return  otherPair.getElem1().equals(elem1) && otherPair.getElem2().equals(elem2);
	}
	
	
}
