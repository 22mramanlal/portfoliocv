package ls.properties.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Properties;

import ls.properties.exceptions.AppException;

public class IOFiles{
	
	public static String[] fileDBRead(String fileName) {
		String [] str = new String[4];
		Properties prop = new Properties();
		InputStream input = null;

		try {
			
			input = IOFiles.class.getClassLoader().getResourceAsStream(fileName);
			prop.load(input);
			
			int i = prop.size();
			
			if(i!=str.length)
				throw new AppException("Not enough arguments on dbInfo file.");
			
			str[0] = prop.getProperty("servername");
			str[1] = prop.getProperty("databasename");
			str[2] = prop.getProperty("user");
			str[3] = prop.getProperty("password");
	 
		} catch (IllegalArgumentException | IOException | NullPointerException e) {
			throw new AppException("Erro loading configuration file.", e);
		} finally {
			closeFileReader(input);
		}
		return str;
	}
	
	public static String fileRead(String fileName) {
		String str = "";
		StringBuilder result = new StringBuilder();
		BufferedReader br = null;

		try {
			URL s = ClassLoader.getSystemResource("");
			String path = s.getPath().substring(1);
			br = new BufferedReader(new FileReader(path + fileName));
			
			while((str = br.readLine()) != null) {
				result.append(str);
				result.append("\n");
			}
			
			result.delete(result.length() - 1, result.length());
	 
		} catch (IOException e) {
			throw new AppException("Erro loading configuration file.", e);
		} finally {
			closeBufferedReader(br);
		}
		return result.toString();
	}

	private static void closeFileReader(InputStream input) {
		try {
			if (input != null)
				input.close();
		} catch (IOException e) {
			throw new AppException("Error closing file", e);
		}	
	}
	
	private static void closeBufferedReader(BufferedReader br) {
		try {
			if (br != null)
				br.close();
		} catch (IOException e) {
			throw new AppException("Error closing file", e);
		}	
	}

	public static void fileWrite(String info, String fileName) {
		PrintWriter pw = null;

		try{

			URL s = ClassLoader.getSystemResource("");
			String path = s.getPath().substring(1);
			pw  = new PrintWriter(path + fileName);
			
			int idx = 0, idx1 = 0;
			
			while (idx1 != -1) {
				idx1 = info.indexOf("\n",idx);
				
				if(idx1 == -1) 
					pw.println(info.substring(idx));
				else
					pw.println(info.substring(idx, idx1));
				idx = idx1 + 1;
			}

		}catch(IOException e){
			System.out.println("File Output error!");
		}
		finally {
			closeFileWriter(pw);
		}
	}
	
	private static void closeFileWriter(PrintWriter pw) {
		try {
			if (pw != null)
				pw.close();
		} catch (Exception e) {
			System.out.println("Error closing file");
		}	
	}
	
	public static boolean existsFile(String path){
		String s = ClassLoader.getSystemResource("").getPath().substring(1) + path;
		File f = new File(s);
		
		return f.exists();
	}

}
