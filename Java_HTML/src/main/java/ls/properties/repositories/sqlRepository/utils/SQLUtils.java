package ls.properties.repositories.sqlRepository.utils;

import java.util.List;

import ls.properties.domain.entities.IEntity;
import ls.properties.utils.Pair;

public abstract class SQLUtils {

	public static StringBuilder getInsert(List<Pair<String, Object>> info) {

		StringBuilder sb = new StringBuilder("?");

		for (int i = 1; i < info.size(); i++)
			sb.append(", ?");

		return sb;
	}

	public static StringBuilder getFields(List<Pair<String, Object>> info) {
		String[] s = getArrayFromList(info);
		StringBuilder sb = new StringBuilder(s[0]);

		for (int i = 1; i < s.length; ++i) {
			sb.append(", ");
			sb.append(s[i]);
		}

		return sb;
	}

	private static String[] getArrayFromList(List<Pair<String, Object>> info) {
		String[] str = new String[info.size()];

		for (int i = 0; i < info.size(); ++i)
			str[i] = info.get(i).getElem1();

		return str;
	}

	public static Object[] getValues(List<Pair<String, Object>> info) {
		Object[] obj = new Object[info.size()];

		for (int i = 0; i < info.size(); ++i)
			obj[i] = info.get(i).getElem2();

		return obj;
	}

	public static void validateEntity(IEntity obj) {
		if (obj == null)
			throw new IllegalArgumentException("Null parameter.");
	}

	public static String getCondition(List<Pair<String, Object>> info,
			String separator) {
		String[] str = getArrayFromList(info);
		StringBuilder sb = new StringBuilder(str[0]);

		sb.append(" = ? ");

		for (int i = 1; i < str.length; i++) {
			sb.append(separator);
			sb.append(" ");
			sb.append(str[i]);
			sb.append(" = ? ");
		}

		return sb.toString();
	}

	public static String getSetCondition(List<Pair<String, Object>> fields) {
		return getCondition(fields, ",");
	}

	public static String getWhereCondition(List<Pair<String, Object>> key) {
		return getCondition(key, "AND");
	}

	public static void validate(String nameTable, String instruction) {

		if (nameTable == null || instruction == null)
			throw new IllegalArgumentException("Null parameter.");
		
		if (nameTable.isEmpty() || instruction.isEmpty())
			throw new IllegalArgumentException("Empty parameter.");
		
	}

	public static String getAddInstruct(String entityName,
			List<Pair<String, Object>> fields) {

			StringBuilder sb = new StringBuilder("INSERT INTO ");
			sb.append(entityName);
			sb.append(" VALUES (");
			sb.append(SQLUtils.getInsert(fields));
			sb.append(");");

			return sb.toString();
	}

	public static String getDeleteInstruct(String entityName,
			List<Pair<String, Object>> key) {

		StringBuilder sb = new StringBuilder("DELETE FROM ");
		sb.append(entityName);
		sb.append(" WHERE ");
		sb.append(SQLUtils.getWhereCondition(key));
		sb.append(";");

		return sb.toString();
	}

	public static String getUpdateInstruct(String entityName,
			List<Pair<String, Object>> fields, List<Pair<String, Object>> key) {
		StringBuilder sb = new StringBuilder("UPDATE ");
		sb.append(entityName);
		sb.append(" SET ");
		sb.append(SQLUtils.getSetCondition(fields));
		sb.append(" WHERE ");
		sb.append(SQLUtils.getWhereCondition(key));
		sb.append(";");

		return sb.toString();
	}

	public static StringBuilder getInstruct(String entityName,
			List<Pair<String, Object>> fields) {

		StringBuilder sb = new StringBuilder("SELECT ");
		sb.append(SQLUtils.getFields(fields));
		sb.append(" FROM ");
		sb.append(entityName);
		return sb;
	}
}
