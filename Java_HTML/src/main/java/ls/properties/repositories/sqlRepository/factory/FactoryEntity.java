package ls.properties.repositories.sqlRepository.factory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.InvalidFunctionalityException;
import ls.properties.exceptions.RepositoryException;
import ls.properties.repositories.sqlRepository.SQLRepository;
import ls.properties.utils.Pair;

public abstract class FactoryEntity {

	private static FactoryEntity[] factories = { new ClientFactory("Client"),
			new PropertyFactory("Property"), new LocalFactory("Local"),
			new RentalFactory("Rental") };

	protected String name;

	public FactoryEntity(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	protected SQLRepository dbr;

	public void setDbr(SQLRepository dbr) {
		this.dbr = dbr;
	}

	public static FactoryEntity getFactoryEntity(IEntity obj,
			SQLRepository dbRepository) {
		for (FactoryEntity fe : factories) {
			if (fe.getName().equals(obj.getTypeName())) {
				fe.setDbr(dbRepository);
				return fe;
			}
		}

		throw new InvalidFunctionalityException(
				"There is no fatory for entity: " + obj.getTypeName());
	}

	public List<IEntity> getEntityList(ResultSet result) {
		List<IEntity> entities = new ArrayList<IEntity>();

		if (result == null)
			return entities;

		try {
			while (result.next()) {
				entities.add(this.createEntity(result));
			}
		} catch (SQLException e) {
			throw new RepositoryException("Erro reading database result info.",
					e);
		}
		return entities;
	}

	protected Client getClient(String username) {

		Client c = new Client(username, "", "", "");
		List<Pair<String, Object>> conditionVal = new ArrayList<Pair<String, Object>>();
		conditionVal.add(new Pair<String, Object>("username", username));
		return (Client) dbr.get(c, conditionVal).get(0);
	}

	protected Property getProperty(int id) {
		Property p = new Property(id, "", "", 1, null, null);
		List<Pair<String, Object>> conditionVal = new ArrayList<Pair<String, Object>>();
		conditionVal.add(new Pair<String, Object>("id", id));
		return (Property) dbr.get(p, conditionVal).get(0);
	}

	protected abstract IEntity createEntity(ResultSet rs) throws SQLException;

	public abstract List<Pair<String, Object>> getFields(List<Pair<String, Object>> allFields);
	
	public List<Pair<String, Object>> getKey(List<Pair<String, Object>> key){
		
		List<Pair<String, Object>> keys = new ArrayList<>();
		
		for (Pair<String, Object> pair : key) {
			if(pair.getElem2() == null) {
				keys.add(new Pair<String, Object>(pair.getElem1(), 0));
			}
			else {
				List<Pair<String, Object>> keys1 = getKeyForPair(pair);
			
				if(keys1.isEmpty())
					keys.add(pair);
				else
					keys.addAll(keys1);
			}
			
		}
		return keys;
	}

	public static List<Pair<String, Object>> getKeyForPair(Pair<String, Object> pair) {
		
		Object obj = pair.getElem2();
		List<Pair<String, Object>> key = new ArrayList<>();
		
		if (obj instanceof Property) {
			key.add(new Pair<String, Object>("id", ((Property)obj).getId()));
			return key;
		}
		
		if (obj instanceof Client){
			key.add(new Pair<String, Object>(pair.getElem1(), ((Client)obj).getUsername()));
			return key;
		}
		
		if (obj instanceof Local){
			key.add(new Pair<String, Object>("region", ((Local)obj).getRegion()));
			key.add(new Pair<String, Object>("locale", ((Local)obj).getLocale()));
			return key;
		}
		
		return key;
	}

}
