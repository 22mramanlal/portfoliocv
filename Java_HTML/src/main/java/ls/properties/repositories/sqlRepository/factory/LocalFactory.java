package ls.properties.repositories.sqlRepository.factory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Local;
import ls.properties.utils.Pair;

public class LocalFactory extends FactoryEntity {

	public LocalFactory(String name) {
		super(name);
	}

	@Override
	protected IEntity createEntity(ResultSet rs) throws SQLException {
		String region = rs.getString("region");
		String locale = rs.getString("locale");

		return new Local(region, locale);
	}

	@Override
	public List<Pair<String, Object>> getFields(
			List<Pair<String, Object>> allFields) {
		return allFields;
	}

	public static Collection<? extends Pair<String, Object>> getKeys(Pair<String, Object> pair) {
		
		List<Pair<String, Object>> key = new ArrayList<>();
		
		if (pair.getElem2() == null) {
			key.add(new Pair<String, Object>("region", ""));
			key.add(new Pair<String, Object>("locale", ""));
			return key;
		}
		
		key.add(new Pair<String, Object>("region", ((Local)pair.getElem2()).getRegion()));
		key.add(new Pair<String, Object>("locale", ((Local)pair.getElem2()).getLocale()));
		
		return key;
	}

}
