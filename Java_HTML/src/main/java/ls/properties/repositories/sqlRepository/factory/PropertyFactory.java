package ls.properties.repositories.sqlRepository.factory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.utils.Pair;

public class PropertyFactory extends FactoryEntity {

	public PropertyFactory(String name) {
		super(name);
	}
	@Override
	protected IEntity createEntity(ResultSet rs) throws SQLException {
		
		int id = rs.getInt("id");
		int price = rs.getInt("price");
		Local location = new Local(rs.getString("region"), rs.getString("locale"));
		Client owner = getClient(rs.getString("owner"));
		String sort = rs.getString("sort");
		String text = rs.getString("text");
		
		return new Property(id, sort, text, price, location, owner);
	}
	@Override
	public List<Pair<String, Object>> getFields(
			List<Pair<String, Object>> allFields) {
		
		List<Pair<String, Object>> fields = new ArrayList<>();
		int idx = 0;
		
		fields.add(allFields.get(idx++));
		fields.add(allFields.get(idx++));
		fields.add(allFields.get(idx++));
		fields.add(allFields.get(idx++));
		fields.addAll(LocalFactory.getKeys(allFields.get(idx++)));
		fields.add(ClientFactory.getKeys(allFields.get(idx++)));
		
		return fields;
	}
	
	public static Pair<String, Object> getKeys(Pair<String, Object> pair) {
		if (pair.getElem2() == null)
			return new Pair<String, Object>("id", pair.getElem2());
		return new Pair<String, Object>("id", ((Property)pair.getElem2()).getId());
	}	

}
