package ls.properties.repositories.sqlRepository;

import java.util.List;

import ls.properties.domain.entities.IEntity;
import ls.properties.exceptions.RepositoryException;
import ls.properties.repositories.IRepository;
import ls.properties.repositories.sqlRepository.factory.FactoryEntity;
import ls.properties.repositories.sqlRepository.utils.SQLFuncionalities;
import ls.properties.repositories.sqlRepository.utils.SQLUtils;
import ls.properties.utils.IOFiles;
import ls.properties.utils.Pair;

public class SQLRepository implements IRepository {

	protected SQLFuncionalities dbf;

	public SQLRepository() {
		this(new SQLFuncionalities(), new String[] {});
	}

	public SQLRepository(String dbConfFileName, String... filesNames) {
		this(new SQLFuncionalities(dbConfFileName), filesNames);
	}

	public SQLRepository(SQLFuncionalities dbFuncionalities,
			String... filesNames) {
		this.dbf = dbFuncionalities;
		createRepository(filesNames);
	}

	private void createRepository(String... createFilesNames) {
		String[] files = new String[2];
		if (createFilesNames.length == 0)
			files = new String[] { "resources/CREATE_DB.sql",
					"resources/CREATE_TABLES_INSERT_DEFAULTS.sql" };
		else
			files = createFilesNames;

		createDataBase(files);
		createTables(files);
	}

	private void createTables(String[] files) {
		String instruction = IOFiles.fileRead(files[1]);
		this.dbf.execute(instruction);
	}

	private void createDataBase(String[] files) {

		String instruction = IOFiles.fileRead(files[0]);
		String dataBaseName = this.dbf.getDataBaseName();

		this.dbf.setDataBaseName("master");
		this.dbf.execute(instruction);
		this.dbf.setDataBaseName(dataBaseName);
	}

	@Override
	public void add(IEntity obj) {
		SQLUtils.validateEntity(obj);

		String entityName = obj.getTypeName();
		FactoryEntity fe = FactoryEntity.getFactoryEntity(obj, this);
		List<Pair<String, Object>> fields = fe.getFields(obj.getAllFields());

		String instruction = SQLUtils.getAddInstruct(entityName, fields);

		this.dbf.setPrepareStatement(entityName, instruction, SQLUtils
				.getFields(fields).toString(), SQLUtils.getValues(fields));

		this.dbf.executeInsert();
	}

	@Override
	public void delete(IEntity obj) {
		SQLUtils.validateEntity(obj);
		FactoryEntity fe = FactoryEntity.getFactoryEntity(obj, this);
		List<Pair<String, Object>> key = fe.getKey(obj.getKey());
		
		if(!existEntity(obj, key))
			throw new RepositoryException("The entity you want to delete doesn't exist");

		String entityName = obj.getTypeName();
		String instruction = SQLUtils.getDeleteInstruct(entityName, key);

		this.dbf.setPrepareStatement(entityName, instruction, SQLUtils
				.getFields(key).toString(), SQLUtils.getValues(key));
		this.dbf.executeUpdate("The delete wasn't performed.",
				"The data you want to delete don't exist");
	}

	@Override
	public void update(IEntity obj) {
		SQLUtils.validateEntity(obj);

		FactoryEntity fe = FactoryEntity.getFactoryEntity(obj, this);
		List<Pair<String, Object>> key = fe.getKey(obj.getKey());
	
		if(!existEntity(obj, key))
			throw new RepositoryException("The entity you want to update doesn't exist");
		
		String entityName = obj.getTypeName();
		List<Pair<String, Object>> fields = fe.getFields(obj.getAllFields());
		
		String instruction = SQLUtils.getUpdateInstruct(entityName, fields, key);

		fields.addAll(key);
		this.dbf.setPrepareStatement(entityName, instruction, SQLUtils
				.getFields(fields).toString(), SQLUtils.getValues(fields));
		this.dbf.executeUpdate("The update wasn't performed.", "Failed update.");
	}

	@Override
	public List<IEntity> get(IEntity obj, List<Pair<String, Object>> key) {
		SQLUtils.validateEntity(obj);

		String entityName = obj.getTypeName();
		FactoryEntity fe = FactoryEntity.getFactoryEntity(obj, this);
		List<Pair<String, Object>> fields = fe.getFields(obj.getAllFields());
		key = fe.getKey(key);

		StringBuilder instruction = SQLUtils.getInstruct(entityName, fields);
		instruction.append(" WHERE ");
		instruction.append(SQLUtils.getWhereCondition(key));
		instruction.append(";");

		return getExecute(obj, entityName, key, instruction);
	}

	@Override
	public List<IEntity> getAll(IEntity obj) {
		SQLUtils.validateEntity(obj);

		String entityName = obj.getTypeName();
		FactoryEntity fe = FactoryEntity.getFactoryEntity(obj, this);
		List<Pair<String, Object>> fields = fe.getFields(obj.getAllFields());

		return getExecute(obj, entityName, fields,
				SQLUtils.getInstruct(entityName, fields));
	}

	private List<IEntity> getExecute(IEntity obj, String entityName,
			List<Pair<String, Object>> fields, StringBuilder instruction) {

		FactoryEntity fe = FactoryEntity.getFactoryEntity(obj, this);

		this.dbf.setPrepareStatement(entityName, instruction.toString(),
				SQLUtils.getFields(fields).toString(),
				SQLUtils.getValues(fields));

		List<IEntity> entities = fe.getEntityList(this.dbf.executeQuery());
		this.dbf.closeConnection();

		return entities;
	}

	private boolean existEntity(IEntity e, List<Pair<String, Object>> key) {
		if(get(e, key).isEmpty())
			return false;
		return true;
	}
}
