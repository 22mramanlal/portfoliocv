package ls.properties.repositories.sqlRepository.factory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.IEntity;
import ls.properties.utils.Pair;

public class ClientFactory extends FactoryEntity{

	public ClientFactory(String name) {
		super(name);
	}

	@Override
	protected IEntity createEntity(ResultSet rs) throws SQLException {
		String username = rs.getString("username");
		String email = rs.getString("email");
		String pass = rs.getString("pass");
		String fullName = rs.getString("fullName");
		
		return new Client(username, pass, email, fullName);
	}

	@Override
	public List<Pair<String, Object>> getFields(
			List<Pair<String, Object>> allFields) {
		return allFields;
	}

	public static Pair<String, Object> getKeys(Pair<String, Object> pair) {
		if (pair.getElem2() == null)
			return pair;
		return new Pair<String, Object>(pair.getElem1(), ((Client)pair.getElem2()).getUsername());
	}
}
