package ls.properties.repositories.sqlRepository.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;

import ls.properties.exceptions.RepositoryException;
import ls.properties.utils.IOFiles;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public class SQLFuncionalities {

	protected Statement stmt = null;
	protected ResultSet rs = null;
	protected Connection con = null;
	protected PreparedStatement pstmt = null;

	private String serverName;
	private String dataBaseName;
	private String user;
	private String passWord;

	public SQLFuncionalities() {
		this("resources/config.properties");
	}

	public SQLFuncionalities(String file) {
		String[] sqlInfo = IOFiles.fileDBRead(file);
		serverName = sqlInfo[0];
		dataBaseName = sqlInfo[1];
		user = sqlInfo[2];
		passWord = sqlInfo[3];
	}
	
	public String getDataBaseName() {
		return this.dataBaseName;
	}
	
	public void setDataBaseName(String dbName) {
		this.dataBaseName = dbName;
	}

	public void openConnection() {

		SQLServerDataSource dataSource = new SQLServerDataSource();
		dataSource.setServerName(serverName);
		dataSource.setDatabaseName(dataBaseName);
		dataSource.setUser(user);
		dataSource.setPassword(passWord);

		try {
			con = dataSource.getConnection();
			con.setAutoCommit(false);
			stmt = con.createStatement();
		} catch (SQLException e) {
			throw new RepositoryException("Failed connection with data base.", e);
		}
	}

	public void closeConnection() {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				throw new RepositoryException("Failed closing connection.", e);
			}
		}
	}

	public void setPrepareStatement(String nameTable, String instruction,
			String fields, Object... values) {

		SQLUtils.validate(nameTable, instruction);

		try {
			openConnection();
			pstmt = con.prepareStatement(instruction, PreparedStatement.RETURN_GENERATED_KEYS);
			if (instruction.contains("?"))
				setPStmt(nameTable, fields, values);

		} catch (SQLException | RepositoryException e) {
			closeConnection();
			throw new RepositoryException("Error on database.", e);
		}
	}

	private void setPStmt(String nameTable, String fields, Object... values) {
		try {
			if (fields.isEmpty())
				fields = "*";
			int numbOfColums = executeQuery(nameTable, fields);

			if (values.length != numbOfColums)
				throw new IllegalArgumentException(
						"There are no enough values to insert the tuple on table");

			setPStmtValues(nameTable, numbOfColums, values);

		} catch (SQLException e) {
			throw new RepositoryException(
					"Erro setting insert prepared statement.", e);
		}
	}

	private int executeQuery(String nameTable, String fields)
			throws SQLException {
		StringBuilder str = new StringBuilder("Select ");
		str.append(fields);
		str.append(" From ");
		str.append(nameTable);

		rs = stmt.executeQuery(str.toString());
		return rs.getMetaData().getColumnCount();
	}

	private void setPStmtValues(String nameTable, int numbOfColums,
			Object[] values) throws SQLException {
		for (int i = 1; i <= numbOfColums; ++i) {
			switch (rs.getMetaData().getColumnType(i)) {
			case Types.INTEGER:
				pstmt.setInt(i, (int) values[i - 1]);
				break;
			case Types.VARCHAR:
				pstmt.setString(i, (String) values[i - 1]);
				break;
			case Types.NVARCHAR:
				pstmt.setString(i, (String) values[i - 1]);
				break;
			case Types.TIMESTAMP:
				pstmt.setTimestamp(i, (Timestamp) values[i - 1]);
				break;
			case Types.BIT:
				pstmt.setInt(i, (int) ((boolean) values[i - 1] ? 1 : 0));
				break;
			case Types.CHAR:
				pstmt.setString(i, (String) values[i - 1]);
				break;
			default:
				throw new SQLException("Data don't support.\n Table: "
						+ nameTable + "\n Column: "
						+ rs.getMetaData().getColumnLabel(i));
			}
		}
	}

	public void executeInsert() {
		try {
			pstmt.executeUpdate();
			commit();
		} catch (SQLException e) {
			roldback();
			closeConnection();
			if (e.toString().contains("duplicate"))
				throw new RepositoryException("Value is duplicate.", e);
			throw new RepositoryException("Failed insertion.", e);
		}
		closeConnection();
	}

	public void executeUpdate(String msg, String msg1) {
		try {
			pstmt.executeUpdate();
			commit();
		} catch (SQLException e) {
			roldback();
			closeConnection();
			throw new RepositoryException(msg1, e);
		}
		closeConnection();
	}

	private void commit() {
		try {
			con.commit();
			con.setAutoCommit(true);
		} catch (SQLException e) {
			throw new RepositoryException("Commit error.", e);
		}
	}

	private void roldback() {
		try {
			con.rollback();
		} catch (SQLException e1) {
			throw new RepositoryException("Roldback error.", e1);
		}
	}

	public ResultSet executeQuery() {
		try {
			rs = pstmt.executeQuery();
			commit();
		} catch (SQLException e) {
			roldback();
			closeConnection();
			throw new RepositoryException("Query erro cause by database.", e);
		}
		return rs;
	}

	public void execute(String instruction) {
		openConnection();
		try {
			this.stmt.executeUpdate(instruction);
		} catch (SQLException e) {
			roldback();
			closeConnection();
			throw new RepositoryException("Erro creating DB Repository", e);
		}
		commit();
		closeConnection();
	}

}
