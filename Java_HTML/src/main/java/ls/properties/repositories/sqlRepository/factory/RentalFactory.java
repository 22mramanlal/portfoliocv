package ls.properties.repositories.sqlRepository.factory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.utils.Pair;

public class RentalFactory extends FactoryEntity {

	public RentalFactory(String name) {
		super(name);
	}

	@Override
	protected IEntity createEntity(ResultSet rs) throws SQLException {

		int id = rs.getInt("id");
		Client renter = getClient(rs.getString("renter"));
		Property property = getProperty(id);
		int cy = rs.getInt("cy");
		int cw = rs.getInt("cw");
		boolean state = rs.getBoolean("rentState");
		
		return new Rental(property, cy, cw, renter, state,
				rs.getTimestamp("orderDate"), rs.getTimestamp("acceptanceDate"));
	}

	@Override
	public List<Pair<String, Object>> getFields(List<Pair<String, Object>> allFields) {
		List<Pair<String, Object>> fields = new ArrayList<>();
		int idx = 0;
		
		fields.add(PropertyFactory.getKeys(allFields.get(idx++)));
		Pair<String, Object> p1 = allFields.get(idx++);
		Pair<String, Object> p2 = allFields.get(idx++);
		fields.add(ClientFactory.getKeys(allFields.get(idx++)));
		fields.add(p1);
		fields.add(p2);
		fields.add(allFields.get(idx++));
		fields.add(allFields.get(idx++));
		fields.add(allFields.get(idx++));
		
		return fields;
	}
				
}
