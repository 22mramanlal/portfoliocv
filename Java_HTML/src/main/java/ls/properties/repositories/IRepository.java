package ls.properties.repositories;

import java.util.Collection;
import java.util.List;

import ls.properties.domain.entities.IEntity;
import ls.properties.utils.Pair;

public interface IRepository {

	public void add(IEntity obj);
	
	public void delete(IEntity obj);
	
	public void update(IEntity obj);
	
	/*
	 * Returns a list of All Entities of type obj  
	 * or null if there is no IEntity of type obj
	*/
	public Collection<IEntity> getAll(IEntity obj);
	
	/*
	 * Returns a list of All Entities of type obj with the same values of key 
	 * or null if there is no object with same values
	*/
	public Collection<IEntity> get(IEntity obj, List<Pair<String,Object>>key);

}
