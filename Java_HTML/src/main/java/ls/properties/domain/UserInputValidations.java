package ls.properties.domain;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.DuplicatedEntityException;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.repositories.IRepository;
import ls.properties.utils.Pair;

public class UserInputValidations {
	
	
	
	public static void userAutentication(String userName, String passWord, IRepository rep){
		Client dummy = new Client(null, null, null, null);
		List<Pair<String,Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String,Object>("username", userName));
		key.add(new Pair<String,Object>("pass", passWord));
		
		Collection<IEntity> u = rep.get(dummy, key);
		
		if(u == null || u.isEmpty())
			throw new InvalidUserParametersException("Failed user authentication\nInvalid usernamelog or passwordlog");
	}
	
	public static void duplicatedRental(Property property, int year, int cw, IRepository rep){
		
		Rental dummy = new Rental(null, 0, 0, null, false, null, null);
		List<Pair<String,Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String,Object>("property", property));
		key.add(new Pair<String,Object>("cy", year));
		key.add(new Pair<String,Object>("cw", cw));
		
		Collection<IEntity> r = rep.get(dummy, key);
		
		if(r!=null && !r.isEmpty())			
			throw new DuplicatedEntityException("There is already a rental to the specified date");
	}
	
	public static void duplicatedUserName(String userName, IRepository rep){
		Client dummy = new Client(null, null, null, null);
		
		List<Pair<String,Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String,Object>("username", userName));
				
		Collection<IEntity> u = rep.get(dummy, key);
		
		if(u != null && !u.isEmpty())
			throw new DuplicatedEntityException("UserName already exists");
	}
	
	public static void existLocation(String location, IRepository rep){
		String[] local =  location.split("-");
		
		Local dummy = new Local(null, null);
		List<Pair<String,Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String,Object>("locale", local[1]));
		key.add(new Pair<String,Object>("region", local[0]));
		
		Collection<IEntity> l = rep.get(dummy, key);
		
		if(l == null || l.isEmpty())			
			throw new InvalidUserParametersException("Inexistent location");	
	}
	
	public static void existProperty(String id, IRepository rep){
		validateId(id);
		
		Property dummy = new Property(0, null, null, 0, null, null);
		List<Pair<String,Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String,Object>("property", Integer.parseInt(id)));

		Collection<IEntity> p = rep.get(dummy, key);
		
		if(p == null || p.isEmpty())			
			throw new InvalidUserParametersException("Inexistent property id");	
	}
	
	public static void existUser(String userName, IRepository rep){
		Client dummy = new Client(null, null, null, null);

		List<Pair<String,Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String,Object>("username", userName));

		Collection<IEntity> u = rep.get(dummy, key);
		
		if(u == null || u.isEmpty())			
			throw new InvalidUserParametersException("Inexistent username");
	}

	public static void existRental(String id, String year, String cw, IRepository rep){
		validateId(id);
		validateYear(year);
		validateCw(cw);
		
		Rental dummy = new Rental(null, 0, 0, null, false, null, null);

		List<Pair<String,Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String,Object>("property", Integer.parseInt(id)));
		key.add(new Pair<String,Object>("cy", Integer.parseInt(year)));
		key.add(new Pair<String,Object>("cw", Integer.parseInt(cw)));

		
		Collection<IEntity> r = rep.get(dummy, key);
		
		if(r == null || r.isEmpty())			
			throw new InvalidUserParametersException("Inexistent rental");
	}

	public static void validateEmail(String email){
		if(!email.contains("@"))
			throw new InvalidUserParametersException("Invalid email");
	}
	
	public static void validateLocal(String location){
		if(!location.contains("-"))
			throw new InvalidUserParametersException("Invalid Location format");
		String[] local = location.split("-");
		
		if(local.length!=2 || local[0].isEmpty() || local[1].isEmpty())
			throw new InvalidUserParametersException("Invalid Location format");
	}	
	
	public static void validateId(String id){
		validateNumeric(id, "Invalid id");
	}
	
	public static void validateYear(String year){
		validateNumeric(year, "Invalid year");
		
		if(Integer.parseInt(year)<0)
			throw new InvalidUserParametersException("Invalid year");
	}
	
	public static void validateCw(String cw){
		validateNumeric(cw, "Invalid cw");
		
		if(Integer.parseInt(cw)<0 || Integer.parseInt(cw)>52)
			throw new InvalidUserParametersException("Invalid cw");
	}
	
	public static void validateNumeric(String num, String msg){
		
		try{
			Integer.parseInt(num);
		}catch(NumberFormatException e){
			throw new InvalidUserParametersException(msg);
		}
	}
	
	public static boolean isRenterOrOwnerOfRental(Client c, Rental r){
		return r.getProperty().getOwner().getUsername().equals(c.getUsername())
				||
				r.getRenter().getUsername().equals(c.getUsername());
	}
	
	public static boolean isOwnerOfProperty(Client c, Property p){
		return p.getOwner().getUsername().equals(c.getUsername());
	}

	public static void validatePort(String port) {
			validateNumeric(port, "Invalid port");
			if(Integer.parseInt(port)<0)
				throw new InvalidUserParametersException("Invalid port");
	}

	public static void duplicatedLocal(String region, String locale, IRepository rep) {
		Local dummy = new Local(null, null);

		List<Pair<String,Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String,Object>("region", region));
		key.add(new Pair<String,Object>("locale", locale));
		
		Collection<IEntity> l= rep.get(dummy, key);
		
		if(l != null && !l.isEmpty())
			throw new DuplicatedEntityException("Locale already exists");
	}

	public static void validatePrice(String price) {
		validateNumeric(price, "Invalid id");
	}
	
}
