package ls.properties.domain.entitiesManagers;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.domain.UserInputValidations;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.repositories.IRepository;
import ls.properties.utils.Pair;

public class ClientManager extends EntityManager{
	
	private PropertyManager propertyManager;
	
	public ClientManager(IRepository rep, IManagerSupplier manager) {
		super(rep);
		propertyManager = (PropertyManager)manager.getManager("Property");
	}
	
	
	public Client createClient(String username, String pass, String email, String fullname){
		validateFileds(username, fullname, email, pass);
		
		Client c = new Client(username, pass, email, fullname);
				
		rep.add(c);
		
		return c;
	}
	
	public Client getClient(String username){
		Client dummy = new Client(null, null, null, null);
		List<Pair<String,Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String,Object>("username", username));
			
		Collection<IEntity> client = rep.get(dummy, key);
		
		if(client == null || client.isEmpty())  
			throw new InexistentEntityException("Inexistent Client");
		
		return (Client)client.iterator().next();
	}
	
	public void deleteClient(Client c){
		getClient(c.getUsername());
		
		deleteClientDepedencies(c);
		
		rep.delete(c);			
	}
	
	public void deleteClientDepedencies(Client c){
		Collection<Property> properties = propertyManager.getPropertiesOf(c);
		
		if(properties == null || properties.isEmpty())
			return;
			
		for(Property p : properties)
			propertyManager.deleteProperty(p);
	}
	
	public void updateClient(Client c){
		getClient(c.getUsername());
		
		rep.update(c);
	}
	
	public  Collection<Client> getAllClients(){		
		Collection<IEntity> allClients = rep.getAll(new Client(null, null, null, null));
		
		return (allClients==null)? null : castToClient(allClients);
	} 
	
	private Collection<Client> castToClient(Collection<IEntity> allClients) {
		Collection<Client> clients = new LinkedList<Client>();
		for(IEntity e : allClients)
			clients.add((Client)e);
		
		return clients;
	}

	public void userAuthentication(String userName, String passWord){
		UserInputValidations.userAutentication(userName, passWord, rep);
	}
	
	private void validateFileds(String username, String fullname, String email, String pass) {
		if(username == null || username.isEmpty())
			throw new InvalidUserParametersException("Invalid username");
		if(fullname == null || fullname.isEmpty())
			throw new InvalidUserParametersException("Invalid fullname");
		if(pass == null || pass.isEmpty())
			throw new InvalidUserParametersException("Invalid password");
		if(email == null || email.isEmpty())
			throw new InvalidUserParametersException("Invalid email");

		UserInputValidations.validateEmail(email);
		UserInputValidations.duplicatedUserName(username, rep);
	}
	
}
