package ls.properties.domain.entitiesManagers;

import ls.properties.repositories.IRepository;

public abstract class EntityManager {
	protected IRepository rep;
	
	public EntityManager(IRepository rep){
		this.rep = rep;
	}
}
