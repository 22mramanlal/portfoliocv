package ls.properties.domain.entitiesManagers;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.domain.UserInputValidations;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.repositories.IRepository;
import ls.properties.utils.Pair;

public class PropertyManager extends EntityManager{
	
	private RentalManager rentalManager;
	
	public PropertyManager(IRepository rep, IManagerSupplier manager){
		super(rep);
		rentalManager = (RentalManager)manager.getManager("Rental");
	}
	
	public Property createProperty(String sort, String text, String price, Local location, Client owner){
		validateFields(sort, text, price, location, owner);
		
		Collection<IEntity> allProperties = rep.getAll(new Property(0, null, null, 0, null, null));
		
		int propId = getMaxId(allProperties) + 1;
		
		Property p = new Property(propId, sort, text, Integer.parseInt(price), location, owner);
		rep.add(p);
		
		return p;
	}
	
	public Property getProperty(String id){
		UserInputValidations.validateId(id);
		
		Property dummy = new Property(0, null, null, 0, null, null);
		List<Pair<String,Object>> key=  new LinkedList<Pair<String, Object>>();
		key.add(new Pair<String, Object>("id", Integer.parseInt(id)));
		
		Collection<IEntity> property = rep.get(dummy, key);
		
		if(property == null || property.isEmpty())
			throw new InexistentEntityException("Inexistent Property");
		
		return (Property)property.iterator().next();
	}
	
	public void deleteProperty(Property p){
		getProperty(Integer.toString(p.getId()));
		
		deletePropertyDepedencies(p);
			
		rep.delete(p);
	}

	private void deletePropertyDepedencies(Property p) {
		Collection<Rental> rentals = rentalManager.getRentalsFor(p);
		
		if(rentals == null || rentals.isEmpty())
			return;
		
		for(Rental r : rentals)
			rentalManager.deleteRental(r);
	}
	
	public void updateProperty(Property p){
		getProperty(Integer.toString(p.getId()));

		rep.update(p);
	}

	public  Collection<Property> getAllProperties(){		
		Collection<IEntity> allProperties = rep.getAll(new Property(0, null, null, 0, null, null));
		
		return (allProperties==null)? null : castToProperty(allProperties);
	} 
	
	private Collection<Property> castToProperty(Collection<IEntity> allProperties) {
		Collection<Property> properties = new LinkedList<Property>();
		for(IEntity e : allProperties)
			properties.add((Property)e);
		
		return properties;
	}
	
	private static int getMaxId(Collection<IEntity> list){
		if(list == null)
			return 0;
		
		int max = 0;
		
		for(IEntity p : list)
			if(((Property)p).getId() > max)
				max = ((Property)p).getId();
				
		return max;
	} 
	
	private void validateFields(String sort, String text, String price, Local location, Client owner){
		if(sort == null || sort.isEmpty())
			throw new InvalidUserParametersException("Invalid sort");
		if(text == null || text.isEmpty())
			throw new InvalidUserParametersException("Invalid text");
		if(price == null || price.isEmpty())
			throw new InvalidUserParametersException("Invalid price");
		if(location == null)
			throw new InvalidUserParametersException("Invalid location");
		if(owner == null)
			throw new InvalidUserParametersException("Invalid owner");
		
		UserInputValidations.validatePrice(price);
		UserInputValidations.userAutentication(owner.getUsername(), owner.getPass(), rep);
	}

	public void isOwnerOfProperty(Client c, Property p) {
		if(!UserInputValidations.isOwnerOfProperty(c, p))
			throw new InvalidUserParametersException("Only owner of property has permition to this operation");
	}

	public Collection<Property> getPropertiesOf(Client client) {
		Property dummy = new Property(0, null, null, 0, null, null);
		
		List<Pair<String,Object>> key = new LinkedList<Pair<String, Object>>();
		key.add(new Pair<String, Object>("owner", client));
		
		Collection<IEntity> allProperties = rep.get(dummy, key);
		
		return (allProperties==null)? null : castToProperty(allProperties);
	}

	public Collection<Property> getPropertiesLocated(Local local) {
		Property dummy = new Property(0, null, null, 0, null, null);
		
		List<Pair<String,Object>> key = new LinkedList<Pair<String, Object>>();
		key.add(new Pair<String, Object>("location", local));
		
		Collection<IEntity> allProperties = rep.get(dummy, key);
		
		return (allProperties==null)? null : castToProperty(allProperties);
	}

	public Collection<Property> getPropertiesOfType(String type) {
		Property dummy = new Property(0, null, null, 0, null, null);
		
		List<Pair<String,Object>> key = new LinkedList<Pair<String, Object>>();
		key.add(new Pair<String, Object>("sort", type));
		
		Collection<IEntity> allProperties = rep.get(dummy, key);
		
		return (allProperties==null)? null : castToProperty(allProperties);
	}
}
