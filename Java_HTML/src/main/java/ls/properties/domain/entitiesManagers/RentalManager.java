package ls.properties.domain.entitiesManagers;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.domain.UserInputValidations;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.repositories.IRepository;
import ls.properties.utils.Pair;

public class RentalManager extends EntityManager{
	
	public RentalManager(IRepository rep){
		super(rep);
	}

	public Rental createRental(Property property, Client renter, String cy, String cw){
		validateArguments(property, renter, cy, cw);
		
		Rental r = new Rental(property, Integer.parseInt(cy), Integer.parseInt(cw), 
				renter, false, new Timestamp(System.currentTimeMillis()), null);
		
		rep.add(r);
	
		return r;
	}
	
	public Rental getRental(Property property, String cy, String cw){
		UserInputValidations.validateCw(cw);
		UserInputValidations.validateYear(cy);
		Rental dummy = new Rental(null, 0, 0, null, false, null, null);
		
		List<Pair<String, Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String, Object>("property", property));
		key.add(new Pair<String, Object>("cy", Integer.parseInt(cy)));
		key.add(new Pair<String, Object>("cw", Integer.parseInt(cw)));
		
		Collection<IEntity> rental = rep.get(dummy, key);
		
		if(rental == null || rental.isEmpty())
			throw new InexistentEntityException("Inexistent Rental");
		
		return (Rental)rental.iterator().next();
	}
	
	public void deleteRental(Rental r){
		getRental(r.getProperty(), Integer.toString(r.getCy()), Integer.toString(r.getCw()));
		
		rep.delete(r);
	}
	
	public void updateRental(Rental r){
		getRental(r.getProperty(), Integer.toString(r.getCy()), Integer.toString(r.getCw()));
		
		rep.update(r);
	}
	
	public  Collection<Rental> getAllRentals(){		
		Collection<IEntity> allRentals = rep.getAll(new Rental(null, 0, 0, null, false, null, null));
		
		return (allRentals==null)? null : castToRental(allRentals);
	} 
	
	private Collection<Rental> castToRental(Collection<IEntity> allRentals) {
		Collection<Rental> rentals = new LinkedList<Rental>();
		for(IEntity e : allRentals)
			rentals.add((Rental)e);
		
		return rentals;
	}
	
	public void isRenterOrOwnerOfRental(Client c, Rental r) {
		if(!UserInputValidations.isRenterOrOwnerOfRental(c, r))
			throw new InvalidUserParametersException("Only owner or renter has permition to this operation");
	}
	
	private void validateArguments(Property property, Client renter, String cy, String cw) {
		
		if(property == null)
			throw new InvalidUserParametersException("Invalid User");
		if(renter == null)
			throw new InvalidUserParametersException("Invalid Renter");
		if(cw == null || cw.isEmpty())
			throw new InvalidUserParametersException("Invalid cw");
		if(cy == null || cy.isEmpty())
			throw new InvalidUserParametersException("Invalid year");
	
		UserInputValidations.validateYear(cy);
		UserInputValidations.validateCw(cw);
		UserInputValidations.userAutentication(renter.getUsername(), renter.getPass(), rep);
		UserInputValidations.duplicatedRental(property, Integer.parseInt(cy), Integer.parseInt(cw), rep);	
	}

	public Collection<Rental> getRentalsOf(Client client) {
		Rental dummy = new Rental(null, 0, 0, null, false, null, null);
		
		List<Pair<String,Object>> key = new LinkedList<Pair<String, Object>>();
		key.add(new Pair<String, Object>("renter", client));
		
		Collection<IEntity> rentals = rep.get(dummy, key);
		
		return (rentals==null)? null : castToRental(rentals);
	}

	public Collection<Rental> getRentalsFor(Property property) {
		Rental dummy = new Rental(null, 0, 0, null, false, null, null);
		
		List<Pair<String,Object>> key = new LinkedList<Pair<String, Object>>();
		key.add(new Pair<String, Object>("property", property));
		
		Collection<IEntity> rentals = rep.get(dummy, key);
		
		return (rentals==null)? null : castToRental(rentals);
	}

	public Collection<Rental> getRentalsForOnYear(Property property, int year) {
		Rental dummy = new Rental(null, 0, 0, null, false, null, null);
		
		List<Pair<String,Object>> key = new LinkedList<Pair<String, Object>>();
		key.add(new Pair<String, Object>("property", property));
		key.add(new Pair<String, Object>("cy", year));
		
		Collection<IEntity> rentals = rep.get(dummy, key);
		
		return (rentals==null)? null : castToRental(rentals);
	}
}
