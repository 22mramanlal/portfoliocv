package ls.properties.domain.entitiesManagers;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.domain.UserInputValidations;
import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.repositories.IRepository;
import ls.properties.utils.Pair;

public class LocalManager extends EntityManager{
	
	private PropertyManager propertyManager;	
	
	public LocalManager(IRepository rep, IManagerSupplier manager){
		super(rep);
		propertyManager = (PropertyManager)manager.getManager("Property");
	}
	
	public Local createLocal(String region, String locale){
		validateFields(region, locale);
		Local l = new Local(region, locale); 
		
		rep.add(l);
		
		return l;
	}

	public Local getLocal(String region, String locale){
		Local dummy = new Local(null, null);
		List<Pair<String,Object>> key = new LinkedList<Pair<String,Object>>();
		key.add(new Pair<String,Object>("locale", locale));
		key.add(new Pair<String,Object>("region", region));
				
		Collection<IEntity> local = rep.get(dummy, key);
		
		if(local == null || local.isEmpty())
			throw new InexistentEntityException("Inexistent Local");
		return (Local)local.iterator().next();
	}
	
	public void deleteLocal(Local l){
		getLocal(l.getRegion(), l.getLocale());
			
		deleteLocalDepedencies(l);
		
		rep.delete(l);
	}
	
	private void deleteLocalDepedencies(Local l){
		Collection<Property> properties = propertyManager.getPropertiesLocated(l);
		
		if(properties == null || properties.isEmpty())
			return;
			
		for(Property p : properties)
			propertyManager.deleteProperty(p);
	}
	
	
	public void updateLocal(Local l){
		getLocal(l.getRegion(), l.getLocale());
			
		rep.update(l);
	}
	
	public  Collection<Local> getAllLocals(){		
		Collection<IEntity> allLocals = rep.getAll(new Local(null, null));
		
		return (allLocals==null)? null : castToLocal(allLocals);
	} 
	
	private Collection<Local> castToLocal(Collection<IEntity> allLocals) {
		Collection<Local> locals = new LinkedList<Local>();
		for(IEntity e : allLocals)
			locals.add((Local)e);
		
		return locals;
	}

	private void validateFields(String region, String locale) {
		if(region == null || region.isEmpty())
			throw new InvalidUserParametersException("Invalid region");
		if(locale == null || locale.isEmpty())
			throw new InvalidUserParametersException("Invalid locale");
		
		UserInputValidations.duplicatedLocal(region, locale, rep);
	}
}
