package ls.properties.domain.entities;

import java.util.LinkedList;
import java.util.List;

import ls.properties.utils.Pair;

public class Client implements IEntity{
	
	private static String typeName = "Client";

	private String username;
	public String getUsername() {return username;}

	private String pass;
	public String getPass() {return pass;}
	public void setPass(String pass) {this.pass = pass;}
	
	private String email;
	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}
	
	private String fullname;
	public String getFullname() {return fullname;}
	public void setFullname(String fullname) {this.fullname = fullname;}

	public Client(String username, String pass, String email, String fullname) {
		this.username = username;
		this.pass = pass;
		this.email = email;
		this.fullname = fullname;
	}
	
	@Override
	public String getTypeName() {
		return Client.typeName;
	}
	
	@Override
	public List<Pair<String, Object>> getKey() {
		List<Pair<String, Object>> l = new LinkedList<Pair<String, Object>>();
		l.add(new Pair<String,Object>("username", username));
		
		return l;
	}
	@Override
	public List<Pair<String, Object>> getAllFields() {
		List<Pair<String, Object>> l = getKey();
		l.add(new Pair<String,Object>("pass", pass));
		l.add(new Pair<String,Object>("email", email));
		l.add(new Pair<String,Object>("fullname", fullname));
		
		return l;
	}
	
	@Override
	public boolean equals(Object other){
		if(other == this)
			return true;
		if(!(other instanceof Client))
			return false;
		
		Client othr = (Client)other;
		
		return othr.getEmail().equals(email) &&
				othr.getFullname().endsWith(fullname) &&
				othr.getPass().equals(pass) &&
				othr.getUsername().equals(username);
				
	}

}
