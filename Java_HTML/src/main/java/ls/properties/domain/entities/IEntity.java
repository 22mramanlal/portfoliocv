package ls.properties.domain.entities;

import java.util.List;

import ls.properties.utils.Pair;

public interface IEntity {
	
	public String getTypeName();
	public List<Pair<String,Object>> getKey();
	public List<Pair<String,Object>> getAllFields();
	
}
