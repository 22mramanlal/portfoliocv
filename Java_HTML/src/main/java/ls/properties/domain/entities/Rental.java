package ls.properties.domain.entities;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import ls.properties.utils.Pair;

public class Rental implements IEntity{
	
	private static String typeName = "Rental";
	
	private Property property;
	public Property getProperty() {return property;}
	
	private int cy;
	public int getCy() {return cy;}
	
	private int cw;
	public int getCw() {return cw;}
	
	private Client renter;
	public Client getRenter() {return renter;}
	public void setRenter(Client renter) {this.renter = renter;}

	private boolean rentState;
	public boolean getRentState() {return rentState;}
	public void setRentState(boolean rentState) {this.rentState = rentState;}
	
	private Timestamp orderDate;
	public Timestamp getOrderDate() {return orderDate;}
	public void setOrderDate(Timestamp orderDate) {this.orderDate = orderDate;}
	
	private Timestamp acceptanceDate;
	public Timestamp getAcceptanceDate() {return acceptanceDate;}
	public void setAcceptanceDate(Timestamp acceptanceDate) {this.acceptanceDate = acceptanceDate;}
		
	public Rental(Property property, int cy, int cw, Client renter, boolean rentState, Timestamp orderDate, Timestamp acceptanceDate) {
		this.property = property;
		this.renter = renter;
		this.cy = cy;
		this.cw = cw;
		this.rentState = rentState;
		this.orderDate = orderDate;
		this.acceptanceDate = acceptanceDate;
	}
	
	@Override
	public String getTypeName() {
		return Rental.typeName;
	}
	
	@Override
	public List<Pair<String, Object>> getKey() {
		List<Pair<String, Object>> l = new LinkedList<Pair<String,Object>>();
		l.add(new Pair<String, Object>("property", property));
		l.add(new Pair<String, Object>("cy", cy));
		l.add(new Pair<String, Object>("cw", cw));
		
		return l;
	}
	
	@Override
	public List<Pair<String, Object>> getAllFields() {
		List<Pair<String, Object>> l = getKey();
		l.add(new Pair<String, Object>("renter", renter));
		l.add(new Pair<String, Object>("rentState", rentState));
		l.add(new Pair<String, Object>("orderDate", orderDate));
		l.add(new Pair<String, Object>("acceptanceDate", acceptanceDate));

		return l;
	}
	
	@Override
	public boolean equals(Object other){
		if(other == this)
			return true;
		if(!(other instanceof Rental))
			return false;
		
		Rental othr = (Rental)other;
		
		return othr.getCw() == cw &&
				othr.getCy() == cy &&
				othr.getOrderDate().equals(orderDate) &&
				othr.getProperty().equals(property) &&
				othr.getRenter().equals(renter) &&
				((othr.getAcceptanceDate() == null && acceptanceDate == null) || othr.getAcceptanceDate().equals(acceptanceDate));
				
	}
}
