package ls.properties.domain.entities;

import java.util.LinkedList;
import java.util.List;

import ls.properties.utils.Pair;

public class Local implements IEntity{
	
	private static String typeName = "Local";
	
	private String region;
	public String getRegion() {return region;}

	private String locale;
	public String getLocale() {return locale;}
	
	public Local(String region, String locale) {
		this.region = region;
		this.locale = locale;
	}
	
	@Override
	public String getTypeName() {
		return Local.typeName;
	}
	
	@Override
	public List<Pair<String, Object>> getKey() {
		List<Pair<String, Object>> l = new LinkedList<Pair<String,Object>>();
		l.add(new Pair<String, Object>("region", region));
		l.add(new Pair<String, Object>("locale", locale));
		
		return l;
	}
	
	@Override
	public List<Pair<String, Object>> getAllFields() {
		return getKey();
	}
	
	@Override
	public boolean equals(Object other){
		if(other == this)
			return true;
		if(!(other instanceof Local))
			return false;
		
		Local othr = (Local)other;
		
		return othr.getLocale().equals(locale) && othr.getRegion().equals(region);
	}

}
