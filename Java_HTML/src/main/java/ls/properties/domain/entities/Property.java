package ls.properties.domain.entities;

import java.util.LinkedList;
import java.util.List;

import ls.properties.utils.Pair;

public class Property implements IEntity{
	
	private static String typeName = "Property";
	
	private int id;
	public int getId() {return id;}
	
	private String sort;
	public String getSort() {return sort;}
	public void setSort(String sort) {this.sort = sort;}
	
	private String text;
	public String getText() {return text;}
	public void setText(String text) {this.text = text;}
	
	private int price;
	public int getPrice() {return price;}
	public void setPrice(int price) {this.price = price;}
	
	private Local location;
	public Local getLocation() {return location;}
	public void setLocation(Local location) {this.location = location;}
	
	private Client owner;
	public Client getOwner() {return owner;}
	public void setOwner(Client owner) {this.owner = owner;}
	
	public Property(int id, String sort, String text, int price, Local location, Client owner) {
		this.id = id;
		this.sort = sort;
		this.text = text;
		this.price = price;
		this.location = location;
		this.owner = owner;
	}
	
	@Override
	public String getTypeName() {
		return Property.typeName;
	}
	
	@Override
	public List<Pair<String, Object>> getKey() {
		List<Pair<String, Object>> l = new LinkedList<Pair<String,Object>>();
		l.add(new Pair<String, Object>("id", id));
		
		return l;
	}

	@Override
	public List<Pair<String, Object>> getAllFields() {
		List<Pair<String, Object>> l = getKey();
		l.add(new Pair<String, Object>("sort", sort));
		l.add(new Pair<String, Object>("text", text));
		l.add(new Pair<String, Object>("price", price));
		l.add(new Pair<String, Object>("location", location));
		l.add(new Pair<String, Object>("owner", owner));
		
		return l;
	}
	
	@Override
	public boolean equals(Object other){
		if(other == this)
			return true;
		if(!(other instanceof Property))
			return false;
		
		Property othr = (Property)other;
		
		return othr.getId() == id &&
				othr.getLocation().equals(location) &&
				othr.getOwner().equals(owner) &&
				othr.getPrice() == price &&
				othr.getSort().equals(sort) &&
				othr.getText().equals(text);
	}
	
}
