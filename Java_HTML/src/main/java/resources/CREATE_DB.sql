/*
*	ISEL-DEETC-LEIC
*	2013-2014
*	Semestre Verao
*	LS-LID41-G05
* 	Rental Management
*/

use master;

if not exists (Select name From master.dbo.sysdatabases Where name = N'PropertyApp')
begin
create database PropertyApp;
end