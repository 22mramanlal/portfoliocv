/*
*	ISEL-DEETC-LEIC
*	2013-2014
*	Semestre Verao
*	LS-LID41-G05
* 	Rental Management
*/

if not exists (Select name From master.dbo.sysdatabases Where name = N'TestPropertyApp')
begin
create database TestPropertyApp;
end