package ls.properties.commands.utils;

import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;

import ls.properties.commands.responses.ICommandResponse;
import ls.properties.commands.structure.Command;
import ls.properties.commands.suppliers.commandSupplier.ICommandSupplier;
import ls.properties.commands.suppliers.commandSupplier.tree.CommandTreeSupplier;
import ls.properties.exceptions.RepositoryException;

import org.junit.Test;

public class CommandTreeSupplierTest {

	private static class FakeCommand extends Command {

		public FakeCommand() {
			super(null);
		}

		@Override
		public ICommandResponse execute(List<String> pathArguments,
				String commandArguments) throws RepositoryException {
			return null;
		}
	}

	private ICommandSupplier commandSupplier = new CommandTreeSupplier();
	private static FakeCommand[] fc = { new FakeCommand(), new FakeCommand(),
			new FakeCommand(), new FakeCommand(), new FakeCommand(),
			new FakeCommand(), new FakeCommand(), new FakeCommand(),
			new FakeCommand(), new FakeCommand(), new FakeCommand(),
			new FakeCommand(), new FakeCommand() };

	private static String[] paths = { "GET/users", "GET/properties",
			"GET/users/Parameter", "GET/users/Parameter/rentals",
			"GET/users/Parameter/properties/owned",
			"GET/properties/details/Parameter",
			"GET/properties/type/Parameter", "GET/properties/owner/Parameter",
			"GET/properties/location/Parameter",
			"GET/properties/Parameter/rentals",
			"GET/properties/Parameter/rentals/Parameter",
			"GET/properties/Parameter/rentals/Parameter/Parameter",
			"GET/rentals/Parameter/abc" };

	@Test
	public void test() {
		loadCommandsOnTree(commandSupplier);

		assertTrue(commandSupplier != null);

		int idx = 0;

		for (String str : paths) {
			assertTrue(commandSupplier.getCommand(str,
					new LinkedList<String>()).equals(fc[idx++]));
		}
	}

	private static void loadCommandsOnTree(ICommandSupplier commandSupplier) {
		addGet(commandSupplier);
	}

	private static void addGet(ICommandSupplier commandSupplier) {
		int idx = 0;
		int idxP = 0;
		commandSupplier.addCommand(paths[idxP++], fc[idx++]);
		commandSupplier.addCommand(paths[idxP++], fc[idx++]);

		commandSupplier.addCommand(paths[idxP++], fc[idx++]);
		commandSupplier.addCommand(paths[idxP++], fc[idx++]);
		commandSupplier.addCommand(paths[idxP++], fc[idx++]);

		commandSupplier.addCommand(paths[idxP++],	fc[idx++]);
		commandSupplier.addCommand(paths[idxP++], fc[idx++]);
		commandSupplier.addCommand(paths[idxP++], fc[idx++]);
		commandSupplier.addCommand(paths[idxP++], fc[idx++]);

		commandSupplier.addCommand(paths[idxP++], fc[idx++]);
		commandSupplier.addCommand(paths[idxP++], fc[idx++]);
		commandSupplier.addCommand(paths[idxP++], fc[idx++]);

		commandSupplier.addCommand(paths[idxP++], fc[idx++]);
	}
}
