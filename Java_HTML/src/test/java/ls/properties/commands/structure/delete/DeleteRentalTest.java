package ls.properties.commands.structure.delete;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.util.ArrayList;

import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;

import org.junit.Before;
import org.junit.Test;

public class DeleteRentalTest extends DeleteT{
	Client owner = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	Client renter = new Client("Andre", "pass", "andre@mail.com", "Andre Ramanlal");
	Local location = new Local("Lisboa", "Oeiras");
	Property property = new Property(1, "Vivenda", "Luxurious", 500000, location, owner);
	Rental rental = new Rental(property, 2015, 2, renter, false, new Timestamp(System.currentTimeMillis()), null);
	
	
	@Before
	public void initTest(){
		rep = new FakeRepository();
		rep.add(owner);
		rep.add(renter);
		rep.add(location);
		rep.add(property);
		rep.add(rental);
		pathArguments = new ArrayList<String>(); 
		commandArguments = "";
		initManagerSupplier();
		cmd = new DRentals(manager);
	}
	
	protected void fillPathArguments() {
		pathArguments.add(Integer.toString(property.getId()));
		pathArguments.add(Integer.toString(rental.getCy()));
		pathArguments.add(Integer.toString(rental.getCw()));
	}
	
	@Override
	protected void setCommandArguments() {
		commandArguments = "usernamelog=Bruno&passwordlog=pass";
	}
	
	
	@Test
	public void testNormalCaseWithOwner() {
		fillPathArguments();
		setCommandArguments();
		cmd.execute(pathArguments, commandArguments);
		
		Rental dummy = new Rental(null, 0, 0, null, false, null, null);
		
		assertEquals(rep.get(dummy , rental.getKey()), null);
	}
	
	@Test
	public void testNormalCaseWithRenter() {
		fillPathArguments();
		commandArguments = "usernamelog=Andre&passwordlog=pass";
		cmd.execute(pathArguments, commandArguments);		
		
		Rental dummy = new Rental(null, 0, 0, null, false, null, null);
		
		assertEquals(rep.get(dummy , rental.getKey()), null);
	}
	
	@Test
	public void testDeleteNonExistentProperty() {
		pathArguments.add(Integer.toString(property.getId()+10));
		pathArguments.add(Integer.toString(rental.getCy()));
		pathArguments.add(Integer.toString(rental.getCw()));
		setCommandArguments();
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			if(e.getMessage().contains("Property"))
				return;
		}
		
		fail("InexistentEntityException should be thrown");				
	}
	
	@Test
	public void testDeleteNonExistentRental() {
		pathArguments.add(Integer.toString(property.getId()));
		pathArguments.add(Integer.toString(rental.getCy()));
		pathArguments.add(Integer.toString(rental.getCw()+10));
		setCommandArguments();
		
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(InexistentEntityException e){
			if(e.getMessage().contains("Rental"))
				return;
		}
		
		fail("InexistentEntityException should be thrown");
				
	}
	
	@Test
	public void testDeleteRentalWithNonPendingState() {
		fillPathArguments();
		setCommandArguments();
		
		rental.setRentState(true);
		rep.update(rental);
		
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(InvalidUserParametersException e){
			if(e.getMessage().contains("Can only delete rentls not accepted")){
				rental.setRentState(false);
				return;
			}
		}
			
		rental.setRentState(false);
		fail("InvalidUserParametersException should be thrown");
	}
	
}
