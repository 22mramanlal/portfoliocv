package ls.properties.commands.structure;

import static org.junit.Assert.fail;

import java.util.List;

import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.suppliers.managerSupplier.map.MapManagerSupplier;
import ls.properties.domain.entitiesManagers.ClientManager;
import ls.properties.domain.entitiesManagers.LocalManager;
import ls.properties.domain.entitiesManagers.PropertyManager;
import ls.properties.domain.entitiesManagers.RentalManager;
import ls.properties.exceptions.InvalidUserParametersException;
import ls.properties.repositories.IRepository;

public class CommandTBase {
	protected Command cmd;
	protected List<String> pathArguments;
	protected String commandArguments;
	protected IRepository rep;
	protected IManagerSupplier manager;
	
	public void initManagerSupplier() {
		manager = new MapManagerSupplier();
		
		RentalManager rM = new RentalManager(rep);
		manager.addManager(rM, "Rental");
		
		PropertyManager pM = new PropertyManager(rep, manager);
		manager.addManager(pM, "Property");
		
		LocalManager lM= new LocalManager(rep, manager);
		manager.addManager(lM, "Local");
		
		ClientManager cM = new ClientManager(rep, manager);
		manager.addManager(cM, "Client");
	}
	
	
	
	
	
	
	public void testNullCommandArguments() {
		try{
			cmd.execute(pathArguments, null);
		}catch(InvalidUserParametersException e){
			return;
		}		
		fail("InvalidUserParametersException should be thrown");
	}
			
	public void testEmptyCommandArguments() { 
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(InvalidUserParametersException e){
			return;
		}		
		fail("InvalidUserParametersException should be thrown");
	}
	
	public void testNullPathArguments() {
		try{
			cmd.execute(null, commandArguments);
		}catch(IllegalArgumentException e){
			return;
		}		
		fail("IllegalArgumentException should be thrown");
	}

	public void testInvalidPathArgumentsSize() {
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(InvalidUserParametersException e){
			return;
		}		
		fail("InvalidUserParametersException should be thrown");
	}
	
	public void testFailUserAuthenticationCase(){
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(InvalidUserParametersException e){
			if(e.getMessage().contains("authentication"))
				return;
		}
		
		fail("InvalidUserParametersException should be thrown");
	}
	
	public void testWithInsuficientArgumentsCase(String argumentMissing){
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InvalidUserParametersException e){
			if(e.getMessage().contains(argumentMissing))
				return;
		}
		
		fail("InvalidUserParametersException should be thrown");		
	}

}
