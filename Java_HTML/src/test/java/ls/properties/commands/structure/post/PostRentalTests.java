package ls.properties.commands.structure.post;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import ls.properties.commands.responses.responseEntity.ResponseRental;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;

import org.junit.Before;
import org.junit.Test;

public class PostRentalTests extends PostT{	
	private static Client owner = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	private static Client renter = new Client("Andre", "pass", "andre@mail.com", "Andre Ramanlal");
	private static Local location = new Local("Lisboa", "Oeiras");
	private static Property property = new Property(1, "Vivenda", "Luxurius", 500000, location, owner); 
	
	
	@Before
	public void initTest(){
		rep = new FakeRepository();
		rep.add(property);
		rep.add(owner);
		rep.add(renter);
		pathArguments = new ArrayList<String>(); 
		commandArguments = "";
		initManagerSupplier();
		cmd = new PRentals(manager);
	}
	
	private void fillPathArguments(){
		pathArguments.add(Integer.toString(property.getId()));
	}
	private void setCommandArguments() {
		commandArguments = "year=2015&cw=22&usernamelog=Andre&passwordlog=pass";
	}
	
	@Test
	public void testNullPathArguments() {
		setCommandArguments();
		super.testNullPathArguments();
	}
	
	@Test
	public void testNullCommandArguments() {
		fillPathArguments();
		super.testNullCommandArguments();
	}
	
	@Test
	public void testInvalidPathArgumentsSize() {
		setCommandArguments();
		super.testInvalidPathArgumentsSize();
	}
	
	@Test
	public void testEmptyCommandArguments() {
		fillPathArguments(); 
		super.testEmptyCommandArguments();
	}

	@Test
	public void testNormalCase() {
		pathArguments.add(Integer.toString(property.getId()));
		int cw = 22;
		int cy = 2015;
		setCommandArguments();
		
		ResponseRental resp = (ResponseRental)cmd.execute(pathArguments, commandArguments);
		Rental rental = resp.getRental();
		
		assertEquals(rental.getProperty(), property);
		assertEquals(rental.getCw(), cw);
		assertEquals(rental.getCy(), cy);
		assertEquals(rental.getRenter(), renter);
		assertEquals(rental.getRentState(), false);
	}
	
	@Test
	public void testAddDuplicatedRentalCase() {
		pathArguments.add(Integer.toString(property.getId()));
		commandArguments = "year=2015&cw=22&usernamelog=Andre&passwordlog=pass";
		super.testDuplicatedCase();
	}
	
	@Test
	public void testAddRentalWithInvalidCwAndCyCase() {
		pathArguments.add(Integer.toString(property.getId()));
		try{
			commandArguments = "year=2s15&cw=22&usernamelog=Andre&passwordlog=pass";
			cmd.execute(pathArguments, commandArguments);
		}catch(InvalidUserParametersException e1){
			try{
				commandArguments = "year=2015&cw=v2&usernamelog=Andre&passwordlog=pass";
				cmd.execute(pathArguments, commandArguments);
			}catch(InvalidUserParametersException e2){return;}	
		}
		
		fail("InvalidUserParametersException should be thrown");
	}
	
	@Test
	public void testPostNonExistentProperty() {
		pathArguments.add("999999");
		commandArguments = "year=2014&cw=6&usernamelog=Andre&passwordlog=pass";
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			if(e.getMessage().contains("Property"))
				return;
		}
		
		fail("InexistentEntityException should be thrown");				
	}

	@Test
	public void testPostNonAllArgumentsProperty() {
		commandArguments = "year=2014&usernamelog=Andre&passwordlog=pass";
		super.testWithInsuficientArgumentsCase("cw");	
	}
	
	@Test 
	public void testFailUserAuthentication(){
		pathArguments.add(Integer.toString(property.getId()));
		commandArguments = "year=2014&cw=2&usernamelog=Andre&passwordlog=Invalid";
		super.testFailUserAuthenticationCase();
	}
}
