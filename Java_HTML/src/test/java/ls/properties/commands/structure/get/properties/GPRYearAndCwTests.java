package ls.properties.commands.structure.get.properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import ls.properties.commands.responses.responseEntity.ResponseRental;
import ls.properties.commands.structure.Command;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.repositories.IRepository;

import org.junit.Test;

public class GPRYearAndCwTests extends GPRentalsT{
	
	@Override
	protected Command getCommandToTest(IRepository rep) {
		return new GPRYearAndCw(manager);
	}
	
	private void fillPathArguments(){
		pathArguments.add(Integer.toString(property1.getId()));
		pathArguments.add(Integer.toString(rental1.getCy()));
		pathArguments.add(Integer.toString(rental1.getCw()));
	}
	
	@Test
	public void testNullPathArguments(){
		super.testNullPathArguments();
	}
	
	@Test
	public void testInvalidPathArgumentsSize(){
		super.testInvalidPathArgumentsSize();
	}
	
	@Test
	public void testNormalCase(){
		fillPathArguments();
		
		ResponseRental resp = (ResponseRental)cmd.execute(pathArguments, commandArguments);
		
		Rental rental = resp.getRental();
		
		assertEquals(rental, rental1);
	}
	
	@Test
	public void testInexistentPropertyCase(){
		pathArguments.add("100");
		pathArguments.add(Integer.toString(rental1.getCy()));
		pathArguments.add(Integer.toString(rental1.getCw()));
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	@Test
	public void testInexistentRentalCase(){
		pathArguments.add(Integer.toString(property1.getId()));
		pathArguments.add(Integer.toString(rental1.getCy()));
		pathArguments.add(Integer.toString(rental1.getCw()+20));
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
		
}
