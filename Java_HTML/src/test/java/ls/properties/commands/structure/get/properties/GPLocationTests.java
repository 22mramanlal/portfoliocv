package ls.properties.commands.structure.get.properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.LinkedList;

import ls.properties.commands.responses.responseEntities.ResponseProperties;
import ls.properties.commands.responses.responseEntities.ResponsePropertiesOnLocation;
import ls.properties.commands.structure.CommandTBase;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;

import org.junit.Before;
import org.junit.Test;

public class GPLocationTests extends CommandTBase{

	private static Client client = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	private static Local location1 = new Local("Lisboa", "Oeiras");
	private static Local location2 = new Local("Lisboa", "Cascais");
	private static Local location3 = new Local("Lisboa", "Amadora");
	private static Property property1 = new Property(1, "Vivenda", "Luxurious", 500000, location1, client);
	private static Property property2 = new Property(2, "Vivenda", "Luxurious", 550000, location2, client);
	private static Property property3 = new Property(3, "Vivenda", "Luxurious", 400000, location1, client);
	
	
	@Before
	public void initTests(){
		rep = new FakeRepository();
		rep.add(client);
		rep.add(location1);
		rep.add(location2);
		rep.add(location3);
		rep.add(property1);
		rep.add(property2);
		rep.add(property3);
		pathArguments = new LinkedList<String>();
		commandArguments = "";
		initManagerSupplier();
		cmd = new GPLocation(manager);
	}
	
	private void fillPathArguments(){
		pathArguments.add(location1.getRegion()+"-"+location1.getLocale());
	}
	
	@Test
	public void testNullPathArguments(){
		super.testNullPathArguments();
	}
	
	@Test
	public void testInvalidPathArgumentsSize(){
		super.testInvalidPathArgumentsSize();
	}

	@Test
	public void testNormalCase(){
		fillPathArguments();
		
		ResponseProperties resp = (ResponseProperties)cmd.execute(pathArguments, commandArguments);
	
		Local local = ((ResponsePropertiesOnLocation)(resp)).getLocal();
		Collection<Property> properties = resp.getProperties();
		
		assertTrue(properties.contains(property1));
		assertTrue(properties.contains(property3));
		assertTrue(properties.size() == 2);
		assertEquals(local, location1);
	}
	
	@Test
	public void testInexistentLocationCase(){
		pathArguments.add("Lisboa-Olivais");
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	@Test
	public void testLocationWithNoPropertiesCase(){
		pathArguments.add(location3.getRegion()+"-"+location3.getLocale());
		
		ResponseProperties resp = (ResponseProperties)cmd.execute(pathArguments, commandArguments);
		
		Local local = ((ResponsePropertiesOnLocation)(resp)).getLocal();
		Collection<Property> properties = resp.getProperties();
		
		assertEquals(properties, null);
		assertEquals(local, location3);
	}
	
	@Test
	public void testInvalidLocationFormatCase(){
		pathArguments.add(location1.getRegion()+"/"+location1.getLocale());
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InvalidUserParametersException e){
			return;
		}
		
		fail("InvalidUserParametersException should be thrown");
	}
	
}
