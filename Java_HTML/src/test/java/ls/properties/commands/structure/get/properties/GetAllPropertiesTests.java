package ls.properties.commands.structure.get.properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.LinkedList;

import ls.properties.commands.responses.responseEntities.ResponseProperties;
import ls.properties.commands.structure.CommandTBase;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;

import org.junit.Before;
import org.junit.Test;

public class GetAllPropertiesTests extends CommandTBase {
	private static Client client1 = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	private static Client client2 = new Client("Carmen", "pass", "carmen@mail.com", "Carmen Fleitas");
	private static Local location = new Local("Lisboa", "Oeiras");
	private static Property property1 = new Property(1, "Vivenda", "Luxurious", 500000, location, client1);
	private static Property property2 = new Property(2, "Vivenda", "Luxurious", 550000, location, client1);
	private static Property property3 = new Property(3, "Hotel", "4 estrelas", 600000, location, client1);
	private static Property property4 = new Property(4, "Hotel", "5 estrelas", 650000, location, client2);
	
	@Before
	public void initTests(){
		rep = new FakeRepository();
		rep.add(client1);
		rep.add(client2);
		rep.add(location);
		rep.add(property1);
		rep.add(property2);
		rep.add(property3);
		rep.add(property4);
		pathArguments = new LinkedList<String>();
		commandArguments = "";
		initManagerSupplier();
		cmd = new GPAllProperties(manager);
	}


	@Test
	public void getPropertiesAllPropertiesNormalCaseTest(){
		ResponseProperties resp = (ResponseProperties)cmd.execute(pathArguments, commandArguments);
		
		Collection<Property> properties = resp.getProperties();
		
		assertTrue(properties.contains(property1));
		assertTrue(properties.contains(property2));
		assertTrue(properties.contains(property3));
		assertTrue(properties.contains(property4));
		assertTrue(properties.size() == 4);
	}

	@Test
	public void getPropertiesAlWithnoPropertiesTest(){
		rep = new FakeRepository();
		initManagerSupplier();
		cmd = new GPAllProperties(manager);
		ResponseProperties resp = (ResponseProperties)cmd.execute(pathArguments, commandArguments);
		
		Collection<Property> properties = resp.getProperties();
		
		assertEquals(properties, null);
	}
	
}
