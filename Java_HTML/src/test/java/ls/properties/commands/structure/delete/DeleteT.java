package ls.properties.commands.structure.delete;

import ls.properties.commands.structure.CommandTBase;

import org.junit.Test;

public abstract class DeleteT  extends CommandTBase{	
	protected abstract void setCommandArguments();
	protected abstract void fillPathArguments();
	
	@Test
	public void testNullPathArguments() {
		setCommandArguments();
		super.testNullPathArguments();
	}
	
	@Test
	public void testNullCommandArguments() {
		fillPathArguments();
		super.testNullCommandArguments();
	}
	
	@Test
	public void testInvalidPathArgumentsSize() {
		setCommandArguments();
		super.testInvalidPathArgumentsSize();
	}
	
	@Test
	public void testEmptyCommandArguments() {
		fillPathArguments();	
		super.testEmptyCommandArguments();
	}

	@Test
	public void testFailUserAuthentication() {
		fillPathArguments();
		commandArguments = "usernamelog=Bruno&passwordlog=Invalid";
		super.testFailUserAuthenticationCase();
	}
	
	@Test
	public void testInsuficientArguments() {
		fillPathArguments();
		commandArguments = "passwordlog=Invalid";
		super.testWithInsuficientArgumentsCase("usernamelog");
	}
	
}
