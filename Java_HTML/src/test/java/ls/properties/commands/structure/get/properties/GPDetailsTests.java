package ls.properties.commands.structure.get.properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.LinkedList;

import ls.properties.commands.responses.responseEntity.ResponseProperty;
import ls.properties.commands.structure.CommandTBase;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.InexistentEntityException;

import org.junit.Before;
import org.junit.Test;

public class GPDetailsTests extends CommandTBase{

	private static Client client = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	private static Local location = new Local("Lisboa", "Oeiras");
	private static Property property1 = new Property(1, "Vivenda", "Luxurious", 500000, location, client);
	private static Property property2 = new Property(2, "Vivenda", "Luxurious", 550000, location, client);
	
	@Before
	public void initTests(){
		rep = new FakeRepository();
		rep.add(client);
		rep.add(location);
		rep.add(property1);
		rep.add(property2);
		pathArguments = new LinkedList<String>();
		commandArguments = "";
		initManagerSupplier();
		cmd = new GPDetails(manager);
	}
	
	public void fillPathArguments(){
		pathArguments.add(Integer.toString(property1.getId()));
	}
	
	@Test
	public void testNullPathArguments(){
		super.testNullPathArguments();
	}
	
	@Test
	public void testInvalidPathArgumentsSize(){
		super.testInvalidPathArgumentsSize();
	}
	
	@Test
	public void testNormalCase(){
		fillPathArguments();
		
		ResponseProperty resp = (ResponseProperty)cmd.execute(pathArguments, commandArguments);
		Property property = resp.getProperty();		
		
		assertEquals(property, property1);
	}
	
	@Test
	public void testNonExistentPropertyCase(){
		pathArguments.add("10");
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityExceptiuon should be thrown");
	}
	
}
