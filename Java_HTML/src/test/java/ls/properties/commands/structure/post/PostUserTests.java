package ls.properties.commands.structure.post;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import ls.properties.commands.responses.responseEntity.ResponseClient;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;

import org.junit.Before;
import org.junit.Test;


public class PostUserTests extends PostT{
		
		@Before
		public void initTest(){
			rep = new FakeRepository();
			rep.add(new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas"));
			pathArguments = new ArrayList<String>(); 
			commandArguments = "";
			initManagerSupplier();
			cmd = new PUser(manager);
		}
		
		@Test
		public void testNullCommandArguments() {
			super.testNullCommandArguments();
		}
				
		@Test
		public void testEmptyCommandArguments() { 
			super.testEmptyCommandArguments();
		}
		
		@Test
		public void testNormalCase() {
			String username = "Andre", pass = "pass", email = "andre@mail.com", fullname = "Andre Ramanlal";
			
			commandArguments = "username=Andre&password=pass&email=andre@mail.com&fullname=Andre+Ramanlal&usernamelog=Bruno&passwordlog=pass";
			ResponseClient resp = (ResponseClient) cmd.execute(pathArguments, commandArguments);
			Client c = resp.getClient();
			
			assertEquals(c.getFullname(), fullname);
			assertEquals(c.getUsername(), username);
			assertEquals(c.getEmail(), email);
			assertEquals(c.getPass(), pass);
			assertEquals(resp.getProperties(), null);
		}
		
		@Test
		public void testAddDuplicatedUser(){	
			commandArguments = "username=Andre&password=pass&email=andre@mail.com&fullname=Andre+Ramanlal&usernamelog=Bruno&passwordlog=pass";
			super.testDuplicatedCase();
		}
		
		@Test 
		public void testFailUserAuthentication(){
			commandArguments = "username=Andre&password=pass&email=andre@mail.com&fullname=Andre+Ramanlal&usernamelog=Bruno&passwordlog=InvalidPass";
			super.testFailUserAuthenticationCase();
		}
			
		@Test
		public void testPostNonAllArgumentsProperty() {
			commandArguments = "username=Bruno&password=Dantas&email=bd@mail.com&usernamelog=BillGates&passwordlog=Microsoft";
			super.testWithInsuficientArgumentsCase("fullname");
		}
			
	}

