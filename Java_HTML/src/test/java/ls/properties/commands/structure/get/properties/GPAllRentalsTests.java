package ls.properties.commands.structure.get.properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;

import ls.properties.commands.responses.responseEntities.ResponseRentalsForProperty;
import ls.properties.commands.structure.Command;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.repositories.IRepository;

import org.junit.Test;

public class GPAllRentalsTests extends GPRentalsT{

	@Override
	protected Command getCommandToTest(IRepository rep) {
		return new GPRAllRentals(manager);
	}
	
	private void fillPathArguments(){
		pathArguments.add(Integer.toString(property1.getId()));
	}
	
	@Test
	public void testNullPathArguments(){
		super.testNullPathArguments();
	}
	
	@Test
	public void testInvalidPathArgumentsSize(){
		super.testInvalidPathArgumentsSize();
	}
	
	@Test
	public void testNormalCase(){
		fillPathArguments();
		
		ResponseRentalsForProperty resp = (ResponseRentalsForProperty)cmd.execute(pathArguments, commandArguments);
		
		Property property = resp.getProperty();
		Collection<Rental> rentals = resp.getRentals();
		
		assertEquals(property, property1);
		assertTrue(rentals.contains(rental1));
		assertTrue(rentals.contains(rental2));
		assertTrue(rentals.contains(rental3));
		assertTrue(rentals.contains(rental4));
		assertTrue(rentals.contains(rental5));
		assertTrue(rentals.size() == 5);
	}
	
	@Test
	public void testInexistentPropertyCase(){
		pathArguments.add("100");
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	@Test
	public void testPropertyWithNoRentalsCase(){
		pathArguments.add(Integer.toString(propertyWithNoRentals.getId()));
		
		ResponseRentalsForProperty resp = (ResponseRentalsForProperty)cmd.execute(pathArguments, commandArguments);
		
		Property property = resp.getProperty();
		Collection<Rental> rentals = resp.getRentals();
		
		assertEquals(property, propertyWithNoRentals);
		assertEquals(rentals, null);
	}

}
