package ls.properties.commands.structure.get.users;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.LinkedList;

import ls.properties.commands.responses.responseEntity.ResponseClient;
import ls.properties.commands.structure.CommandTBase;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.InexistentEntityException;

import org.junit.Before;
import org.junit.Test;

public class GetSingleUserTests extends CommandTBase {
	
	private static Client client = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	
	@Before
	public void initTests(){
		rep = new FakeRepository();
		rep.add(client);
		pathArguments = new LinkedList<String>();
		commandArguments = "";
		initManagerSupplier();
		cmd = new GSingleUser(manager);
	}
		
	private void fillPathArguments(){
		pathArguments.add(client.getUsername());
	}
	
	
	@Test
	public void testNullPathArguments(){
		super.testNullPathArguments();
	}
	
	@Test
	public void testInvalidPathArgumentsSize(){
		super.testNullPathArguments();
	}
	
	@Test
	public void getUsersOneUserWithNoPropertiesTest(){
		fillPathArguments();
		
		ResponseClient resp = (ResponseClient)cmd.execute(pathArguments, commandArguments);
		Client c = resp.getClient();
		
		assertEquals(client, c);
		assertEquals(resp.getProperties(), null);
	}
	
	@Test
	public void getUsersOneUserWithPropertiesTest(){
		fillPathArguments();
		Local l = new Local("Lisboa", "Restelo");
		Property p = new Property(1, "Hotal", "5 Stars", 500000, l, client);
		rep = new FakeRepository();
		rep.add(client);
		rep.add(l);
		rep.add(p);
		initManagerSupplier();
		cmd = new GSingleUser(manager);
		
		
		ResponseClient resp = (ResponseClient)cmd.execute(pathArguments, commandArguments);
		Client c = resp.getClient();
		Collection<Property> properties = resp.getProperties();
		
		assertEquals(client, c);		
		assertTrue(properties.size()==1);
		assertEquals(properties.iterator().next(), p);
	}
	
	@Test
	public void getNonExistentUserTest(){
		pathArguments.add("Inexistent UserName");

		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
}
