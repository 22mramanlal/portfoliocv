package ls.properties.commands.structure.post;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import ls.properties.commands.responses.responseEntity.ResponseProperty;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;

import org.junit.Before;
import org.junit.Test;

public class PostPropertyTests extends PostT{
	private static Client c = new Client("Bruno", "pass", "bruno@email.com", "Bruno Dantas");;
	private static Local l = new Local("Lisboa", "Odivelas");

	@Before
	public void initTest(){
		rep = new FakeRepository();
		rep.add(c);
		rep.add(l);
		pathArguments = new ArrayList<String>(); 
		commandArguments = "";
		initManagerSupplier();
		cmd = new PProperty(manager);
	}
		
	@Test
	public void testNullCommandArguments() {
		super.testNullCommandArguments();
	}	

	@Test
	public void testEmptyCommandArguments() { 
		super.testEmptyCommandArguments();
	}
	
	@Test
	public void testNormalCase() {
		String type = "Vivenda", description = "Muito Elegante";
		int price = 300000;
		
		commandArguments = "type=Vivenda&description=Muito+Elegante&price=300000&location=Lisboa-Odivelas&usernamelog=Bruno&passwordlog=pass";
		ResponseProperty resp = (ResponseProperty) cmd.execute(pathArguments, commandArguments);
		Property p = resp.getProperty();
		
		assertEquals(p.getId(), 1);
		assertEquals(p.getPrice(), price);
		assertEquals(p.getSort(), type);
		assertEquals(p.getText(), description);
		assertEquals(p.getLocation(), l);
		assertEquals(p.getOwner(), c);
	}

	@Test 
	public void testFailUserAuthentication(){
		commandArguments = "type=Vivenda&description=Muito+Elegante&price=300000&location=Lisboa-Odivelas&usernamelog=Bruno&passwordlog=Invalid";
		super.testFailUserAuthenticationCase();
	}
	
	@Test
	public void testAddPropertyWithInvalidPriceCase() {
		commandArguments = "type=Vivenda&description=Muito+Elegante&price=3sss0&location=Lisboa-Odivelas&usernamelog=Bruno&passwordlog=pass";
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(InvalidUserParametersException e){
			return;
		}
		
		fail("InvalidUserParametersException should be thrown");
	}
	
	@Test
	public void testAddPropertyWithInexistentLocationFormatCase() {
		commandArguments = "type=Vivenda&description=Muito+Elegante&price=30000&location=Lisboa-Cascais&usernamelog=Bruno&passwordlog=pass";
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	@Test
	public void testAddPropertyWithInvalidLocationFormatCase() {
		commandArguments = "type=Vivenda&description=Muito+Elegante&price=30000&location=Odivelas/Lisboa&usernamelog=Bruno&passwordlog=pass";
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(InvalidUserParametersException e){
			if(e.getMessage().contains("location"))
				return;
		}
		
		fail("InvalidUserParametersException should be thrown");
	}
	
	@Test
	public void testPostNonAllArgumentsProperty() {
		commandArguments = "type=Vivenda&description=Muito+Elegante&location=Lisboa-Odivelas&usernamelog=Dantas&passwordlog=Dantas";
		super.testWithInsuficientArgumentsCase("price");		
	}
	
}
