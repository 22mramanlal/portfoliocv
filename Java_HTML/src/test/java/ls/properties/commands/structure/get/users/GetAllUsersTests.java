package ls.properties.commands.structure.get.users;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.LinkedList;

import ls.properties.commands.responses.responseEntities.ResponseClients;
import ls.properties.commands.structure.CommandTBase;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;

import org.junit.Before;
import org.junit.Test;

public class GetAllUsersTests extends CommandTBase {
	
	private static Client client1 = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	private static Client client2 = new Client("Andre", "pass", "andre@mail.com", "Andre Ramanlal");
	private static Client client3 = new Client("Carmen", "pass", "carmen@mail.com", "Carmen Fleitas");
	private static Client client4 = new Client("Bill", "pass", "bill@mail.com", "Bill Gates");
	
	@Before
	public void initTests(){
		rep = new FakeRepository();
		rep.add(client1);
		rep.add(client2);
		rep.add(client3);
		rep.add(client4);
		pathArguments = new LinkedList<String>();
		commandArguments = "";
		initManagerSupplier();
		cmd = new GAllUsers(manager);
	}

	@Test
	public void getUsersAllUsersNormalCaseTest() {
		ResponseClients resp = (ResponseClients)cmd.execute(pathArguments, commandArguments);
	
		Collection<Client> clients = resp.getClients();
		
		assertTrue(clients.contains(client1));
		assertTrue(clients.contains(client2));
		assertTrue(clients.contains(client3));
		assertTrue(clients.contains(client4));
		assertTrue(clients.size() == 4);
	}
	
	@Test
	public void getUsersOneUserNonExistentUserTest(){
		rep = new FakeRepository();
		initManagerSupplier();
		cmd = new GAllUsers(manager);
		ResponseClients resp = (ResponseClients)cmd.execute(pathArguments, commandArguments);
		
		Collection<Client> clients = resp.getClients();
		
		assertEquals(clients, null);
	}
}

