package ls.properties.commands.structure.patch;
import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.util.ArrayList;

import ls.properties.commands.structure.CommandTBase;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;

import org.junit.Before;
import org.junit.Test;

public class PatchRentalTests extends CommandTBase{
	private static Client owner = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	private static Client renter = new Client("Andre", "pass", "andre@mail.com", "Andre Ramanlal");
	private static Local location = new Local("Lisboa", "Oeiras");
	private static Property property = new Property(1, "Vivenda", "Luxurius", 500000, location, owner);
	Rental rental = new Rental(property, 2015, 2, renter, false, new Timestamp(System.currentTimeMillis()), null);
	
	@Before
	public void initTest(){
		rep = new FakeRepository();
		rep.add(owner);
		rep.add(renter);
		rep.add(location);
		rep.add(property);
		rep.add(rental);
		pathArguments = new ArrayList<String>(); 
		commandArguments = "";
		initManagerSupplier();
		cmd = new PchRentals(manager);
	}
		
	private void fillPathArguments(){
		pathArguments.add(Integer.toString(property.getId()));
		pathArguments.add(Integer.toString(rental.getCy()));
		pathArguments.add(Integer.toString(rental.getCw()));
	}
	private void setCommandArguments(){
		commandArguments = "usernamelog=Bruno&passwordlog=pass";
	}
	
	@Test
	public void testNullPathArguments() {
		setCommandArguments();
		super.testNullPathArguments();
	}
	
	@Test
	public void testNullCommandArguments() {
		fillPathArguments();
		super.testNullCommandArguments();
	}

	@Test
	public void testInvalidPathArgumentsSize() {
		setCommandArguments();
		super.testInvalidPathArgumentsSize();
	}
	
	@Test
	public void testEmptyCommandArguments() {
		fillPathArguments();
		super.testEmptyCommandArguments();
	}
	
	@Test
	public void testWithInsuficientCommandArguments(){
		fillPathArguments();
		commandArguments = "usernamelog=Bruno";
		super.testWithInsuficientArgumentsCase("passwordlog");
	}
	
	@Test
	public void testNormalCase() {
		fillPathArguments();
		setCommandArguments();
		cmd.execute(pathArguments, commandArguments);		
		rental.setRentState(false);
	}
	
	@Test
	public void testRenterPatchingHisRental() {
		fillPathArguments();
		commandArguments = "usernamelog=Andre&passwordlog=pass";
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(InvalidUserParametersException e){
			if(e.getMessage().contains("Only owner of property has permition to this operation"))
				return;
		}
		
		fail("InvalidUserParametersException should be thrown");	
	}
	
	@Test
	public void testpatchNonExistentProperty() {
		pathArguments.add("999999");
		pathArguments.add(Integer.toString(rental.getCy()));
		pathArguments.add(Integer.toString(rental.getCw()));
		setCommandArguments();
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			if(e.getMessage().contains("Property"))
				return;
		}
		
		fail("InexistentEntityException should be thrown");				
	}
	
	@Test
	public void testPatchNonExistentRental() {
		pathArguments.add(Integer.toString(property.getId()));
		pathArguments.add("2100");
		pathArguments.add(Integer.toString(rental.getCw()));
		setCommandArguments();
		
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(InexistentEntityException e){
			if(e.getMessage().contains("Rental"))
				return;
		}
		
		fail("InexistentEntityException should be thrown");		
	}
	
	@Test
	public void testPatchRentalWithNonPendingState() {
		fillPathArguments();
		setCommandArguments();
				
		cmd.execute(pathArguments, commandArguments);
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InvalidUserParametersException e){
			if(e.getMessage().contains("already patched"))
				return;
		}
		
		fail("InvalidUserParameter should be thrown");
	}

}
