package ls.properties.commands.structure.get.properties;

import java.sql.Timestamp;
import java.util.LinkedList;

import ls.properties.commands.structure.Command;
import ls.properties.commands.structure.CommandTBase;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.repositories.IRepository;

import org.junit.Before;

public abstract class GPRentalsT extends CommandTBase{
	protected static Client owner = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	protected static Client renter = new Client("Andre", "pass", "andre@mail.com", "Andre Ramanlal");
	protected static Local location1 = new Local("Lisboa", "Oeiras");
	protected static Property property1 = new Property(1, "Vivenda", "Luxurious", 500000, location1, owner);
	protected static Property property2 = new Property(2, "Vivenda", "Luxurious", 550000, location1, owner);
	protected static Property propertyWithNoRentals = new Property(3, "Vivenda", "Luxurious", 550000, location1, owner);
	protected static Rental rental1 = new Rental(property1, 2015, 1, renter, false, new Timestamp(System.currentTimeMillis()), null);
	protected static Rental rental2 = new Rental(property1, 2015, 2, renter, false, new Timestamp(System.currentTimeMillis()), null);
	protected static Rental rental3 = new Rental(property1, 2015, 3, renter, false, new Timestamp(System.currentTimeMillis()), null);
	protected static Rental rental4 = new Rental(property1, 2016, 4, renter, false, new Timestamp(System.currentTimeMillis()), null);
	protected static Rental rental5 = new Rental(property1, 2016, 5, renter, false, new Timestamp(System.currentTimeMillis()), null);
	protected static Rental rental6 = new Rental(property2, 2015, 1, renter, false, new Timestamp(System.currentTimeMillis()), null);
	protected static Rental rental7 = new Rental(property2, 2015, 2, renter, false, new Timestamp(System.currentTimeMillis()), null);
	
	@Before
	public void initTests(){
		rep = new FakeRepository();
		rep.add(owner);
		rep.add(renter);
		rep.add(location1);
		rep.add(property1);
		rep.add(property2);
		rep.add(propertyWithNoRentals);
		rep.add(rental1);
		rep.add(rental2);
		rep.add(rental3);
		rep.add(rental4);
		rep.add(rental5);
		rep.add(rental6);
		rep.add(rental7);
		pathArguments = new LinkedList<String>();
		commandArguments = "";
		initManagerSupplier();
		cmd = getCommandToTest(rep);
	}
	
	protected abstract Command getCommandToTest(IRepository rep);
	
}

