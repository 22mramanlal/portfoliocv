package ls.properties.commands.structure.get.users;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.LinkedList;

import ls.properties.commands.responses.responseEntities.ResponseRentalsOfClient;
import ls.properties.commands.structure.CommandTBase;
import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.InexistentEntityException;

import org.junit.Before;
import org.junit.Test;

public class GetUserRentalsTests extends CommandTBase{
	
	private static Client owner = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	private static Client renter = new Client("Carmen", "pass", "carmen@mail.com", "Carmen Fleitas");
	private static Client clientWithNoRentals = new Client("Andre", "pass", "andre@mail.com", "Andre Ramanlal");
	private static Local location = new Local("Lisboa", "Oeiras");
	private static Property property1 = new Property(1, "Vivenda", "Luxurious", 500000, location, owner);
	private static Property property2 = new Property(2, "Vivenda", "Luxurious", 550000, location, owner);
	private static Property property3 = new Property(3, "Hotel", "4 estrelas", 600000, location, owner);
	private static Rental rental1 = new Rental(property1, 2015, 4, renter, false, new Timestamp(System.currentTimeMillis()), null);
	private static Rental rental2 = new Rental(property2, 2015, 5, renter, false, new Timestamp(System.currentTimeMillis()), null);
	private static Rental rental3 = new Rental(property3, 2015, 6, owner, false, new Timestamp(System.currentTimeMillis()), null);
	
	@Before
	public void initTests(){
		rep = new FakeRepository();
		rep.add(owner);
		rep.add(renter);
		rep.add(clientWithNoRentals);
		rep.add(location);
		rep.add(property1);
		rep.add(property2);
		rep.add(property3);
		rep.add(rental1);
		rep.add(rental2);
		rep.add(rental3);
		pathArguments = new LinkedList<String>();
		commandArguments = "";
		initManagerSupplier();
		cmd = new GURentals(manager);
	}
	
	private void fillPathArguments(){
		pathArguments.add(renter.getUsername());
	}
	
	@Test
	public void testInvalidPathArgumentSize(){
		super.testInvalidPathArgumentsSize();
	}
	
	@Test
	public void testNullPathArgument(){
		super.testNullPathArguments();
	}

	@Test
	public void testNormalCase(){
		fillPathArguments();
		ResponseRentalsOfClient resp = (ResponseRentalsOfClient)cmd.execute(pathArguments, commandArguments);
		
		Client client = resp.getClient();
		Collection<Rental> rentals = resp.getRentals();
		
		assertEquals(client, renter);
		assertTrue(rentals.contains(rental1));
		assertTrue(rentals.contains(rental2));
		assertTrue(rentals.size() == 2);
	}
	
	@Test
	public void testNonExistentUserCase(){
		pathArguments.add("Inexistent UserName");
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	@Test
	public void testUserHasRentHisOwnPropertyCase(){
		pathArguments.add(owner.getUsername());
		
		ResponseRentalsOfClient resp = (ResponseRentalsOfClient)cmd.execute(pathArguments, commandArguments);
		
		Client client = resp.getClient();
		Collection<Rental> rentals = resp.getRentals();
		
		assertEquals(client, owner);
		assertTrue(rentals.contains(rental3));
		assertTrue(rentals.size() == 1);
	}
	
	@Test
	public void testUserWithNoRentalsUserCase(){
		pathArguments.add(clientWithNoRentals.getUsername());
		
		ResponseRentalsOfClient resp = (ResponseRentalsOfClient)cmd.execute(pathArguments, commandArguments);
		
		Client client = resp.getClient();
		Collection<Rental> rentals = resp.getRentals();
		
		assertEquals(client, clientWithNoRentals);
		assertEquals(rentals, null);
	}
}
