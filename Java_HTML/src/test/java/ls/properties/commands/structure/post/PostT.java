package ls.properties.commands.structure.post;

import static org.junit.Assert.fail;
import ls.properties.commands.structure.CommandTBase;
import ls.properties.exceptions.DuplicatedEntityException;

public class PostT extends CommandTBase{	
	
	public void testDuplicatedCase(){
		cmd.execute(pathArguments, commandArguments);
		try{
			cmd.execute(pathArguments, commandArguments);
		}catch(DuplicatedEntityException e){
			return;
		}
		
		fail("DuplicatedEntityException should be thrown");	
	}
	
}
