package ls.properties.commands.structure.delete;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import ls.properties.domain.FakeRepository;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.InexistentEntityException;

import org.junit.Before;
import org.junit.Test;



public class DeletePropertiesTests extends DeleteT{	
	Client owner = new Client("Bruno", "pass", "bruno@mail.com", "Bruno Dantas");
	Local location = new Local("Lisboa", "Oeiras");
	Property property = new Property(1, "Vivenda", "Luxurious", 500000, location, owner);
	
	@Before
	public void initTest(){
		rep = new FakeRepository();
		rep.add(owner);
		rep.add(location);
		rep.add(property);
		pathArguments = new ArrayList<String>(); 
		commandArguments = "";
		initManagerSupplier();
		cmd = new DProperty(manager);
	}

	@Override
	protected void fillPathArguments() {
		pathArguments.add(Integer.toString(property.getId()));
	}
	
	@Override
	protected void setCommandArguments() {
		commandArguments = "usernamelog=Bruno&passwordlog=pass";
	}
	
	@Test
	public void testNormalCase() {
		fillPathArguments();
		setCommandArguments();
		cmd.execute(pathArguments, commandArguments);
		
		Property dummy = new Property(1, null, null, 0, null, null);
		
		assertEquals(rep.get(dummy , property.getKey()), null);
	}	
	
	@Test
	public void testDeleteNonExistentProperty() {
		pathArguments.add(Integer.toString(property.getId()+10));
		setCommandArguments();
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			if(e.getMessage().contains("Property"))
				return;
		}
		
		fail("InvalidUserParametersException should be thrown");				
	}

}
