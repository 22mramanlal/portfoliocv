package ls.properties.commands.structure.get.properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;

import ls.properties.commands.responses.responseEntities.ResponseRentals;
import ls.properties.commands.structure.Command;
import ls.properties.domain.entities.Rental;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.repositories.IRepository;

import org.junit.Test;

public class GPRYearTests extends GPRentalsT{
	@Override
	protected Command getCommandToTest(IRepository rep) {
		return new GPRYear(manager);
	}
	
	private void fillPathArguments(){
		pathArguments.add(Integer.toString(property1.getId()));
		pathArguments.add(Integer.toString(rental1.getCy()));
	}
	
	@Test
	public void testNullPathArguments(){
		super.testNullPathArguments();
	}
	
	@Test
	public void testInvalidPathArgumentsSize(){
		super.testInvalidPathArgumentsSize();
	}
	
	@Test
	public void testNormalCase(){
		fillPathArguments();
		
		ResponseRentals resp = (ResponseRentals)cmd.execute(pathArguments, commandArguments);
		
		Collection<Rental> rentals = resp.getRentals();
		
		assertTrue(rentals.contains(rental1));
		assertTrue(rentals.contains(rental2));
		assertTrue(rentals.contains(rental3));
		assertTrue(rentals.size() == 3);
	}
	
	@Test
	public void testInexistentPropertyCase(){
		pathArguments.add("100");
		pathArguments.add(Integer.toString(rental1.getCy()));
		
		try{
			cmd.execute(pathArguments, commandArguments);	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	@Test
	public void testPropertyWithNoRentalsCase(){
		pathArguments.add(Integer.toString(propertyWithNoRentals.getId()));
		pathArguments.add(Integer.toString(rental1.getCy()));
		
		ResponseRentals resp = (ResponseRentals)cmd.execute(pathArguments, commandArguments);
		
		Collection<Rental> rentals = resp.getRentals();
		
		assertEquals(rentals, null);
	}
	
	
	
	@Test
	public void testPropertyWithNoRentalsOnChosenYearCase(){
		pathArguments.add(Integer.toString(property1.getId()));
		pathArguments.add("2100");
		
		ResponseRentals resp = (ResponseRentals)cmd.execute(pathArguments, commandArguments);
		
		Collection<Rental> rentals = resp.getRentals();
		
		assertEquals(rentals, null);
	}
}
