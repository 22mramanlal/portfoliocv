package ls.properties.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.util.Collection;

import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.domain.entitiesManagers.RentalManager;
import ls.properties.exceptions.DuplicatedEntityException;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;

import org.junit.Before;
import org.junit.Test;

public class RentalManagerTests {

	private static RentalManager rentalManager;
	private static Client renter = new Client("Bruno", "pass", "bruno@email.com", "Bruno Dantas");
	private static Client owner = new Client("Andre", "pass", "andre@email.com", "Andre Ramanlal");
	private static Local location = new Local("Lisboa", "Cascais");
	private static Property property = new Property(1, "House", "Good place to live", 20000, location, owner);
	private static Property propertyWithNoRentals = new Property(1, "House", "Luxurious", 40000, location, owner);
	private static int cy = 2015;
	private static int cw = 2;
	Rental rental = new Rental(property, 2016, 1, renter, false, new Timestamp(System.currentTimeMillis()), null);

	@Before
	public void cleanRentalManager(){
		FakeRepository rep = new FakeRepository();
		rentalManager = new RentalManager(rep);
		rep.add(owner);
		rep.add(renter);
		rep.add(location);
		rep.add(property);
		rep.add(propertyWithNoRentals);
		rep.add(rental);
	}
	
	
	//CREAT_RENTAL	
	@Test
	public void addRentalTest(){
		Rental r = rentalManager.createRental(property, renter, Integer.toString(cy), Integer.toString(cw));
	
		assertEquals(r.getCw(), cw);
		assertEquals(r.getCy(), cy);
		assertEquals(r.getAcceptanceDate(), null);
		assertEquals(r.getProperty(), property);
		assertEquals(r.getRenter(), renter);
		assertEquals(r, rentalManager.getRental(property, Integer.toString(cy), Integer.toString(cw)));
	}
	
	@Test
	public void addRentalWithInvalidCwAndYearTest(){
		catchException(property, renter, Integer.toString(cy), "78");
		catchException(property, renter, "-1", Integer.toString(cw));
	}
	
	@Test
	public void addDuplicatedRentalTest(){
		rentalManager.createRental(property, renter, Integer.toString(cy), Integer.toString(cw)); 
		
		try{
			rentalManager.createRental(property, renter, Integer.toString(cy), Integer.toString(cw));		
		}catch(DuplicatedEntityException e){
			return;
		}	
		
		fail("DuplicatedEntityException should be thrown");
	}
	
	@Test
	public void addRentalWithNullAndEmptyParametersTest(){
		String week = Integer.toString(cw);
		String year = Integer.toString(cy);
		
		catchException(null, renter, year, week);
		catchException(property, null, year, week);
		catchException(property, renter, null, week);
		catchException(property, renter, year, null);
		catchException(property, renter, year, "");
		catchException(property, renter, "", week);		
	}
	
	private static void catchException(Property property, Client renter, String cy, String cw){
		try{
			rentalManager.createRental(property, renter, cy, cw);		
		}catch(InvalidUserParametersException e){
			return;
		}	
		
		fail("InvalidUserParametersException should be thrown");
	}
	
	
	//DELETE
	@Test
	public void  deleteRentalTest(){
		rentalManager.deleteRental(rental);
	}
	
	@Test
	public void  deleteInexistentRentalTest(){
		try{
			rentalManager.deleteRental(new Rental(property, 2000, cw, renter, false, null, null));			
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistenEntityException should be thrown");	
	}

	
	//UPDATE
	@Test
	public void  updateRentalSameObjectTest(){
		rental.setRenter(owner);
		rentalManager.updateRental(rental);
		assertEquals(rentalManager.getRental(rental.getProperty(),  Integer.toString(rental.getCy()), 
												Integer.toString(rental.getCw())), rental);	
	}
	
	@Test
	public void  updateRentalOtherObjectWithKeytoObjectAddedTest(){
		Rental r2 = new Rental(rental.getProperty(), rental.getCy(), rental.getCw(), renter, 
							rental.getRentState(), rental.getOrderDate(), rental.getAcceptanceDate());  
		r2.setRenter(owner);
		rentalManager.updateRental(r2);
		assertEquals(rentalManager.getRental(rental.getProperty(),  Integer.toString(rental.getCy()), 
														Integer.toString(rental.getCw())), r2);
	}

	@Test
	public void  updateInexistentRentalTest(){
		try{
			rentalManager.updateRental(new Rental(property, 2020, cw, renter, rental.getRentState(),
													rental.getAcceptanceDate(), rental.getAcceptanceDate()));
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}

	
	//GET_RENTAL
	@Test
	public void  getRentalTest(){		
		String cy = Integer.toString(rental.getCy());
		String cw = Integer.toString(rental.getCw());
		assertEquals(rental, rentalManager.getRental(rental.getProperty(), cy, cw));	
	}
	
	@Test
	public void  getInexistentRentalTest(){
		try{
		rentalManager.getRental(rental.getProperty(),"2000", Integer.toString(rental.getCw()));
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should bw thrown");
	}

	
	//GET_ALL_RENTALS
	@Test
	public void  getAllRentalsTest(){
		Rental r1 = rentalManager.createRental(property, renter, Integer.toString(cy), Integer.toString(cw));
		Rental r2 = rentalManager.createRental(property, renter, Integer.toString(cy), "40");
		Rental r3 = rentalManager.createRental(property, renter, "2050", Integer.toString(cw));
		Collection<Rental> allRentals = rentalManager.getAllRentals();
		
		assertTrue(allRentals.size() == 4);
		assertTrue(allRentals.contains(r1));
		assertTrue(allRentals.contains(r2));
		assertTrue(allRentals.contains(r3));
		assertTrue(allRentals.contains(rental));
	}
		
	@Test
	public void  getAllRentalsWithNoRentalsTest(){
		rentalManager = new RentalManager(new FakeRepository());
		Collection<Rental> allRentals = rentalManager.getAllRentals();
		
		assertEquals(allRentals, null);
	}
	
	
	
	@Test
	public void  isRenterOrOwnerOfRentalPassTest(){
		rentalManager.isRenterOrOwnerOfRental(owner, rental);
		rentalManager.isRenterOrOwnerOfRental(renter, rental);
	}
		
	@Test
	public void  isRenterOrOwnerOfRentalFailTest(){
		Client c = new Client("Dummy", "pass", "dummy@email.com", "Sr.Dummy");
		
		try{
			rentalManager.isRenterOrOwnerOfRental(c, rental);	
		}catch(InvalidUserParametersException e){
			if(e.getMessage().contains("permition"))
				return;
		}
		
		fail("InvalidUserParametersException should be thrown");
	}
	
	@Test
	public void getRentalsOfUserWithRentalsTest(){
		Collection<Rental> rentals = rentalManager.getRentalsOf(renter);
		
		assertTrue(rentals.size()==1);
		assertEquals(rentals.iterator().next(), rental);
	}
	
	@Test
	public void getRentalsOfUserWithNoRentalsTest(){
		Collection<Rental> rentals = rentalManager.getRentalsOf(owner);
		
		assertEquals(rentals, null);
	}

	@Test
	public void getRentalsForPropertyWithRentalsTest(){
		Collection<Rental> rentals = rentalManager.getRentalsFor(property);
		
		assertTrue(rentals.size()==1);
		assertEquals(rentals.iterator().next(), rental);
	}
	
	@Test
	public void getRentalsForPropertyWithNoRentalsTest(){
		Collection<Rental> rentals = rentalManager.getRentalsFor(propertyWithNoRentals);
		
		assertEquals(rentals, null);
	}
}
