package ls.properties.domain;

import ls.properties.commands.suppliers.managerSupplier.IManagerSupplier;
import ls.properties.commands.suppliers.managerSupplier.map.MapManagerSupplier;
import ls.properties.domain.entitiesManagers.ClientManager;
import ls.properties.domain.entitiesManagers.LocalManager;
import ls.properties.domain.entitiesManagers.PropertyManager;
import ls.properties.domain.entitiesManagers.RentalManager;
import ls.properties.repositories.IRepository;

import org.junit.Before;

public class EntityManagerT {
	protected IManagerSupplier manager;
	protected IRepository rep;
	
	@Before
	public void initManagerSupplier() {
		rep = new FakeRepository();
		manager = new MapManagerSupplier();
		
		RentalManager rM = new RentalManager(rep);
		manager.addManager(rM, "Rental");
		
		PropertyManager pM = new PropertyManager(rep, manager);
		manager.addManager(pM, "Property");
		
		LocalManager lM= new LocalManager(rep, manager);
		manager.addManager(lM, "Local");
		
		ClientManager cM = new ClientManager(rep, manager);
		manager.addManager(cM, "Client");
	}

}
