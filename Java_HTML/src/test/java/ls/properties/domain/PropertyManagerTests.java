package ls.properties.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.util.Collection;

import ls.properties.commands.suppliers.managerSupplier.map.MapManagerSupplier;
import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.Local;
import ls.properties.domain.entities.Property;
import ls.properties.domain.entities.Rental;
import ls.properties.domain.entitiesManagers.PropertyManager;
import ls.properties.domain.entitiesManagers.RentalManager;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;

import org.junit.Before;
import org.junit.Test;

public class PropertyManagerTests extends EntityManagerT{
	
	private static PropertyManager propertyManager;
	private static String sort = "Hotel";
	private static String text = "4 Stars";
	private static String price = "500000";
	private static Local location = new Local("Lisboa", "Cascais");
	private static Local locationWithNoProperties = new Local("Lisboa", "Oeiras");
	private static Client owner = new Client("Bruno", "pass", "Bruno@email.com", "Bruno Dantas");
	private static Client ownerWithNoProperties = new Client("Carmen", "pass", "Carmen@email.com", "Carmen Fleitas");
	private static Property property = new Property(0, sort, text, Integer.parseInt(price), location, owner);
	
	@Before
	public void cleanPropertyManager(){
		propertyManager = (PropertyManager)manager.getManager("Property");
		rep.add(owner);
		rep.add(ownerWithNoProperties);
		rep.add(location);
		rep.add(locationWithNoProperties);
		rep.add(property);
	}
	
	
	//CREAT_CLIENT	
	@Test
	public void addPropertyTest(){
		Property p = propertyManager.createProperty(sort, text, price, location, owner);
	
		assertEquals(p.getId(), 1);
		assertEquals(p.getLocation(), location);
		assertEquals(p.getOwner(), owner);
		assertEquals(p.getPrice(), Integer.parseInt(price));
		assertEquals(p.getSort(), sort);
		assertEquals(p.getText(), text);
		assertEquals(p, propertyManager.getProperty(Integer.toBinaryString(p.getId())));
	}
	
	@Test
	public void addPropertyWithInvalidPriceTest(){
		String invalidPrice = "Invalid Price"; 
		try{
			propertyManager.createProperty(sort, text, invalidPrice, location, owner);	
		}catch(InvalidUserParametersException e){
			return;
		}
		
		fail("InvalidUserParametersException should be thrown");	
	}
	
	@Test
	public void addPropertyWithNullAndEmptyParametersTest(){
		catchException(null, text, price, location, owner); 
		catchException(sort, null, price, location, owner);
		catchException(sort, text, null, location, owner);
		catchException(sort, text, price, null, owner);
		catchException(sort, text, price, location, null);
		
		
		catchException("", text, price, location, owner);
		catchException(sort, "", price, location, owner);
		catchException(sort, text, "", location, owner);
	}
	
	private static void catchException(String sort, String text, String price, Local location, Client owner){
		try{
			propertyManager.createProperty(sort, text, price, location, owner);		
		}catch(InvalidUserParametersException e){
			return;
		}	
		
		fail("InvalidUserParametersException should be thrown");
	}

	
	//DELETE
	@Test
	public void  deletePropertyTest(){
		propertyManager = (PropertyManager)manager.getManager("Property");
		rep.add(owner);
		rep.add(property);
		
		Rental r =  new Rental(property, 2015, 2, owner, false, 
				new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis()));
		
		rep.add(r);
		propertyManager.deleteProperty(property);
		
		try{
			propertyManager.getProperty(Integer.toString(property.getId()));
		}catch(InexistentEntityException e){
			assertEquals(rep.get(r, r.getKey()), null);
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	@Test
	public void  deleteInexistentPropertyTest(){	
		try{
			propertyManager.deleteProperty(new Property(10, sort, text, Integer.parseInt(price), location, owner));	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntity should be thrown");	
	}

	
	//UPDATE
	@Test
	public void  updatePropertySameObjectTest(){
		property.setSort("Garage");
		property.setText("Larga garage");
		
		propertyManager.updateProperty(property);
		assertEquals(propertyManager.getProperty(Integer.toString(property.getId())), property);	
	}
	
	@Test
	public void  updatePropertyOtherObjectWithKeytoObjectAddedTest(){
		Property p2 = new Property(property.getId(), sort, text, Integer.parseInt(price), location, owner);
		p2.setSort("Garage");
		p2.setText("Large garage");
		
		propertyManager.updateProperty(p2);
		assertEquals(propertyManager.getProperty(Integer.toString(p2.getId())), p2);	
	}

	@Test
	public void  updateInexistentPropertyTest(){
		try{
			propertyManager.updateProperty(new Property(100, sort, text, Integer.parseInt(price), location, owner));
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	
	//GET_PROPERTY
	@Test
	public void  getPropertyTest(){
		assertEquals(property, propertyManager.getProperty(Integer.toString(property.getId())));	
	}
	
	@Test
	public void  getInexistentPropertyTest(){
		try{
			propertyManager.getProperty(Integer.toString(10));	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}

	
	//GET_ALL_PROPERTIES
	@Test
	public void  getAllPropertiesTest(){
		Property p1 = propertyManager.createProperty(sort, text, price, location, owner);
		Property p2 = propertyManager.createProperty("Hotel", "3 Stars", "350000", location, owner);
		Property p3 = propertyManager.createProperty("Garage", "Good conditions", "3000", location, owner);
		Collection<Property> allProperties = propertyManager.getAllProperties();
		
		assertTrue(allProperties.size() == 4);
		assertTrue(allProperties.contains(p1));
		assertTrue(allProperties.contains(p2));
		assertTrue(allProperties.contains(p3));
		assertTrue(allProperties.contains(property));
	}
		
	@Test
	public void  getAllPropertiessWithNoPropertiesTest(){
		rep = new FakeRepository();
		manager = new MapManagerSupplier();
		
		RentalManager rM = new RentalManager(rep);
		manager.addManager(rM, "Rental");
		
		PropertyManager pM = new PropertyManager(rep, manager);
		manager.addManager(pM, "Property");
		
		propertyManager = (PropertyManager)manager.getManager("Property");
		Collection<Property> allProperties = propertyManager.getAllProperties();
		
		assertEquals(allProperties, null);
	}

	
	
	@Test
	public void isOwnerOfPropertySuccessTest(){
		propertyManager.isOwnerOfProperty(owner, property);
	}
	
	@Test
	public void isOwnerOfPropertyFailTest(){
		try{
			propertyManager.isOwnerOfProperty(ownerWithNoProperties, property);	
		}catch(InvalidUserParametersException e){
			if(e.getMessage().contains("permition"))
				return;
		}
		
		fail("InvalidUserParametersException should be thrown");
	}

	@Test
	public void getPropertiesOfClientWithPropertiesTest(){
		Collection<Property> properties = propertyManager.getPropertiesOf(owner);
		
		assertTrue(properties.size()==1);
		assertTrue(properties.contains(property));
	}
	
	@Test
	public void getPropertiesOfClientWithNoPropertiesTest(){
		Collection<Property> properties = propertyManager.getPropertiesOf(ownerWithNoProperties);
		
		assertEquals(properties, null);
	}
	
	@Test
	public void getPropertiesLocatiedOnLocationWithPropertiesTest(){
		Collection<Property> properties = propertyManager.getPropertiesLocated(location);
		
		assertTrue(properties.size()==1);
		assertEquals(properties.iterator().next(), property);
	}
	
	@Test
	public void getPropertiesOfTypeWithPropertiesTest(){
		Collection<Property> properties = propertyManager.getPropertiesOfType(property.getSort());
		
		assertTrue(properties.size()==1);
		assertEquals(properties.iterator().next(), property);
	}
	
	@Test
	public void getPropertiesOfTypeWithNoPropertiesTest(){
		Collection<Property> properties = propertyManager.getPropertiesOfType("InaxistentType");
		
		assertEquals(properties, null);
	}
	
}
