package ls.properties.domain;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ls.properties.domain.entities.IEntity;
import ls.properties.repositories.IRepository;
import ls.properties.utils.Pair;

public class FakeRepository implements IRepository {

	Map<String, Map<String, IEntity>> maps = new HashMap<String, Map<String,IEntity>>();
	 
	
	@Override
	public void add(IEntity obj) {
		Map<String,IEntity> map = getMapTo(obj.getTypeName());
		String key  = getKey(obj);
		
		if(map == null){
			Map<String, IEntity> newMap = new HashMap<String, IEntity>();
			newMap.put(key, obj);
			maps.put(obj.getTypeName(), newMap);
		}
		else{
			map.put(key, obj);
		}
		
	}

	@Override
	public void delete(IEntity obj) {
		Map<String,IEntity> map = getMapTo(obj.getTypeName());
		String key = getKey(obj);
		map.remove(key);
	}

	@Override
	public void update(IEntity obj) {
		delete(obj);
		Map<String,IEntity> map = getMapTo(obj.getTypeName());
		map.put(getKey(obj), obj);
	}

	@Override
	public List<IEntity> getAll(IEntity obj) {
		List<IEntity> all = new LinkedList<IEntity>();
		
		Map<String,IEntity> map = getMapTo(obj.getTypeName());
		if(map == null)
			return null;
		
		for(IEntity entity : map.values())
			all.add(entity);
		
		return all;
	}

	@Override
	public List<IEntity> get(IEntity obj, List<Pair<String, Object>> key) {
		Map<String,IEntity> map = getMapTo(obj.getTypeName());
		if(map == null)			
			return null;
			
		List<IEntity> selected = new LinkedList<IEntity>();
		
		
		for(IEntity e : map.values())
			if(hasValuesRequired(e, key))
				selected.add(e);
		
 		return (selected.isEmpty())? null : selected;
	}

	
	private boolean hasValuesRequired(IEntity e, List<Pair<String, Object>> key) {
		List<Pair<String, Object>> entityFields = e.getAllFields();
		
		for(Pair<String, Object> p : key)
			if(!entityContainsField(entityFields, p))
				return false;
				
		return true;
	}
	
	private static boolean entityContainsField(List<Pair<String, Object>> entityFields, Pair<String, Object> field){
		for(Pair<String,Object> p : entityFields)
			if(p.equals(field))
				return true;
		
		return false;
	} 
	
	private Map<String, IEntity> getMapTo(String mapWanted){
		for(String key : maps.keySet())
			if(key.equals(mapWanted))
				return maps.get(key);
		
		return null;
	}
	
	private static String getKey(IEntity obj){
		StringBuilder sb = new StringBuilder();
		
		List<Pair<String,Object>> key = obj.getKey();
		
		for(Pair<String,Object> p : key){
			sb.append(p.getElem1());
			sb.append(" = ");
			if(p.getElem2() instanceof IEntity)
				sb.append(getKey((IEntity)p.getElem2()));
			else
				sb.append(p.getElem2());		
		}
			
		return sb.toString();
	}
	
}

