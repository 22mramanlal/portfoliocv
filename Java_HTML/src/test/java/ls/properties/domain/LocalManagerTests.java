package ls.properties.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;

import ls.properties.domain.entities.Local;
import ls.properties.domain.entitiesManagers.LocalManager;
import ls.properties.exceptions.DuplicatedEntityException;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;

import org.junit.Before;
import org.junit.Test;

public class LocalManagerTests extends EntityManagerT{
	
	private static LocalManager localManager;
	private static String region = "Lisboa";
	private static String locale = "Amadora";
	
	
	@Before
	public void cleanClientManager(){
		localManager = (LocalManager)manager.getManager("Local");
	}

	//CREAT_LOCAL
	@Test
	public void addLocalTest(){
		Local l = localManager.createLocal(region, locale);
	
		assertEquals(l.getRegion(), region);
		assertEquals(l.getLocale(), locale);
		assertEquals(l, localManager.getLocal(region, locale));
	}
	
	@Test
	public void addDuplicatedLocalTest(){
		localManager.createLocal(region, locale); 
		
		try{
			localManager.createLocal(region, locale); 		
		}catch(DuplicatedEntityException e){
			return;
		}	
		
		fail("DuplicatedEntityException should be thrown");
	}
	
	@Test
	public void addClientWithNullAndEmptyParametersTest(){
		catchException(null, locale); 
		catchException(region, null);

		catchException("", locale);
		catchException(region, "");
	}
	
	private static void catchException(String region, String locale){
		try{
			localManager.createLocal(region, locale); 		
		}catch(InvalidUserParametersException e){
			return;
		}	
		
		fail("InvalidUserParametersException should be thrown");
	}

	
	//DELETE
	@Test
	public void  deleteLocalTest(){
		Local l = localManager.createLocal(region, locale);
		
		localManager.deleteLocal(l);
		
		try{
			localManager.getLocal(region, locale);
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	@Test
	public void  deleteInexistentClientTest(){
		localManager.createLocal(region, locale);
		
		try{
			localManager.deleteLocal(new Local("Inexistent region", locale));	
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");			
	}

	
	//UPDATE
	@Test
	public void  updateInexistentClientTest(){
		localManager.createLocal(region, locale);
		try{
			localManager.updateLocal(new Local("Inexistent Region", locale));
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	
	//GET_CLIENT
	@Test
	public void  getClientTest(){
		Local l = localManager.createLocal(region, locale);
		
		assertEquals(l, localManager.getLocal(region, locale));	
	}
	
	@Test
	public void  getInexistentClientTest(){
		localManager.createLocal(region, locale);
		
		try{
			localManager.getLocal("Inexistent region", locale);
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}

	
	//GET_ALL_CLIENTs
	@Test
	public void  getAllClientsTest(){
		Local l1 = localManager.createLocal(region, locale);
		Local l2 = localManager.createLocal("Lisboa", "Sintra");
		Local l3 = localManager.createLocal("Lisboa", "Oreias");
		Collection<Local> allLocals = localManager.getAllLocals();

		assertTrue(allLocals.size() == 3);
		assertTrue(allLocals.contains(l1));
		assertTrue(allLocals.contains(l2));
		assertTrue(allLocals.contains(l3));
	}
		
	@Test
	public void  getAllClientsWithNoClientsTest(){
		Collection<Local> allLocals = localManager.getAllLocals();
		
		assertEquals(allLocals, null);
	}
	
}
