package ls.properties.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;

import ls.properties.domain.entities.Client;
import ls.properties.domain.entitiesManagers.ClientManager;
import ls.properties.exceptions.DuplicatedEntityException;
import ls.properties.exceptions.InexistentEntityException;
import ls.properties.exceptions.InvalidUserParametersException;

import org.junit.Before;
import org.junit.Test;

public class ClientManagerTests extends EntityManagerT{

	private static ClientManager clientManager;
	private static String userName = "Bruno";
	private static String pass = "pass";
	private static String email = "bruno@email.com";
	private static String fullName = "Bruno Dantas";
	
	@Before
	public void cleanClientManager(){
		clientManager = (ClientManager)manager.getManager("Client");
	}
	
	
	//CREAT_CLIENT	
	@Test
	public void addClientTest(){
		Client c = clientManager.createClient(userName, pass, email, fullName);
	
		assertEquals(c.getEmail(), email);
		assertEquals(c.getPass(), pass);
		assertEquals(c.getUsername(), userName);
		assertEquals(c.getFullname(), fullName);
		assertEquals(c, clientManager.getClient(userName));
	}
	
	@Test
	public void addClientWithInvalidEmailTest(){
		String invalidEmail = "Invalid Email"; 
		try{
			clientManager.createClient(userName, pass, invalidEmail, fullName);	
		}catch(InvalidUserParametersException e){
			return;
		}
		
		fail("InvalidUserParametersException should be thrown");	
	}
	
	@Test
	public void addDuplicatedClientTest(){
		clientManager.createClient(userName, pass, email, fullName); 
		
		try{
			clientManager.createClient(userName, pass, email, fullName);		
		}catch(DuplicatedEntityException e){
			return;
		}	
		
		fail("DuplicatedEntityException should be thrown");
	}
	
	@Test
	public void addClientWithNullAndEmptyParametersTest(){
		catchException(null, pass, email, fullName); 
		catchException(userName, null, email, fullName);
		catchException(userName, pass, null, fullName);
		catchException(userName, pass, email, null);
		
		catchException("", pass, email, fullName);
		catchException(userName, "", email, fullName);
		catchException(userName, pass, "", fullName);
		catchException(userName, pass, email, "");
	}
	
	private static void catchException(String userName, String pass, String email, String fullName){
		try{
			clientManager.createClient(userName, pass, email, fullName);		
		}catch(InvalidUserParametersException e){
			return;
		}	
		
		fail("InvalidUserParametersException should be thrown");
	}

	
	//DELETE
	@Test
	public void  deleteClientTest(){
		Client c = clientManager.createClient(userName, pass, email, fullName);
		
		clientManager.deleteClient(c);
		
		try{
			clientManager.getClient(userName);
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	
	@Test
	public void  deleteInexistentClientTest(){
		clientManager.createClient(userName, pass, email, fullName);
		
		try{
			clientManager.deleteClient(new Client("Inexistent User",pass,email,fullName));			
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistenEntityException should be thrown");	
	}
	
	
	//UPDATE
	@Test
	public void  updateClientSameObjectTest(){
		Client c = clientManager.createClient(userName, pass, email, fullName);
		c.setPass("123");
		c.setFullname("Bruno Sousa");
		clientManager.updateClient(c);
		assertEquals(clientManager.getClient(userName), c);	
	}
	
	@Test
	public void  updateClientOtherObjectWithKeytoObjectAddedTest(){
		clientManager.createClient(userName, pass, email, fullName);
		Client c = new Client(userName, null, email, null);
		c.setPass("123");
		c.setFullname("Bruno Sousa");
		clientManager.updateClient(c);
		assertEquals(clientManager.getClient(userName), c);	
	}

	@Test
	public void  updateInexistentClientTest(){
		clientManager.createClient(userName, pass, email, fullName);
		try{
			clientManager.updateClient(new Client("Inexisten Cliente", pass, email, fullName));
		}catch(InexistentEntityException e){
			return;
		}
		
		fail("InexistentEntityException should be thrown");
	}
	

	//GET_CLIENT
	@Test
	public void  getClientTest(){
		Client c = clientManager.createClient(userName, pass, email, fullName);
		
		assertEquals(c, clientManager.getClient(userName));	
	}
	
	@Test
	public void  getInexistentClientTest(){
		clientManager.createClient(userName, pass, email, fullName);
		try{
			clientManager.getClient("Inexistent Client");
		}catch(InexistentEntityException e){
			return;
		}
			
		fail("InexistentEntityException should be thrown");
	}
	
	
	//GET_ALL_CLIENTS
	@Test
	public void  getAllClientsTest(){
		Client c1 = clientManager.createClient(userName, pass, email, fullName);
		Client c2 = clientManager.createClient("Andre", "pass", "Andre@email.com", "Andre Ramanlal");
		Client c3 = clientManager.createClient("Camen", "pass", "Carmen@email.com", "Carmen Fleitas");
		Collection<Client> allClients = clientManager.getAllClients();
		
		assertTrue(allClients.size() == 3);
		assertTrue(allClients.contains(c1));
		assertTrue(allClients.contains(c2));
		assertTrue(allClients.contains(c3));
	}
		
	@Test
	public void  getAllClientsWithNoClientsTest(){
		Collection<Client> allClients = clientManager.getAllClients();
		
		assertEquals(allClients, null);
	}
	
			
	//USER_AUTHENTICATION
	@Test
	public void  userAuthenticationSuccessTest(){
		Client c = clientManager.createClient(userName, pass, email, fullName);
		
		clientManager.userAuthentication(c.getUsername(), c.getPass());	
	}
	
	@Test
	public void  userAuthenticationFailTest(){
		Client c = clientManager.createClient(userName, pass, email, fullName);
		
		try{
			clientManager.userAuthentication(c.getUsername(), "wrong pass");	
		}catch(InvalidUserParametersException e){
			return;
		}
		
		fail("InvalidUserParametersException should be thrown");	
	}
	
}

