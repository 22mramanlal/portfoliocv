package ls.properties.repositories.sqlRepository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ls.properties.domain.entities.Client;
import ls.properties.domain.entities.IEntity;
import ls.properties.domain.entities.Property;
import ls.properties.exceptions.RepositoryException;
import ls.properties.utils.Pair;

import org.junit.Test;

public class DBRepositoryTest {

	static String fileConfigName = "resources/testconfig.properties";
	static String fileDBCreateName = "resources/CREATE_TABLES_INSERT_DEFAULTS.sql";
	static String fileTbCreateName = "resources/CREATE_DB.sql";
	static SQLRepository dbr;

	static {
		dbr = new SQLRepository(fileConfigName, fileTbCreateName,
				fileDBCreateName);
	}

	@Test
	public void createDBRepoTest() {
		// Arrange

		// Act

		// Assert
		assertNotNull(dbr);
		assertNotNull(dbr.dbf);
	}

	@Test
	public void readDBNameTest() throws SQLException {
		// Arrange
		String nameTable = "master.dbo.sysdatabases";
		String field = "name";
		String value = "TestPropertyApp";

		// Act
		dbr.dbf.setPrepareStatement(nameTable, "Select " + field + " From "
				+ nameTable + " Where name = N'" + value + "'", field, value);
		ResultSet rs = dbr.dbf.executeQuery();

		// Assert
		assertFalse(rs == null);
		assertTrue(rs.next());
		assertTrue(rs.getString(1).equals("TestPropertyApp"));
		dbr.dbf.closeConnection();
	}

	// GET
	@Test
	public void getNormalCaseTest() {
		// Arrange
		Client c = new Client("BillGates", "", "", "");
		List<Pair<String, Object>> key = new ArrayList<>();
		key.add(new Pair<String, Object>("username", "BillGates"));

		// Act
		List<IEntity> list = dbr.get(c, key);

		// Assert
		assertFalse(list.isEmpty());
		assertTrue(list.get(0).getTypeName() == c.getTypeName());
		assertTrue(list.get(0).getKey().get(0).getElem1()
				.equals(c.getKey().get(0).getElem1()));
		assertTrue(list.get(0).getKey().get(0).getElem2()
				.equals(c.getKey().get(0).getElem2()));
	}

	@Test
	public void getNullEntityTest() {
		// Arrange
		List<Pair<String, Object>> key = new ArrayList<>();
		key.add(new Pair<String, Object>("username", "BillGates"));

		// Act
		try {
			dbr.get(null, key);
		} catch (IllegalArgumentException e) {
			return;
		}

		// Assert
		fail("IllegalArgumentException should be thrown.");
	}

	// GET ALL
	@Test
	public void getAllTest() {
		// Arrange
		Client c = new Client("BillGates", "", "", "");
		List<Pair<String, Object>> key = new ArrayList<>();
		key.add(new Pair<String, Object>("username", "22mr"));
		Client c1 = (Client) dbr.get(new Client("22mr", "", "", ""), key)
				.get(0);

		// Act
		List<IEntity> list = dbr.getAll(c);

		// Assert
		assertFalse(list.isEmpty());
		assertTrue(list.get(0).getTypeName().equals(c.getTypeName()));
		assertTrue(list.get(0).getKey().get(0).getElem1()
				.equals(c1.getKey().get(0).getElem1()));
		assertTrue(list.get(0).getKey().get(0).getElem2()
				.equals(c1.getKey().get(0).getElem2()));
		assertTrue(list.get(1).getKey().get(0).getElem1()
				.equals(c.getKey().get(0).getElem1()));
	}

	@Test
	public void getAllNullEntityTest() {
		// Arrange

		// Act
		try {
			dbr.getAll(null);
		} catch (IllegalArgumentException e) {
			return;
		}

		// Assert
		fail("IllegalArgumentException should be thrown.");
	}
	

	@Test
	public void getAllEntityWithNullFieldsTest() {
		// Arrange
		Property p = new Property(0, null, null, 0, null, null);

		// Act
		List<IEntity> list = dbr.getAll(p);

		// Assert
		assertFalse(list == null);
	}

	// ADD
	@Test
	public void addTest() {
		// Arrange
		Client c = new Client("22mr", "indian", "22mr@gmail.com",
				"Andre Ramanlal");
		List<Pair<String, Object>> key = new ArrayList<>();
		key.add(new Pair<String, Object>("username", "22mr"));

		// Act
		if (!dbr.get(c, key).isEmpty())
			dbr.delete(c);

		dbr.add(c);
		List<IEntity> list = dbr.get(c, key);

		// Assert
		assertFalse(list.isEmpty());
		assertTrue(list.get(0).getTypeName().equals(c.getTypeName()));
		assertTrue(list.get(0).getKey().get(0).getElem1()
				.equals(c.getKey().get(0).getElem1()));
		assertTrue(list.get(0).getKey().get(0).getElem2()
				.equals(c.getKey().get(0).getElem2()));
	}

	@Test
	public void addNullEntityTest() {
		// Arrange
		List<Pair<String, Object>> key = new ArrayList<>();
		key.add(new Pair<String, Object>("username", "BillGates"));

		// Act
		try {
			dbr.add(null);
		} catch (IllegalArgumentException e) {
			return;
		}

		// Assert
		fail("IllegalArgumentException should be thrown.");
	}

	@Test
	public void addEntityWithNullParameterTest() {
		// Arrange
		Client c = new Client("22mr", null, "22mr@gmail.com", "Andre Ramanlal");
		List<Pair<String, Object>> key = new ArrayList<>();
		key.add(new Pair<String, Object>("username", "BillGates"));

		// Act
		try {
			dbr.add(c);
		} catch (RepositoryException e) {
			return;
		}

		// Assert
		fail("RepositoryException should be thrown.");
	}

	// DELETE
	@Test
	public void deleteTest() {
		// Arrange
		Client c = new Client("calmen", "cuban", "calmen@gmail.com",
				"Andre Ramanlal");
		List<Pair<String, Object>> key = new ArrayList<>();
		key.add(new Pair<String, Object>("username", "calmen"));

		// Act
		if (dbr.get(c, key).isEmpty())
			dbr.add(c);

		dbr.delete(c);
		List<IEntity> list = dbr.get(c, key);

		// Assert
		assertTrue(list.isEmpty());
	}

	@Test
	public void deleteInexistentEntityTest() {
		// Arrange
		Client c = new Client("22mr2", "indian1", "22mr@gmail.com",
				"Andre Ramanlal");

		// Act
		try {
			dbr.delete(c);
		} catch (RepositoryException e) {
			return;
		}

		// Assert
		fail("RepositoryException should be thrown.");
	}

	// UPDATE
	@Test
	public void updateTest() {
		// Arrange
		Client c = new Client("22mr", "indian1", "22mr@gmail.com",
				"Andre Ramanlal");
		List<Pair<String, Object>> key = new ArrayList<>();
		key.add(new Pair<String, Object>("username", "22mr"));

		// Act
		dbr.update(c);
		List<IEntity> list = dbr.get(c, key);

		// Assert
		assertFalse(list.isEmpty());
		assertTrue(list.get(0).getTypeName().equals(c.getTypeName()));
		assertTrue(list.get(0).getKey().get(0).getElem1()
				.equals(c.getKey().get(0).getElem1()));
		assertTrue(list.get(0).getKey().get(0).getElem2()
				.equals(c.getKey().get(0).getElem2()));
		assertTrue(((Client) (list.get(0))).getPass().equals(c.getPass()));
	}

	@Test
	public void updateNonExistentEntityTest() {
		// Arrange
		Client c = new Client("22mr3", "indian1", "22mr@gmail.com",
				"Andre Ramanlal");
		// Act
		try {
			dbr.update(c);
		} catch (RepositoryException e) {
			return;
		}

		// Assert
		fail("RepositoryException should be thrown.");
	}
}
