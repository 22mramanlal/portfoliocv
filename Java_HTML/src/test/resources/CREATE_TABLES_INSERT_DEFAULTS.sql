/*
*	ISEL-DEETC-LEIC
*	2013-2014
*	Semestre Verao
*	LS-LID41-G05
* 	Rental Management
*/

set nocount on;
set xact_abort on;

use TestPropertyApp;
begin transaction
raiserror('Starting...',0,1) --info

--create and insert defaults

/*
*	dbo.Client
*/
if object_id('dbo.Client') is null
begin
create table dbo.Client
(
	username varchar(30) CONSTRAINT pk_Client PRIMARY KEY,
	pass nvarchar(30) NOT NULL,
	email varchar(200) NOT NULL,
	fullname nvarchar(200) NOT NULL
);

insert into dbo.Client 
	values('BillGates', 'Microsoft', 'bilgates@email.com','Bill Gates');
end

/*
*	dbo.Local
*/
if object_id('dbo.Local') is null
begin
create table dbo.Local
(
	locale nvarchar(50) NOT NULL,
	region nvarchar(50) NOT NULL,
	CONSTRAINT pk_Local PRIMARY KEY (region, locale)
);

insert into dbo.Local 
	values('Albergaria-a-Velha', 'Aveiro'), ('Anadia', 'Aveiro'), ('Arouca', 'Aveiro'),
		  ('Aveiro', 'Aveiro'), ('Castelo de Paiva', 'Aveiro'), ('Espinho', 'Aveiro'), 
		  ('Estarreja', 'Aveiro'), ('ilhavo', 'Aveiro'), ('Mealhada', 'Aveiro'), ('Murtosa', 'Aveiro'), 
		  ('Oliveira de Azemeis', 'Aveiro'), ('Oliveira do Bairro', 'Aveiro'), ('Ovar', 'Aveiro'), 
		  ('Santa Maria da Feira', 'Aveiro'), ('Sao Joao da Madeira', 'Aveiro'), ('Sever do Vouga', 'Aveiro'), 
		  ('Vagos', 'Aveiro'), ('Vale de Cambra', 'Aveiro'), ('Aljustrel', 'Beja'), ('Almodovar', 'Beja'), 
		  ('Alvito', 'Beja'), ('Barrancos', 'Beja'), ('Beja', 'Beja'), ('Castro Verde', 'Beja'),
		  ('Cuba', 'Beja'), ('Ferreira do Alentejo', 'Beja'), ('Mertola', 'Beja'), ('Moura', 'Beja'), 
		  ('Odemira', 'Beja'), ('Ourique', 'Beja'), ('Serpa', 'Beja'), ('Vidigueira', 'Beja'), 
		  ('Barcelos', 'Braga'), ('Braga', 'Braga'), ('Cabeceiras de Basto', 'Braga'), 
		  ('Celorico de Basto', 'Braga'), ('Esposende', 'Braga'), ('Fafe', 'Braga'), 
		  ('Guimaraes', 'Braga'), ('P�voa de Lanhoso', 'Braga'), ('Terras de Bouro', 'Braga'), 
		  ('Vieira do Minho', 'Braga'), ('Vila Nova de Famalicao', 'Braga'), ('Vila Verde', 'Braga'), 
		  ('Vizela', 'Braga'), ('Braganza', 'Braganza'), ('Carrazeda de Ansiaes', 'Braganza'), 
		  ('Freixo de Espada a Cinta', 'Braganza'), ('Macedo de Cavaleiros', 'Braganza'), 
		  ('Miranda do Douro', 'Braganza'), ('Mirandela Mogadouro', 'Braganza'), 
		  ('Torre de Moncorvo', 'Braganza'), ('Vila Flor', 'Braganza'), ('Vimioso Vinhais', 'Braganza'), 
		  ('Castelo Branco', 'Castelo Branco'), ('Covilha', 'Castelo Branco'), 
		  ('Fundao', 'Castelo Branco'), ('Idanha-a-Nova', 'Castelo Branco'), 
		  ('Oleiros', 'Castelo Branco'), ('Penamacor', 'Castelo Branco'), 
		  ('Proenca-a-Nova', 'Castelo Branco'), ('Serta', 'Castelo Branco'), 
		  ('Vila de Rei', 'Castelo Branco'), ('Vila Velha de R�dao', 'Castelo Branco'), 
		  ('Cantanhede', 'Coimbra'), ('Coimbra', 'Coimbra'), ('Condeixa-a-Nova', 'Coimbra'), 
		  ('Figueira da Foz', 'Coimbra'), ('G�is', 'Coimbra'), ('Lousa', 'Coimbra'), 
		  ('Mira', 'Coimbra'), ('Miranda do Corvo', 'Coimbra'), ('Montemor-o-Velho', 'Coimbra'), 
		  ('Oliveira do Hospital', 'Coimbra'), ('Pampilhosa da Serra', 'Coimbra'), 
		  ('Penacova', 'Coimbra'), ('Penela', 'Coimbra'), ('Soure', 'Coimbra'), ('Tabua', 'Coimbra'), 
		  ('Vila Nova de Poiares', 'Coimbra'), ('Alandroal', 'Evora'), ('Arraiolos', 'Evora'), 
		  ('Borba', 'Evora'), ('Estremoz', 'Evora'), ('evora', 'Evora'), 
		  ('Montemor-o-Novo', 'Evora'), ('Mora', 'Evora'), ('Mourao', 'Evora'), ('Portel', 'Evora'), 
		  ('Redondo', 'Evora'), ('Reguengos de Monsaraz', 'Evora'), ('Vendas Novas', 'Evora'), 
		  ('Viana do Alentejo', 'Evora'), ('Vila Vicosa', 'Evora'), ('Albufeira', 'Faro'), 
		  ('Alcoutim', 'Faro'), ('Aljezur', 'Faro'), ('Castro Marim', 'Faro'), ('Faro', 'Faro'), 
		  ('Lagoa', 'Faro'), ('Lagos', 'Faro'), ('Loule', 'Faro'), ('Monchique', 'Faro'), 
		  ('Olhao', 'Faro'), ('Portimao', 'Faro'), ('Sao Bras de Alportel', 'Faro'), 
		  ('Silves', 'Faro'), ('Tavira', 'Faro'), ('Vila do Bispo', 'Faro'), 
		  ('Vila Real de Santo Ant�nio', 'Faro'), ('Aguiar da Beira', 'Guarda'), 
		  ('Almeida', 'Guarda'), ('Celorico da Beira', 'Guarda'), 
		  ('Figueira de Castelo Rodrigo', 'Guarda'), ('Fornos de Algodres', 'Guarda'), 
		  ('Gouveia', 'Guarda'), ('Guarda Manteigas', 'Guarda'), ('Meda', 'Guarda'), 
		  ('Pinhel', 'Guarda'), ('Sabugal', 'Guarda'), ('Seia', 'Guarda'), 
		  ('Trancoso', 'Guarda'), ('Vila Nova de Foz Coa', 'Guarda'), ('Alcobaca', 'Leiria'), 
		  ('Alvaiazere', 'Leiria'), ('Ansiao', 'Leiria'), ('Batalha', 'Leiria'), 
		  ('Bombarral', 'Leiria'), ('Caldas da Rainha', 'Leiria'), 
		  ('Castanheira de Pera', 'Leiria'), ('Figueir� dos Vinhos', 'Leiria'), 
		  ('Leiria', 'Leiria'), ('Marinha Grande', 'Leiria'), ('Nazare', 'Leiria'), 
		  ('�bidos', 'Leiria'), ('Pedr�gao Grande', 'Leiria'), ('Peniche', 'Leiria'), 
		  ('Pombal', 'Leiria'), ('Porto de M�s', 'Leiria'), ('Alenquer', 'Lisboa'), 
		  ('Amadora', 'Lisboa'), ('Arruda dos Vinhos', 'Lisboa'), ('Azambuja', 'Lisboa'), 
		  ('Cadaval', 'Lisboa'), ('Cascais', 'Lisboa'), ('Lisboa', 'Lisboa'), 
		  ('Loures', 'Lisboa'), ('Lourinha', 'Lisboa'), ('Mafra', 'Lisboa'), 
		  ('Odivelas', 'Lisboa'), ('Oeiras', 'Lisboa'), ('Sintra', 'Lisboa'), 
		  ('Sobral de Monte Agraco', 'Lisboa'), ('Torres Vedras', 'Lisboa'), 
		  ('Vila Franca de Xira', 'Lisboa'), ('Alter do Chao', 'Portalegre'), 
		  ('Arronches', 'Portalegre'), ('Avis', 'Portalegre'), 
		  ('Campo Maior', 'Portalegre'), ('Castelo de Vide', 'Portalegre'), 
		  ('Crato', 'Portalegre'), ('Elvas', 'Portalegre'), ('Fronteira', 'Portalegre'), 
		  ('Gaviao', 'Portalegre'), ('Marvao', 'Portalegre'), ('Monforte', 'Portalegre'), 
		  ('Nisa', 'Portalegre'), ('Ponte de Sor', 'Portalegre'), 
		  ('Portalegre', 'Portalegre'), ('Sousel', 'Portalegre'), ('Amarante', 'Porto'), 
		  ('Baiao', 'Porto'), ('Felgueiras', 'Porto'), ('Gondomar', 'Porto'), 
		  ('Lousada', 'Porto'), ('Maia', 'Porto'), ('Marco de Canaveses', 'Porto'), 
		  ('Matosinhos', 'Porto'), ('Porto', 'Porto'), 
		  ('Pacos de Ferreira', 'Porto'), ('Paredes', 'Porto'), ('Penafiel', 'Porto'), 
		  ('P�voa de Varzim', 'Porto'), ('Santo Tirso', 'Porto'), ('Trofa', 'Porto'), 
		  ('Valongo', 'Porto'), ('Vila do Conde', 'Porto'), ('Vila Nova de Gaia', 'Porto'),
		  ('Abrantes', 'Santarem'), ('Alcanena', 'Santarem'), ('Almeirim', 'Santarem'), 
		  ('Alpiarca', 'Santarem'), ('Benavente', 'Santarem'), ('Cartaxo', 'Santarem'), 
		  ('Chamusca', 'Santarem'), ('Constancia', 'Santarem'), ('Coruche', 'Santarem'), 
		  ('Entroncamento', 'Santarem'), ('Ferreira do Zezere', 'Santarem'), 
		  ('Golega', 'Santarem'), ('Macao', 'Santarem'), ('Ourem', 'Santarem'), 
		  ('Rio Maior', 'Santarem'), ('Salvaterra de Magos', 'Santarem'), 
		  ('Santarem', 'Santarem'), ('Sardoal', 'Santarem'), ('Tomar', 'Santarem'), 
		  ('Torres Novas', 'Santarem'), ('Vila Nova da Barquinha', 'Santarem'), 
		  ('Alcacer do Sal', 'Setubal'), ('Alcochete', 'Setubal'), ('Almada', 'Setubal'), 
		  ('Barreiro', 'Setubal'), ('Grandola', 'Setubal'), ('Moita', 'Setubal'), 
		  ('Montijo', 'Setubal'), ('Palmela', 'Setubal'), ('Santiago do Cacem', 'Setubal'), 
		  ('Seixal', 'Setubal'), ('Sesimbra', 'Setubal'), ('Setubal', 'Setubal'), 
		  ('Sines', 'Setubal'), ('Arcos de Valdevez', 'Viana do Castelo'), 
		  ('Caminha', 'Viana do Castelo'), ('Melgaco', 'Viana do Castelo'), 
		  ('Moncao', 'Viana do Castelo'), ('Paredes de Coura', 'Viana do Castelo'), 
		  ('Ponte da Barca', 'Viana do Castelo'), ('Ponte de Lima', 'Viana do Castelo'), 
		  ('Valenca', 'Viana do Castelo'), ('Viana do Castelo', 'Viana do Castelo'), 
		  ('Vila Nova de Cerveira', 'Viana do Castelo'), ('Alij�', 'Vila Real'), 
		  ('Boticas', 'Vila Real'), ('Chaves', 'Vila Real'), ('Mesao Frio', 'Vila Real'), 
		  ('Mondim de Basto', 'Vila Real'), ('Montalegre', 'Vila Real'), ('Murca', 'Vila Real'), 
		  ('Peso da Regua', 'Vila Real'), ('Ribeira de Pena', 'Vila Real'), ('Sabrosa', 'Vila Real'), 
		  ('Santa Marta de Penaguiao', 'Vila Real'), ('Valpacos', 'Vila Real'), 
		  ('Vila Pouca de Aguiar', 'Vila Real'), ('Vila Real', 'Vila Real'), ('Armamar', 'Viseu'), 
		  ('Carregal do Sal', 'Viseu'), ('Castro Daire', 'Viseu'), ('Cinfaes', 'Viseu'), 
		  ('Lamego', 'Viseu'), ('Mangualde', 'Viseu'), ('Moimenta da Beira', 'Viseu'), 
		  ('Mortagua', 'Viseu'), ('Nelas', 'Viseu'), ('Oliveira de Frades', 'Viseu'), 
		  ('Penalva do Castelo', 'Viseu'), ('Penedono', 'Viseu'), ('Resende', 'Viseu'), 
		  ('Santa Comba Dao', 'Viseu'), ('Sao Joao da Pesqueira', 'Viseu'), 
		  ('Sao Pedro do Sul', 'Viseu'), ('Satao', 'Viseu'), ('Sernancelhe', 'Viseu'), 
		  ('Tabuaco', 'Viseu'), ('Tarouca', 'Viseu'), ('Tondela', 'Viseu'), 
		  ('Vila Nova de Paiva', 'Viseu'), ('Viseu', 'Viseu'), ('Vouzela', 'Viseu'), 
		  ('Angra do Heroismo', 'Acores'), ('Calheta', 'Acores'), ('Corvo', 'Acores'), 
		  ('Horta', 'Acores'), ('Lagoa', 'Acores'), ('Lajes das Flores', 'Acores'), 
		  ('Lajes do Pico', 'Acores'), ('Madalena', 'Acores'), ('Nordeste', 'Acores'), 
		  ('Ponta Delgada', 'Acores'), ('Povoacao', 'Acores'), ('Ribeira Grande', 'Acores'), 
		  ('Santa Cruz da Graciosa', 'Acores'), ('Santa Cruz das Flores', 'Acores'), 
		  ('Sao Roque do Pico', 'Acores'), ('Velas', 'Acores'), 
		  ('Vila da Praia da Vit�ria', 'Acores'), ('Vila do Porto', 'Acores'), 
		  ('Vila Franca do Campo', 'Acores'), ('Calheta', 'Madeira'), 
		  ('Camara de Lobos', 'Madeira'), ('Funchal', 'Madeira'), ('Machico', 'Madeira'), 
		  ('Ponta do Sol', 'Madeira'), ('Porto Moniz', 'Madeira'), ('Porto Santo', 'Madeira'), 
		  ('Ribeira Brava', 'Madeira'), ('Santa Cruz', 'Madeira'), ('Santana', 'Madeira'), 
		  ('Sao Vicente', 'Madeira');
		  
end

/*
*	dbo.Property
*/
if object_id('dbo.Property') is null
begin
create table Property(
id int CONSTRAINT pk_Property PRIMARY KEY,
sort nvarchar(30) NOT NULL,
text varchar(50) NOT NULL,
price int NOT NULL,
region nvarchar(50) NOT NULL,
locale nvarchar(50) NOT NULL,
owner varchar(30) NOT NULL,
CONSTRAINT fk_Property FOREIGN KEY (owner) References Client(username),
CONSTRAINT fk1_Property FOREIGN KEY(region, locale) References Local(region, locale)
);

insert into dbo.Property 
values(1, 'Studio', 'Equiped', 50000, 'Lisboa', 'Cascais', 'BillGates'),
			(2, 'Studio', 'Well located', 60000, 'Lisboa', 'Oeiras', 'BillGates');
	end
 
 /*
*	dbo.Rental
*/

if object_id('dbo.Rental') is null
begin
create table dbo.Rental
(
	id int,
	renter nvarchar(30) NOT NULL,
	cy int NOT NULL,
	cw int NOT NULL,
	rentState bit NOT NULL,
	orderDate smalldatetime NOT NULL,
	acceptanceDate smalldatetime,
	CONSTRAINT pk_Rental PRIMARY KEY(id, cw, cy),
	CONSTRAINT fk_Rental FOREIGN KEY(id) REFERENCES Property(id)
);

end
	
raiserror('... OK',0,1) --info
commit