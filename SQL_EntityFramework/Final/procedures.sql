USE BIT;

GO
IF OBJECT_ID('BIT_SCHEMA.Procedure_Atribuir_Servicos_Urgentes') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Atribuir_Servicos_Urgentes;

IF OBJECT_ID('BIT_SCHEMA.Procedure_Atribuir_Servicos_Expresso') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Atribuir_Servicos_Expresso;

IF OBJECT_ID('BIT_SCHEMA.Procedure_Atribuir_Servicos_Normais') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Atribuir_Servicos_Normais;

IF OBJECT_ID('BIT_SCHEMA.Procedure_Processar_Servicos_Em_Analise') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Processar_Servicos_Em_Analise;

IF OBJECT_ID('BIT_SCHEMA.Procedure_Servicos_Entregues_Fora_Do_Prazo') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Servicos_Entregues_Fora_Do_Prazo;

IF OBJECT_ID('BIT_SCHEMA.Procedure_Atribui_Servico_A_Estafeta') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Atribui_Servico_A_Estafeta;

IF OBJECT_ID('BIT_SCHEMA.Procedure_Passar_Servivos_De_Estafeta_Para_Outro_Estafeta') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Passar_Servivos_De_Estafeta_Para_Outro_Estafeta;

IF OBJECT_ID('BIT_SCHEMA.Procedure_Inserir_Morada') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Inserir_Morada;

IF OBJECT_ID('BIT_SCHEMA.Procedure_Inserir_Pessoa') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Inserir_Pessoa; 

IF OBJECT_ID('BIT_SCHEMA.Procedure_Inserir_Contactavel') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Inserir_Contactavel;	

IF OBJECT_ID('BIT_SCHEMA.Procedure_Inserir_Particular') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Inserir_Particular; 

IF OBJECT_ID('BIT_SCHEMA.Procedure_Inserir_Empresa') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Inserir_Empresa;	

IF OBJECT_ID('BIT_SCHEMA.Procedure_Remover_Cliente_Servico') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Remover_Cliente_Servico;	

IF OBJECT_ID('BIT_SCHEMA.Procedure_Remover_Empresa') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Remover_Empresa; 

IF OBJECT_ID('BIT_SCHEMA.Procedure_Remover_Particular') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Remover_Particular;	
	
IF OBJECT_ID('BIT_SCHEMA.Procedure_Inserir_Funcionario') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Inserir_Funcionario;	

IF OBJECT_ID('BIT_SCHEMA.Procedure_Remover_Funcionario_Servico') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Remover_Funcionario_Servico;	

IF OBJECT_ID('BIT_SCHEMA.Procedure_Remover_Funcionario') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Remover_Funcionario;	

IF OBJECT_ID('BIT_SCHEMA.Procedure_Inserir_Servico') IS NOT NULL
	DROP PROCEDURE BIT_SCHEMA.Procedure_Inserir_Servico;	

GO





GO
CREATE PROCEDURE BIT_SCHEMA.Procedure_Atribui_Servico_A_Estafeta @servicoID INT
AS
BEGIN
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	BEGIN TRANSACTION
		DECLARE @lojaEntrega INT = (SELECT S.lojaEntrega FROM BIT_SCHEMA.Servico AS S WHERE S.id = @servicoID)

		DECLARE @estafetaParaPassarServico INT =	(SELECT	TOP (1) FD.estafeta
													FROM	BIT_SCHEMA.Function_Estafetas_Disponiveis() AS FD
													WHERE	FD.loja = @lojaEntrega
																AND
															FD.nServicos<10
													ORDER BY FD.nServicos ASC)

		IF (@estafetaParaPassarServico IS NULL)
			RETURN
		
		UPDATE	BIT_SCHEMA.Servico
		SET		estafeta = @estafetaParaPassarServico
		WHERE	id = @servicoID
	COMMIT
END





GO
CREATE PROCEDURE BIT_SCHEMA.Procedure_Atribuir_Servicos_Urgentes
AS 
BEGIN

	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION
		DECLARE	servicosNormaisEmAnalise CURSOR LOCAL FORWARD_ONLY 
		FOR		(SELECT  Servico.id	FROM	Servico		WHERE	Servico.tipo IN ('URGENTE','URGENTE COM RECOLHA','URGENTE FORA')
																AND Servico.estado = 'AN�LISE')
		OPEN	servicosNormaisEmAnalise


		DECLARE @curServico INT
		FETCH NEXT FROM servicosNormaisEmAnalise INTO @curServico


		WHILE @@FETCH_STATUS = 0
		BEGIN
		
			EXEC BIT_SCHEMA.Procedure_Atribui_Servico_A_Estafeta @curServico
			
			FETCH NEXT FROM servicosDoEstafeta INTO @curServico

		END

		CLOSE servicosNormaisEmAnalise
		DEALLOCATE servicosNormaisEmAnalise
	COMMIT
END






GO
CREATE PROCEDURE BIT_SCHEMA.Procedure_Atribuir_Servicos_Expresso
AS 
BEGIN

	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION
		DECLARE	servicosNormaisEmAnalise CURSOR LOCAL FORWARD_ONLY 
		FOR		(SELECT  Servico.id	FROM	Servico		WHERE	Servico.tipo IN ('EXPRESSO','EXPRESSO COM RECOLHA','EXPRESSO FORA')
																AND Servico.estado = 'AN�LISE')
		OPEN	servicosNormaisEmAnalise


		DECLARE @curServico INT
		FETCH NEXT FROM servicosNormaisEmAnalise INTO @curServico


		WHILE @@FETCH_STATUS = 0
		BEGIN
		
			EXEC BIT_SCHEMA.Procedure_Atribui_Servico_A_Estafeta @curServico
			
			FETCH NEXT FROM servicosDoEstafeta INTO @curServico

		END

		CLOSE servicosNormaisEmAnalise
		DEALLOCATE servicosNormaisEmAnalise
	COMMIT
END






GO
CREATE PROCEDURE BIT_SCHEMA.Procedure_Atribuir_Servicos_Normais
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION
		DECLARE	servicosNormaisEmAnalise CURSOR LOCAL FORWARD_ONLY 
		FOR		(SELECT  Servico.id	FROM	Servico		WHERE	Servico.tipo = 'NORMAL'
																AND Servico.estado = 'AN�LISE')
		OPEN	servicosNormaisEmAnalise


		DECLARE @curServico INT
		FETCH NEXT FROM servicosNormaisEmAnalise INTO @curServico


		WHILE @@FETCH_STATUS = 0
		BEGIN
		
			EXEC BIT_SCHEMA.Procedure_Atribui_Servico_A_Estafeta @curServico
			
			FETCH NEXT FROM servicosDoEstafeta INTO @curServico

		END

		CLOSE servicosNormaisEmAnalise
		DEALLOCATE servicosNormaisEmAnalise
	COMMIT
END





GO
CREATE PROCEDURE BIT_SCHEMA.Procedure_Processar_Servicos_Em_Analise
AS 
BEGIN
	
	EXEC BIT_SCHEMA.Procedure_Atribuir_Servicos_Urgentes
	EXEC BIT_SCHEMA.Procedure_Atribuir_Servicos_Expresso
	EXEC BIT_SCHEMA.Procedure_Atribuir_Servicos_Normais

END






GO
CREATE PROCEDURE BIT_SCHEMA.Procedure_Servicos_Entregues_Fora_Do_Prazo
AS 
BEGIN
	
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRANSACTION
		SELECT	S.id, S.dataRequisicao, S.dataConclusao
		FROM	BIT_SCHEMA.Servico AS S
		WHERE	S.estado = 'CONCLU�DO'
					AND
				BIT_SCHEMA.Function_Servico_Foi_Entregue_Fora_Do_Prazo(id) = 1
	COMMIT
END






GO
CREATE PROCEDURE BIT_SCHEMA.Procedure_Passar_Servivos_De_Estafeta_Para_Outro_Estafeta @estafeta INT
AS
BEGIN
	
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION

		DECLARE @lojaDoEstafeta INT = (SELECT F.loja	FROM BIT_SCHEMA.Funcionario AS F	WHERE F.id = @estafeta)

		DECLARE	servicosDoEstafeta CURSOR LOCAL FORWARD_ONLY 
		FOR		(SELECT	S.id	FROM	BIT_SCHEMA.Servico AS S		WHERE	S.estafeta = @estafeta AND S.estado = 'EM PROGRESSO')
		OPEN	 servicosDoEstafeta


		DECLARE @curServico INT
		FETCH NEXT FROM servicosDoEstafeta INTO @curServico


		WHILE @@FETCH_STATUS = 0
		BEGIN
		
			DECLARE @estafetaParaPassarServico INT =	(SELECT	TOP (1) FD.estafeta
														FROM	BIT_SCHEMA.Function_Estafetas_Disponiveis() AS FD
														WHERE	FD.loja = @lojaDoEstafeta
																	AND
																FD.nServicos<10
																	AND
																FD.estafeta <> @estafeta
														ORDER BY FD.nServicos ASC)

			IF (@estafetaParaPassarServico IS NULL)
			BEGIN
				COMMIT
				RETURN
			END
			
			UPDATE	BIT_SCHEMA.Servico
			SET		estafeta = @estafetaParaPassarServico
			WHERE	id = @curServico

			FETCH NEXT FROM servicosDoEstafeta INTO @curServico

		END

		CLOSE servicosDoEstafeta
		DEALLOCATE servicosDoEstafeta

	COMMIT
END





GO
CREATE PROC BIT_SCHEMA.Procedure_Inserir_Morada(	@moradaId INT OUTPUT, @nPorta VARCHAR(10), @andar VARCHAR(10),  
													@distrito VARCHAR(50), @concelho VARCHAR(50), @freguesia VARCHAR(50), 
													@localidade VARCHAR(100), @arteriaTipo VARCHAR(100), 
													@arteriaTitulo VARCHAR(100), @arteriaDesignacao VARCHAR(100), 
													@codPostal INT, @extCodPostal INT)
AS
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	BEGIN TRANSACTION
	
		DECLARE @idCodPostalView INT = BIT_SCHEMA.Function_CodigoPostalView_obterId(	@distrito, @concelho, @freguesia, @localidade, 
																						@arteriaTipo, @arteriaTitulo, @arteriaDesignacao, 
																						@nPorta, @codPostal, @extCodPostal);
																		
		SET @moradaId = BIT_SCHEMA.Function_Morada_obterId( @idCodPostalView, @nPorta, @andar);
		IF (@moradaId IS NULL)
		BEGIN
			INSERT INTO BIT_SCHEMA.Morada (morada, nPorta, andar) VALUES (@idCodPostalView, @nPorta, @andar);
			SET @moradaId = (	SELECT M.id 
								FROM BIT_SCHEMA.Morada AS M
								WHERE M.andar = @andar AND M.morada = @idCodPostalView AND M.nPorta = @nPorta);
		END
	COMMIT	
GO





GO
CREATE PROC BIT_SCHEMA.Procedure_Inserir_Contactavel(	@id INT OUTPUT, @nome VARCHAR(100),@contacto INT, @idMorada INT)
AS
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRANSACTION
		
			INSERT INTO Contactavel (nome, morada) VALUES (@nome, @idMorada);

			SET @id = (	SELECT C.id 
						FROM BIT_SCHEMA.Contactavel AS C
						WHERE C.morada = @idMorada AND C.nome = @nome);
			INSERT INTO Contacto (id, nTelefone) VALUES (@id, @contacto);

	COMMIT
GO






GO
CREATE PROC BIT_SCHEMA.Procedure_Inserir_Pessoa(	@nome VARCHAR(100),@contacto INT, @idMorada INT, 
													@sobrenome VARCHAR(30), @email VARCHAR(100), @pessoaId INT OUTPUT)
AS
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRANSACTION

		IF (@pessoaId IS NULL)
			EXEC BIT_SCHEMA.Procedure_Inserir_Contactavel	@pessoaId OUTPUT, @nome, @contacto, @idMorada;
		
		INSERT INTO BIT_SCHEMA.Pessoa (id, sobrenome, email) VALUES (@pessoaId, @sobrenome, @email);
	COMMIT
GO





GO
CREATE PROC BIT_SCHEMA.Procedure_Inserir_Particular(	@nome VARCHAR(100),@contacto INT, @nif INT, @idMorada INT, 
														@sobrenome VARCHAR(30), @email VARCHAR(100) )
AS
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRANSACTION

		DECLARE @id INT;

		EXEC BIT_SCHEMA.Procedure_Inserir_Contactavel	@id OUTPUT, @nome, @contacto, @idMorada;
		EXEC BIT_SCHEMA.Procedure_Inserir_Pessoa @nome, @contacto, @idMorada, @sobrenome, @email, @id OUTPUT;
		INSERT INTO BIT_SCHEMA.Cliente(nif, contactavel) VALUES (@nif, @id);
		INSERT INTO ParticularTable (id, pessoa) VALUES (@nif, @id);
			
	COMMIT
GO





GO
CREATE PROC BIT_SCHEMA.Procedure_Inserir_Empresa(	@nome VARCHAR(100),@contacto INT, @nif INT, @idMorada INT, 
													@interfacer VARCHAR(50))
AS
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRANSACTION
		
		DECLARE @id INT;

		EXEC BIT_SCHEMA.Procedure_Inserir_Contactavel	@id OUTPUT, @nome, @contacto, @idMorada;
		INSERT INTO BIT_SCHEMA.Cliente(nif, contactavel) VALUES (@nif, @id);
		INSERT INTO BIT_SCHEMA.EmpresaTable(id, interfacerId) VALUES (@nif, @interfacer)

	COMMIT
GO





GO
CREATE PROC BIT_SCHEMA.Procedure_Remover_Cliente_Servico( @idCliente INT)
AS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION		
		DECLARE @idServico TABLE(
			id INT
		); 
		
		INSERT @idServico	SELECT S.id
							FROM BIT_SCHEMA.Servico AS S
							WHERE S.cliente = @idCliente;

		DELETE FROM BIT_SCHEMA.Tentativa WHERE servico IN (SELECT * FROM @idServico);
		DELETE FROM BIT_SCHEMA.Servico WHERE cliente = @idCliente;

	COMMIT
GO

 



GO
CREATE PROC BIT_SCHEMA.Procedure_Remover_Particular( @nif INT)
AS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION		

	DECLARE @idContactavel INT = (	SELECT C.contactavel
									FROM BIT_SCHEMA.Cliente AS C
									WHERE C.nif = @nif);
		
	EXEC BIT_SCHEMA.Procedure_Remover_Cliente_Servico @nif;
	DELETE FROM BIT_SCHEMA.ParticularTable WHERE id = @nif;
	DELETE FROM BIT_SCHEMA.Cliente WHERE nif = @nif;

	DELETE FROM BIT_SCHEMA.Pessoa WHERE id = @idContactavel;
	DELETE FROM BIT_SCHEMA.Contacto WHERE id = @idContactavel;
	DELETE FROM BIT_SCHEMA.Contactavel WHERE id = @idContactavel;

	COMMIT
GO




GO
CREATE PROC BIT_SCHEMA.Procedure_Remover_Empresa( @nif INT)
AS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION		

	DECLARE @idContactavel INT = (	SELECT C.contactavel
									FROM BIT_SCHEMA.Cliente AS C
									WHERE C.nif = @nif);
		
	EXEC BIT_SCHEMA.Procedure_Remover_Cliente_Servico @nif;
	DELETE FROM BIT_SCHEMA.EmpresaTable WHERE id = @nif;
	DELETE FROM BIT_SCHEMA.Cliente WHERE nif = @nif;

	DELETE FROM BIT_SCHEMA.Contacto WHERE id = @idContactavel;
	DELETE FROM BIT_SCHEMA.Contactavel WHERE id = @idContactavel;

	COMMIT
GO





GO
CREATE PROC BIT_SCHEMA.Procedure_Inserir_Funcionario(	@nome VARCHAR(100), @contacto INT, @idMorada INT, 
														@sobrenome VARCHAR(30), @email VARCHAR(100), @estado VARCHAR(15), 
														@loja INT, @funcao VARCHAR(15))
AS
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRANSACTION
		
		DECLARE @pessoaId INT;
		EXEC BIT_SCHEMA.Procedure_Inserir_Pessoa	@nome, @contacto, @idMorada, @sobrenome, @email, @pessoaId OUTPUT;

		INSERT INTO BIT_SCHEMA.Funcionario(id, loja, estado) VALUES (@pessoaId, @loja, @estado);

		IF (@funcao IS NOT NULL)
		BEGIN 
			INSERT INTO BIT_SCHEMA.Funcionario_Funcao(funcionario, funcao) VALUES (@pessoaId, @funcao);
			IF (@funcao LIKE 'ESTAFETA')
				INSERT INTO BIT_SCHEMA.Estafeta(id) VALUES (@pessoaId);
		END
	COMMIT
GO





GO
CREATE PROC BIT_SCHEMA.Procedure_Remover_Funcionario_Servico( @idFuncionario INT)
AS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION		
		DECLARE @idServico TABLE(
			id INT
		); 
		
		INSERT @idServico	SELECT S.id
							FROM BIT_SCHEMA.Servico AS S
							WHERE S.estafeta = @idFuncionario;

		DELETE FROM BIT_SCHEMA.Tentativa WHERE servico IN (SELECT * FROM @idServico);
		DELETE FROM BIT_SCHEMA.Servico WHERE estafeta = @idFuncionario;

	COMMIT
GO





GO
CREATE PROC BIT_SCHEMA.Procedure_Remover_Funcionario( @idContactavel INT)
AS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION		
		
		EXEC BIT_SCHEMA.Procedure_Remover_Funcionario_Servico @idContactavel;

		IF EXISTS(SELECT * FROM BIT_SCHEMA.Estafeta WHERE id = @idContactavel)
			DELETE FROM BIT_SCHEMA.Estafeta WHERE id = @idContactavel;

		DELETE FROM BIT_SCHEMA.Funcionario_Funcao WHERE funcionario = @idContactavel;
		DELETE FROM BIT_SCHEMA.Funcionario WHERE id = @idContactavel;

		DELETE FROM BIT_SCHEMA.Pessoa WHERE id = @idContactavel;
		DELETE FROM BIT_SCHEMA.Contacto WHERE id = @idContactavel;
		DELETE FROM BIT_SCHEMA.Contactavel WHERE id = @idContactavel;

	COMMIT
GO




GO
CREATE PROC BIT_SCHEMA.Procedure_Inserir_Servico (	@tipo VARCHAR(50), @idMoradaEntrega INT, 
													@idMoradaFaturacao INT, @dataRequisicao SMALLDATETIME, 
													 @estado VARCHAR(20), 
													@idCliente INT, @idLojaEntrega SMALLINT, @idLojaRecepcao SMALLINT, @id INT OUTPUT)
AS
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	BEGIN TRANSACTION		
		
		DECLARE @loja SMALLINT

		IF(@idLojaEntrega IS NOT NULL)
			SET @loja = @idLojaEntrega
		ELSE
			SET @loja = @idLojaRecepcao


		DECLARE @servicosDisponiveis TABLE(tipo VARCHAR(15),
											cnt INT)
		
		INSERT INTO @servicosDisponiveis SELECT *  
										FROM BIT_SCHEMA.Function_Servicos_Disponiveis_Para_Loja(@loja)


		IF NOT EXISTS(	SELECT *
						FROM	@servicosDisponiveis AS SD
						WHERE	CHARINDEX(SD.tipo, @tipo) > 0
									AND
								SD.cnt > 0)
		BEGIN
			RAISERROR('Nao existem recursos suficientes para aceitar o Servi�o', 16, 1) 
			ROLLBACK
			RETURN
		END

		DECLARE @table TABLE (id int);

		INSERT INTO BIT_SCHEMA.Servico(	tipo, moradaEntrega, moradaFaturacao, dataRequisicao, 
										estado, cliente, lojaEntrega, lojaRecepcao) 
										OUTPUT INSERTED.id INTO @table
										VALUES (@tipo, @idMoradaEntrega, @idMoradaFaturacao, @dataRequisicao, 
										@estado, @idCliente, @idLojaEntrega, @idLojaRecepcao);
										

		SET @id =	(SELECT T.id 
					 FROM @table AS T);

	COMMIT
GO