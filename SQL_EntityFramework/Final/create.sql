﻿SET NOCOUNT ON;
SET XACT_ABORT ON

GO

USE [master];
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'BIT')
BEGIN
  RAISERROR('A remover a base de dados BIT ...',0,1); --info
    ALTER DATABASE [BIT] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
    DROP DATABASE [BIT];
END
GO

RAISERROR('A criar a base de dados BIT ...',0,1); --info
CREATE DATABASE BIT;
GO

USE BIT;
IF EXISTS (SELECT name FROM sys.schemas WHERE name = N'BIT_SCHEMA')
BEGIN
  RAISERROR('A remover o squema BIT_SCHEMA ...',0,1); --info
    DROP SCHEMA [BIT_SCHEMA];
END

GO
CREATE SCHEMA BIT_SCHEMA
GO

GO
BEGIN TRANSACTION

IF OBJECT_ID('BIT_SCHEMA.Particular') IS NOT NULL
	DROP VIEW BIT_SCHEMA.Particular;

IF OBJECT_ID('BIT_SCHEMA.Empresa') IS NOT NULL
	DROP VIEW BIT_SCHEMA.Empresa;

IF OBJECT_ID('BIT_SCHEMA.Empregado') IS NOT NULL
	DROP VIEW BIT_SCHEMA.Empregado;

IF OBJECT_ID('BIT_SCHEMA.Funcionario_Funcao') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Funcionario_Funcao;

IF OBJECT_ID('BIT_SCHEMA.Tentativa') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Tentativa;

IF OBJECT_ID('BIT_SCHEMA.Servico') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Servico;

IF OBJECT_ID('BIT_SCHEMA.Estafeta') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Estafeta;
	
IF OBJECT_ID('BIT_SCHEMA.Funcionario') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Funcionario;
	
IF OBJECT_ID('BIT_SCHEMA.Funcao') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Funcao;

IF OBJECT_ID('BIT_SCHEMA.Loja') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Loja;

IF OBJECT_ID('BIT_SCHEMA.TipoServico') IS NOT NULL
	DROP TABLE BIT_SCHEMA.TipoServico;

IF OBJECT_ID('BIT_SCHEMA.EmpresaTable') IS NOT NULL
	DROP TABLE BIT_SCHEMA.EmpresaTable;

IF OBJECT_ID('BIT_SCHEMA.ParticularTable') IS NOT NULL
	DROP TABLE BIT_SCHEMA.ParticularTable;

IF OBJECT_ID('BIT_SCHEMA.Cliente') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Cliente;

IF OBJECT_ID('BIT_SCHEMA.Pessoa') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Pessoa;

IF OBJECT_ID('BIT_SCHEMA.Contacto') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Contacto;
	
IF OBJECT_ID('BIT_SCHEMA.Contactavel') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Contactavel;

IF OBJECT_ID('BIT_SCHEMA.Morada') IS NOT NULL
	DROP TABLE BIT_SCHEMA.Morada;
	
IF OBJECT_ID('BIT_SCHEMA.CodigoPostalView') IS NOT NULL
	DROP VIEW BIT_SCHEMA.CodigoPostalView;

GO	
CREATE VIEW BIT_SCHEMA.CodigoPostalView 
AS 
	SELECT	CP.id, D.distrito AS distrito, C.Concelho AS concelho, 
			F.freguesia AS freguesia, CP.Nome_localidade AS localidade, CP.arteria_tipo AS arteriaTipo, 
			CP.arteria_Titulo AS arteriaTitulo, CP.arteria_designacao AS arteriaDesignacao, 
			CP.n_cod_postal AS codigoPostal, CP.extensao_n_do_cod_postal AS extensaoCodPostal
	FROM CTT.dbo.CodigoPostal AS CP INNER JOIN CTT.dbo.Distrito AS D INNER JOIN CTT.dbo.Concelho AS C INNER JOIN CTT.dbo.Freguesia AS F
	ON (F.concelho = C.id AND F.distrito = C.distrito)
	ON (C.distrito = D.id)
	ON (CP.concelho = C.id AND CP.freguesia = F.id AND CP.distrito = D.id)
GO

CREATE TABLE BIT_SCHEMA.Morada (
	id INT IDENTITY CONSTRAINT pk_morada PRIMARY KEY,
	morada INT NOT NULL,
	nPorta VARCHAR(10) NOT NULL,
	andar VARCHAR(10) DEFAULT 0,
);

CREATE TABLE BIT_SCHEMA.Contactavel (
   id INT IDENTITY CONSTRAINT pk_contactavel PRIMARY KEY,
   nome VARCHAR(100) NOT NULL,
   morada INT NOT NULL,
   CONSTRAINT fk_contactavel FOREIGN KEY(id) REFERENCES BIT_SCHEMA.Morada(id),
   CONSTRAINT ak_contactavel UNIQUE(nome, morada)
);

CREATE TABLE BIT_SCHEMA.Contacto (
   id INT,
   nTelefone INT,
   CONSTRAINT pk_contacto PRIMARY KEY(id, nTelefone),
   CONSTRAINT fk_contacto FOREIGN KEY(id) REFERENCES BIT_SCHEMA.Contactavel(id),
);
	
CREATE TABLE BIT_SCHEMA.Pessoa (
   id INT CONSTRAINT pk_pessoa PRIMARY KEY,
   sobrenome VARCHAR(30) NOT NULL,
   email VARCHAR(100) NOT NULL, 
   CONSTRAINT fk_pessoa FOREIGN KEY(id) REFERENCES BIT_SCHEMA.Contactavel(id),
   CONSTRAINT ch_pessoa CHECK (email LIKE '%@%'),
);

CREATE TABLE BIT_SCHEMA.Cliente (
	nif INT CONSTRAINT pk_cliente PRIMARY KEY, 
	contactavel INT UNIQUE,
	CONSTRAINT fk_cliente FOREIGN KEY(contactavel) REFERENCES BIT_SCHEMA.Contactavel(id),
	CONSTRAINT ch_cliente CHECK (nif < 1000000000),
);

CREATE TABLE BIT_SCHEMA.ParticularTable (
	id INT	CONSTRAINT pk_particular PRIMARY KEY,
	pessoa INT UNIQUE,
	CONSTRAINT fk_particular FOREIGN KEY(id) REFERENCES BIT_SCHEMA.Cliente(nif),
	CONSTRAINT fk1_particular FOREIGN KEY(pessoa) REFERENCES BIT_SCHEMA.Pessoa(id),
);

CREATE TABLE BIT_SCHEMA.EmpresaTable (
	id INT CONSTRAINT pk_empresa PRIMARY KEY,
	interfacerId VARCHAR(50),
	CONSTRAINT fk_empresa FOREIGN KEY(id) REFERENCES BIT_SCHEMA.Cliente(nif),
);

CREATE TABLE BIT_SCHEMA.TipoServico (
	tipo VARCHAR(50) CONSTRAINT pk_tipoServico PRIMARY KEY,
	precoBase DECIMAL(3,1) NOT NULL,
	CONSTRAINT ch_tipoServico CHECK (tipo IN (	'URGENTE', 'URGENTE FORA' , 'URGENTE COM RECOLHA' , 'EXPRESSO', 'EXPRESSO FORA', 
												'EXPRESSO COM RECOLHA', 'NORMAL')),
);

CREATE TABLE BIT_SCHEMA.Loja (
	id SMALLINT IDENTITY CONSTRAINT pk_loja PRIMARY KEY,
	designacao VARCHAR(50),
	operacional BIT DEFAULT 0,
	morada INT NOT NULL,
	CONSTRAINT fk_loja FOREIGN KEY(morada) REFERENCES BIT_SCHEMA.Morada(id),
);

CREATE TABLE BIT_SCHEMA.Funcao (
	tipo VARCHAR(15) CONSTRAINT pk_funcao PRIMARY KEY,
	salarioBase DECIMAL(7,2) NOT NULL,
	CONSTRAINT ch_funcao CHECK (tipo IN ('SECRETÁRIO', 'GESTOR', 'ESTAFETA', 'CONTABILISTA')),
);

CREATE TABLE BIT_SCHEMA.Funcionario (
	id INT CONSTRAINT pk_funcionario PRIMARY KEY,
	estado VARCHAR(15) DEFAULT 'DISPONIVEL',
	loja SMALLINT,
	CONSTRAINT fk_funcionario FOREIGN KEY(id) REFERENCES BIT_SCHEMA.Pessoa(id),
	CONSTRAINT fk2_funcionario FOREIGN KEY(loja) REFERENCES BIT_SCHEMA.Loja(id),
	CONSTRAINT ch_funcionario CHECK (estado IN ( 'DISPONIVEL', 'INDISPONIVEL' , 'AUSENTE')),
);

CREATE TABLE BIT_SCHEMA.Estafeta (
	id INT CONSTRAINT pk_estafeta PRIMARY KEY,
	CONSTRAINT fk_estafeta FOREIGN KEY(id) REFERENCES BIT_SCHEMA.Funcionario(id),
);

CREATE TABLE BIT_SCHEMA.Servico (
	id INT IDENTITY CONSTRAINT pk_servico PRIMARY KEY,
	tipo VARCHAR(50) NOT NULL,
	estafeta INT,
	moradaEntrega INT NOT NULL,
	moradaFaturacao INT,
	dataRequisicao SMALLDATETIME DEFAULT getDate(),
	dataConclusao SMALLDATETIME,
	estado VARCHAR(20) DEFAULT 'ANÁLISE',
	cliente INT NOT NULL,
	lojaEntrega SMALLINT,
	lojaRecepcao SMALLINT NOT NULL,
	CONSTRAINT fk_servico FOREIGN KEY(cliente) REFERENCES BIT_SCHEMA.Cliente(nif),
	CONSTRAINT fk1_servico FOREIGN KEY(estafeta) REFERENCES BIT_SCHEMA.Estafeta(id),
	CONSTRAINT fk2_servico FOREIGN KEY(moradaEntrega) REFERENCES BIT_SCHEMA.Morada(id),
	CONSTRAINT fk3_servico FOREIGN KEY(moradaFaturacao) REFERENCES BIT_SCHEMA.Morada(id),
	CONSTRAINT fk4_servico FOREIGN KEY(tipo) REFERENCES BIT_SCHEMA.TipoServico(tipo),
	CONSTRAINT fk5_servico FOREIGN KEY(lojaEntrega) REFERENCES BIT_SCHEMA.Loja(id),
	CONSTRAINT fk6_servico FOREIGN KEY(lojaRecepcao) REFERENCES BIT_SCHEMA.Loja(id),
	CONSTRAINT ch_servico CHECK (estado IN ('ANÁLISE', 'EM PROGRESSO' , 'CANCELADO' , 'CONCLUÍDO', 'EXPIRADO')),
	
);

CREATE TABLE BIT_SCHEMA.Tentativa (
	id tinyint,
	servico INT NOT NULL,
	dataTentativa SMALLDATETIME NOT NULL,
	descricao VARCHAR(200),
	sucesso BIT DEFAULT 0,
	CONSTRAINT pk_tentativa PRIMARY KEY(id, servico),
	CONSTRAINT fk_tentativa FOREIGN KEY(servico) REFERENCES BIT_SCHEMA.Servico(id),
);

CREATE TABLE BIT_SCHEMA.Funcionario_Funcao (
	funcionario INT NOT NULL,
	funcao VARCHAR(15) NOT NULL,

	CONSTRAINT pk_funcionario_funcao PRIMARY KEY(funcionario, funcao),
	CONSTRAINT fk_funcionario_funcao foreign key(funcionario) REFERENCES BIT_SCHEMA.Funcionario(id),
	CONSTRAINT fk1_funcionario_funcao foreign key(funcao) REFERENCES BIT_SCHEMA.Funcao(tipo),
);

GO
CREATE VIEW BIT_SCHEMA.Empregado
AS 
	SELECT	C.id, C.nome, P.sobrenome, P.email, CO.nTelefone, C.morada, F.estado, F.loja
	FROM	BIT_SCHEMA.Contactavel AS C INNER JOIN BIT_SCHEMA.Contacto AS CO INNER JOIN BIT_SCHEMA.Pessoa AS P 
			INNER JOIN BIT_SCHEMA.Funcionario AS F
				ON P.id = F.id
				ON P.id = CO.id
				ON C.id = CO.id
GO

GO
CREATE VIEW BIT_SCHEMA.Empresa
AS 
	SELECT	C.id, C.nome, CO.nTelefone, CI.nif, C.morada, E.interfacerId
	FROM	BIT_SCHEMA.Contactavel AS C INNER JOIN BIT_SCHEMA.Contacto AS CO INNER JOIN BIT_SCHEMA.EmpresaTable AS E 
			INNER JOIN BIT_SCHEMA.Cliente AS CI
				ON E.id = CI.nif
				ON CO.id = CI.contactavel
				ON C.id = CO.id
GO

GO
CREATE VIEW BIT_SCHEMA.Particular
AS 
	SELECT	C.id, C.nome, P.sobrenome, P.email, CO.nTelefone, CI.nif, C.morada
	FROM	BIT_SCHEMA.Contactavel AS C INNER JOIN BIT_SCHEMA.Contacto AS CO INNER JOIN BIT_SCHEMA.Pessoa AS P 
			INNER JOIN BIT_SCHEMA.Cliente AS CI
				ON P.id = CI.contactavel
				ON P.id = CO.id
				ON C.id = CO.id
GO

-- INSERIR DEFAULT TABLES VALUES --

-- INSERIR FUNCAO --
INSERT INTO BIT_SCHEMA.Funcao (tipo, salarioBase) VALUES	('GESTOR', 800), ('SECRETÁRIO', 600), ('CONTABILISTA', 700), 
															('ESTAFETA', 600);


-- INSERIR TIPOSERVICO --
INSERT INTO BIT_SCHEMA.TipoServico (tipo, precoBase) VALUES ('URGENTE', 15),('URGENTE FORA' , 20),('URGENTE COM RECOLHA' , 20),
															('EXPRESSO', 10),('EXPRESSO FORA', 15),('EXPRESSO COM RECOLHA', 15),
															('NORMAL',7.5);


RAISERROR('... OK',0,1) --info
COMMIT