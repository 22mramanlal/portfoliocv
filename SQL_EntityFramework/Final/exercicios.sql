USE BIT;

-- 1
-- Resolvido no ficheiro populateBIT.sql e unpopulateBIT.sql 

-- 2
-- Resolvido no ficheiro populateBIT.sql e unpopulateBIT.sql 


-- 3
--Actualizar a fun��o de um empregado.

		DECLARE @idFuncionario INT
		DECLARE @funcaoAntiga VARCHAR(15)
		DECLARE @funcaoNova VARCHAR(15)

		DELETE FROM BIT_SCHEMA.Funcionario_Funcao WHERE funcionario = @idFuncionario AND funcao = @funcaoAntiga
		
		INSERT INTO BIT_SCHEMA.Funcionario_Funcao (funcionario, funcao) VALUES (@idFuncionario , @funcaoNova)

--4. 
--Obter uma morada em fun��o de um c�digo postal.


		DECLARE @codPostal SMALLINT
		DECLARE @extCodPostal SMALLINT

		SELECT CPV.distrito, CPV.concelho,CPV.freguesia,  CPV.localidade, CPV.arteriaTipo, CPV.arteriaTitulo,
			   CPV.arteriaDesignacao,  CPV.codigoPostal,  CPV.extensaoCodPostal
		From BIT_SCHEMA.CodigoPostalView AS CPV 
		WHERE CPV.codigoPostal = @codPostal AND CPV.extensaoCodPostal = @extCodPostal



-- 5
-- Processar os servi�os com o estado em an�lise, atribuindo-lhes um estafeta
-- automaticamente

	 EXEC BIT_SCHEMA.Procedure_Processar_Servicos_Em_Analise

-- 6
--Simular uma entrega, em que a probabilidade de sucesso � 95%. Essa
--tentativa deve ficar registada como correspondendo a uma tentativa de
--entrega real.

	DECLARE @SucessoInsucesso SMALLINT = 99 *RAND();
		
	DECLARE @idTentativa TINYINT;
	DECLARE @idServico INT;

	IF (@SucessoInsucesso >= 0 AND @SucessoInsucesso < 94)
		BEGIN
			--SUCESSO
			
			INSERT BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao) VALUES ( @idTentativa, @idServico, getDATE(), 'Ausente', 0);
			INSERT BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao) VALUES ( @idTentativa, @idServico, getDATE(), 'Encomenda Entregue', 1);	

		END
	ELSE
		BEGIN
			--INSUCESSO 

			INSERT BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao) VALUES ( @idTentativa, @idServico, getDATE(), 'Ausente', 0);
			INSERT BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao) VALUES ( @idTentativa, @idServico, getDATE(), 'Ausente', 0);
			INSERT BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao) VALUES ( @idTentativa, @idServico, getDATE(), 'Ausente', 0);

		END


-- 7
BIT_SCHEMA.Function_Servicos_Disponiveis_Para_Loja(@loja SMALLINT);


-- 8
-- Resolvido no ficheiro populateBIT.sql e unpopulateBIT.sql 



--9. 
--Cancelar um servi�o.

	DECLARE @idServico INT

	update BIT_SCHEMA.Servico 
	SET estado = 'CANCELADO' 
	WHERE id = @idServico

-- 10
	
	BIT_SCHEMA.Function_Estafetas_Disponiveis_Para_Aceitar_Servicos()


-- 11
-- Listar os servi�os que foram entregues fora do prazo espect�vel.

	EXEC BIT_SCHEMA.Procedure_Servicos_Entregues_Fora_Do_Prazo


-- 12
-- Alterar os servi�os de um determinado estafeta, reafectando-os a outros,
-- garantindo a qualidade de servi�o e uma equidade da distribui��o
-- entre os estafetas dispon�veis.

	DECLARE @idEstafeta INT

	EXEC BIT_SCHEMA.Procedure_Passar_Servivos_De_Estafeta_Para_Outro_Estafeta @idEstafeta