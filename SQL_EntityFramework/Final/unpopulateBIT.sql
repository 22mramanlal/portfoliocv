USE BIT;


-- REMOVER EMPRESAS --
EXEC BIT_SCHEMA.Procedure_Remover_Empresa 1685253;

EXEC BIT_SCHEMA.Procedure_Remover_Empresa 1050191;


-- REMOVER PARTICULARES --
EXEC BIT_SCHEMA.Procedure_Remover_Particular 12345678;

EXEC BIT_SCHEMA.Procedure_Remover_Particular 87654321;


-- REMOVER EMPREGADO --
DECLARE @idFuncionario INT = (	SELECT id
								FROM BIT_SCHEMA.Pessoa
								WHERE sobrenome = 'bebito');
EXEC BIT_SCHEMA.Procedure_Remover_Funcionario @idFuncionario;


SET @idFuncionario = (	SELECT id
						FROM BIT_SCHEMA.Pessoa
						WHERE sobrenome = 'bubu');
EXEC BIT_SCHEMA.Procedure_Remover_Funcionario @idFuncionario;


SET @idFuncionario = (	SELECT id
						FROM BIT_SCHEMA.Pessoa
						WHERE sobrenome = 'mimimi');
EXEC BIT_SCHEMA.Procedure_Remover_Funcionario @idFuncionario;


SET @idFuncionario = (	SELECT id
						FROM BIT_SCHEMA.Pessoa
						WHERE sobrenome = 'akshay');
EXEC BIT_SCHEMA.Procedure_Remover_Funcionario @idFuncionario;


SET @idFuncionario = (	SELECT id
						FROM BIT_SCHEMA.Pessoa
						WHERE sobrenome = 'bruno');
EXEC BIT_SCHEMA.Procedure_Remover_Funcionario @idFuncionario;


SET @idFuncionario = (	SELECT id
						FROM BIT_SCHEMA.Pessoa
						WHERE sobrenome = 'nelias');
EXEC BIT_SCHEMA.Procedure_Remover_Funcionario @idFuncionario;


SET @idFuncionario = (	SELECT id
						FROM BIT_SCHEMA.Pessoa
						WHERE sobrenome = 'miakshay');
EXEC BIT_SCHEMA.Procedure_Remover_Funcionario @idFuncionario;


SET @idFuncionario = (	SELECT id
						FROM BIT_SCHEMA.Pessoa
						WHERE sobrenome = 'Neves');
EXEC BIT_SCHEMA.Procedure_Remover_Funcionario @idFuncionario;


SET @idFuncionario = (	SELECT id
						FROM BIT_SCHEMA.Pessoa
						WHERE sobrenome = 'Valadares');
EXEC BIT_SCHEMA.Procedure_Remover_Funcionario @idFuncionario;


SET @idFuncionario = (	SELECT id
						FROM BIT_SCHEMA.Pessoa
						WHERE sobrenome = 'Silva');
EXEC BIT_SCHEMA.Procedure_Remover_Funcionario @idFuncionario;


-- REMOVER LOJAS --

DELETE FROM BIT_SCHEMA.Loja;


-- REMOVER TENTATIVAS --

DELETE FROM BIT_SCHEMA.Tentativa;

-- REMOVER SERVICO --

DELETE FROM BIT_SCHEMA.Servico;


-- LIMPAR IDENTITY DAS TABELAS--
DBCC CHECKIDENT ('BIT_SCHEMA.Contactavel', RESEED, 0);
DBCC CHECKIDENT ('BIT_SCHEMA.Loja', RESEED, 0);
DBCC CHECKIDENT ('BIT_SCHEMA.Servico', RESEED, 0);


SELECT * FROM BIT_SCHEMA.Contactavel
SELECT * FROM BIT_SCHEMA.Pessoa
SELECT * FROM BIT_SCHEMA.Cliente
SELECT * FROM BIT_SCHEMA.EmpresaTable
SELECT * FROM BIT_SCHEMA.Estafeta
SELECT * FROM BIT_SCHEMA.ParticularTable
SELECT * FROM BIT_SCHEMA.Contacto
SELECT * FROM BIT_SCHEMA.Funcionario_Funcao
SELECT * FROM BIT_SCHEMA.Funcionario
SELECT * FROM BIT_SCHEMA.Loja
SELECT * FROM BIT_SCHEMA.Morada
SELECT * FROM BIT_SCHEMA.Tentativa