USE BIT;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Estafeta_e_Estafeta') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Estafeta_e_Estafeta;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Estafeta_Servico_Estafeta_Disponivel') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Estafeta_Servico_Estafeta_Disponivel;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Loja_Nao_Esta_Operacional_Na_Insercao') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Loja_Nao_Esta_Operacional_Na_Insercao;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Loja_Tem_Funcionarios_Para_Estar_Operacional') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Loja_Tem_Funcionarios_Para_Estar_Operacional;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Servico_Lojas_Para_Servico_Estao_Operacionais') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Servico_Lojas_Para_Servico_Estao_Operacionais;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Trigger_Servico_Tipo_E_Concelhos') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Trigger_Servico_Tipo_E_Concelhos;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Funcionario_Funcao_Apagar_Funcionario_Altera_Estado_Da_Loja') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Funcionario_Funcao_Apagar_Funcionario_Altera_Estado_Da_Loja;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Funcionario_Disponibilidade_Altera_Operacionalidade_Da_Loja') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Funcionario_Disponibilidade_Altera_Operacionalidade_Da_Loja;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Invalida_Alteracao_De_Estado') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Invalida_Alteracao_De_Estado;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Tentativa_Com_Successo_Altera_Data_De_Conclusao_De_Servico') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Tentativa_Com_Successo_Altera_Data_De_Conclusao_De_Servico;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Funcionario_Funcao_Combinacoes') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Funcionario_Funcao_Combinacoes;
	
IF OBJECT_ID('BIT_SCHEMA.Trigger_Servico_Estafeta_Associado_E_Da_Loja_De_Entrega') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Servico_Estafeta_Associado_E_Da_Loja_De_Entrega;
	
IF OBJECT_ID('BIT_SCHEMA.Trigger_Servico_Lojas_Envolvidas') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Servico_Lojas_Envolvidas;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Funcao_Proibir_Insert_Delete') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Funcao_Proibir_Insert_Delete;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Servico_No_Insert_Nao_Pode_Estar_Com_Sucesso') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Servico_No_Insert_Nao_Pode_Estar_Com_Sucesso;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Tentativa_Servico_Concluido_Ou_Expirado') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Tentativa_Servico_Concluido_Ou_Expirado;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Concluido_Ou_Expirado_Com_Informacao_Existente_Em_Tentativas') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Concluido_Ou_Expirado_Com_Informacao_Existente_Em_Tentativas;

IF OBJECT_ID('BIT_SCHEMA.Trigger_Servico_Inserido_Com_Estafeta_Fica_Em_Progresso') IS NOT NULL
	DROP TRIGGER BIT_SCHEMA.Trigger_Servico_Inserido_Com_Estafeta_Fica_Em_Progresso;










GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Estafeta_e_Estafeta
ON BIT_SCHEMA.Estafeta
AFTER INSERT	
AS 
BEGIN 

	IF EXISTS(	SELECT *
				FROM Inserted AS I
				WHERE I.id NOT IN (	SELECT FF.funcionario
									FROM BIT_SCHEMA.Funcionario_Funcao AS FF 
									WHERE FF.funcao = 'ESTAFETA'))
	BEGIN 
		RAISERROR('Todos os funcionarios devem ser Estafetas', 16, 1) 
		ROLLBACK
	END
	
END






GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Funcionario_Funcao_Apagar_Funcionario_Altera_Estado_Da_Loja
ON BIT_SCHEMA.Funcionario_Funcao
AFTER DELETE
AS 
BEGIN 
	
	DECLARE @lojasEFuncoesDasFuncoesApagadas TABLE(loja INT,
												funcao VARCHAR(15))

	INSERT INTO @lojasEFuncoesDasFuncoesApagadas	SELECT	DISTINCT L.id AS loja, FF.funcao AS funcao
													FROM	BIT_SCHEMA.Loja AS L INNER JOIN BIT_SCHEMA.Funcionario_Funcao AS FF INNER JOIN BIT_SCHEMA.Funcionario AS F
													ON		F.id = FF.funcionario
													ON		L.id = F.loja
													WHERE	F.id IN (SELECT D.funcionario FROM Deleted AS D)
																AND
															FF.funcao IN ('GESTOR','SECRET��RIO','ESTAFETA')

	UPDATE BIT_SCHEMA.Loja
	SET operacional = 0
	WHERE id IN (	SELECT L.loja
					FROM (	SELECT loja AS loja, COUNT(funcao) AS cnt
							FROM @lojasEFuncoesDasFuncoesApagadas
							GROUP BY (loja)	) AS L
					WHERE L.cnt <> 3	)

END




GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Funcionario_Disponibilidade_Altera_Operacionalidade_Da_Loja
ON BIT_SCHEMA.Funcionario
AFTER UPDATE
AS 
BEGIN 

	DECLARE @lojasEFuncoesDasFuncoesApagadas TABLE(loja INT,
													funcao VARCHAR(15))

	INSERT INTO @lojasEFuncoesDasFuncoesApagadas	SELECT	DISTINCT L.id AS loja, FF.funcao AS funcao
													FROM	BIT_SCHEMA.Loja AS L INNER JOIN BIT_SCHEMA.Funcionario_Funcao AS FF INNER JOIN BIT_SCHEMA.Funcionario AS F
													ON		F.id = FF.funcionario
													ON		L.id = F.loja
													WHERE	F.id IN (	SELECT D.id 
																		FROM Deleted AS D INNER JOIN Inserted AS I 
																		ON	 D.id = I.id 
																		WHERE D.estado <> I.estado	)
																AND
															FF.funcao IN ('GESTOR','SECRETÁRIO','ESTAFETA')
																AND
															(F.estado = 'AUSENTE' OR F.estado = 'INDISPONIVEL')

	UPDATE BIT_SCHEMA.Loja
	SET operacional = 0
	WHERE id IN (	SELECT L.loja
					FROM (	SELECT loja AS loja, COUNT(funcao) AS cnt
							FROM @lojasEFuncoesDasFuncoesApagadas
							GROUP BY (loja)	) AS L
					WHERE L.cnt <> 3	)

END





GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Loja_Nao_Esta_Operacional_Na_Insercao
ON BIT_SCHEMA.Loja
AFTER INSERT
AS 
BEGIN 
	
	IF EXISTS(	SELECT *
				FROM Inserted
				WHERE Inserted.operacional = 1)
	BEGIN
		RAISERROR('Lojas so podem estar operacionais quando têm no minimo 1 Estafeta, 1 Gestor e 1 Secreatario', 16, 1) 
		ROLLBACK
	END
	
END





GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Invalida_Alteracao_De_Estado
ON BIT_SCHEMA.Servico
AFTER UPDATE
AS 
BEGIN 
	
	IF EXISTS(	SELECT	*
				FROM	Inserted As I INNER JOIN Deleted AS D
				ON		I.id = D.id
				WHERE	(I.estado <> D.estado
							AND
						(D.estado IN ('CONCLU�DO','CANCELADO','EXPIRADO')
							OR
						(I.estado = 'AN�LISE' AND D.estado = 'EM PROGRESSO'))))
	BEGIN
		RAISERROR('Invalida altera��o de estado se Servi�o', 16, 1) 
		ROLLBACK
	END
	
END






GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Loja_Tem_Funcionarios_Para_Estar_Operacional
ON BIT_SCHEMA.Loja
AFTER UPDATE
AS 
BEGIN 

	DECLARE @lojaFuncoes TABLE(	loja INT,
								funcao VARCHAR(15));
								

	INSERT INTO @lojaFuncoes	SELECT	DISTINCT L.id, FF.funcao
								FROM	BIT_SCHEMA.Funcionario_Funcao AS FF INNER JOIN BIT_SCHEMA.Funcionario AS F INNER JOIN Inserted AS L
								ON		F.loja = L.id
								ON		FF.funcionario = F.id
								WHERE	FF.funcao IN ('ESTAFETA','SECRET�RIO','GESTOR')
											AND
										F.estado = 'DISPONIVEL'
											AND
										L.operacional = 1

										
	IF EXISTS( SELECT *
				FROM	(SELECT loja, COUNT(funcao) AS cnt
						FROM @lojaFuncoes
						GROUP BY (loja)) AS F
				WHERE F.cnt <> 3)
	BEGIN
		RAISERROR('Lojas so podem estar operacionais quando têm no minimo 1 Estafeta, 1 Gestor e 1 Secreatario', 16, 1) 
		ROLLBACK
	END

END






GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Tentativa_Com_Successo_Altera_Data_De_Conclusao_De_Servico
ON BIT_SCHEMA.Tentativa
AFTER INSERT
AS 
BEGIN 
	
	DECLARE @servicosConcluidos TABLE(	servico INT,
										dataConclusao DATE)

	INSERT INTO @servicosConcluidos	SELECT	I.servico, I.dataTentativa
									FROM	Inserted AS I
									WHERE	I.sucesso = 1
	

	UPDATE	BIT_SCHEMA.Servico
	SET		dataConclusao = (SELECT SC.dataConclusao FROM @servicosConcluidos AS SC WHERE SC.servico = id )
	WHERE	id IN (SELECT S.servico FROM @servicosConcluidos AS S)

END









GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Servico_Lojas_Para_Servico_Estao_Operacionais
ON BIT_SCHEMA.Servico
AFTER INSERT
AS 
BEGIN 
	
	IF EXISTS (	SELECT *
				FROM Inserted INNER JOIN BIT_SCHEMA.Loja AS Loja
				ON Loja.id = Inserted.lojaEntrega OR Loja.id = Inserted.lojaRecepcao
				WHERE Loja.operacional = 0)
	BEGIN
		RAISERROR('Loja seleccionada nao se encontra operacional', 16, 1) 
		ROLLBACK
	END
	
END






GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Trigger_Servico_Tipo_E_Concelhos
ON BIT_SCHEMA.Servico
AFTER INSERT, UPDATE
AS 
BEGIN 
	
	DECLARE @lojaEntregaConcelho TABLE(servico INT,
										tipo VARCHAR(50),
										distrito TINYINT,
										concelho TINYINT);

	DECLARE @lojaRecepcaoConcelho TABLE(servico INT,
										tipo VARCHAR(50),
										distrito TINYINT,
										concelho TINYINT);
	

	INSERT INTO @lojaEntregaConcelho	SELECT Moradas.servico, Moradas.tipo, CP.distrito, CP.concelho
										FROM	CTT.dbo.CodigoPostal AS CP
													INNER JOIN
												(SELECT Inserted.id AS servico, Inserted.tipo AS tipo, Loja.morada
													FROM Inserted INNER JOIN Loja
													ON Inserted.lojaEntrega = Loja.id ) AS Moradas 
													INNER JOIN 
												BIT_SCHEMA.Morada AS M
										ON M.id = Moradas.morada
										ON M.morada = CP.id

	INSERT INTO @lojaRecepcaoConcelho	SELECT Moradas.servico, Moradas.tipo, CP.distrito, CP.concelho
										FROM	CTT.dbo.CodigoPostal AS CP
													INNER JOIN
												(SELECT Inserted.id AS servico, Inserted.tipo AS tipo, Loja.morada
													FROM Inserted INNER JOIN Loja
													ON Inserted.lojaRecepcao = Loja.id ) AS Moradas 
													INNER JOIN 
												BIT_SCHEMA.Morada AS M
										ON M.id = Moradas.morada
										ON M.morada = CP.id

	DECLARE @servicosMoradas TABLE(servico INT,
									tipo VARCHAR(50),
									distritoEntrega TINYINT,
									distritoRecepcao TINYINT,
									concelhoEntrega TINYINT,
									concelhoRecepcao TINYINT);


	INSERT INTO @servicosMoradas	SELECT Entrega.servico, Entrega.tipo, Entrega.distrito, Recepcao.distrito, Entrega.concelho, Recepcao.concelho
									FROM @lojaEntregaConcelho AS Entrega INNER JOIN @lojaRecepcaoConcelho AS Recepcao
									ON Entrega.servico = Recepcao.servico


	IF EXISTS(	SELECT *
				FROM @servicosMoradas AS SM
				WHERE (	SM.tipo = 'EXPRESSO FORA' OR SM.tipo = 'URGENTE FORA') 
						AND (SM.distritoRecepcao=SM.distritoEntrega AND SM.concelhoRecepcao=SM.concelhoEntrega))
	BEGIN
		RAISERROR('EXPRESSO FORA OU URGENTE FORA sao Servicos a serem utilizados quando se realizam fora do mesmo concelho', 16, 1) 
		ROLLBACK
	END

	
	IF EXISTS(	SELECT	*
				FROM	Inserted AS I
				WHERE	I.tipo IN ('NORMAL', 'EXPRESSO', 'EXPRESSO COM RECOLHA', 'URGENTE', 'URGENTE COM RECOLHA') 
							AND
						I.lojaEntrega IS NOT NULL)
	BEGIN
		RAISERROR('Servico Normais, EXPRESSO, URGENTE, URGENTE COM RECOLHA, EXPRESSO COM RECOLHA so podem ser realizados dentro do mesmo concelho', 16, 1) 
		ROLLBACK
	END
	
END





GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Funcionario_Funcao_Combinacoes
ON BIT_SCHEMA.Funcionario_Funcao
AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON
	IF EXISTS ( SELECT C.contador
				FROM (	SELECT COUNT(FF.funcao) AS contador
						FROM BIT_SCHEMA.Funcionario_Funcao AS FF
						GROUP BY (FF.funcionario)) AS C
				WHERE C.contador > 2)
		BEGIN
			RAISERROR('N�o � poss�vel combinar mais do que duas fun��es.\n',16,1)
			ROLLBACK
			RETURN
		END	

	IF EXISTS (SELECT * 
				FROM BIT_SCHEMA.Funcionario_Funcao AS FF
				WHERE FF.funcao = 'ESTAFETA' AND
				FF.funcionario IN (	SELECT FF.funcionario 
									FROM BIT_SCHEMA.Funcionario_Funcao AS FF
									WHERE	FF.funcao = 'CONTABILISTA' 
											OR FF.funcao = 'SECRET�RIO'
											OR FF.funcao = 'GESTOR'))
		BEGIN
			RAISERROR('N�o � poss�vel combinar os tipos de fun��es: \n
					<Estafeta, Gestor>\n
					<Estafeta, Contabilista>\n
					<Estafeta, Secret�rio>\n',16,1)
			ROLLBACK
			RETURN
		END				
END
GO






GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Servico_Estafeta_Associado_E_Da_Loja_De_Entrega
ON BIT_SCHEMA.Servico
AFTER UPDATE
AS
	SET NOCOUNT ON

	IF EXISTS(	SELECT	* 
				FROM	Inserted AS I
				WHERE	I.estafeta IS NULL
							AND
						BIT_SCHEMA.Function_Diferentes_Concelhos(I.lojaEntrega, I.lojaRecepcao) = 1
							AND
						BIT_SCHEMA.Function_Estafeta_Esta_Associado_A_Loja(I.estafeta, I.lojaEntrega) = 0)
	BEGIN
		RAISERROR('Em caso de servicos para fora do concelho de recepcao, o Estafeta associado ao Servi�o tem que ser o Estafeta da Loja que realiza a entrega.',16,1)
		ROLLBACK
		RETURN
	END
GO	






GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Servico_Lojas_Envolvidas
ON BIT_SCHEMA.Servico
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON

	IF EXISTS ( SELECT	 *
				FROM	Inserted AS I
				WHERE	BIT_SCHEMA.Function_Diferentes_Concelhos(I.moradaEntrega, BIT_SCHEMA.Function_Morada_Da_Loja(I.lojaRecepcao)) = 1
							AND
						I.lojaEntrega IS NULL)
	BEGIN
		RAISERROR('Quando um servi�o � requisitado numa loja com um concelho diferente da morada de entrega, � necess�rio envolver duas lojas.',16,1)
		ROLLBACK
		RETURN
	END

	IF EXISTS (SELECT	*
				FROM 	Inserted AS I 
				WHERE	BIT_SCHEMA.Function_Diferentes_Concelhos(I.moradaEntrega, BIT_SCHEMA.Function_Morada_Da_Loja(I.lojaRecepcao)) = 1
							AND 
						I.lojaEntrega IS NOT NULL
							AND
						BIT_SCHEMA.Function_Diferentes_Concelhos(I.moradaEntrega, BIT_SCHEMA.Function_Morada_Da_Loja(I.lojaentrega)) = 1)
	BEGIN
		RAISERROR('Loja de entrega deve ter o mesmo concelho que a morada de entrega.',16,1)
		ROLLBACK
		RETURN
	END

END
GO





GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Funcao_Proibir_Insert_Delete ON BIT_SCHEMA.Funcao
INSTEAD OF INSERT, DELETE
AS
BEGIN
	RAISERROR('N�o � possivel inserir nem remover Fun��es.',16,1)
	ROLLBACK
END






GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Servico_No_Insert_Nao_Pode_Estar_Com_Sucesso ON BIT_SCHEMA.Servico
AFTER UPDATE
AS
BEGIN
	
	UPDATE BIT_SCHEMA.Servico 
	SET estado = 'EM PROGRESSO'
	WHERE id IN (SELECT I.id
				FROM Inserted AS I INNER JOIN Deleted AS D
				ON I.estafeta <> D.estafeta
				WHERE D.estafeta IS NULL AND I.estafeta IS NOT NULL)

END 
GO 






GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Tentativa_Servico_Concluido_Ou_Expirado ON BIT_SCHEMA.Tentativa
AFTER INSERT
AS
BEGIN
	
	IF EXISTS (SELECT * FROM Inserted WHERE sucesso = 1)
		BEGIN
			UPDATE BIT_SCHEMA.Servico SET estado = 'CONCLU�DO' WHERE BIT_SCHEMA.Servico.id IN (SELECT id FROM Inserted WHERE sucesso = 1)
		END

	ElSE IF EXISTS (SELECT * FROM BIT_SCHEMA.Function_Tentativas_Falhadas() WHERE insucessos = 3)
		BEGIN
			UPDATE BIT_SCHEMA.Servico 
			SET estado = 'CANCELADO' 
			WHERE BIT_SCHEMA.Servico.id IN (SELECT TF.servico 
											FROM BIT_SCHEMA.Function_Tentativas_Falhadas() as TF
											WHERE TF.insucessos = 3)
		END
END
GO






GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Concluido_Ou_Expirado_Com_Informacao_Existente_Em_Tentativas ON BIT_SCHEMA.Servico
AFTER UPDATE
AS
BEGIN

	IF EXISTS(	SELECT *
				FROM Inserted AS I
				WHERE I.estado = 'CONCLU�DO' AND I.id NOT IN 	(SELECT T.servico
																FROM BIT_SCHEMA.Tentativa AS T
																WHERE T.sucesso = 1))
	BEGIN
		RAISERROR('N�o � possivel colocar servico como concluido',16,1)
		ROLLBACK
	END

	IF EXISTS(	SELECT *
				FROM Inserted AS I
				WHERE I.estado = 'EXPIRADO' AND I.id NOT IN (	SELECT id 
																FROM BIT_SCHEMA.Function_Tentativas_Falhadas()
																WHERE insucessos = 3))
	BEGIN
		RAISERROR('N�o � possivel colocar servico como Expirado sem realizar 3 tentativas',16,1)
		ROLLBACK
	END

END
GO

GO
CREATE TRIGGER BIT_SCHEMA.Trigger_Servico_Inserido_Com_Estafeta_Fica_Em_Progresso ON BIT_SCHEMA.Servico
AFTER INSERT
AS
BEGIN
	
	UPDATE BIT_SCHEMA.Servico
	SET estado = 'EM PROGRESSO'
	WHERE id IN (	SELECT I.id
					FROM INSERTED AS I 
					WHERE I.estado = 'AN�LISE' AND estafeta<>NULL)

END
GO
