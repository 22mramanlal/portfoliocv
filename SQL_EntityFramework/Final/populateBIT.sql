USE BIT;

-- INSERIR EMPRESAS --
DECLARE @idMorada INT;
EXEC BIT_SCHEMA.Procedure_Inserir_Morada @idMorada OUTPUT,  '', '', 'Lisboa', 'Odivelas', 'Fam�es', 'Fam�es',
										 'Rua', 'Major', 'Jo�o Lu�s de Moura', 1685, 253;

EXEC BIT_SCHEMA.Procedure_Inserir_Empresa	'Hypromat Portugal, S.A. (Elefante Azul)', 214787660, 1685253, @idMorada, 'Mar�a';


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '6', 'ESQ', 'Lisboa', 'Lisboa', 'Nossa Senhora de F�tima', 
											'Lisboa', 'Avenida', '', 'Rep�blica', 1050, 191;

EXEC BIT_SCHEMA.Procedure_Inserir_Empresa	'Sysnetic - Core Mobility Solutions, Unipessoal, Lda.', 217604400,
											 1050191, @idMorada, 'Joana'


-- ACTUALIZAR EMPRESSAS --
UPDATE BIT_SCHEMA.Empresa SET interfacerId = 'AAA' WHERE id = 1;

UPDATE BIT_SCHEMA.Empresa SET nome = 'New Name Empresa' WHERE id = 2;


-- INSERIR PARTICULAR --

EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '7', 'CV/ESQ', 'Lisboa', 'Odivelas', 'Odivelas', 'Odivelas', 
											'Avenida', 'Dom', 'Dinis', 2675, 331;

EXEC BIT_SCHEMA.Procedure_Inserir_Particular	'Bart', 219222333, 12345678, @idMorada, 'bartinho', 'bart@gmail.com'



EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '3', '1�/DTO', 'Lisboa', 'Lisboa', 'Alc�ntara', 'Lisboa', 
											'Rua', '', 'Lu�s de Cam�es', 1300, 355;

EXEC BIT_SCHEMA.Procedure_Inserir_Particular	'Alice', 219444555, 87654321, @idMorada, 'alice', 'alice@gmail.com'



-- ACTUALIZAR PARTICULAR --
UPDATE BIT_SCHEMA.Pessoa SET sobrenome = 'bartin' WHERE email = 'bart@gmail.com';

UPDATE BIT_SCHEMA.Contacto SET nTelefone = 219666777 WHERE id = (	SELECT id 
																	FROM BIT_SCHEMA.Contactavel 
																	WHERE nome = 'Alice');


-- INSERIR LOJA --
EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, 12, 'R/C', 'Lisboa', 'Lisboa', 'Alc�ntara', 'Lisboa', 'Cal�ada', 
											'', 'Boa-Hora', 1300, 999;
INSERT INTO BIT_SCHEMA.Loja (designacao, morada) VALUES ('Esta��o de Correios JUNQUEIRA (LISBOA)', @idMorada);


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, 24, '', 'Lisboa', 'Odivelas', 'Odivelas', 'Odivelas', 
											'Rua', NULL, 'Esp�rito Santo', 2675, 999;
INSERT INTO BIT_SCHEMA.Loja (designacao, morada) VALUES ('Esta��o de Correios ODIVELAS', @idMorada);


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, 12, '', 'Lisboa', 'Lisboa', 'Alc�ntara', 'Lisboa', 
											'Rua', '', 'Jo�o de Barros', 1300, 319;
INSERT INTO BIT_SCHEMA.Loja (designacao, morada) VALUES ('Esta��o de Correios Alc�ntara', @idMorada);


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, 12, '', 'Lisboa', 'Oeiras', 'Carnaxide', 'Carnaxide', 
											'Rua', '', 'Fraternidade Oper�ria', 2794, 90;
INSERT INTO BIT_SCHEMA.Loja (designacao, morada) VALUES ('Esta��o de Correios Carnaxide', @idMorada);


-- INSERIR EMPREGADO --

DECLARE @idLoja1 INT = (SELECT id 
						FROM BIT_SCHEMA.Loja 
						WHERE designacao = 'Esta��o de Correios JUNQUEIRA (LISBOA)');

DECLARE @idLoja2 INT = (SELECT id 
						FROM BIT_SCHEMA.Loja 
						WHERE designacao = 'Esta��o de Correios ODIVELAS');

DECLARE @idLoja3 INT = (SELECT id 
						FROM BIT_SCHEMA.Loja 
						WHERE designacao = 'Esta��o de Correios Alc�ntara');

EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '8', '8� DTO', 'Lisboa', 'Lisboa', 'Alc�ntara', 'Lisboa', 
											'Cal�ada', NULL, 'Boa-Hora', 1300, 95;

EXEC BIT_SCHEMA.Procedure_Inserir_Funcionario	'Andre', 969696969, @idMorada, 'bebito', 'andre1@gmail.com', 'DISPONIVEL', 
												@idLoja1, 'ESTAFETA'


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '173', '2� DTO', 'Lisboa', 'Lisboa', 'Penha de Fran�a', 
											'Lisboa', 'Largo', NULL, 'Penha de Fran�a', 1170, 298;

EXEC BIT_SCHEMA.Procedure_Inserir_Funcionario	'Miguel', 969797979, @idMorada, 'bubu', 'miguel@gmail.com', 'DISPONIVEL', 
												@idLoja1, 'GESTOR'


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '9', 'RC/ESQ', 'Lisboa', 'Odivelas', 'Odivelas', 
											'Odivelas', 'Avenida', 'Dom', 'Dinis', 2675, 331;

EXEC BIT_SCHEMA.Procedure_Inserir_Funcionario	'Calmen', 969898989, @idMorada, 'mimimi', 'cal@gmail.com', 'INDISPONIVEL', 
												@idLoja1, 'SECRET�RIO'


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '20', '2� ESQ', 'Lisboa', 'Lisboa', 'Alc�ntara', 'Lisboa', 
											'Cal�ada', NULL, 'Boa-Hora', 1300, 95;

EXEC BIT_SCHEMA.Procedure_Inserir_Funcionario	'Akshay', 969191919, @idMorada, 'akshay', 'akshay@gmail.com', 'DISPONIVEL', 
												@idLoja2, 'SECRET�RIO'


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '173', '7� DTO', 'Lisboa', 'Lisboa', 'Penha de Fran�a', 
											'Lisboa', 'Largo', NULL, 'Penha de Fran�a', 1170, 298;

EXEC BIT_SCHEMA.Procedure_Inserir_Funcionario	'Bruno', 969292929, @idMorada, 'bruno', 'bruno@gmail.com', 'DISPONIVEL', 
												@idLoja2, 'GESTOR'


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '11', '2�/DTO', 'Lisboa', 'Odivelas', 'Odivelas', 
											'Odivelas', 'Avenida', 'Dom', 'Dinis', 2675, 331;

EXEC BIT_SCHEMA.Procedure_Inserir_Funcionario	'Nelias', 969393939, @idMorada, 'nelias', 'Nelias@gmail.com', 'DISPONIVEL', 
												@idLoja2, 'ESTAFETA'


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '9', '1�/DTO', 'Lisboa', 'Odivelas', 'Odivelas', 
											'Odivelas', 'Avenida', 'Dom', 'Dinis', 2675, 331;

EXEC BIT_SCHEMA.Procedure_Inserir_Funcionario	'Miguel Akshay', 969393935, @idMorada, 'miakshay', 'miakshay@gmail.com', 'DISPONIVEL', 
												@idLoja2, 'ESTAFETA'
												

EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '12', '2� DTO', 'Lisboa', 'Lisboa', 'Alc�ntara', 'Lisboa', 
											'Cal�ada', NULL, 'Boa-Hora', 1300, 95;

EXEC BIT_SCHEMA.Procedure_Inserir_Funcionario	'Jose', 969688969, @idMorada, 'Neves', 'jneves@gmail.com', 'DISPONIVEL', 
												@idLoja3, 'ESTAFETA'


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '111', '5� DTO', 'Lisboa', 'Lisboa', 'Penha de Fran�a', 
											'Lisboa', 'Largo', NULL, 'Penha de Fran�a', 1170, 298;

EXEC BIT_SCHEMA.Procedure_Inserir_Funcionario	'Quichane', 969567979, @idMorada, 'Valadares', 'quich@gmail.com', 'DISPONIVEL', 
												@idLoja3, 'GESTOR'


EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '9', 'RC/ESQ', 'Lisboa', 'Odivelas', 'Odivelas', 
											'Odivelas', 'Avenida', 'Dom', 'Dinis', 2675, 331;

EXEC BIT_SCHEMA.Procedure_Inserir_Funcionario	'Fatima', 969778989, @idMorada, 'Silva', 'silvinha@gmail.com', 'DISPONIVEL', 
												@idLoja3, 'SECRET�RIO'



-- ACTUALIZAR EMPREGADO --
UPDATE BIT_SCHEMA.Empregado SET estado = 'DISPONIVEL' WHERE id = (	SELECT id 
																	FROM BIT_SCHEMA.Empregado 
																	WHERE nome = 'Calmen');

UPDATE BIT_SCHEMA.Empregado SET email = 'andre@gmail.com' WHERE id = (	SELECT id 
																	FROM BIT_SCHEMA.Empregado 
																	WHERE email = 'andre1@gmail.com') ;

INSERT INTO BIT_SCHEMA.Funcionario_Funcao(funcao, funcionario) 
VALUES ('CONTABILISTA', (	SELECT id 
							FROM BIT_SCHEMA.Empregado 
							WHERE nome = 'Akshay'));


-- ACTUALIZAR LOJA --

UPDATE BIT_SCHEMA.Loja SET operacional = 1 WHERE id = @idLoja1;

UPDATE BIT_SCHEMA.Loja SET operacional = 1 WHERE id = @idLoja2;

UPDATE BIT_SCHEMA.Loja SET operacional = 1 WHERE id = @idLoja3;

-- INSERIR SERVICO --
DECLARE @date date;
DECLARE @idServico1 INT, @idServico2 INT, @idServico3 INT, @idServico4 INT, @idServico5 INT, @idServico6 INT;


/* SERVICO 1 */
EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '3', '1�/DTO', 'Lisboa', 'Lisboa', 'Alc�ntara', 'Lisboa', 
											'Rua', NULL, 'Lu�s de Cam�es', 1300, 355;

DECLARE @idMoradaFaturacao INT, @idMoradaEntrega INT;
EXEC BIT_SCHEMA.Procedure_Inserir_Morada	@idMorada OUTPUT, '', '', 'Lisboa', 'Odivelas', 'Fam�es', 'Fam�es', 
											'Rua', 'Major', 'Jo�o Lu�s de Moura', 1685, 253;
SET @idMoradaEntrega = (SELECT C.morada
						FROM BIT_SCHEMA.Contactavel AS C
						WHERE nome = 'Alice');
set @date = '2014-01-25 12:35:00';

EXEC BIT_SCHEMA.Procedure_Inserir_Servico	'URGENTE FORA', @idMoradaEntrega, @idMoradaFaturacao, @date, 
											'AN�LISE', 1685253, @idLoja1, @idLoja2, @idServico1 OUTPUT

DECLARE @idEstafeta INT;
SET @idEstafeta = (SELECT id FROM BIT_SCHEMA.Empregado WHERE nome = 'Nelias')
UPDATE BIT_SCHEMA.Servico SET estafeta = @idEstafeta WHERE id = @idServico1



/* SERVICO 2 */
SET @idMoradaEntrega = (SELECT C.morada
						FROM BIT_SCHEMA.Contactavel AS C
						WHERE nome = 'Bart');
SET @idMoradaFaturacao = (SELECT C.morada
						FROM BIT_SCHEMA.Contactavel AS C
						WHERE nome = 'Alice');
set @date = '2014-05-12 12:35:00';

EXEC BIT_SCHEMA.Procedure_Inserir_Servico	'URGENTE FORA', @idMoradaEntrega, @idMoradaFaturacao, @date, 
											'AN�LISE', 87654321, @idLoja1, @idLoja2, @idServico2 OUTPUT

SET @idEstafeta = (SELECT id FROM BIT_SCHEMA.Empregado WHERE nome = 'Nelias')
UPDATE BIT_SCHEMA.Servico SET estafeta = @idEstafeta WHERE id = @idServico2


/* SERVICO 3 */
SET @idMoradaEntrega = (SELECT C.morada
						FROM BIT_SCHEMA.Contactavel AS C
						WHERE nome = 'Hypromat Portugal, S.A. (Elefante Azul)');

EXEC BIT_SCHEMA.Procedure_Inserir_Servico	'EXPRESSO FORA', @idMoradaEntrega, NULL, @date, 
											'AN�LISE', 1685253, @idLoja2, @idLoja1, @idServico3 OUTPUT

SET @idEstafeta = (SELECT id FROM BIT_SCHEMA.Empregado WHERE nome = 'Andre')
UPDATE BIT_SCHEMA.Servico SET estafeta = @idEstafeta WHERE id = @idServico3


/* SERVICO 4 */
SET @idMoradaEntrega = (SELECT C.morada
						FROM BIT_SCHEMA.Contactavel AS C
						WHERE nome = 'New Name Empresa');

SET @idMoradaFaturacao = (SELECT C.morada
						FROM BIT_SCHEMA.Contactavel AS C
						WHERE nome = 'Hypromat Portugal, S.A. (Elefante Azul)');

EXEC BIT_SCHEMA.Procedure_Inserir_Servico	'URGENTE FORA', @idMoradaEntrega, @idMoradaFaturacao, @date, 
											'AN�LISE', 1050191, @idLoja1, @idLoja2, @idServico4 OUTPUT

SET @idEstafeta = (SELECT id FROM BIT_SCHEMA.Empregado WHERE nome = 'Nelias')
UPDATE BIT_SCHEMA.Servico SET estafeta = @idEstafeta WHERE id = @idServico4

/* SERVICO 5 */
SET @idMoradaEntrega = (SELECT C.morada
						FROM BIT_SCHEMA.Contactavel AS C
						WHERE nome = 'Hypromat Portugal, S.A. (Elefante Azul)');

EXEC BIT_SCHEMA.Procedure_Inserir_Servico	'URGENTE FORA', @idMoradaEntrega, NULL, @date, 
											'AN�LISE', 1685253, @idLoja2, @idLoja1, @idServico5 OUTPUT


/* SERVICO 6 */
SET @idMoradaEntrega = (SELECT C.morada
						FROM BIT_SCHEMA.Contactavel AS C
						WHERE nome = 'Hypromat Portugal, S.A. (Elefante Azul)');

EXEC BIT_SCHEMA.Procedure_Inserir_Servico	'URGENTE FORA', @idMoradaEntrega, NULL, @date, 
											'AN�LISE', 1685253, @idLoja3, @idLoja2, @idServico6 OUTPUT


SET @idEstafeta = (SELECT id FROM BIT_SCHEMA.Empregado WHERE nome = 'Jose')
UPDATE BIT_SCHEMA.Servico SET estafeta = @idEstafeta WHERE id = @idServico6

-- INSERIR TENTAIVA --
	--Servico1 Concluido
set @date = '2014-01-25 16:25:00';
INSERT INTO BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao, sucesso) Values(1, @idServico1, @date,'Concluido' ,1);

	--Servico2 1 Tentativa
set @date = '2014-01-25 13:05:00';
INSERT INTO BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao, sucesso) Values(1, @idServico2, @date,'N�o Presente', 0);


	--Servico3 Cancelado
set @date = '2014-01-25 08:25:00';
INSERT INTO BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao, sucesso) Values(1, @idServico3, @date,'N�o Presente', 0);
set @date = '2014-01-25 09:25:00';
INSERT INTO BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao, sucesso) Values(2, @idServico3, @date,'N�o Presente', 0);
set @date = '2014-01-25 12:25:00';
INSERT INTO BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao, sucesso) Values(3, @idServico3, @date,'N�o Presente e Cancelado', 0);

	--Servico4 
set @date = '2014-01-25 14:25:00';
INSERT INTO BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao, sucesso) Values(1, @idServico4, @date,'N�o Presente', 0);
set @date = '2014-01-25 16:55:00';
INSERT INTO BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao, sucesso) Values(2, @idServico4, @date,'N�o Presente', 0);

	--Serico 5 Sem Estafeta

	--Servico 6
	set @date = '2014-01-25 14:55:00';
INSERT INTO BIT_SCHEMA.Tentativa (id, servico, dataTentativa, descricao, sucesso) Values(1, @idServico6, @date,'Concluido', 1);

SELECT * FROM BIT_SCHEMA.Contactavel
SELECT * FROM BIT_SCHEMA.Pessoa
SELECT * FROM BIT_SCHEMA.Cliente
SELECT * FROM BIT_SCHEMA.EmpresaTable
SELECT * FROM BIT_SCHEMA.Estafeta
SELECT * FROM BIT_SCHEMA.ParticularTable
SELECT * FROM BIT_SCHEMA.Contacto
SELECT * FROM BIT_SCHEMA.Funcionario_Funcao
SELECT * FROM BIT_SCHEMA.Funcionario
SELECT * FROM BIT_SCHEMA.Loja
SELECT * FROM BIT_SCHEMA.Morada
