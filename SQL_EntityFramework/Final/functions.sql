USE BIT;

IF OBJECT_ID('BIT_SCHEMA.Function_Concelho_De_Morada') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Concelho_De_Morada;

IF OBJECT_ID('BIT_SCHEMA.Function_Distrito_De_Morada') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Distrito_De_Morada;
	
IF OBJECT_ID('BIT_SCHEMA.Function_Estafeta_Esta_Associado_A_Loja') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Estafeta_Esta_Associado_A_Loja;

IF OBJECT_ID('BIT_SCHEMA.Function_Diferentes_Concelhos') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Diferentes_Concelhos;
	
IF OBJECT_ID('BIT_SCHEMA.Function_Morada_Da_Loja') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Morada_Da_Loja;

IF OBJECT_ID('BIT_SCHEMA.Function_Tentativas_Falhadas') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Tentativas_Falhadas;

IF OBJECT_ID('BIT_SCHEMA.Function_Servico_Foi_Entregue_Fora_Do_Prazo') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Servico_Foi_Entregue_Fora_Do_Prazo;

IF OBJECT_ID('BIT_SCHEMA.Function_Estafetas_Disponiveis') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Estafetas_Disponiveis;

IF OBJECT_ID('BIT_SCHEMA.Function_Estafetas_Disponiveis_Para_Aceitar_Servicos') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Estafetas_Disponiveis_Para_Aceitar_Servicos;

IF OBJECT_ID('BIT_SCHEMA.Function_Morada_obterId') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Morada_obterId;

IF OBJECT_ID('BIT_SCHEMA.Function_CodigoPostalView_obterId') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_CodigoPostalView_obterId;

IF OBJECT_ID('BIT_SCHEMA.Function_Servicos_Disponiveis_Para_Loja') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Servicos_Disponiveis_Para_Loja;

IF OBJECT_ID('BIT_SCHEMA.Function_Servicos_Da_Loja_Do_Tipo') IS NOT NULL
	DROP FUNCTION BIT_SCHEMA.Function_Servicos_Da_Loja_Do_Tipo;

	



	


GO
CREATE FUNCTION BIT_SCHEMA.Function_Concelho_De_Morada(@id INT)
RETURNS INT
AS
BEGIN
	
	RETURN (SELECT TOP 1 CP.concelho
			FROM CTT.dbo.CodigoPostal AS CP
			WHERE CP.id = (	SELECT M.morada 
							FROM BIT_SCHEMA.Morada AS M 
							WHERE M.id = @id));

END
GO






GO
CREATE FUNCTION BIT_SCHEMA.Function_Distrito_De_Morada(@id INT)
RETURNS INT
AS
BEGIN

	RETURN (SELECT TOP 1 CP.distrito
			FROM CTT.dbo.CodigoPostal AS CP
			WHERE CP.id = (	SELECT M.morada 
							FROM BIT_SCHEMA.Morada AS M 
							WHERE M.id = @id));

END
GO






GO
CREATE FUNCTION BIT_SCHEMA.Function_Diferentes_Concelhos(@entrega INT, @recepcao INT)
RETURNS INT
AS
BEGIN
	IF(@entrega = @recepcao OR @entrega IS NULL)
		RETURN 0;
	
	DECLARE @concelhoEntrega INT = BIT_SCHEMA.Function_Concelho_De_Morada(@entrega);
	DECLARE @concelhoRecepcao INT = BIT_SCHEMA.Function_Concelho_De_Morada(@recepcao);

	DECLARE @distritoEntrega INT = BIT_SCHEMA.Function_Distrito_De_Morada(@entrega);
	DECLARE @distritoRecepcao INT = BIT_SCHEMA.Function_Distrito_De_Morada(@recepcao);

	IF (@distritoRecepcao <> @distritoEntrega OR @concelhoEntrega <> @concelhoRecepcao)
		RETURN 1;

	RETURN 0;
END

GO






GO
CREATE FUNCTION BIT_SCHEMA.function_Estafeta_Esta_Associado_A_Loja(@estafetaId INT, @lojaRecepcaoId INT)
RETURNS INT
AS
BEGIN
		
	IF EXISTS (	SELECT *
				FROM BIT_SCHEMA.Funcionario AS F
				WHERE F.id = @estafetaId AND F.loja = @lojaRecepcaoId)
	RETURN 1;
	
	RETURN 0;
END

GO






GO

CREATE FUNCTION BIT_SCHEMA.Function_Morada_Da_Loja(@loja INT)
RETURNS INT
AS
BEGIN
		BEGIN
		RETURN (SELECT L.morada
				FROM  BIT_SCHEMA.Loja as L
				WHERE L.id = @loja);
		END
RETURN 0;
END	
GO





GO
CREATE FUNCTION BIT_SCHEMA.Function_Tentativas_Falhadas ()
RETURNS @servicos TABLE (servico INT,
						insucessos INT)
AS
BEGIN

	INSERT INTO @servicos	SELECT T.servico, COUNT(T.id)
							FROM BIT_SCHEMA.Tentativa AS T 
							WHERE T.sucesso = 0 
							GROUP BY T.servico
	RETURN

END





GO
CREATE FUNCTION BIT_SCHEMA.Function_Servico_Foi_Entregue_Fora_Do_Prazo (@servico INT)
RETURNS INT
AS 
BEGIN
	
	DECLARE @tentativa		TINYINT		= (	SELECT T.id 
											FROM BIT_SCHEMA.Tentativa AS T 
											WHERE T.servico = @servico AND T.sucesso = 1)

	DECLARE @tipo			VARCHAR(50) = (SELECT tipo FROM BIT_SCHEMA.Servico WHERE id = @servico)
	DECLARE @dataRequisicao DATE		= (SELECT S.dataRequisicao FROM BIT_SCHEMA.Servico AS S WHERE S.id = @servico);
	DECLARE @dataConclusao	DATE		= (SELECT S.dataConclusao FROM BIT_SCHEMA.Servico AS S WHERE S.id  = @servico);

	DECLARE @diferencaHoras INT			= DATEDIFF(HOUR, @dataRequisicao, @dataConclusao);
	DECLARE @diferencaDias	INT			= DATEDIFF(DAY, @dataRequisicao, @dataConclusao);
	DECLARE @horaRequisicao INT			= DATEPART(HOUR, @dataRequisicao);
	DECLARE @horaConclusao	INT			= DATEPART(HOUR, @dataConclusao);


	IF(@tipo IN ('URGENTE', 'URGENTE FORA', 'URGENTE COM RECOLHA'))
	BEGIN
		IF( @horaRequisicao <= 18)
		BEGIN
			IF(@diferencaDias != 0)
				RETURN 1
		END
		ELSE
		BEGIN
			IF((@diferencaDias = 1 AND @horaConclusao > 12) OR @diferencaDias > 1)
				RETURN 1
		END
	END
 
	IF(@tipo IN ('EXPRESSO', 'EXPRESSO FORA', 'EXPRESSO COM RECOLHA') AND  @diferencaHoras>24)
		RETURN 1
	
	IF(@tipo = 'NORMAL' AND @diferencaHoras > 48)
		RETURN 1
	
	RETURN 0

END





GO
CREATE FUNCTION BIT_SCHEMA.Function_Estafetas_Disponiveis()
RETURNS @estafetasContagem TABLE(	estafeta INT,
									loja SMALLINT,
									nServicos INT)
AS 
BEGIN

	DECLARE @dataActual SMALLDATETIME = (SELECT CAST(GETDATE() AS SMALLDATETIME))
	DECLARE @diaActual INT = DATEPART(DAY, @dataActual)
	DECLARE @mesActual INT = DATEPART(MONTH, @dataActual)
	DECLARE @anoActual INT = DATEPART(YEAR, @dataActual)	



	DECLARE @estafetasServicos TABLE(	estafeta INT,
										loja SMALLINT,
										servico INT)

	INSERT INTO @estafetasServicos	SELECT	F.id, F.loja, S.id
									FROM	BIT_SCHEMA.Servico AS S INNER JOIN BIT_SCHEMA.Funcionario AS F INNER JOIN BIT_SCHEMA.Funcionario_Funcao AS FF
									ON		FF.funcionario = F.id
									ON		S.estafeta = F.id
									WHERE	FF.funcao = 'ESTAFETA'	
												AND
											F.estado = 'DISPONIVEL'
												AND
											(S.estado = 'EM PROGRESSO'
												OR
											(S.estado IN ('CONCLU�DO','EXPIRADO') 
												AND 
											DATEPART(DAY, S.dataConclusao) = @diaActual 
												AND	
											DATEPART(MONTH, S.dataConclusao) = @mesActual 
												AND 
											DATEPART(YEAR, S.dataConclusao) = @anoActual))
				
	INSERT INTO @estafetasContagem	SELECT ES.estafeta, ES.loja, COUNT(ES.servico)
									FROM @estafetasServicos AS ES
									GROUP BY ES.estafeta, ES.loja
									UNION 
									SELECT Func.id, Func.loja, 0
									FROM BIT_SCHEMA.Funcionario as Func INNER JOIN BIT_SCHEMA.Estafeta as Est
									ON 	Func.id = Est.id
									WHERE Func.id NOT IN (SELECT ESer.estafeta FROM @estafetasServicos as ESer)
	RETURN 

END




GO
CREATE FUNCTION BIT_SCHEMA.Function_Estafetas_Disponiveis_Para_Aceitar_Servicos()
RETURNS @estafetasDisponiveis TABLE (estafeta INT PRIMARY KEY,
									 loja SMALLINT,
									 contagemServicos INT)
AS
BEGIN

	INSERT INTO @estafetasDisponiveis	SELECT FFD.estafeta, FFD.loja, FFD.nServicos 
										FROM BIT_SCHEMA.Function_Funcionarios_Disponiveis() AS FFD
										WHERE FFD.nServicos < 10
	
	RETURN
END





GO
CREATE FUNCTION BIT_SCHEMA.Function_CodigoPostalView_obterId(	@distrito VARCHAR(50), @concelho VARCHAR(50), 
																@freguesia VARCHAR(50), @localidade VARCHAR(100), 
																@arteriaTipo VARCHAR(100), @arteriaTitulo VARCHAR(100) = NULL, 
																@arteriaDesignacao VARCHAR(100), @nPorta VARCHAR(10), 
																@codPostal INT, @extCodPostal INT)
RETURNS INT
AS
	BEGIN
		DECLARE @id INT;
		SELECT @id = (	SELECT TOP 1 CPV.id 
						FROM BIT_SCHEMA.CodigoPostalView AS CPV
						WHERE	CPV.distrito = @distrito AND CPV.concelho = @concelho AND
								CPV.freguesia = @freguesia AND CPV.localidade = @localidade AND
								CPV.arteriaTipo = @arteriaTipo AND CPV.arteriaDesignacao = @arteriaDesignacao 
								AND CPV.codigoPostal = @codPostal AND CPV.extensaoCodPostal = @extCodPostal
						ORDER BY CPV.id ASC);
		RETURN @id;
	END

GO






GO
CREATE FUNCTION BIT_SCHEMA.Function_Morada_obterId(@idCodPostalView INT, @nPorta VARCHAR(10), @andar VARCHAR(10))
RETURNS INT
AS
	BEGIN
		DECLARE @id INT;
		SELECT @id = (SELECT M.id 
						FROM BIT_SCHEMA.Morada AS M
						WHERE	M.morada = @idCodPostalView AND M.nPorta = @nPorta AND M.andar = @andar);
		RETURN @id;
	END

GO




GO
 CREATE FUNCTION BIT_SCHEMA.Function_Servicos_Da_Loja_Do_Tipo(@loja SMALLINT, @tipo VARCHAR(15))
RETURNS INT
AS
BEGIN

	DECLARE @dataActual SMALLDATETIME = (SELECT CAST(GETDATE() AS SMALLDATETIME))
	DECLARE @diaActual INT = DATEPART(DAY, @dataActual)
	DECLARE @mesActual INT = DATEPART(MONTH, @dataActual)
	DECLARE @anoActual INT = DATEPART(YEAR, @dataActual)	
	
	RETURN (SELECT	COUNT(S.id)
			FROM	BIT_SCHEMA.Servico AS S INNER JOIN BIT_SCHEMA.Funcionario AS F
			ON		S.estafeta = F.id
			WHERE	S.lojaEntrega = @loja OR (S.lojaRecepcao = @loja AND S.lojaEntrega IS NULL)
						AND
					S.tipo = @tipo
						AND
					(S.estado IN ('AN�LISE','EM PROGRESSO')
						OR
					(S.estado IN ('EXPIRADO','CONCLU�DO')		AND 
														DATEPART(DAY, S.dataConclusao) = @diaActual 
																AND	
														DATEPART(MONTH, S.dataConclusao) = @mesActual 
																AND 
														DATEPART(YEAR, S.dataConclusao) = @anoActual)	)
						AND
					F.estado = 'DISPONIVEL')

END
GO









GO
CREATE FUNCTION BIT_SCHEMA.Function_Servicos_Disponiveis_Para_Loja(@loja SMALLINT)
RETURNS @servicosDisponiveis TABLE(tipo VARCHAR(15),
									cnt INT)
AS
BEGIN
	
	DECLARE @funcionariosDaLoja INT = (	SELECT	COUNT(F.id)
										FROM	BIT_SCHEMA.Funcionario AS F
										WHERE	F.id IN (SELECT E.id FROM BIT_SCHEMA.Estafeta AS E)
													AND
												F.loja = @loja
													AND
												F.estado = 'DISPONIVEL')

	INSERT INTO @servicosDisponiveis VALUES ('URGENTE', ((10*@funcionariosDaLoja)-BIT_SCHEMA.Function_Servicos_Da_Loja_Do_Tipo(@loja, 'URGENTE')))
	INSERT INTO @servicosDisponiveis VALUES ('EXPRESSO', ((2*10*@funcionariosDaLoja)-BIT_SCHEMA.Function_Servicos_Da_Loja_Do_Tipo(@loja, 'EXPRESSO')))
	INSERT INTO @servicosDisponiveis VALUES ('NORMAL', ((3*10*@funcionariosDaLoja)-BIT_SCHEMA.Function_Servicos_Da_Loja_Do_Tipo(@loja, 'NORMAL')))

	RETURN
END
GO
