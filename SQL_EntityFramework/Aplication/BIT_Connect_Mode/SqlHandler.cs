﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIT_Connect_Mode {

    public class SqlHandler {

        private static readonly String ID_PARAMETER = "@inputId";
        
        public bool ExistID(string tableName, int inputId) {
            string query = String.Format(SqlUtils.GET_ID, tableName);
            Dictionary<int, object[]> tableIds = GetById(query, inputId);
            return ( tableIds.Count != 0 && tableIds[1].Length != 0) ?
                true : false;
        }

        public Dictionary<int, object[]> GetById(string query, int inputId) {
            
            using(SqlConnection conn = new SqlConnection(SqlUtils.CONNECTION_STR)) {
                conn.Open();
                
                using (SqlCommand cmd = conn.CreateCommand()) {
                    cmd.CommandText = query;
                    cmd.Transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);

                    cmd.Parameters.Add(new SqlParameter(ID_PARAMETER, SqlDbType.Int));
                    cmd.Parameters[ID_PARAMETER].Value = inputId;

                    using (SqlDataReader dReader = cmd.ExecuteReader()) {
                       return SqlUtils.GetResults(dReader);
                    }
                }
            }
        }

        public void Update(string query, int inputId, int serviceId) {

            using (SqlConnection conn = new SqlConnection(SqlUtils.CONNECTION_STR)) {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand()) {
                    cmd.CommandText = query;
                    cmd.Transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);

                    cmd.Parameters.Add(new SqlParameter(ID_PARAMETER, SqlDbType.Int));
                    cmd.Parameters.Add(new SqlParameter("@serviceId", SqlDbType.Int));
                    cmd.Parameters[ID_PARAMETER].Value = inputId;
                    cmd.Parameters["@serviceId"].Value = serviceId;

                    using (SqlDataReader dReader = cmd.ExecuteReader()) {
                    }
                }
            }
        }

        public Dictionary<int, object[]> Get(string query) {

            using (SqlConnection conn = new SqlConnection(SqlUtils.CONNECTION_STR)) {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand()) {
                    cmd.CommandText = query;
                    cmd.Transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);

                    using (SqlDataReader dReader = cmd.ExecuteReader()) {
                        return SqlUtils.GetResults(dReader);
                    }
                }
            }
        }

        public int DeleteByID(string query, int inputId){

            using (SqlConnection conn = new SqlConnection(SqlUtils.CONNECTION_STR)) {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand()) {
                    cmd.CommandText = query;

                    cmd.Parameters.Add(new SqlParameter(ID_PARAMETER, SqlDbType.Int));
                    cmd.Parameters[ID_PARAMETER].Value = inputId;
      
                    return cmd.ExecuteNonQuery();

                }
            }
        }
        
        public void ExecuteNonQuery(string procedureName, int inputId, String parameter) {

            using (SqlConnection conn = new SqlConnection(SqlUtils.CONNECTION_STR)) {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand()) {
                    cmd.CommandText = procedureName;

                    cmd.Parameters.Add(parameter, SqlDbType.Int).Value = inputId;

                    cmd.ExecuteNonQuery();
  
                }
            }
        }

    }
}
