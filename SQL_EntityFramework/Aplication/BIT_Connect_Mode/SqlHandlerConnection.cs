﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIT_Connect_Mode {

    public class SqlHandlerConnection : IDisposable {


        public static readonly String CONNECTION_STR =      @"Data Source=.;
                                                            Initial Catalog=BIT;
                                                            Integrated Security=true;
                                                            User Id=si2;
                                                            Password=si2";

        private readonly SqlConnection conn = new SqlConnection(CONNECTION_STR);

        public SqlConnection GetConnection() {
            conn.Open();
            return conn;
        }
        
        public SqlCommand GetCommand(IsolationLevel isolation) {
            SqlCommand cmd = conn.CreateCommand();
            cmd.Transaction = conn.BeginTransaction(isolation);
            return cmd;
        }

        public void Commit(SqlCommand cmd) {
            if (cmd.Transaction == null)
                throw new InvalidOperationException("Any Transaction were initialized!");

            cmd.Transaction.Commit();
        }

        public void CloseCommand(SqlCommand cmd, Action<SqlCommand> a) {
            a.Invoke(cmd);
            cmd.Parameters.Clear();
            cmd.Transaction.Dispose();
            cmd.Dispose();
        }

        public void RollBack(SqlCommand cmd) {
            if (cmd.Transaction == null)
                throw new InvalidOperationException("Any Transaction were initialized!");

            cmd.Transaction.Rollback();
        }

        public void Dispose() {
            if (conn != null)
                conn.Dispose();
        }

    }
}
