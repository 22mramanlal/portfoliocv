﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIT_Connect_Mode {

    public class SqlUtils {

        public static readonly String CONNECTION_STR = @"Data Source=.;
                                                          Initial Catalog=BIT;
                                                          Integrated Security=true;
                                                          User Id=si2;
                                                          Password=si2";

        //QUERY ID

        public static readonly string GET_ID = "SELECT T.[id] " +
                                               "FROM [BIT].[BIT_SCHEMA].[{0}] AS T " +
                                               "WHERE T.id = @inputId";

        
        //CLIENTE QUERIES
        public static readonly string CLIENT_GET_NAME_BY_ID = "SELECT C.[name] AS name" +
                                                              "FROM [BIT].[BIT_SCHEMA].[Cliente] AS Cl " +
                                                                    "INNER JOIN [BIT].[BIT_SCHEMA].[Contactavel] AS C " +
                                                                        "ON (C.id = Cl.contactavel AND Cl.id = @inputId)";

        
        //QUERIES

        public static readonly string GET_DELIVERS_FOR_SERVICE = "SELECT    T.[id] AS tentativa, " +
                                                                           "T.[dataTentativa], T.[descricao], T.[sucesso] \n" +  
                                                                  "FROM [BIT].[BIT_SCHEMA].[Servico] AS S " + 
                                                                     "INNER JOIN [BIT].[BIT_SCHEMA].[Tentativa] AS T \n" +
                                                                        "ON (S.[id] = T.[servico])" +
                                                                  "WHERE S.[id] = @inputId";

        public static readonly string GET_CLIENT_NAME_BY_ID =   "SELECT nome \n" +
                                                                "FROM [BIT].[BIT_SCHEMA].[Contactavel] AS C " +
                                                                    "INNER JOIN [BIT].[BIT_SCHEMA].[Cliente] as Cl \n" +
                                                                        "ON (C.id = Cl.contactavel)" +
                                                                "WHERE Cl.nif = @inputId";

        public static readonly string GET_EMPLOYEE_NAME_BY_ID = "SELECT nome \n" +
                                                                "FROM [BIT].[BIT_SCHEMA].[Contactavel] AS C " + 
                                                                "WHERE C.id = @inputId";


        public static readonly string GET_SERVICE_BY_SHOP_ID =  "SELECT   S.id, S.estafeta, S.cliente, S.tipo," +
                                                                        " S.dataRequisicao, S.dataConclusao," + 
                                                                        " S.estado, S.lojaRecepcao, S.lojaEntrega\n" +
                                                                "FROM [BIT].[BIT_SCHEMA].[Servico] AS S \n" + 
                                                                "WHERE S.lojaRecepcao = @inputId";
        
        public static readonly string GET_SERVICE_STATE_BY_SHOP_ID = "SELECT S.[estado] " +
                                                                     "FROM [BIT].[BIT_SCHEMA].[Servico] AS S " +
                                                                     "WHERE S.[lojaRecepcao] = @inputId";

        public static readonly string GET_SERVICE_ID_BY_COURIER_ID = "SELECT S.[id] " +
                                                                      "FROM [BIT].[BIT_SCHEMA].[Servico] AS S " +
                                                                      "WHERE S.[estafeta] = @inputId";

        public static readonly string GET_SERVICE_SHOP_ID_BY_ID = "SELECT S.[lojaRecepcao], S.[lojaEntrega] " +
                                                                  "FROM [BIT].[BIT_SCHEMA].[Servico] AS S " +
                                                                  "WHERE S.[estafeta] = @inputId";

        public static readonly string DELETE_SHOP = "DELETE FROM [BIT].[BIT_SCHEMA].[Loja] " +
                                                    "WHERE id = @inputId";
        
        public static readonly string GET_SHOPS_PROGRESS_ANALISE = "SELECT S.[lojaRecepcao] " +
					                                                "FROM [BIT].[BIT_SCHEMA].[Servico] AS S " +
						                                            "WHERE (S.[estado] = 'ANÁLISE' OR S.[estado] = 'EM PROGRESSO') AND S.[lojaRecepcao] = @inputId";

        public static readonly string GET_SHOPS_EMPLOYE = "SELECT F.[id] " +
                                                          "FROM [BIT].[BIT_SCHEMA].[Funcionario] AS F " +
                                                          "WHERE F.[loja] = @inputId";

        public static readonly string GET_COURIER_ID_BY_SERVICE_ID = "SELECT S.[estafeta] " +
                                                                      "FROM [BIT].[BIT_SCHEMA].[Servico] AS S " +
                                                                      "WHERE S.[id] = @inputId";
           
        public static readonly string PROCEDURE_REMOVE_EMPLOYEE_SERVICE = " EXEC [BIT].[BIT_SCHEMA].[Procedure_Remover_Funcionario_Servico] @idFuncionario";

        public static readonly string PROCEDURE_REMOVE_EMPLOYEE = " EXEC [BIT].[BIT_SCHEMA].[Procedure_Remover_Funcionario] @idContactavel";

        public static readonly string UPDATE_DELIVERY_SHOP_TO_NULL = "UPDATE [BIT].[BIT_SCHEMA].[Servico] SET lojaEntrega = NULL WHERE lojaEntrega = @idLoja";

        public static readonly string UPDATE_COURIER_SERVICE = "UPDATE [BIT].[BIT_SCHEMA].[Servico] SET estafeta = @inputId WHERE id = @serviceId";
        
        public static readonly string COURIER_SERVICES = "SELECT S.[id] \n" +
                                                         "FROM [BIT].[BIT_SCHEMA].[Servico] AS S \n"+
                                                         "WHERE S.[estafeta] = @inputId " +
                                                         "AND (S.estado <> 'CONCLUÍDO' AND S.estado <> 'CANCELADO')";

        public static readonly string PROCEDURE_CHANGE_EMPLOYEE = " EXEC [BIT].[BIT_SCHEMA].[Procedure_Passar_Servivos_De_Estafeta_Para_Outro_Estafeta] @estafeta";

        public static readonly string UPDATE_COURIER_STATE = "UPDATE [BIT].[BIT_SCHEMA].[Funcionario] SET estado = 'INDISPONIVEL' WHERE id = @idEstafeta";

        // AUXILIAR METHODS

        public static Dictionary<int, object[]> GetResults(SqlDataReader dReader) {

            if (!dReader.Read()) return new Dictionary<int, object[]>();

            object[] colunmsName = GetColunmsName(dReader);
            Dictionary<int, object[]> results = new Dictionary<int, object[]>();

            int idx = 0;
            results.Add(idx++, colunmsName);

            do {
                colunmsName = new object[colunmsName.Length];
                dReader.GetValues(colunmsName);
                results.Add(idx++, colunmsName);
            } while (dReader.Read());

            return results;
        }
                
        private static string[] GetColunmsName(SqlDataReader dReader) {

            int colsNumber = dReader.FieldCount;
            string[] colunms = new string[colsNumber];

            for (int i = 0; i < colsNumber; ++i)
                colunms[i] = dReader.GetName(i);

            return colunms;
        }
    }
}
