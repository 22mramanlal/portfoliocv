﻿using BIT_Connect_Mode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplication {

    public class ConnectFunctionalities {

        private static SqlHandler sql = new SqlHandler();

        public static readonly String ID_SHOP = "idShop";
        public static readonly String BEGIN_DATE = "beginDate";
        public static readonly String END_DATE = "endDate";
        public static readonly String ID_COURIER = "idCourier";
        private static readonly String INVALID_PARAMS = "\nInvalid input parameters.\n";
        private static readonly String NO_ELEMENTS_TO_SHOW = "\nNo elements to show.\n";

        public static void ShowServicesBetweenDates(Dictionary<String, String> args) {

            int inputId = Int32.Parse(args[ID_SHOP]);
            if (!sql.ExistID("Loja", inputId)) {
                Console.WriteLine(INVALID_PARAMS);
                return;
            }

            Dictionary<int, object[]> shopServices = sql.GetById(SqlUtils.GET_SERVICE_BY_SHOP_ID, inputId);

            if (shopServices.Count == 0) {
                Console.WriteLine(NO_ELEMENTS_TO_SHOW);
                return;
            }


            PrintServiceBetweenDates(shopServices, args[BEGIN_DATE], args[END_DATE]);            
        }

        static void PrintServiceBetweenDates(Dictionary<int, object[]> services, string bDate, string eDate) {
            object[] colsName = services[0];
            
            int idxBDate = FindIndex(colsName, "dataRequisicao");

            DateTime date1 = Convert.ToDateTime(bDate);
            DateTime date2 = Convert.ToDateTime(eDate);
            DateTime date3;

            for (int i = 1; i < services.Count; ++i) {
                date3 = Convert.ToDateTime(services[i][idxBDate]);

                if ((DateTime.Compare(date1, date3) >= 0) && (DateTime.Compare(date2, date3) <= 0)) {
                    Console.WriteLine("Service Info:");
                    PrintRowResults(colsName);
                    PrintServiceInfo(colsName, services[i]);
                }
            }
        }

        private static void PrintServiceInfo(object[] fieldNames, object[] service) {

            int estafetaIdx = FindIndex(fieldNames, "estafeta");
            int clienteIdx = FindIndex(fieldNames, "cliente");

            int serviceId = Convert.ToInt32(service[0]);
            string estafetaIdValue = Convert.ToString(service[estafetaIdx]);
            string estafeta = (estafetaIdValue != null && estafetaIdValue != "") ? 
                                            GetContactavelName(SqlUtils.GET_EMPLOYEE_NAME_BY_ID, Convert.ToInt32(service[estafetaIdx]))
                                            : "Not Assigned";

            service[estafetaIdx] = estafeta;
            service[clienteIdx] = GetContactavelName(SqlUtils.GET_CLIENT_NAME_BY_ID, Convert.ToInt32(service[clienteIdx]));
            
            PrintRowResults(service);
            Console.WriteLine("Service details of delivers.");
            PrintDeliversInfo(serviceId);
            Console.WriteLine("\n");

        }

        private static void PrintDeliversInfo(int serviceId) {
            Dictionary<int, object[]> serviceDelivers = sql.GetById(SqlUtils.GET_DELIVERS_FOR_SERVICE, serviceId);
            PrintResults(serviceDelivers);
        }

        public static string GetContactavelName(string query, int inputId) {
            Dictionary<int, object[]> contactavelName = sql.GetById(query, inputId);
            return contactavelName[1][0].ToString();
        }

        private static int FindIndex(object[] colsName, string value) {
            for (int i = 0; i < colsName.Length; ++i)
                if (colsName[i].Equals(value))
                    return i;
            return -1;
        }

        public static void SetCourierToUnavailable(Dictionary<String, String> args) {

            int courierId = Int32.Parse(args[ID_COURIER]);
            if (!sql.ExistID("Estafeta", courierId)) {
                Console.WriteLine(INVALID_PARAMS);
                Console.WriteLine("Or this worker is not a Courier.\n");
                return;
            }
            
            Dictionary<int, object[]> services = sql.GetById(SqlUtils.COURIER_SERVICES, courierId);
            Console.WriteLine("Service associated to courier: ");
            PrintResults(services);

            sql.ExecuteNonQuery(SqlUtils.PROCEDURE_CHANGE_EMPLOYEE, courierId, "@estafeta");

            int[] servicesIds = GetServicesIds(services);
            object[] newCouriers = new object[servicesIds.Length];

            for (int i = 0; i < newCouriers.Length; ++i )
                newCouriers[i] = sql.GetById(SqlUtils.GET_COURIER_ID_BY_SERVICE_ID, servicesIds[i])[1][0];

            PrintPreviousInfo(servicesIds, newCouriers);
            Console.WriteLine("Do you wanna to proceed Y/N ?");
            int readed = Console.Read();

            if (Convert.ToChar(readed).Equals('N'))
                RevertProccess(courierId, servicesIds);
            else
                sql.ExecuteNonQuery(SqlUtils.UPDATE_COURIER_STATE, courierId, "@idEstafeta");            
        }

        private static void RevertProccess(int courierId, int[] servicesIds) {
            foreach (int id in servicesIds)
                sql.Update(SqlUtils.UPDATE_COURIER_SERVICE, courierId, id);
        }

        private static void PrintPreviousInfo(int[] servicesIds, object[] newCouriers) {

            Console.WriteLine("New services distribution: ");
            Console.WriteLine("  idService  |  idCourier  |");
            for (int i = 0; i < newCouriers.Length; ++i)
                Console.WriteLine("      " + servicesIds[i] + "      |" + newCouriers[i] + "      |");
        }

        private static int[] GetServicesIds(Dictionary<int, object[]> services) {
            int[] ids = new int[services.Count - 1];

            for (int i = 1; i < services.Count; ++i ) 
                ids[i - 1] = Convert.ToInt32(services[i][0]);

            return ids;
        }

        public static void DeleteShop(Dictionary<String, String> args) {

             int shopId = Int32.Parse(args[ID_SHOP]);
             if (!sql.ExistID("Loja", shopId)) {
                 Console.WriteLine(INVALID_PARAMS);
                 return;
             }

             Dictionary<int, object[]> shopIDs = sql.GetById(SqlUtils.GET_SHOPS_PROGRESS_ANALISE, shopId);

             if (shopIDs.Count != 0 && shopIDs[1].Length != 0) {
                 Console.WriteLine("Can't Delete this Shop.\n");
                 return;
             }


             Dictionary<int, object[]> employees = sql.GetById(SqlUtils.GET_SHOPS_EMPLOYE, shopId);


             for (int j = 1; j < employees.Count; ++j) {
                 int employee = Convert.ToInt32(employees[j][0]);
                 sql.ExecuteNonQuery(SqlUtils.PROCEDURE_REMOVE_EMPLOYEE_SERVICE, employee, "@idFuncionario");
                 sql.ExecuteNonQuery(SqlUtils.PROCEDURE_REMOVE_EMPLOYEE, employee, "@idContactavel");
             }

             sql.ExecuteNonQuery(SqlUtils.UPDATE_DELIVERY_SHOP_TO_NULL, shopId, "@idLoja");
             int count = sql.DeleteByID(SqlUtils.DELETE_SHOP, shopId);
            
             Console.WriteLine("Was Deleted " + count + " Shops.\n");
         }

        private static void PrintResults(Dictionary<int, object[]> data) {
            int i = 0;  
            for (; i < data.Count; ++i)
                PrintRowResults(data[i]);
            if (i == 0)
                Console.WriteLine(NO_ELEMENTS_TO_SHOW + "\n");
        }

        private static void PrintRowResults(object[] rowValues) {

            string row = "----------------------------------------------------------------------";
            int size = rowValues.Length;

            foreach (object o in rowValues) {
                PrintValue(o);
                Console.Write(" | ");
            }

            Console.WriteLine();
            Console.WriteLine(row);
        }

        private static void PrintValue(object value) {

            if (value.GetType().IsAssignableFrom(typeof(bool)))
                value = (!(bool)value) ? "Não" : "Sim";

            if (value.GetType().IsAssignableFrom(typeof(DateTime)))
                value = ((DateTime)value).ToShortDateString();

            Console.Write(value);
        }
    }
}
