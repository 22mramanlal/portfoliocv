﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplication {
    
    public class Utils {

        public static readonly String ID_SHOP = "idShop";
        public static readonly String BEGIN_DATE = "beginDate";
        public static readonly String END_DATE = "endDate";
        public static readonly String ID_COURIER = "idCourier";
    }
}
