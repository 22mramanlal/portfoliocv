﻿using BITv2;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    

namespace Aplication{

   public  class DisconnectFunctionalities{
       
        public static readonly String ID_SHOP = "idShop";
        public static readonly String BEGIN_DATE = "beginDate";
        public static readonly String END_DATE = "endDate";
        public static readonly String ID_COURIER = "idCourier";


        private static int GetID(String id){
            return Int32.Parse(id);
        }

        private static DateTime GetDate(String date){
            return Convert.ToDateTime(date);
        }



        public  static void ShowServicesBetweenDates(Dictionary<String, String> args) {
            String idArg, beginDateArg, endDateArg;
            int idShop;
            DateTime beginDate, endDate;

            try {
                idArg = args[ID_SHOP];
                beginDateArg = args[BEGIN_DATE];
                endDateArg = args[END_DATE];

                idShop = GetID(idArg);
                beginDate = GetDate(beginDateArg);
                endDate = GetDate(endDateArg);
            } catch (Exception) {
                throw new Exception("Invalid Arguments");
            }
            
     
            using (var ctx = new BITEntities()){
                
                var services = ctx.Servico.Where(service => 
                    (service.lojaRecepcao == idShop) && (service.dataRequisicao >= beginDate && service.dataRequisicao <= endDate)
                );

                foreach (var service in services)
                    Console.WriteLine(string.Format("\t{0}", service.id));
            
                ShowSpecificService(services, ctx);
            }        
        }

        private static void ShowSpecificService(IEnumerable<Servico> services, BITEntities ctx) { 
            String input;
            int serviceID;

            PrintShowSpecificServiceInfo();

            while ((input = Console.ReadLine()) != string.Empty) {
                try{
                    serviceID = GetID(input);
                } catch (Exception) {
                    Console.WriteLine("Invalid arguments");
                    PrintShowSpecificServiceInfo();
                    continue;
                }

                if (!services.Any((service)=> service.id == serviceID)) {
                    Console.WriteLine("Invalid ServiceID");
                    PrintShowSpecificServiceInfo();
                    continue;
                }

                PrintServiceInfo(serviceID, ctx);
                PrintShowSpecificServiceInfo();
            }
        }

        private static void PrintServiceInfo(int serviceID, BITEntities ctx) {
            IEnumerable<Tentativa> attempts = ctx.Tentativa.Where((attempt)=> attempt.servico==serviceID);

            foreach (Tentativa attempt in attempts) {
                Console.WriteLine("Attemp id : {0}", attempt.id);
                Console.WriteLine("Attemp success : {0}", attempt.sucesso);
                Console.WriteLine("Attemp date : {0}", attempt.dataTentativa);
                Console.WriteLine("Attemp description : {0}", attempt.descricao);
                Console.WriteLine("");
            }
        }

        private static void PrintShowSpecificServiceInfo() {
            Console.WriteLine("Insert an service id to watch it or Enter to EXIT");
            Console.Write(">");
        }
       
       
        public static void SetCourierToUnavailable(Dictionary<String, String> args) { 
            String courierIDArg;
            int courierID;
            Estafeta courier;

            try { 
                courierIDArg = args[ID_COURIER];
                courierID = GetID(courierIDArg);
            } catch (Exception) {
                throw new Exception("Invalid Arguments");
            }

            using(var ctx = new BITEntities()){
                try {
                    courier = ctx.Estafeta.First((c) => c.id == courierID);
                } catch (Exception) {
                    throw new Exception("Inexistent Courier");
                }

                var services = ctx.Servico.Where((service) => service.estado.Equals("EM PROGRESSO") && service.estafeta == courierID);

                ShowChangesOnServices(services, courierID);

                if (GetUserConsent()){
                    Funcionario func = ctx.Funcionario.First((f) => f.id == courier.id);

                    ctx.Procedure_Passar_Servivos_De_Estafeta_Para_Outro_Estafeta(courierID);

                    func.estado = "INDISPONIVEL";
                    ctx.SaveChanges();
                }
            }
        }

        private static bool GetUserConsent() { 
            String input;

            while (true) {
                input = Console.ReadLine();

                if (input.Equals("N") || input.Equals("n"))
                    return false;

                if (input.Equals("Y") || input.Equals("y"))
                    return true;

                Console.WriteLine("Invalid Input");
            }

        }

        public static void ShowChangesOnServices(IEnumerable<Servico> services, int courierID) {
            Console.WriteLine("Currently Courier {0} has the folowing services", courierID);

            foreach (Servico service in services)
                Console.WriteLine("\t"+service.id);

            Console.WriteLine("The services possible to pass to other couriers will be passed, and this courrier{0} will not acept more work until his state changes.", courierID);
            Console.WriteLine("Do you want to contuniue ? (Y/N)");
            Console.WriteLine(">");
        }


        public static void DeleteShop(Dictionary<String, String> args) {
            String idShopArg;
            int idShop;
            Loja shop;

            try { 
                idShopArg = args[ID_SHOP];
                idShop = GetID(idShopArg);
            } catch (Exception) {
                throw new Exception("Invalid Arguments");
            }

            using(var ctx = new BITEntities()){
                shop = ctx.Loja.First((s)=>s.id == idShop);

                if (shop == null)
                    throw new Exception("Inexistent shop");

                if (ShopHasWorkToFinish(shop, ctx))
                    throw new Exception("Shop has works not finished!");

                int oper = 0;
                do {
                    try {
                        switch (oper){
                            case 0: RemoveServicesAndDepedencies(shop, ctx); oper = 1; break;
                            case 1: RemoveWorkersAndDependencies(shop, ctx); oper = 2; break;
                            case 2: RemoveShop(shop, ctx); oper = 3; break;
                            default: return;
                        }
                    } catch (DbUpdateConcurrencyException ex) {                        
                        ex.Entries.Single().Reload(); 
                    }
                } while (oper != 3); 
            }
        }

        private static bool ShopHasWorkToFinish(Loja shop, BITEntities ctx) {
            return ctx.Servico.Any((service) => 
               (service.lojaEntrega == shop.id || service.lojaRecepcao == shop.id) && (service.estado.Equals("ANÁLISE") || service.estado.Equals("EM PROGRESSO"))
            );
        }

        private static void RemoveShop(Loja shop, BITEntities ctx) {
            ctx.Loja.Remove(shop);
            ctx.SaveChanges();
        }

        private static void RemoveServicesAndDepedencies(Loja shop, BITEntities ctx) {
            IEnumerable<Servico> services = ctx.Servico.Where((service) => service.lojaRecepcao == shop.id || service.lojaEntrega == shop.id);
            
            List<int> servicesIDs = new List<int>();
            services.ToList().ForEach((service)=>servicesIDs.Add(service.id));

            IEnumerable<Tentativa> attempts = ctx.Tentativa.Where((attempt)=> servicesIDs.Contains(attempt.servico));

            attempts.ToList().ForEach((attempt)=>ctx.Tentativa.Remove(attempt));

            services.ToList().ForEach((service)=>ctx.Servico.Remove(service));
            ctx.SaveChanges();
        }

        private static void RemoveWorkersAndDependencies(Loja shop, BITEntities ctx) { 
            IEnumerable<Funcionario> workers = ctx.Funcionario.Where((worker)=>worker.loja == shop.id);

            workers.ToList().ForEach((worker)=>ctx.Procedure_Remover_Funcionario(worker.id));
            ctx.SaveChanges();
        }

    }

}

