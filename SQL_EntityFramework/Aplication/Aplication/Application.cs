﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplication{

    class Application {

        private static readonly Dictionary<String, Action<Dictionary<String, String>>> disconnectFuncionalities = new Dictionary<String, Action<Dictionary<String, String>>>();
        private static readonly Dictionary<String, Action<Dictionary<String, String>>> connectFuncionalities = new Dictionary<String, Action<Dictionary<String, String>>>();
        

        //FUNCTIONALITIES
        private static readonly String DELETE_SHOP = "Remove_Shop";
        private static readonly String SET_COURIER_UNAVAILABLE = "Set_Courier_Unavailable";
        private static readonly String SHOW_SERVICE_BETWEEN_DATES = "Show_Services_Between_Dates";
        private static readonly String HELP = "Help";
        private static readonly String EXIT = "Exit";
        private static readonly String DELETE_SHOP_NUMBER = "1";
        private static readonly String SET_COURIER_UNAVAILABLE_NUMBER = "2";
        private static readonly String SHOW_SERVICE_BETWEEN_DATES_NUMBER = "3";
        private static readonly String HELP_NUMBER = "4";
        private static readonly String EXIT_NUMBER = "5";

        //FUNCTIONALITIES ARGUMENTS


        static Application(){
            disconnectFuncionalities.Add(DELETE_SHOP, DisconnectFunctionalities.DeleteShop);
            disconnectFuncionalities.Add(SET_COURIER_UNAVAILABLE, DisconnectFunctionalities.SetCourierToUnavailable);
            disconnectFuncionalities.Add(SHOW_SERVICE_BETWEEN_DATES, DisconnectFunctionalities.ShowServicesBetweenDates);


            disconnectFuncionalities.Add(DELETE_SHOP_NUMBER, DisconnectFunctionalities.DeleteShop);
            disconnectFuncionalities.Add(SET_COURIER_UNAVAILABLE_NUMBER, DisconnectFunctionalities.SetCourierToUnavailable);
            disconnectFuncionalities.Add(SHOW_SERVICE_BETWEEN_DATES_NUMBER, DisconnectFunctionalities.ShowServicesBetweenDates);

            connectFuncionalities.Add(DELETE_SHOP, ConnectFunctionalities.DeleteShop);
            connectFuncionalities.Add(SET_COURIER_UNAVAILABLE, ConnectFunctionalities.SetCourierToUnavailable);
            connectFuncionalities.Add(SHOW_SERVICE_BETWEEN_DATES, ConnectFunctionalities.ShowServicesBetweenDates);

            connectFuncionalities.Add(DELETE_SHOP_NUMBER, ConnectFunctionalities.DeleteShop);
            connectFuncionalities.Add(SET_COURIER_UNAVAILABLE_NUMBER, ConnectFunctionalities.SetCourierToUnavailable);
            connectFuncionalities.Add(SHOW_SERVICE_BETWEEN_DATES_NUMBER, ConnectFunctionalities.ShowServicesBetweenDates);
        }


        
    /*
     User insere uma funcionalidade e os argumentos da mesma
     Argumentos = argName=value&argName=value
     Convertes para um diccionario 
     Passas a action que sabe responder a esse funcionalidade
     
     * ex: DELETE_USER userID=5&userName=Dantas
     convertes os argumentos para 2 arguemntos
     e passas ah action functionalities["DELETE_USER"](dictionary)
     */
        
        public void Run() {
         
            while (true) {

                PrintFunctionality();
                String[] input = ReadCommand();

                if (input[0] == EXIT || input[0] == EXIT_NUMBER)
                    break;
                if (input[0] == HELP || input[0] == HELP_NUMBER){
                    PrintHelpInformation(); continue;
                }

                if (input.Length != 3){
                    Console.WriteLine("\tInvalid Function. Consult Help.\n");
                    //continue;
                }

                Dictionary<String, String> dictionary = GetArguments(input[1]);
                if (dictionary == null)
                    continue;

                CallFunction(input[0],dictionary,input[2]);
            }
        }


        public void CallFunction(String command, Dictionary<String,String> dictionary,String mode){

            if (mode == "Connect_Mode")
                connectFuncionalities[command](dictionary);
            else if (mode == "Disconnect_Mode")
                disconnectFuncionalities[command](dictionary);
            else 
                Console.WriteLine("\tInvalid Function. Consult Help.\n");

        }

        private String[] ReadCommand(){
            Console.Write(">");
            return Console.ReadLine().Split(' ');
        }

        private Dictionary<String, String> GetArguments(String input){

            Dictionary<String, String> dictionary = new Dictionary<String, String>();
            String[] args = input.Split('&');
            String [] str;

            foreach (String s in args){
                str = s.Split('=');
                if (str.Length != 2){
                    Console.WriteLine("\tInvalid Function. Consult Help.\n");
                    return null;
                }
                dictionary.Add(str[0], str[1]);
            }
                
            return dictionary;
        }
      
        private void PrintFunctionality() {
            Console.WriteLine("Functionalities:");
            Console.WriteLine("\t" + DELETE_SHOP_NUMBER + " " + DELETE_SHOP + " " + DisconnectFunctionalities.ID_SHOP + "=value");
            Console.WriteLine("\t" + SET_COURIER_UNAVAILABLE_NUMBER + " " + SET_COURIER_UNAVAILABLE + " " + DisconnectFunctionalities.ID_COURIER + "=value");
            Console.WriteLine("\t" + SHOW_SERVICE_BETWEEN_DATES_NUMBER + " " + SHOW_SERVICE_BETWEEN_DATES + " " + DisconnectFunctionalities.ID_SHOP + "=value&" + DisconnectFunctionalities.BEGIN_DATE + "=value&" + DisconnectFunctionalities.END_DATE + "=value");
            Console.WriteLine("\t" + HELP_NUMBER + " " + HELP);
            Console.WriteLine("\t" + EXIT_NUMBER + " " + EXIT);
            Console.WriteLine("Modes:");
            Console.WriteLine("\t" + "Connect_Mode or Disconnect_Mode");
            Console.WriteLine("\n");
        }

        private static void PrintHelpInformation(){
            Console.WriteLine("Example to call Function:");
            Console.WriteLine("\t Remove_Shop idShop=value&beginDate=value&endDate=value Connect_Mode");
            Console.WriteLine("\t Or");
            Console.WriteLine("\t 1 idShop=value&beginDate=value&endDate=value Disconnect_Mode");
            Console.WriteLine("Example Date Format:");
            Console.WriteLine("\t YYYY-MM-DD\n");
        }

    }
}
