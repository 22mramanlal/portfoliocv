﻿using System;

public class Builder<T>
{
    public MethodBehaviorBuilder<T> On<W, E>(Func<W, E> func)
    {
        return new MethodBehaviorBuilder<T>(func);
    }

    public MethodBehaviorBuilder<T> On<W, X, E>(Func<W, X, E> func)
    {
        return new MethodBehaviorBuilder<T>(func);
    }

    public MethodBehaviorBuilder<T> On<W, X, Y, E>(Func<W, X, Y, E> func)
    {
        return new MethodBehaviorBuilder<T>(func);
    }
}

