﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;


public class MethodBehaviorBuilder<T>
{
    private Type t;
    private Dictionary<MethodInfo, MethodStructure> msDictionary;
    private MethodStructure currMs;

    public MethodBehaviorBuilder()
    {
        this.t = typeof(T);
        this.msDictionary = new Dictionary<MethodInfo, MethodStructure>();
        this.currMs = new MethodStructure();
    }

    public MethodBehaviorBuilder(Delegate on) : this()
    {
        AffectOnMethod(on);
    }

    #region On
    public MethodBehaviorBuilder<T> On<W, E>(Func<W, E> func)
    {
        AffectOnMethod(func);
        return this;
    }
    
    public MethodBehaviorBuilder<T> On<W, X, E>(Func<W, X, E> func)
    {
        AffectOnMethod(func);
        return this;
    }

    public MethodBehaviorBuilder<T> On<W, X, Y, E>(Func<W, X, Y, E> func)
    {
        AffectOnMethod(func);
        return this;
    }
    #endregion On
    #region DoBefore
    public MethodBehaviorBuilder<T> DoBefore<W>(Action<W> act)
    {
        AffectBefore(act);
        return this;
    }
    
    public MethodBehaviorBuilder<T> DoBefore<W, X>(Action<W, X> act)
    {
        AffectBefore(act);
        return this;
    }

    public MethodBehaviorBuilder<T> DoBefore<W, X, Y>(Action<W, X, Y> act)
    {
        AffectBefore(act);
        return this;
    }
    #endregion DoBefore
    #region DoAfter
    public MethodBehaviorBuilder<T> DoAfter<W>(Action<W> act)
    {
        AffectAfter(act);
        return this;
    }

    public MethodBehaviorBuilder<T> DoAfter<W, X>(Action<W, X> act)
    {
        AffectAfter(act);
        return this;
    }

    public MethodBehaviorBuilder<T> DoAfter<W, X, Y>(Action<W, X, Y> act)
    {
        AffectAfter(act);
        return this;
    }
    #endregion DoAfter
    #region Replace
    public MethodBehaviorBuilder<T> Replace<W, E>(Func<W, E> func)
    {
        AffectReplace(func);
        return this;
    }

    public MethodBehaviorBuilder<T> Replace<W, X, E>(Func<W, X, E> func)
    {
        AffectReplace(func);
        return this;
    }

    public MethodBehaviorBuilder<T> Replace<W, X, Y, E>(Func<W, X, Y, E> func)
    {
        AffectReplace(func);
        return this;
    }
    #endregion Replace
    public T Make()
    {
        AffectOnMethod(null);
        return DynamicProxyFactory.MakeProxy<T>((T)Activator.CreateInstance(t), 
                                                new MethodsInterceptor(msDictionary));
    }
    #region Affect
    private void AffectOnMethod(Delegate onM)
    {
        Delegate currMsOn = currMs.GetOn();
        if (currMsOn != null)
        {
            MethodInfo m = currMsOn.Method;
            if (msDictionary.ContainsKey(m))
            {
                msDictionary.Remove(m);
                msDictionary[m] = currMs;
            }
            else
                msDictionary.Add(m, currMs);
                
            currMs = new MethodStructure();
        }
      
        currMs.SetOn(onM);
    }

    private void AffectBefore(Delegate b)
    {
        currMs.AddBefore(b);
    }

    private void AffectAfter(Delegate a)
    {
        currMs.AddAfter(a);
    }

    private void AffectReplace(Delegate rep)
    {
        currMs.SetReplace(rep);
    }
    #endregion Affect
}
