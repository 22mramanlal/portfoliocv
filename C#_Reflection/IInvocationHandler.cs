﻿public interface IInvocationHandler
{
    object OnCall(CallInfo info);
}
