﻿using System;
using System.Collections.Generic;
using System.Reflection;

public class MethodsInterceptor : IInvocationHandler
{
    private Dictionary<MethodInfo, MethodStructure> msd;

    public MethodsInterceptor(Dictionary<MethodInfo, MethodStructure> dictionary)
    {
        this.msd = dictionary;
    }

    public object OnCall(CallInfo info)
    {
        object res = null;
        MethodInfo m = info.TargetMethod;
        if (msd.ContainsKey(m))
        {
            MethodStructure ms = msd[m];

            Delegate before = ms.GetBefore();
            
            if(before != null)
                before.DynamicInvoke(info.Parameters);

            Delegate rep = ms.GetReplace();
            res = rep != null ? rep.DynamicInvoke(info.Parameters) : info.TargetMethod.Invoke(info.Target, info.Parameters);

            Delegate after = ms.GetAfter();

            if(after != null)
                after.DynamicInvoke(info.Parameters);
        }
        else
            res = info.TargetMethod.Invoke(info.Target, info.Parameters);

        return res;
    }
}
