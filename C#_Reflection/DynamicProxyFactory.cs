﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

public class DynamicProxyFactory
{
    #region Utility methods 
     
        private static IEnumerable<MethodInfo> GetVirtualMethods(Type t)
        {
            MethodInfo[] tMInfo = t.GetMethods(BindingFlags.Public | BindingFlags.Instance);

            foreach(MethodInfo method in tMInfo)
                if(method.IsVirtual)
                    yield return method;
        }

        private static Type[] GetParametersType(ParameterInfo[] parameter)
        {
            Type[] type = new Type[parameter.Length];
        
            for (int i = 0; i < parameter.Length; ++i)
                type[i] = parameter[i].ParameterType;

            return type;
        }

        private static void MethodsRedifiner(IEnumerable<MethodInfo> tMInfo, TypeBuilder tb, FieldBuilder tTarget, FieldBuilder ihHandler)
        {
            ParameterInfo[] methodParameters;
            Type[] parametersType;
            MethodBuilder mb;

            foreach (MethodInfo m in tMInfo)
            {
                methodParameters = m.GetParameters();
                parametersType = GetParametersType(methodParameters);

                mb = tb.DefineMethod(m.Name, MethodAttributes.Public | MethodAttributes.Virtual, m.ReturnType, parametersType);

                ILGenerator methodIL = mb.GetILGenerator();        

                //Method Arguments
                LocalBuilder argArray = methodIL.DeclareLocal(typeof(object[]));
                methodIL.Emit(OpCodes.Ldc_I4, methodParameters.Length);
                methodIL.Emit(OpCodes.Newarr, typeof(object));
                methodIL.Emit(OpCodes.Stloc, argArray);

                //argArray possitions types init
                for (int i = 0; i < methodParameters.Length; ++i)
                {
                    ParameterInfo pInfo = methodParameters[i];

                    methodIL.Emit(OpCodes.Ldloc, argArray);
                    methodIL.Emit(OpCodes.Ldc_I4, i);
                    methodIL.Emit(OpCodes.Ldarg_S, i + 1);
                    if (pInfo.ParameterType.IsPrimitive || pInfo.ParameterType.IsValueType)
                        methodIL.Emit(OpCodes.Box, pInfo.ParameterType);
                    methodIL.Emit(OpCodes.Stelem_Ref);
                }
                
                //t MethodInfo
                LocalBuilder methodInfo = methodIL.DeclareLocal(typeof(MethodInfo));
                methodIL.Emit(OpCodes.Ldtoken, m);
                methodIL.Emit(OpCodes.Call, typeof(MethodBase).GetMethod("GetMethodFromHandle", new Type[] { typeof(RuntimeMethodHandle) }));
                methodIL.Emit(OpCodes.Castclass, typeof(MethodInfo));
                methodIL.Emit(OpCodes.Stloc, methodInfo);

                //IInvocationHandler instance
                methodIL.Emit(OpCodes.Ldarg_0);
                methodIL.Emit(OpCodes.Ldfld, ihHandler);
                 
                //CallInfo Constructor
                Type[] ct = { typeof(MethodInfo), typeof(object), typeof(object[]) };
                methodIL.Emit(OpCodes.Ldloc, methodInfo);
                if (tTarget != null)
                {
                    methodIL.Emit(OpCodes.Ldarg_0);
                    methodIL.Emit(OpCodes.Ldfld, tTarget);
                }
                else
                    methodIL.Emit(OpCodes.Ldnull);
                methodIL.Emit(OpCodes.Ldloc, argArray);
                methodIL.Emit(OpCodes.Newobj, typeof(CallInfo).GetConstructor(ct));

                //IInvocationHandler.OnCall(CallInfo info)
                methodIL.Emit(OpCodes.Callvirt, ihHandler.FieldType.GetMethod("OnCall"));

                Type mt = m.ReturnType;
                if (m.ReturnType == typeof(void))
                    methodIL.Emit(OpCodes.Pop);
                else
                    if (mt.IsPrimitive || mt.IsValueType)
                        methodIL.Emit(OpCodes.Unbox_Any, mt);
                
                methodIL.Emit(OpCodes.Ret); 
            }
        }

    #endregion UtilityMethods
    
    public static T MakeProxy<T>(T t, IInvocationHandler iih)
    {
        //Criação do novo tipo: class Proxy : T
        AssemblyName aName = new AssemblyName("ProxyDynamicAssembly");
        AssemblyBuilder aBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(aName, AssemblyBuilderAccess.RunAndSave);

        ModuleBuilder mBuilder = aBuilder.DefineDynamicModule(aName.Name, aName.Name + ".dll");

        TypeBuilder tb = mBuilder.DefineType("Proxy", TypeAttributes.Public);
        tb.SetParent(t.GetType());

        //Proxy contructor: public Proxy(T type, IInvocationHandler iih) {...}
        FieldBuilder tTarget = tb.DefineField("tTarget", typeof(T), FieldAttributes.Private);
        FieldBuilder ihHandler = tb.DefineField("ihHandler", typeof(IInvocationHandler), FieldAttributes.Private);

        Type[] ctorParams = { typeof(T), typeof(IInvocationHandler) };
        ConstructorBuilder cb = tb.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, ctorParams);
        ILGenerator ctorIL = cb.GetILGenerator();

        ctorIL.Emit(OpCodes.Ldarg_0);
        ctorIL.Emit(OpCodes.Ldarg_1);
        ctorIL.Emit(OpCodes.Stfld, tTarget);
        ctorIL.Emit(OpCodes.Ldarg_0);
        ctorIL.Emit(OpCodes.Ldarg_2);
        ctorIL.Emit(OpCodes.Stfld, ihHandler);
        ctorIL.Emit(OpCodes.Ret);

        IEnumerable<MethodInfo> tMInfo = GetVirtualMethods(t.GetType());
        
        MethodsRedifiner(tMInfo, tb, tTarget, ihHandler);

        Type retT = tb.CreateType();
        aBuilder.Save(aName.Name + ".dll");
        T retI = (T)Activator.CreateInstance(retT, new object[] { t, iih});
        return retI;
    }

    public static T MakeProxy<T>(IInvocationHandler iih)
    {
        //Criação do novo tipo: class Proxy : T
        AssemblyName aName = new AssemblyName("IProxyDynamicAssembly");
        AssemblyBuilder aBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(aName, AssemblyBuilderAccess.RunAndSave);

        ModuleBuilder mBuilder = aBuilder.DefineDynamicModule(aName.Name, aName.Name + ".dll");

        TypeBuilder tb = mBuilder.DefineType("Proxy", TypeAttributes.Public);
        tb.AddInterfaceImplementation(typeof(T));

        FieldBuilder ihHandler = tb.DefineField("ihHandler", typeof(IInvocationHandler), FieldAttributes.Private);

        Type[] ctorParams = { typeof(IInvocationHandler) };
        ConstructorBuilder cb = tb.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, ctorParams);
        ILGenerator ctorIL = cb.GetILGenerator();

        ctorIL.Emit(OpCodes.Ldarg_0);
        ctorIL.Emit(OpCodes.Ldarg_1);
        ctorIL.Emit(OpCodes.Stfld, ihHandler);
        ctorIL.Emit(OpCodes.Ret);

        IEnumerable<MethodInfo> tMInfo = GetVirtualMethods(typeof(T));

        MethodsRedifiner(tMInfo, tb, null, ihHandler);

        Type retT = tb.CreateType();
        aBuilder.Save(aName.Name + ".dll");
        T retI = (T)Activator.CreateInstance(retT, new object[] { iih });
        return retI;
    }

    public static Builder<T> With<T>()
    {
        return new Builder<T>();
    } 
}

