﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

public class LoggerInterceptor : IInvocationHandler
{
    private long start;
    private Stopwatch watch = new Stopwatch();
    
    public object OnCall(CallInfo info)
    {
        start = watch.ElapsedTicks;
        // call real method using reflection
        object res = info.TargetMethod.Invoke(
        info.Target,
        info.Parameters);
        Console.WriteLine("Executed in {0} ticks",
        watch.ElapsedTicks - start);
        return res;
    }
}

public class MockInterceptor : IInvocationHandler
{
    public object OnCall(CallInfo info)
    {
        // just a mock interceptor
        // that always returns the same value
        return "some text";
    }
}

public class Foo
{
    public virtual int DoIt(String v)
    {
        Console.WriteLine(
        "AClass.DoIt() with {0}",
        v
        );
        return v.Length;
    }
}

public class Program
{
    public static void Main(string[] args)
    {   
        //Class Proxy test
        IInvocationHandler logInterceptor = new LoggerInterceptor();
        Foo real = new Foo();
        Foo proxy = DynamicProxyFactory.MakeProxy<Foo>(real, logInterceptor);

        Console.WriteLine(proxy.DoIt("12"));
        
        //Interface Proxy Test
        IInvocationHandler mockInterceptor = new MockInterceptor();
        IHelper p = DynamicProxyFactory.MakeProxy<IHelper>(mockInterceptor);
        string s = p.Operation(new Dictionary<int, string>());
        Console.WriteLine(s);
        
        //Fluent API Test
        Foo f = new Foo();
        f = DynamicProxyFactory
                        .With<Foo>()
                        .On<String, int>(f.DoIt)
                        .DoBefore<String>(v => Console.WriteLine("DoItMoreSpecial() with {0}", v))
                        .Replace<String, int>(v => v.GetHashCode())
                        .Make();
        
        Console.WriteLine(f.DoIt("12"));
    }
}

