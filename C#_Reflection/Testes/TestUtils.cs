﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public class TestClassInterceptor : IInvocationHandler
    {
     
        public object OnCall(CallInfo info)
        {     
            object res = info.TargetMethod.Invoke(info.Target, info.Parameters);
            return res;
        }
    }

    public class TestInterfaceInterceptor : IInvocationHandler
    {
        public object OnCall(CallInfo info)
        {
            return "Interface Test";
        }
    }

    public class StringFormater
    {
        private String str;
        
        public String GetString()
        {
            return str;
        }
        public virtual String SetAndReturnString(String s)
        {
            return this.str = s;
        }

        public String StringToLowerAndAffect(String s)
        {
            return this.str = s.ToLower();
        }
    }

    public class Calculater
    {
        public virtual double Divide(double a, double b)
        {
            return a / b;
        }

        public virtual int Multiply(int a, int b, int c)
        {
            return a * b * c;
        }

        public virtual int Add(int a, int b)
        {
            return a + b;
        }

    }

    public interface IClassSchool
    {
        string GetAllStudents();
        string GetStudentEvaluations(string name, string discipline);
    }

    class TestUtils
    {
    }
