﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

    [TestClass]
    public class Testes
    {
        /**
         * 
         * O teste cria um novo proxy para uma classe do tipo Calculater e chamamos o metodo Add,
         * conferindo depois o retorno da chamada ao metodo.
         * 
         */
        [TestMethod]
        public void TestGeneratorProxyForClass()
        {
            //Arrange
                IInvocationHandler testInterceptor = new TestClassInterceptor();
                Calculater real = new Calculater();
                int a = 4;
                int b = 8;
            
            //Act
                Calculater proxy = DynamicProxyFactory.MakeProxy<Calculater>(real, testInterceptor);
                int result = proxy.Add(a, b);

            //Assert
                Assert.AreEqual(result, a + b);

        }

        /**
         * 
         * O teste cria um novo proxy para uma interface onde a chamada a um metodo retorna
         * uma constante porque não é possivel fazer uma chamada a um metodo de uma interface.
         * 
         */
        [TestMethod]
        public void TestGeneratorProxyForInterface()
        {
            //Arrange
            IInvocationHandler interfaceInterceptor = new TestInterfaceInterceptor();
            string txt = "Interface Test";

            //Act
            IClassSchool proxy = DynamicProxyFactory.MakeProxy<IClassSchool>(interfaceInterceptor);
            string result = proxy.GetStudentEvaluations("Nuno", "AVE");

            //Assert
            Assert.AreEqual(result, txt);

        }

        /**
         * 
         * O teste redefine um metodo proxy através da API fluente adicionando comportamento a um metodo.
         * 
         */
        [TestMethod]
        public void TestFluentWithOneMethod()
        {

            //Arrange
            Calculater cal = new Calculater();
            cal = DynamicProxyFactory
                          .With<Calculater>()
                          .On<double, double, double>(cal.Divide)
                          .DoBefore<double, double>((v, x) => Console.WriteLine("Divide {0} + {1}", v, x))
                          .DoAfter<double, double>((v, x) => Console.WriteLine("Result is: {0}", v / x))
                          .Make();

            double a = 4.8, b = 12.6;
            //Act

            double result1 = cal.Divide(a, b);

            //Assert
            Assert.AreEqual(result1, a / b);

        }


        /**
         * 
         * O teste redefine um metodo através da API fluente adicionando comportamento as dois metodos.
         * 
         */
        [TestMethod]
        public void TestFluentWithTwoMethod()
        {

            //Arrange
            Calculater cal = new Calculater();
            cal = DynamicProxyFactory
                          .With<Calculater>()
                          .On<int, int,int>(cal.Add)
                          .DoBefore<int, int>( (v,x) => Console.WriteLine("Add {0} + {1}", v,x))
                          .On<int, int,int, int>(cal.Multiply)
                          .DoBefore<int, int, int>((v,x,y) => Console.WriteLine("Multiply {0} * {1} * {2}", v,x,y))
                          .Make();

            int a = 4, b = 8, c = 12;
            //Act

            int result1 = cal.Add(a, b);
            int result2 = cal.Multiply(a, b, c);


            //Assert
            Assert.AreEqual(result1, a + b);
            Assert.AreEqual(result2, a * b * c);

        }

        /**
         * 
         * O teste redefine um metodo através da API fluente adicionando comportamento a um dos metodos e
         * substituindo por completo o comportamento original de um dos metodos.
         * 
         */
        [TestMethod]
        public void TestFluentWithTwoMethodsAndReplace()
        {

            //Arrange
            Calculater cal = new Calculater();
            cal = DynamicProxyFactory
                          .With<Calculater>()
                          .On<int, int, int>(cal.Add)
                          .DoBefore<int, int>((v, x) => Console.WriteLine("Add {0} + {1}", v, x))
                          .On<int, int, int, int>(cal.Multiply)
                          .DoBefore<int, int, int>((v, x, y) => Console.WriteLine("Multiply {0} * {1} * {2}", v, x, y))
                          .Replace<int, int, int,int>((v, x,y) => { return (v % x); })
                          .Make();

            int a = 4, b = 8, c = 12;
            //Act

            int result1 = cal.Add(a, b);
            int result2 = cal.Multiply(a, b, c);


            //Assert
            Assert.AreEqual(result1, a + b);
            Assert.AreEqual(result2, a % b);

        }

        /**
         * 
         * O teste redefine um metodo através da API fluente com o metodo DoAfter afectando o resultado 
         * da chamado ao metodo.
         * 
         */ 
        [TestMethod]
        public void TestDoAfter()
        {

            //Arrange
            StringFormater f = new StringFormater();
            f = DynamicProxyFactory
                          .With<StringFormater>()
                          .On<String, String>(f.SetAndReturnString)
                          .DoAfter<String>(v => { f.StringToLowerAndAffect(v); })
                          .Make();
 
            //Act
            String txt = f.SetAndReturnString("Ambiente Virtuais de Execução");

            //Assert
            Assert.AreEqual(f.GetString(), txt.ToLower());

        }

        /**
        * 
        * O teste redefine um metodo através da API fluente usando o metodo DoBefore. Neste teste
        * caso o DoBefore não faça a devida afectação na string, quando o metodo do proxy fizesse a chamada
        * ao metodo redefinido, daria uma execpção pois este chamaria um metodo sobre um referencia null.
        * 
        */ 
        [TestMethod]
        public void TestDoBefore()
        {

            //Arrange
            StringFormater f = new StringFormater();
            f = DynamicProxyFactory
                          .With<StringFormater>()
                          .On<String,String>(f.StringToLowerAndAffect)
                          .DoBefore<String>(v => { f.SetAndReturnString(v); })
                          .Make();

            //Act
            String txt = f.StringToLowerAndAffect("Ambiente Virtuais de Execução");

            //Assert
            Assert.AreEqual(f.GetString(), txt.ToLower());

        }

    }

