﻿using System;
using System.Collections.Generic;

public class MethodStructure
{
    private Delegate before;
    private Delegate after;
    private Delegate onMethod;
    private Delegate replace;

    public MethodStructure()
    {
        this.before = null;
        this.after = null;
        this.onMethod = null;
        this.replace = null;
    }

    public Delegate GetBefore()
    {
        return this.before;
    }

    public void AddBefore(Delegate b)
    {
        before = Delegate.Combine(before, b);
    }

    public Delegate GetAfter()
    {
        return this.after;
    }

    public void AddAfter(Delegate a)
    {
        after = Delegate.Combine(after, a);
    }

    public Delegate GetOn()
    {
        return this.onMethod;
    }

    public void SetOn(Delegate on)
    {
        this.onMethod = on;
    }

    public Delegate GetReplace()
    {
        return this.replace;
    }

    public void SetReplace(Delegate rep)
    {
        this.replace = rep;
    }
}