﻿using System.Collections.Generic;

public interface IHelper
{
    string Operation(IDictionary<int, string> param);
}
