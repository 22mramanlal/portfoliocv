﻿namespace ClientInterface
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Clear_Register_Button = new System.Windows.Forms.Button();
            this.Register_Register_Button = new System.Windows.Forms.Button();
            this.Port_Register_TextBox = new System.Windows.Forms.TextBox();
            this.IP_Register_TextBox = new System.Windows.Forms.TextBox();
            this.FileName_Register_TextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Port_Unregister_TextBox = new System.Windows.Forms.TextBox();
            this.IP_Unregister_TextBox = new System.Windows.Forms.TextBox();
            this.FileName_Unregister_TextBox = new System.Windows.Forms.TextBox();
            this.Clear_Unregister_Button = new System.Windows.Forms.Button();
            this.Unregister_Unregister_Button = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ListFiles_ListView = new System.Windows.Forms.ListView();
            this.List_ListFiles_Button = new System.Windows.Forms.Button();
            this.CLear_ListFiles_Button = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.ListLocations_ListView = new System.Windows.Forms.ListView();
            this.label7 = new System.Windows.Forms.Label();
            this.Clear_ListLocations_Button = new System.Windows.Forms.Button();
            this.List_ListLocations_Button = new System.Windows.Forms.Button();
            this.FileName_ListLocations_TextBox = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(1, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(281, 249);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Clear_Register_Button);
            this.tabPage1.Controls.Add(this.Register_Register_Button);
            this.tabPage1.Controls.Add(this.Port_Register_TextBox);
            this.tabPage1.Controls.Add(this.IP_Register_TextBox);
            this.tabPage1.Controls.Add(this.FileName_Register_TextBox);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(273, 223);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Register";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // Clear_Register_Button
            // 
            this.Clear_Register_Button.Location = new System.Drawing.Point(153, 133);
            this.Clear_Register_Button.Name = "Clear_Register_Button";
            this.Clear_Register_Button.Size = new System.Drawing.Size(75, 23);
            this.Clear_Register_Button.TabIndex = 7;
            this.Clear_Register_Button.Text = "Clear";
            this.Clear_Register_Button.UseVisualStyleBackColor = true;
            this.Clear_Register_Button.Click += new System.EventHandler(this.Clear_Register_Button_Click);
            // 
            // Register_Register_Button
            // 
            this.Register_Register_Button.Location = new System.Drawing.Point(54, 133);
            this.Register_Register_Button.Name = "Register_Register_Button";
            this.Register_Register_Button.Size = new System.Drawing.Size(75, 23);
            this.Register_Register_Button.TabIndex = 6;
            this.Register_Register_Button.Text = "Register";
            this.Register_Register_Button.UseVisualStyleBackColor = true;
            this.Register_Register_Button.Click += new System.EventHandler(this.Register_Register_Button_Click);
            // 
            // Port_Register_TextBox
            // 
            this.Port_Register_TextBox.Location = new System.Drawing.Point(113, 81);
            this.Port_Register_TextBox.Name = "Port_Register_TextBox";
            this.Port_Register_TextBox.Size = new System.Drawing.Size(100, 20);
            this.Port_Register_TextBox.TabIndex = 5;
            // 
            // IP_Register_TextBox
            // 
            this.IP_Register_TextBox.Location = new System.Drawing.Point(113, 50);
            this.IP_Register_TextBox.Name = "IP_Register_TextBox";
            this.IP_Register_TextBox.Size = new System.Drawing.Size(100, 20);
            this.IP_Register_TextBox.TabIndex = 4;
            // 
            // FileName_Register_TextBox
            // 
            this.FileName_Register_TextBox.Location = new System.Drawing.Point(113, 22);
            this.FileName_Register_TextBox.Name = "FileName_Register_TextBox";
            this.FileName_Register_TextBox.Size = new System.Drawing.Size(100, 20);
            this.FileName_Register_TextBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Port";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "IP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "FileName";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.Port_Unregister_TextBox);
            this.tabPage2.Controls.Add(this.IP_Unregister_TextBox);
            this.tabPage2.Controls.Add(this.FileName_Unregister_TextBox);
            this.tabPage2.Controls.Add(this.Clear_Unregister_Button);
            this.tabPage2.Controls.Add(this.Unregister_Unregister_Button);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(273, 223);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Unregister";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Port";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "IP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "FileName";
            // 
            // Port_Unregister_TextBox
            // 
            this.Port_Unregister_TextBox.Location = new System.Drawing.Point(115, 98);
            this.Port_Unregister_TextBox.Name = "Port_Unregister_TextBox";
            this.Port_Unregister_TextBox.Size = new System.Drawing.Size(100, 20);
            this.Port_Unregister_TextBox.TabIndex = 4;
            // 
            // IP_Unregister_TextBox
            // 
            this.IP_Unregister_TextBox.Location = new System.Drawing.Point(115, 59);
            this.IP_Unregister_TextBox.Name = "IP_Unregister_TextBox";
            this.IP_Unregister_TextBox.Size = new System.Drawing.Size(100, 20);
            this.IP_Unregister_TextBox.TabIndex = 3;
            // 
            // FileName_Unregister_TextBox
            // 
            this.FileName_Unregister_TextBox.Location = new System.Drawing.Point(115, 21);
            this.FileName_Unregister_TextBox.Name = "FileName_Unregister_TextBox";
            this.FileName_Unregister_TextBox.Size = new System.Drawing.Size(100, 20);
            this.FileName_Unregister_TextBox.TabIndex = 2;
            // 
            // Clear_Unregister_Button
            // 
            this.Clear_Unregister_Button.Location = new System.Drawing.Point(153, 151);
            this.Clear_Unregister_Button.Name = "Clear_Unregister_Button";
            this.Clear_Unregister_Button.Size = new System.Drawing.Size(75, 23);
            this.Clear_Unregister_Button.TabIndex = 1;
            this.Clear_Unregister_Button.Text = "Clear";
            this.Clear_Unregister_Button.UseVisualStyleBackColor = true;
            this.Clear_Unregister_Button.Click += new System.EventHandler(this.Clear_Unregister_Button_Click);
            // 
            // Unregister_Unregister_Button
            // 
            this.Unregister_Unregister_Button.Location = new System.Drawing.Point(53, 151);
            this.Unregister_Unregister_Button.Name = "Unregister_Unregister_Button";
            this.Unregister_Unregister_Button.Size = new System.Drawing.Size(75, 23);
            this.Unregister_Unregister_Button.TabIndex = 0;
            this.Unregister_Unregister_Button.Text = "Unregister";
            this.Unregister_Unregister_Button.UseVisualStyleBackColor = true;
            this.Unregister_Unregister_Button.Click += new System.EventHandler(this.Unregister_Unregister_Button_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ListFiles_ListView);
            this.tabPage3.Controls.Add(this.List_ListFiles_Button);
            this.tabPage3.Controls.Add(this.CLear_ListFiles_Button);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(273, 223);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "List Files";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ListFiles_ListView
            // 
            this.ListFiles_ListView.Location = new System.Drawing.Point(28, 22);
            this.ListFiles_ListView.Name = "ListFiles_ListView";
            this.ListFiles_ListView.Size = new System.Drawing.Size(222, 107);
            this.ListFiles_ListView.TabIndex = 0;
            this.ListFiles_ListView.UseCompatibleStateImageBehavior = false;
            // 
            // List_ListFiles_Button
            // 
            this.List_ListFiles_Button.Location = new System.Drawing.Point(50, 161);
            this.List_ListFiles_Button.Name = "List_ListFiles_Button";
            this.List_ListFiles_Button.Size = new System.Drawing.Size(75, 23);
            this.List_ListFiles_Button.TabIndex = 1;
            this.List_ListFiles_Button.Text = "List";
            this.List_ListFiles_Button.UseVisualStyleBackColor = true;
            this.List_ListFiles_Button.Click += new System.EventHandler(this.List_ListFiles_Button_Click);
            // 
            // CLear_ListFiles_Button
            // 
            this.CLear_ListFiles_Button.Location = new System.Drawing.Point(158, 161);
            this.CLear_ListFiles_Button.Name = "CLear_ListFiles_Button";
            this.CLear_ListFiles_Button.Size = new System.Drawing.Size(75, 23);
            this.CLear_ListFiles_Button.TabIndex = 0;
            this.CLear_ListFiles_Button.Text = "Clear";
            this.CLear_ListFiles_Button.UseVisualStyleBackColor = true;
            this.CLear_ListFiles_Button.Click += new System.EventHandler(this.CLear_ListFiles_Button_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.ListLocations_ListView);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.Clear_ListLocations_Button);
            this.tabPage4.Controls.Add(this.List_ListLocations_Button);
            this.tabPage4.Controls.Add(this.FileName_ListLocations_TextBox);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(273, 223);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "List Locations";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // ListLocations_ListView
            // 
            this.ListLocations_ListView.Location = new System.Drawing.Point(46, 48);
            this.ListLocations_ListView.Name = "ListLocations_ListView";
            this.ListLocations_ListView.Size = new System.Drawing.Size(197, 97);
            this.ListLocations_ListView.TabIndex = 5;
            this.ListLocations_ListView.UseCompatibleStateImageBehavior = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "FileName";
            // 
            // Clear_ListLocations_Button
            // 
            this.Clear_ListLocations_Button.Location = new System.Drawing.Point(153, 174);
            this.Clear_ListLocations_Button.Name = "Clear_ListLocations_Button";
            this.Clear_ListLocations_Button.Size = new System.Drawing.Size(75, 23);
            this.Clear_ListLocations_Button.TabIndex = 3;
            this.Clear_ListLocations_Button.Text = "Clear";
            this.Clear_ListLocations_Button.UseVisualStyleBackColor = true;
            this.Clear_ListLocations_Button.Click += new System.EventHandler(this.Clear_ListLocations_Button_Click);
            // 
            // List_ListLocations_Button
            // 
            this.List_ListLocations_Button.Location = new System.Drawing.Point(63, 174);
            this.List_ListLocations_Button.Name = "List_ListLocations_Button";
            this.List_ListLocations_Button.Size = new System.Drawing.Size(75, 23);
            this.List_ListLocations_Button.TabIndex = 2;
            this.List_ListLocations_Button.Text = "List";
            this.List_ListLocations_Button.UseVisualStyleBackColor = true;
            this.List_ListLocations_Button.Click += new System.EventHandler(this.List_ListLocations_Button_Click);
            // 
            // FileName_ListLocations_TextBox
            // 
            this.FileName_ListLocations_TextBox.Location = new System.Drawing.Point(128, 22);
            this.FileName_ListLocations_TextBox.Name = "FileName_ListLocations_TextBox";
            this.FileName_ListLocations_TextBox.Size = new System.Drawing.Size(100, 20);
            this.FileName_ListLocations_TextBox.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Clear_Register_Button;
        private System.Windows.Forms.Button Register_Register_Button;
        private System.Windows.Forms.TextBox Port_Register_TextBox;
        private System.Windows.Forms.TextBox IP_Register_TextBox;
        private System.Windows.Forms.TextBox FileName_Register_TextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Port_Unregister_TextBox;
        private System.Windows.Forms.TextBox IP_Unregister_TextBox;
        private System.Windows.Forms.TextBox FileName_Unregister_TextBox;
        private System.Windows.Forms.Button Clear_Unregister_Button;
        private System.Windows.Forms.Button Unregister_Unregister_Button;
        private System.Windows.Forms.Button List_ListFiles_Button;
        private System.Windows.Forms.Button CLear_ListFiles_Button;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button Clear_ListLocations_Button;
        private System.Windows.Forms.Button List_ListLocations_Button;
        private System.Windows.Forms.TextBox FileName_ListLocations_TextBox;
        private System.Windows.Forms.ListView ListFiles_ListView;
        private System.Windows.Forms.ListView ListLocations_ListView;

    }
}

