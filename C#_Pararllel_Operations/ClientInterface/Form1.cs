﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientInterface
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ListFiles_ListView.View = View.List;
            ListLocations_ListView.View = View.List;
            RegisterForTest();
        }
       
        #region Buttons

        private void Register_Register_Button_Click(object sender, EventArgs e)
        {
            string[] args = { FileName_Register_TextBox.Text, IP_Register_TextBox.Text, Port_Register_TextBox.Text };
            ushort port;
            if (!ValidateArgs(args) || !TryParsePort(Port_Register_TextBox.Text, out port))
                MessageBox.Show("Invalid Paramenters");
            else
                Register(args[0], args [1], port);
        }

        private void Clear_Register_Button_Click(object sender, EventArgs e)
        {
            FileName_Register_TextBox.Clear();
            IP_Register_TextBox.Clear();
            Port_Register_TextBox.Clear();
        }

        private void Unregister_Unregister_Button_Click(object sender, EventArgs e)
        {
            string[] args = { FileName_Unregister_TextBox.Text, IP_Unregister_TextBox.Text, Port_Unregister_TextBox.Text };
            ushort port;
            if (!ValidateArgs(args) || !TryParsePort(Port_Unregister_TextBox.Text, out port))
                MessageBox.Show("Invalid Paramenters");
            else
                Unregister(args[0], args[1], port);
        }

        private void Clear_Unregister_Button_Click(object sender, EventArgs e)
        {
            FileName_Unregister_TextBox.Clear();
            IP_Unregister_TextBox.Clear();
            Port_Unregister_TextBox.Clear();
        }

        private void List_ListFiles_Button_Click(object sender, EventArgs e)
        {
            ListFiles_ListView.Clear();
            ListFiles();
        }

        private void CLear_ListFiles_Button_Click(object sender, EventArgs e)
        {
            ListFiles_ListView.Clear();
        }

        private void List_ListLocations_Button_Click(object sender, EventArgs e)
        {
            ListLocations_ListView.Clear();
            string[] args = { FileName_ListLocations_TextBox.Text };
            if(!ValidateArgs(args))
                MessageBox.Show("Invalid Paramenters");
            else
                ListLocations(args[0]);
        }

        private void Clear_ListLocations_Button_Click(object sender, EventArgs e)
        {
            FileName_ListLocations_TextBox.Clear();
            ListLocations_ListView.Clear();
        }

        #endregion


        #region Tasks

        private void Register(string fileName, string ip, ushort port)
        {
            Task.Factory
                .StartNew(() => Operations.Register(fileName, ip, port))
                .ContinueWith((t) => MessageBox.Show("Sucess") );
        }

        private void Unregister(string fileName, string ip, ushort port)
        {
            Task.Factory
                .StartNew(() => Operations.Unregister(fileName, ip, port))
                .ContinueWith((t) => MessageBox.Show("Sucess") );
        }

        private void ListFiles()
        {
            Task<List<string>>.Factory
                .StartNew(() => Operations.ListFiles())
                .ContinueWith((files) =>     
                                        files.Result.ForEach( file => ListFiles_ListView.BeginInvoke((Action)(() =>
                                                                                                                    ListFiles_ListView.Items.Add(new ListViewItem(file))
                                                                                                              )
                                                                                                     )
                                                             )
                                            
                              );
        }

        private void ListLocations(string fileName)
        {
            Task.Factory
                .StartNew(() => Operations.ListLocations(fileName))
                .ContinueWith((locations) => 
                                            locations.Result.ForEach( location => ListLocations_ListView.BeginInvoke((Action)(() =>
                                                                                                                                        ListLocations_ListView.Items.Add( new ListViewItem(location))
                                                                                                                              )
                                                                                                                     )
                                                                     )                
                                 );     
        }
        
        #endregion

        private void RegisterForTest()
        {
            string [] fileNames = { "xpto", "xpto", "ypto" };
            string [] ips = { "192.1.1.1", "192.1.1.2", "192.1.1.1" };
            ushort port = 5555;
            for (int i = 0; i < fileNames.Length; ++i)
                Operations.Register(fileNames[i], ips[i], port);
        }

        private static bool ValidateArgs(string [] inputs){

            foreach (string str in inputs)
                if (str == null || str.Equals(String.Empty))
                    return false;
            return true;

        }
        
        private static bool TryParsePort(string txt, out ushort port){
            return  ushort.TryParse(txt, out port);
        }

    }
}
