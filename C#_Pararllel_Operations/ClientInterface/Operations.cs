﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ClientInterface
{
    class Operations
    {

        private const int PORT = 8888;

        public static void Register(string fileName, string adress, ushort port)
        {
            using (TcpClient client = new TcpClient())
            {
                client.Connect(IPAddress.Loopback, PORT);

                StreamWriter output = new StreamWriter(client.GetStream());

                // Send request type line
                output.WriteLine("REGISTER");

                // Send message payload
                output.WriteLine(string.Format("{0}:{1}:{2}", fileName, adress, port));

                // Send message end mark
                output.WriteLine();

                output.Close();
                client.Close();
            }
        }

        public static void Unregister(string file, string adress, ushort port)
        {
            using (TcpClient client = new TcpClient())
            {
                client.Connect(IPAddress.Loopback, PORT);

                StreamWriter output = new StreamWriter(client.GetStream());

                // Send request type line
                output.WriteLine("UNREGISTER");
                // Send message payload
                output.WriteLine(string.Format("{0}:{1}:{2}", file, adress, port));
                // Send message end mark
                output.WriteLine();

                output.Close();
                client.Close();
            }
        }

        public static List<string> ListFiles()
        {
            List<string> ret = new List<string>();
            using (TcpClient socket = new TcpClient())
            {
                socket.Connect(IPAddress.Loopback, PORT);

                StreamWriter output = new StreamWriter(socket.GetStream());

                // Send request type line
                output.WriteLine("LIST_FILES");
                // Send message end mark and flush it
                output.WriteLine();
                output.Flush();

                // Read response
                string line;
                StreamReader input = new StreamReader(socket.GetStream());
                while ((line = input.ReadLine()) != null && line != string.Empty)
                    ret.Add(line);

                output.Close();
                socket.Close();
            }
            return ret;
        }

        public static List<string> ListLocations(string fileName)
        {
            List<string> ret = new List<string>();
            using (TcpClient socket = new TcpClient())
            {
                socket.Connect(IPAddress.Loopback, PORT);

                StreamWriter output = new StreamWriter(socket.GetStream());

                // Send request type line
                output.WriteLine("LIST_LOCATIONS");
                // Send message payload
                output.WriteLine(fileName);
                // Send message end mark and flush it
                output.WriteLine();
                output.Flush();

                // Read response
                string line;
                StreamReader input = new StreamReader(socket.GetStream());
                while ((line = input.ReadLine()) != null && line != string.Empty)
                    ret.Add(line);

                output.Close();
                socket.Close();
            }
            return ret;
        }

    }
}
