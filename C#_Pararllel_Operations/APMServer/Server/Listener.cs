﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Tracker
{
    /// <summary>
    /// This class instances are file tracking servers. They are responsible for accepting 
    /// and managing established TCP connections.
    /// </summary>
    public sealed class Listener
    {
        /// <summary>
        /// TCP port number in use.
        /// </summary>
        private readonly int portNumber;

        private readonly int MAXIMUM_REQUEST = 6;
        private volatile int count = 1;

        /// <summary> Initiates a tracking server instance.</summary>
        /// <param name="_portNumber"> The TCP port number to be used.</param>
        public Listener(int _portNumber) { portNumber = _portNumber; }

        /// <summary>
        ///	Server's main loop implementation.
        /// </summary>
        /// <param name="log"> The Logger instance to be used.</param>
        public void Run(Logger log)
        {
            TcpListener srv = null;
            try
            {
                srv = new TcpListener(IPAddress.Loopback, portNumber);
                srv.Start();
                log.LogMessage("Listener - Waiting for connection requests.");

                AsyncCallback processTCPClient = null;
                srv.BeginAcceptTcpClient(processTCPClient = (ar) =>
                {
                    log.LogMessage("Listener - Waiting for connection requests.");

                    if (count < MAXIMUM_REQUEST)
                    {
                        srv.BeginAcceptTcpClient(processTCPClient, null);
                        Interlocked.Increment(ref count);
                    }

                    TcpClient socket = null;
                    try
                    {
                        socket = srv.EndAcceptTcpClient(ar);
                        Interlocked.Decrement(ref count);

                        socket.LingerState = new LingerOption(true, 10);
                        log.LogMessage(String.Format("Listener - Connection established with {0}.",
                            socket.Client.RemoteEndPoint));
                        // Instantiating protocol handler and associate it to the current TCP connection
                        Handler protocolHandler = new Handler(socket, log);
                        // Synchronously process requests made through de current TCP connection
                        protocolHandler.Run();
                    }
                    catch (Exception e)
                    {
                        log.LogMessage(String.Format("Listener - Exception cougth {0}", e));
                        if (socket != null)
                            socket.Close();
                    }
                    Program.ShowInfo(Store.Instance);
                }, null);

                Console.ReadLine();
            }finally
            {
                log.LogMessage("Listener - Ending.");
                srv.Stop();
            }
        }

    }
}
