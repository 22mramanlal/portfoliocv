﻿using APMServer.Server;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Tracker
{
    /// <summary>
    /// Handles client requests.
    /// </summary>
    public sealed class Handler
    {

        #region Message handlers

        /// <summary>
        /// Data structure that supports message processing dispatch.
        /// </summary>
        private static readonly Dictionary<string, Action<Action, StreamReader, StreamWriter, Logger>> MESSAGE_HANDLERS;

        static Handler()
        {
            MESSAGE_HANDLERS = new Dictionary<string, Action<Action, StreamReader, StreamWriter, Logger>>();
            MESSAGE_HANDLERS["REGISTER"] = ProcessRegisterMessage;
            MESSAGE_HANDLERS["UNREGISTER"] = ProcessUnregisterMessage;
            MESSAGE_HANDLERS["LIST_FILES"] = ProcessListFilesMessage;
            MESSAGE_HANDLERS["LIST_LOCATIONS"] = ProcessListLocationsMessage;
        }

        /// <summary>
        /// Handles REGISTER messages.
        /// </summary>
        private static void ProcessRegisterMessage(Action callBack, StreamReader input, StreamWriter output, Logger log)
        {
            // Read message payload, terminated by an empty line. 
            // Each payload line has the following format
            // <filename>:<ipAddress>:<portNumber>
            AsyncCallback readCallBack = null;
            Utils.BeginReadLine(input, readCallBack = (ar) =>
            {
                if (ar.CompletedSynchronously)
                {
                    callBack();
                    return;
                }

                String result = Utils.EndReadLine(input, ar);

                if (result == null || result.Equals(string.Empty))
                {
                    callBack();
                    return;
                }

                Utils.BeginReadLine(input, readCallBack, null);

                string[] triple = result.Split(':');
                if (triple.Length != 3)
                {
                    log.LogMessage("Handler - Invalid REGISTER message.");
                    return;
                }
                IPAddress ipAddress = IPAddress.Parse(triple[1]);
                ushort port;
                if (!ushort.TryParse(triple[2], out port))
                {
                    log.LogMessage("Handler - Invalid REGISTER message.");
                    return;
                }
                Store.Instance.Register(triple[0], new IPEndPoint(ipAddress, port));

            }, null);
            // This request message does not have a corresponding response message, hence, 
            // nothing is sent to the client.
        }

        /// <summary>
        /// Handles UNREGISTER messages.
        /// </summary>
        private static void ProcessUnregisterMessage(Action callBack, StreamReader input, StreamWriter output, Logger log)
        {
            // Read message payload, terminated by an empty line. 
            // Each payload line has the following format
            // <filename>:<ipAddress>:<portNumber>
            AsyncCallback readCallBack = null;
            Utils.BeginReadLine(input, readCallBack = (ar) =>
            {
                if (ar.CompletedSynchronously)
                {
                    callBack();
                    return;
                }

                String result = Utils.EndReadLine(input, ar);

                if (result == null || result.Equals(string.Empty))
                {
                    callBack();
                    return;
                }

                Utils.BeginReadLine(input, readCallBack, null);

                string[] triple = result.Split(':');
                if (triple.Length != 3)
                {
                    log.LogMessage("Handler - Invalid UNREGISTER message.");
                    return;
                }
                IPAddress ipAddress = IPAddress.Parse(triple[1]);
                ushort port;
                if (!ushort.TryParse(triple[2], out port))
                {
                    log.LogMessage("Handler - Invalid UNREGISTER message.");
                    return;
                }
                Store.Instance.Unregister(triple[0], new IPEndPoint(ipAddress, port));

            }, null);
            // This request message does not have a corresponding response message, hence, 
            // nothing is sent to the client.
        }

        /// <summary>
        /// Handles LIST_FILES messages.
        /// </summary>
        private static void ProcessListFilesMessage(Action callBack, StreamReader input, StreamWriter output, Logger log)
        {
            // Request message does not have a payload.
            // Read end message mark (empty line)
            Utils.BeginReadLine(input, (ar) =>
            {
                if (ar.CompletedSynchronously)
                {
                    callBack();
                    return;
                }

                String aux = Utils.EndReadLine(input, ar);
                string[] trackedFiles = Store.Instance.GetTrackedFiles();

                if (trackedFiles.Length == 0)
                {
                    Utils.BeginWriteLine(output, String.Empty, (arr) => { Utils.EndWriteLine(output, arr); callBack(); }, null);
                    return;
                }

                // Send response message. 
                // The message is composed of multiple lines and is terminated by an empty one.
                // Each line contains a name of a tracked file.
                foreach (string file in trackedFiles)
                    Utils.BeginWriteLine(output, file, (arr) => Utils.EndWriteLine(output, arr), null);

                // End response and flush it.
                Utils.BeginWriteLine(output, String.Empty, (arr) => { Utils.EndWriteLine(output, arr); output.Flush(); callBack(); }, null);
            }, null);
        }

        /// <summary>
        /// Handles LIST_LOCATIONS messages.
        /// </summary>
        private static void ProcessListLocationsMessage(Action callBack, StreamReader input, StreamWriter output, Logger log)
        {
            // Request message payload is composed of a single line containing the file name.
            // The end of the message's payload is marked with an empty line
            Utils.BeginReadLine(input, (arFile) =>
            {
                if (arFile.CompletedSynchronously)
                {
                    callBack();
                    return;
                }
                String result = Utils.EndReadLine(input, arFile);
                Utils.BeginReadLine(input, (arLine) =>
                {
                    if (arLine.CompletedSynchronously)
                    {
                        callBack();
                        return;
                    }
                    Utils.EndReadLine(input, arLine);
                    IPEndPoint[] fileLocations = Store.Instance.GetFileLocations(result);

                    if (fileLocations.Length == 0)
                    {
                        Utils.BeginWriteLine(output, String.Empty, (ar) => { Utils.EndWriteLine(output, ar); callBack(); }, null);
                        return;
                    }

                    // Send response message. 
                    // The message is composed of multiple lines and is terminated by an empty one.
                    // Each line has the following format
                    // <ipAddress>:<portNumber>
                    foreach (IPEndPoint endpoint in fileLocations)
                        Utils.BeginWriteLine(output, string.Format("{0}:{1}", endpoint.Address, endpoint.Port), (ar) => Utils.EndWriteLine(output, ar), null);

                    // End response and flush it.
                    Utils.BeginWriteLine(output, String.Empty, (ar) => { Utils.EndWriteLine(output, ar); output.Flush(); callBack(); }, null);
                }, null);
            }, null);
        }
        
        #endregion

        /// <summary>
        /// The handler's input (from the TCP connection)
        /// </summary>
        private readonly StreamReader input;

        /// <summary>
        /// The handler's output (to the TCP connection)
        /// </summary>
        private readonly StreamWriter output;

        /// <summary>
        /// The Logger instance to be used.
        /// </summary>
        private readonly Logger log;

        private readonly Stream stream;
        private readonly TcpClient connection;

        /// <summary>
        ///	Initiates an instance with the given parameters.
        /// </summary>
        /// <param name="connection">The TCP connection to be used.</param>
        /// <param name="log">the Logger instance to be used.</param>
        public Handler(TcpClient connection, Logger log)
        {
            this.log = log;
            Stream s = connection.GetStream();
            this.output = new StreamWriter(s);
            this.input = new StreamReader(s);
        }

        /// <summary>
        /// Performs request servicing.
        /// </summary>
        public void Run()
        {
            Utils.BeginReadLine(input, (ar) =>
            {
               
               String str = Utils.EndReadLine(input, ar);
         
               if (str == null || str.Equals(string.Empty))
               {
                 //  Close();
                   return;
               }

               str = str.ToUpper();
               if (!MESSAGE_HANDLERS.ContainsKey(str))
               {
                   Close();
               }
               else
               {
                   MESSAGE_HANDLERS[str](Run, input, output, log);
               }

            },null); 
        }

        private void Close()
        {
            try
            {
                if (input != null)
                    input.Close();
                if (output != null)
                    output.Close();
            }catch (Exception)
            {
                log.LogMessage("Exception to close stream.");
            }

            try
            {
                if(connection != null)
                    connection.Close();
            }
            catch (Exception)
            {
                log.LogMessage("Exception to close connection.");
            }

        }

    }
}
