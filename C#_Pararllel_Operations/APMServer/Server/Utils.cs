﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace APMServer.Server
{
    class Utils
    {

        public class GenericAsyncResult<R> : IAsyncResult
        {

            private volatile bool isCompleted;
            private volatile ManualResetEvent waitEvent;
            private readonly AsyncCallback userCallback;
            private readonly object userState;
            private R result;
            private Exception error;


            public GenericAsyncResult(AsyncCallback userCallback, object userState)
            {
                this.userCallback = userCallback;
                this.userState = userState;
            }

            //
            // Complete the underlying asynchronous operation.
            //

            public void OnComplete(R result, Exception error)
            {
                this.result = result;
                this.error = error;
                isCompleted = true;

                Thread.MemoryBarrier();		// Prevent the release followed by acquire hazard! 

                if (waitEvent != null)
                {
                    try
                    {
                        waitEvent.Set();
                        // We can get ObjectDisposedExcption due a benign race, so ignore it!
                    }
                    catch (ObjectDisposedException) { }
                }
                if (userCallback != null)
                    userCallback(this);
            }

            //
            // Return the asynchronous operation's result (called once by EndXxx())
            //

#pragma warning disable 420

            public R Result
            {
                get
                {
                    if (!isCompleted)
                    {
                        AsyncWaitHandle.WaitOne();
                    }
                    if (waitEvent != null)
                        waitEvent.Close();
                    if (error != null)
                        throw error;
                    return result;
                }
            }

            //
            // IAsyncResult interface's implementation.
            //

            public bool IsCompleted { get { return isCompleted; } }

            //
            // This implementation never completes synchronously.
            //

            public bool CompletedSynchronously { get { return false; } }

            public Object AsyncState { get { return userState; } }

            public WaitHandle AsyncWaitHandle
            {
                get
                {
                    if (waitEvent == null)
                    {
                        bool s = isCompleted;
                        ManualResetEvent done = new ManualResetEvent(s);
                        if (Interlocked.CompareExchange(ref waitEvent, done, null) == null)
                        {
                            if (s != isCompleted)
                            {
                                done.Set();
                            }
                        }
                        else
                        {
                            done.Dispose();
                        }
                    }
                    return waitEvent;
                }
            }
#pragma warning restore 420
        }

        public static IAsyncResult BeginReadLine(StreamReader stream, AsyncCallback callBack, Object state)
        {
            AsyncCallback read = null;
            AsyncCallback finalRead = null;
            byte [] arr = new byte[1];
            String line = string.Empty;
            String aux = string.Empty;

            GenericAsyncResult<String> gar = new GenericAsyncResult<String>(callBack, state);

            read = (ar) =>
            {
                if (stream.BaseStream.EndRead(ar) == 0)
                {
                    gar.OnComplete(line, null);
                    return;
                }
                aux = Encoding.UTF8.GetString(arr);
                
                if (aux.Equals("\r"))
                    stream.BaseStream.BeginRead(arr, 0, 1, finalRead = (ary) =>
                                                                    {
                                                                        if (stream.BaseStream.EndRead(ary) == 0)
                                                                        {
                                                                            gar.OnComplete(line, null);
                                                                            return;
                                                                        }
                                                                        aux = Encoding.UTF8.GetString(arr);
                                                                        if (aux.Equals("\n"))
                                                                            gar.OnComplete(line, null);
                                                                        else
                                                                        {
                                                                            line += "\r" + aux;
                                                                            stream.BaseStream.BeginRead(arr, 0, 1, read, null);
                                                                        }
                                                                    }
                     ,null);
                else
                {
                    line += aux;
                    stream.BaseStream.BeginRead(arr, 0, 1, read, null);
                }
            };
            
            stream.BaseStream.BeginRead(arr, 0, 1, read, null);

            return gar;
        }
        public static String EndReadLine(StreamReader stream, IAsyncResult ar)
        {
            return (String)((GenericAsyncResult<String>)ar).Result;
        }

        public static IAsyncResult BeginWriteLine(StreamWriter stream, String str, AsyncCallback callBack, Object state)
        {
            GenericAsyncResult<String> gar = new GenericAsyncResult<String>(callBack, str);

            byte[] bytes = Encoding.UTF8.GetBytes(str + Environment.NewLine);

            stream.BaseStream.BeginWrite(bytes, 0, bytes.Length, (ar) =>
                                                                {
                                                                    try{
                                                                        stream.BaseStream.EndWrite(ar);
                                                                    }catch (Exception){
                                                                        Console.WriteLine("Erooo ao escrever!");
                                                                    }
                                                                    finally
                                                                    {
                                                                        gar.OnComplete(str, null);
                                                                    }
                                                                }
            , null);
           
            return gar;
        }
        public static String EndWriteLine(StreamWriter stream, IAsyncResult ar)
        {
            return ((GenericAsyncResult<String>)ar).Result;
        }
    
    }
}
