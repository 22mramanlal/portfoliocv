﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Tracker
{
    /// <summary>
    /// This class instances are file tracking servers. They are responsible for accepting 
    /// and managing established TCP connections.
    /// </summary>
    public sealed class Listener
    {
        /// <summary>
        /// TCP port number in use.
        /// </summary>
        private readonly int portNumber;

        /// <summary> Initiates a tracking server instance.</summary>
        /// <param name="_portNumber"> The TCP port number to be used.</param>
        public Listener(int _portNumber) { portNumber = _portNumber; }

        private readonly int MAXIMUM_REQUEST = 6;
        private volatile int count = 1;

        /// <summary>
        ///	Server's main loop implementation.
        /// </summary>
        /// <param name="log"> The Logger instance to be used.</param>
        public void Run(Logger log)
        {
            TcpListener srv = null;
            try
            {
                srv = new TcpListener(IPAddress.Loopback, portNumber);
                srv.Start();
                log.LogMessage("Listener - Waiting for connection requests.");

                Action<Task<TcpClient>> processTCPClient = null;
                srv.AcceptTcpClientAsync().ContinueWith(processTCPClient = (t) =>
                {
                    log.LogMessage("Listener - Waiting for connection requests.");

                    if (count < MAXIMUM_REQUEST)
                    {
                        srv.AcceptTcpClientAsync().ContinueWith(processTCPClient);
                        Interlocked.Increment(ref count);
                    }

                    TcpClient socket = null;
                    try
                    {
                        socket = t.Result;
                        Interlocked.Decrement(ref count);

                        socket.LingerState = new LingerOption(true, 10);
                        log.LogMessage(String.Format("Listener - Connection established with {0}.",
                            socket.Client.RemoteEndPoint));
                        // Instantiating protocol handler and associate it to the current TCP connection
                        Handler protocolHandler = new Handler(socket, log);
                        // Synchronously process requests made through de current TCP connection
                        protocolHandler.Run();
                    }
                    catch (Exception e)
                    {
                        log.LogMessage(String.Format("Listener - Exception cougth {0}", e));
                        if (socket != null)
                            socket.Close();
                    }
                    Program.ShowInfo(Store.Instance);
                });

                Console.ReadLine();
            }
            finally
            {
                log.LogMessage("Listener - Ending.");
                srv.Stop();
            }
        }

    }
}
