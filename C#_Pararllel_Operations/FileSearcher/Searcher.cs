﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileSearcher
{

    class SearcherArgs
    {
        public string fileExtension;
        public string pattern;
        public CancellationToken cancellation;
        public Action<string, int, int> actualizeResult;

        public SearcherArgs(string fileExtension, string pattern, CancellationToken cancellation, Action<string, int, int> actualizeResult)
        {
            this.fileExtension = fileExtension;
            this.pattern = pattern;
            this.cancellation = cancellation;
            this.actualizeResult = actualizeResult;
        }

    }

    public class Searcher
    {

        public static Task<Results> FindFilesAsync(String path, String fileExtension, String pattern, CancellationToken cancellation, Action<string, int,int> actualizeResult)
        {
            Results result = new Results();
            SearcherArgs args = new SearcherArgs(fileExtension, pattern, cancellation, actualizeResult);

            return Task.Factory.StartNew(() => SearchDirectoryAsync(path, args, result), cancellation)
                        .ContinueWith((task) => result);
        }

        private static void SearchDirectoryAsync(String path, SearcherArgs args, Results result)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            IEnumerable<DirectoryInfo> folders;
            try
            {
                folders = directoryInfo.EnumerateDirectories();
            }catch (UnauthorizedAccessException)
            {
                return;
            }

            foreach(DirectoryInfo folder in folders){
                if (args.cancellation.IsCancellationRequested)
                    return;
                
                Task.Factory.StartNew( (t) => SearchDirectoryAsync(folder.FullName, args, result) , args.cancellation, TaskCreationOptions.AttachedToParent);
            }

            IEnumerable<FileInfo> files = directoryInfo.EnumerateFiles();

            SearchFilesAsync(files, args, result);
        }

        private static void SearchFilesAsync(IEnumerable<FileInfo> files, SearcherArgs args, Results result)
        {
            foreach (FileInfo file in files)
            {
                if (args.cancellation.IsCancellationRequested)
                    return;

                result.IncrementFilesFound();

                if (file.Extension.Equals(args.fileExtension))
                {
                    result.IncrementFilesWithExtension();
                    Task.Factory.StartNew((t) => ReadFile(file, args, result), args.cancellation, TaskCreationOptions.AttachedToParent);
                }

            }
        }

        private static void ReadFile(FileInfo file, SearcherArgs args, Results result)
        {
            if (args.cancellation.IsCancellationRequested)
                return;
            
            byte[] b;
            FileStream fileStream = null;
            try
            {
                fileStream = file.OpenRead();
                b = new byte[fileStream.Length];
                fileStream.Read(b, 0, (int)fileStream.Length);  
            }
            catch (IOException)
            {
                return;
            }
            finally
            {
                if(fileStream != null)
                    fileStream.Close();
            }

            String txt = Encoding.UTF8.GetString(b, 0, b.Length);
            if (txt.Contains(args.pattern))
            {
                result.AddFile(file.FullName);
                args.actualizeResult(file.FullName, result.countFilesFound, result.countFilesWithExtension);
            }
        }

    }
}
