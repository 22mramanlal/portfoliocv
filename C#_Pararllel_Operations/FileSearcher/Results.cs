﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileSearcher
{
    public class Results
    {
        public List<String> fileNames;
        public int countFilesFound;
        public int countFilesWithExtension;

        public Results()
        {
            this.fileNames = new List<String>();
            countFilesFound = 0;
            countFilesWithExtension = 0;
        }

        public void IncrementFilesFound()
        {
            Interlocked.Increment(ref countFilesFound);
        }

        public void IncrementFilesWithExtension()
        {
            Interlocked.Increment(ref countFilesWithExtension);
        }

        public void AddFile(String fileName)
        {
            lock (fileNames)
            {
                fileNames.Add(fileName);
            }
        }
    }
}
