﻿using FileSearcher;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSearcherInterface
{
    public partial class Form1 : Form
    {
        private CancellationTokenSource cancellation;

        public Form1()
        {
            InitializeComponent();
        }

        #region Buttons

        private void ClearForm_Button_Click(object sender, EventArgs e)
        {
            Directory_TextBox.Clear();
            FileExtension_TextBox.Clear();
            Pattern_TextBox.Clear();
        }

        private void ClearResult_Button_Click(object sender, EventArgs e)
        {
            Result_ListView.Clear();
            FilesFound_Label.Text = String.Empty;
            FilesWithExtension_Label.Text = String.Empty;
        }

        private void Search_Button_Click(object sender, EventArgs e)
        {
            if (!ValidateArgs())
                MessageBox.Show("Invalid Paramenters");
            else
            {
                cancellation = new CancellationTokenSource();
                Search(Directory_TextBox.Text, FileExtension_TextBox.Text, Pattern_TextBox.Text, cancellation);
            }
        }

        private void Cancel_Button_Click(object sender, EventArgs e)
        {
            cancellation.Cancel();
            MessageBox.Show("Cancelated");
        }

        #endregion

        #region Tasks

        private void Search(string directory, string extension, string pattern, CancellationTokenSource cancellation)
        {
            Result_ListView.BeginInvoke((Action)(() => Result_ListView.Clear()));
            MessageBox.Show("Start");
            Searcher.FindFilesAsync(directory, extension, pattern, cancellation.Token, actualizeResults)
                .ContinueWith((t) => { ShowResult(t); MessageBox.Show("Complete"); });
        }
                    
        #endregion

        #region Utils

        private bool ValidateArgs()
        {
            return !Directory_TextBox.Text.Equals(String.Empty) &&
                   !FileExtension_TextBox.Text.Equals(String.Empty) &&
                   !Pattern_TextBox.Text.Equals(String.Empty);
        }

        private void ShowResult(Task<Results> t)
        {
            Results result = t.Result;
           
            FilesFound_Label.BeginInvoke((Action)(() => FilesFound_Label.Text = result.countFilesFound.ToString()));
            FilesWithExtension_Label.BeginInvoke((Action)(() => FilesWithExtension_Label.Text = result.countFilesWithExtension.ToString()));
        }

        private void actualizeResults(string fileName, int countFilesFound, int countFilesWithExtension)
        {
            Result_ListView.BeginInvoke((Action)(() =>
                                                       Result_ListView.Items.Add(new ListViewItem(fileName))
                                                 )
                                       );
                                       
            FilesFound_Label.BeginInvoke((Action)(() => FilesFound_Label.Text = countFilesFound.ToString()));
            FilesWithExtension_Label.BeginInvoke((Action)(() => FilesWithExtension_Label.Text = countFilesWithExtension.ToString()));
        }

        #endregion
    }
}
