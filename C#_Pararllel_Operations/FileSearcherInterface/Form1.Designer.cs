﻿namespace FileSearcherInterface
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Directory_TextBox = new System.Windows.Forms.TextBox();
            this.FileExtension_TextBox = new System.Windows.Forms.TextBox();
            this.Pattern_TextBox = new System.Windows.Forms.TextBox();
            this.Result_ListView = new System.Windows.Forms.ListView();
            this.Search_Button = new System.Windows.Forms.Button();
            this.ClearResult_Button = new System.Windows.Forms.Button();
            this.ClearForm_Button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Cancel_Button = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.FilesFound_Label = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.FilesWithExtension_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Directory_TextBox
            // 
            this.Directory_TextBox.Location = new System.Drawing.Point(97, 16);
            this.Directory_TextBox.Name = "Directory_TextBox";
            this.Directory_TextBox.Size = new System.Drawing.Size(167, 20);
            this.Directory_TextBox.TabIndex = 0;
            // 
            // FileExtension_TextBox
            // 
            this.FileExtension_TextBox.Location = new System.Drawing.Point(97, 51);
            this.FileExtension_TextBox.Name = "FileExtension_TextBox";
            this.FileExtension_TextBox.Size = new System.Drawing.Size(167, 20);
            this.FileExtension_TextBox.TabIndex = 1;
            // 
            // Pattern_TextBox
            // 
            this.Pattern_TextBox.Location = new System.Drawing.Point(97, 82);
            this.Pattern_TextBox.Name = "Pattern_TextBox";
            this.Pattern_TextBox.Size = new System.Drawing.Size(167, 20);
            this.Pattern_TextBox.TabIndex = 2;
            // 
            // Result_ListView
            // 
            this.Result_ListView.Location = new System.Drawing.Point(22, 125);
            this.Result_ListView.Name = "Result_ListView";
            this.Result_ListView.Size = new System.Drawing.Size(574, 140);
            this.Result_ListView.TabIndex = 0;
            this.Result_ListView.UseCompatibleStateImageBehavior = false;
            this.Result_ListView.View = System.Windows.Forms.View.List;
            // 
            // Search_Button
            // 
            this.Search_Button.Location = new System.Drawing.Point(308, 54);
            this.Search_Button.Name = "Search_Button";
            this.Search_Button.Size = new System.Drawing.Size(75, 23);
            this.Search_Button.TabIndex = 4;
            this.Search_Button.Text = "Search";
            this.Search_Button.UseVisualStyleBackColor = true;
            this.Search_Button.Click += new System.EventHandler(this.Search_Button_Click);
            // 
            // ClearResult_Button
            // 
            this.ClearResult_Button.Location = new System.Drawing.Point(409, 19);
            this.ClearResult_Button.Name = "ClearResult_Button";
            this.ClearResult_Button.Size = new System.Drawing.Size(75, 23);
            this.ClearResult_Button.TabIndex = 5;
            this.ClearResult_Button.Text = "Clear Result";
            this.ClearResult_Button.UseVisualStyleBackColor = true;
            this.ClearResult_Button.Click += new System.EventHandler(this.ClearResult_Button_Click);
            // 
            // ClearForm_Button
            // 
            this.ClearForm_Button.Location = new System.Drawing.Point(308, 19);
            this.ClearForm_Button.Name = "ClearForm_Button";
            this.ClearForm_Button.Size = new System.Drawing.Size(75, 23);
            this.ClearForm_Button.TabIndex = 6;
            this.ClearForm_Button.Text = "Clear Form";
            this.ClearForm_Button.UseVisualStyleBackColor = true;
            this.ClearForm_Button.Click += new System.EventHandler(this.ClearForm_Button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Directory";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "File Extension";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Pattern";
            // 
            // Cancel_Button
            // 
            this.Cancel_Button.Location = new System.Drawing.Point(409, 54);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(75, 23);
            this.Cancel_Button.TabIndex = 10;
            this.Cancel_Button.Text = "Cancel";
            this.Cancel_Button.UseVisualStyleBackColor = true;
            this.Cancel_Button.Click += new System.EventHandler(this.Cancel_Button_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(71, 316);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Files Found:";
            // 
            // FilesFound_Label
            // 
            this.FilesFound_Label.AutoSize = true;
            this.FilesFound_Label.Location = new System.Drawing.Point(141, 316);
            this.FilesFound_Label.Name = "FilesFound_Label";
            this.FilesFound_Label.Size = new System.Drawing.Size(0, 13);
            this.FilesFound_Label.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(329, 316);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Files With Extension:";
            // 
            // FilesWithExtension_Label
            // 
            this.FilesWithExtension_Label.AutoSize = true;
            this.FilesWithExtension_Label.Location = new System.Drawing.Point(440, 316);
            this.FilesWithExtension_Label.Name = "FilesWithExtension_Label";
            this.FilesWithExtension_Label.Size = new System.Drawing.Size(0, 13);
            this.FilesWithExtension_Label.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 352);
            this.Controls.Add(this.FilesWithExtension_Label);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.FilesFound_Label);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Cancel_Button);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ClearForm_Button);
            this.Controls.Add(this.ClearResult_Button);
            this.Controls.Add(this.Search_Button);
            this.Controls.Add(this.Result_ListView);
            this.Controls.Add(this.Pattern_TextBox);
            this.Controls.Add(this.FileExtension_TextBox);
            this.Controls.Add(this.Directory_TextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Directory_TextBox;
        private System.Windows.Forms.TextBox FileExtension_TextBox;
        private System.Windows.Forms.TextBox Pattern_TextBox;
        private System.Windows.Forms.ListView Result_ListView;
        private System.Windows.Forms.Button Search_Button;
        private System.Windows.Forms.Button ClearResult_Button;
        private System.Windows.Forms.Button ClearForm_Button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Cancel_Button;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label FilesFound_Label;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label FilesWithExtension_Label;
    }
}

