#include <curl/curl.h>


//Pedido Http atraves de um url
int http_get_file(const char * url, const char * filename){
	
	if( url == '\0' || *filename =='\0') 
		return 0;
	
	CURL *curl;
	FILE *file;

	curl_global_init(CURL_GLOBAL_DEFAULT);
  
	curl = curl_easy_init(); 
	
	if(!curl)
		return 0;		
		
	curl_easy_setopt(curl, CURLOPT_URL, url);

	file = fopen(filename,"wb");
 
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
 
	if(curl_easy_perform(curl)!= CURLE_OK){ 
		printf("Error on Writing on file.\n");
		return 0;
	}

	fclose(file);

	curl_easy_cleanup(curl);
	
	curl_global_cleanup();
 
  return 1;
}
