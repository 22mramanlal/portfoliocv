#include <stdio.h>
#include <curl/curl.h>
#include <jansson.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h> 
#include <sys/stat.h>

#include "workInfo.h"
#include "savethothwork.h"


struct Arguments * pArg; //Estrutura dos Argumentos Recebidos

void getParameter( char ** argv){//Prencimento da Estrutura dos Argumentos Recebidos
	struct Arguments * p = malloc(sizeof(p));
	p->disciplina = argv[1];
	p->turma = argv[2];
	p->ano = argv[3];
	pArg = p;
}

//Gera a Aplicação
int application (int argc, char ** argv){
	
	if(argc != 4){
		printf("Argumentos Insuficientes\n");
		return -1;
	}
	

	getParameter(argv);
	
	const char * url = getAllClassesUrl();
	
	url = getClassUrl(url);
	
	if(url == NULL){
	 free(((char *)url));
	 return -1;
	}
	
	mkdir("Result",0777);
	
	url = getWorksPageUrl(url);

	Pdata data = getAllWorksDetails(url);

	free(((char *)url));
	
	writeResultFile(data);

	free(data->wd);
	
	return 0;
}


int main(int argc, char ** argv){

   return application(argc, argv); 
}




	

