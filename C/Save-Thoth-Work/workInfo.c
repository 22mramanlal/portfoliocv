#include <stdio.h>
#include <curl/curl.h>
#include <jansson.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h> 


#include "workInfo.h"

extern int http_get_file(const char * url, const char * filename);
extern json_t * http_get_json(const char * url, int * retcod);
extern struct Arguments * pArg;
extern const char * getAnexosUrl(char * id);

//Malloc da Estrutura dos itens dos trabalhos
PWorkData mallocStruct (){
	
	PWorkData wd;
	wd = malloc( 9 * sizeof(wd));
	wd->sigla = (char*)malloc(sizeof(wd->sigla));
	wd->titulo = (char*)malloc(sizeof(wd->titulo));
	wd->dPublicacao = (char*)malloc(sizeof(wd->dPublicacao));
	wd->dLimite = (char*)malloc(sizeof(wd->dLimite));
	wd->grupoIndividual = (char*)malloc(sizeof(wd->grupoIndividual));
	wd->relatorio = (char*)malloc(sizeof(wd->relatorio));
	wd->anexo = (char*)malloc(sizeof(wd->anexo));
	return wd;
}

//Retorna o argumento recebido ja identado
const char * getPath(){
	
	int len =  strlen(pArg->disciplina) + strlen(pArg->turma) + strlen(pArg->ano) + 20;
	char * class  = malloc(sizeof(char) * len);
	strcat(class,pArg->disciplina);
	strcat(class," / ");
	strcat(class,pArg->turma);
	strcat(class," / ");
	strcat(class,pArg->ano);

	return class;
	
}

//Retorna o url de todas as turmas
const char * getAllClassesUrl(){

	const char * url = "http://thoth.cc.e.ipl.pt/api/v1";
	int errorCode;
	
	json_t * j = (json_t *) http_get_json(url,&errorCode);
	
	json_t * aux = json_object_get(j, "_links");
	aux = json_object_get(aux, "classes");
	
	free(j);
	
	const char * result = malloc(sizeof(result));
	result = json_string_value(aux);
	
	return result;
}

//Retorna o url da turma
const char * getClassUrl(const char * url){
	
	int errorCode;
	json_t * j = (json_t *) http_get_json(url,&errorCode);

	json_t * aux1;
	
	aux1 = json_object_get(j, "classes");
	free(j);
	
	int i;
	int size = json_array_size(aux1);
	json_t * aux2 = aux1;
	json_t * aux3;
	const char * rr;
	const char * path = getPath();
	int confirmClass = 0;
	for(i = 0; i< size; ++i){
		
		aux2 =  json_array_get(aux1, i);
		aux3 = json_object_get(aux2, "fullName");
		rr = json_string_value(aux3);
		
		if(strcmp(rr, path) == 0){
			 confirmClass = 1;
			 break;
		}
	}		
	
	if(confirmClass == 0){
		printf("Turma Invalida\n");
		return NULL;
	}
	
	free((char*)path);
	aux1 = json_object_get(aux2, "_links");
	
	aux1 = json_object_get(aux1, "self");
	
	const char * result = malloc(sizeof(result));
	result = json_string_value(aux1);
	
	return result;
}

//Retorna o url dos trabalhos
const char * getWorksPageUrl(const char * url){
	int errorCode;
	json_t * aux;
	
	aux = (json_t *) http_get_json(url,&errorCode);

	 aux = json_object_get(aux, "_links");
     aux = json_object_get(aux, "workItems");
     
	const char * result = malloc(sizeof(result));
	result = json_string_value(aux);
	
	return result;
}

//Retorna o nome do documento e guarda o ficheiro na subdirectoria
const char * getDocumento(const char * url, const char * filename){
	
	size_t size = strlen(url);
	char * urlAux = malloc(sizeof(char) * size + 11);
	strcpy(urlAux,url); 
	strcat(urlAux, "/document");
	
	size = strlen(url);
	char * filenameAux = malloc(sizeof(char) * size + 11);
	strcpy(filenameAux,"Result/"); 
	strcat(filenameAux, filename);
	
	if (http_get_file(urlAux, filenameAux) == 0)
		return NULL;

	free((char*)urlAux);
	free((char*)filenameAux);
	return filename;
}

//Retorna o nome do anexo e guarda o ficheiro na subdirectoria
const char * getAnexo(const char * url, const char * filename){

	size_t size = strlen(url);
	char * urlAux =  malloc(sizeof(char) * size + 13);
	strcpy(urlAux,url);
	strcat(urlAux, "/attachment");
	
	size = strlen(url);
	char * filenameAux = malloc(sizeof(char) * size + 11);
	strcpy(filenameAux,"Result/"); 
	strcat(filenameAux, filename);
	
	if(http_get_file(urlAux, filenameAux) == 0)
		return NULL;
	
	free((char*)urlAux);
	free((char*)filenameAux);
	return filename;
}

//Preenche a estrutura dos itens do trabalho e retorna
PWorkData getWorkInfo(json_t * obj, const char * r3){
	
	PWorkData wd = mallocStruct();
	json_t * aux;
	json_t * tTrue = json_string("true");
	json_t * tFalse = json_string("false");
	
	aux = json_object_get(obj, "acronym");
	wd->sigla = json_string_value(aux);
	
	aux = json_object_get(obj, "title");
	wd->titulo = json_string_value(aux);
	
	aux = json_object_get(obj, "startDate");
	wd->dPublicacao = json_string_value(aux);
	
	aux = json_object_get(obj, "dueDate");
	wd->dLimite = json_string_value(aux);
	
	aux = json_object_get(obj, "requiresGroupSubmission");
	wd->grupoIndividual = (json_is_true(aux) != 0) ? json_string_value(tTrue): json_string_value(tFalse);
	
	aux = json_object_get(obj, "reportUploadInfo");
	aux = json_object_get(aux, "isRequired");
	wd->relatorio = (json_is_true(aux) != 0) ? json_string_value(tTrue) : json_string_value(tFalse);

	aux = json_object_get(obj, "attachmentUploadInfo");
	aux = json_object_get(aux, "isRequired");
	wd->anexo = (json_is_true(aux) != 0) ?  json_string_value(tTrue) : json_string_value(tFalse);

	aux = json_object_get(obj, "infoDocument");
	aux = json_object_get(aux, "fileName");
	json_t * aux1 = json_object_get(obj, "id");
	int id = json_integer_value(aux1);
	char idStr[4];
	sprintf(idStr, "%d", id);
	
	const char * url = getAnexosUrl(idStr);
	
	wd->existDocumento = (aux==0) ? 0 : getDocumento(url,json_string_value(aux));

	aux = json_object_get(obj, "attachmentsDocument");
	aux = json_object_get(aux, "fileName");
	wd->existAnexo = (aux==0) ? 0 : getAnexo(url,json_string_value(aux));
	
	free((char*)url);
	
	return wd;
}

//Retorna o url dos Anexos
const char * getAnexosUrl(char * id){
	
	char * url = "http://thoth.cc.e.ipl.pt/classes/";
	char * urlAux = malloc(sizeof(char) * 1000);
	strcpy(urlAux, url);
	
	strcat(urlAux,pArg->disciplina);
	strcat(urlAux,"/");
	strcat(urlAux,pArg->turma);
	strcat(urlAux,"/");
	strcat(urlAux,pArg->ano);
	strcat(urlAux,"/");
	strcat(urlAux,"workitems/");
	strcat(urlAux,id);
		
	return urlAux;
}

//Retorna o Array de estruturas de itens do trabalho
Pdata getAllWorksDetails(const char * r3){
	int errorCode;
	json_t * aux;
	json_t * aux1;
	Pdata data = malloc(sizeof(data));
	json_t * j = (json_t *) http_get_json(r3,&errorCode);
	aux = (json_t *) json_object_get(j, "workItems");
	free(j);
		
	PWorkData pwork;
	size_t size = json_array_size(aux);
	struct WorkData * wd = malloc(size * sizeof(WorkData));
	aux1 =  aux;
	int i;
	for(i = 0; i< size; ++i){
		aux =  json_array_get(aux1, i);
		aux = json_object_get(aux, "_links");
		aux = json_object_get(aux, "self");
		const char * result = json_string_value(aux);
		json_t * j = (json_t *) http_get_json(result,&errorCode);
		pwork = getWorkInfo(j, r3);//chamr metodo que recebe json_t * object e que retorna um struct...
		wd[i] = *pwork;
		free(j);
		free(pwork);
		
	}
	data->wd = wd;
	data->size = size;
	return data;
}

