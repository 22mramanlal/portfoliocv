#include <stdio.h>
#include <curl/curl.h>
#include <jansson.h>
#include <string.h>


 
extern json_t *json_string(const char *value);
 
//Estrutura da memoria para conter informação
struct MemoryStruct {
  char *memory;
  size_t size;
};

//Free Curl
void freeMem(CURL * curl){
	curl_easy_cleanup(curl);
	curl_global_cleanup();
}


//Prenchimento da estrutura que contem a informação do pedido
static size_t WriteToMemory(void * ptr, size_t size, size_t nmemb, void *userdata){
  size_t realsize = nmemb;
  
  struct MemoryStruct * buffer = (struct MemoryStruct *)userdata;
 
  buffer->memory = realloc(buffer->memory, buffer->size + realsize + 1);
  
  if(buffer->memory == NULL) {
        printf("Not Enough Memory\n");
    return 0;
  }
	
  memcpy((&(buffer->memory[buffer->size])), ptr, realsize);
  
  buffer->size += realsize;
  
  buffer->memory[buffer->size] = '\0';
   
   return realsize;
}


//Pedido json através http
json_t * http_get_json(const char * url, int * retcod){
	
	struct MemoryStruct mem;
	mem.memory = malloc(sizeof(mem));  
	mem.size = 0;    
	
	CURL *curl;
	CURLcode res;
	
	curl_global_init(CURL_GLOBAL_DEFAULT);
	
	curl = curl_easy_init();
	if(!curl){
		curl_global_cleanup();
		return NULL;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteToMemory);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&mem);

	if((res = curl_easy_perform(curl)) != CURLE_OK){
		*retcod = (int) res; 
		freeMem(curl);

		
		return NULL;
	}
	
	char * ct;  
	curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &ct);
	
	if( strstr(ct,"application/json") == 0 ){
		freeMem(curl);

		
		return NULL;
	}
	/******/
	
	freeMem(curl);
	
	
	json_error_t error;
	json_t * j = (json_t *)malloc(sizeof(json_t));
	j =  json_loads(mem.memory, 0, &error);
	
	if(mem.memory)
		free(mem.memory);

  return j;
}

