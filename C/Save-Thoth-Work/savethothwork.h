#ifndef savethothwork_h 
#define savethothwork_h

extern WorkData * mallocStruct;
extern void freeStruct (struct WorkData * wd);
extern const char * getAllClassesUrl();
extern const char * getClassUrl(const char * r3);
extern const char * getWorksPageUrl(const char * r3);
extern PWorkData getWorkInfo(json_t * obj);
extern Pdata getAllWorksDetails(const char * r3);
extern void freeWDList(Pdata wd);
extern json_t * http_get_json(const char * url, int * retcod);
extern int http_get_file(const char * url, const char * filename);
extern const char * getPath(char ** argv);
extern int writeResultFile(Pdata wd);


#endif
