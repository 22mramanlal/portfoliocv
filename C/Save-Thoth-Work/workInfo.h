#ifndef workInfo_h 
#define workInfo_h


typedef struct WorkData {
  const char * sigla;
  const char * titulo;
  const char * dPublicacao;
  const char * dLimite;
  const char * grupoIndividual;
  const char * relatorio;
  const char * anexo;
  const char * existDocumento;
  const char * existAnexo;
}WorkData, * PWorkData;


typedef struct Data{

   struct WorkData * wd;
   size_t size;

} * Pdata;

struct Arguments{

   const char * disciplina;
   const char * turma;
   const char * ano;

};

#endif
