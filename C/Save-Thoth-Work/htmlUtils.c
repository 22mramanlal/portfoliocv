#include <stdio.h>
#include <curl/curl.h>
#include <jansson.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h> 
#include "workInfo.h"

	
//Codigo Inicio Html
void htmlStart(FILE * file){
	fprintf(file, "<meta charset=\"utf-8\">");
	fprintf(file,"<html>");
	fprintf(file,"<head>");
	fprintf(file,"<title>");
	fprintf(file,"Result");
	fprintf(file,"</title>");
	fprintf(file,"</head>");
		
}	

//Codigo Fecho Html
void htmlEnd(FILE * file){
		
	fprintf(file,"</html>");	
}

//Inserção de uma linha em Html
void htmlLine(FILE * file, const char * w1, const char * w2){

	fprintf(file, "<p>%s%s</p>", w1, w2);
	
}	

//Inserção de um link em Html
void htmlLink(FILE * file, const char * w1, const char * w2 ){
	
	fprintf(file," <a href=\"./%s \"> %s</a>\n", w2, w1);

}

//Inserção de uma linha Separadora em Html
void htmlSeparateLine(FILE * file){

	fprintf(file, "<hr width=1000>");

}

//Preenchimento Body do Html
void fillHtmlBody(FILE * file, PWorkData wd){
		
		fprintf(file,"<body>");
		
		htmlLine(file, "Acrónimo: ",wd->sigla);
		htmlLine(file, "Título: " ,wd->titulo);
		htmlLine(file, "Data Publicação: ",wd->dPublicacao);
		htmlLine(file, "Data Entrega: ", wd->dLimite);
		htmlLine(file, "Trabalho Individual: ", wd->grupoIndividual);
		htmlLine(file, "Requere Relatorio: ", wd->relatorio);
		
		if(wd->existDocumento !=0)
			htmlLink(file, "Enunciado", wd->existDocumento);
		
		if(wd->existAnexo !=0)
			htmlLink(file, "Anexo",wd->existAnexo);
		
		htmlLine(file, "Requere Anexo(s): ", wd->anexo);
		
		fprintf(file,"</body>");
}
	
	
void writeHtmlBody(FILE * file, Pdata data){
	int i = 0;
	size_t size = data->size;
	WorkData wd;
	while(i<size){
		wd = data->wd[i];
		fillHtmlBody(file, &wd);
		htmlSeparateLine(file);
		++i;
	}
}

// Criação de ficheiro Html e Escrita
void writeResultFile(Pdata data){
		
	FILE * file;
	file = fopen("Result/result.html","wt");
	
	htmlStart(file);
	writeHtmlBody(file,data);
	htmlEnd(file);
		
	fclose(file);
	
} 


